# coding-style

## Linting
- flake8: a multi-linter - it is a wrapper around the PyFlakes, pycodestyle, and Ned Batchelder's McCabe scripts and does a little more
  - connect to container
  - install flake8: `pip install flake8`
  - run flake8: `flake8 /opt/app`
    - NOTE: see the `.flake8` config file for ignored items and config of flake8

## Security
- bandit: a tool designed to find common security issues in Python code
  - connect to container
  - install bandit: `pip install bandit`
  - run bandit: `bandit -r /opt/app`

## Style
- black: The Uncompromising Code Formatter
  - black is installed as a pre-commit hook (see [Version control integration](https://pypi.org/project/black/#version-control-integration) for more information) and is ran on all commits, no other action is required
  ----
  - install pre-commit: `pip install pre-commit`
  - use pre-commit to install the black pre-commit config: `pre-commit install`
  - more information can be found at [black](https://github.com/ambv/black)
