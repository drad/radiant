#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#  SPDX-License-Identifier: GPL-3.0-only
#  Copyright 2025 dradux.com

import logging
from datetime import datetime
from enum import Enum
from config.models import Cdb
from pydantic import BaseModel
from typing import List

logger = logging.getLogger("default")
_db = Cdb.CLIENTS


class InvoiceFormat(str, Enum):
    summary = "summary"
    detail = "detail"


class ClientBase(BaseModel):
    """
    Base document
    """

    name: str | None = None
    nickname: str | None = None
    order: int | None = None
    preferred_invoice_format: InvoiceFormat = InvoiceFormat.summary
    note: str | None = None  # notes about item
    enabled: bool = True  # is the item enabled?


class ClientExt(ClientBase):
    """
    Extended (added by backend logic)
    """

    # note: this needs to be set/overwrote on result instantiation as using
    #  datetime.now() here will only get you now of when worker was started.
    created: datetime = datetime.now()
    updated: datetime = datetime.now()

    creator: str | None = None
    updator: str | None = None


class Client(ClientExt):
    """
    Actual (at DB level)
    """

    id_: str


class ClientResponse(BaseModel):
    """
    Client Response
    """

    total: int = 0
    clients: List[Client] | None = None
