import Vue from 'vue'
import { api } from 'boot/axios'

const state = {
    tasks_loading: true,
    tasklist_loading: true,
    tasks: [],
    //~ tasks: [
        //~ {
            //~ "id_":"default:clean-closet",
            //~ "account":"default",
            //~ "name":"Clean closet",
            //~ "value":1,
            //~ "enabled":true,
            //~ "description":"Clean your closet",
            //~ "icon":"cleaning_services",
            //~ "created":"2021-03-02T01:06:53.796782",
            //~ "updated":"2021-03-04T16:32:58.699432"
        //~ },
        //~ {
            //~ "id_":"default:coding-class",
            //~ "account":"default",
            //~ "name":"Coding class",
            //~ "value":1,
            //~ "enabled":true,
            //~ "description":"Coding class with dad",
            //~ "icon":"engineering",
            //~ "created":"2021-03-01T20:01:56.942747",
            //~ "updated":"2021-03-04T17:20:16.851550"
        //~ }
    //~ ]
    active_task: {},
    show_quick_submit: false,
    task_list: []
      //~ {
        //~ "id_": "05ab46df-1d51-44e8-a64f-2f19f9013892",
        //~ "nickname": "TCKT",
        //~ "name": "Ticket Work",
        //~ "client_project": "RCOG/SUP",
        //~ "order": 2,
        //~ "ticket": null,
        //~ "note": "rails support tickets from zendesk"
      //~ },
      //~ {
        //~ "id_": "073ebc69-2a96-45bd-8999-0ab33e927e20",
        //~ "nickname": "TCKT",
        //~ "name": "Ticket Work",
        //~ "client_project": "WWN/UKSE",
        //~ "order": 20,
        //~ "ticket": null,
        //~ "note": null
      //~ },
      //~ {
        //~ "id_": "68b56351-6e32-4d1c-8e3a-662b9a6b03d5",
        //~ "nickname": "SEC",
        //~ "name": "Security",
        //~ "client_project": "RCOG/SUP",
        //~ "order": 4,
        //~ "ticket": null,
        //~ "note": "security support (questionaires, etc.)"
      //~ },
      //~ {
        //~ "id_": "6be5b725-48bd-4fcf-a27e-59f12dcda9df",
        //~ "nickname": "MTG",
        //~ "name": "Meetings",
        //~ "client_project": "WWN/UKSE",
        //~ "order": 21,
        //~ "ticket": null,
        //~ "note": "general bucket for meetings"
      //~ }
    //~ ]
}

const mutations = {
    setTasksLoading(state, payload) {
        state.tasks_loading = payload
    },
    setTaskListLoading(state, payload) {
        state.tasklist_loading = payload
    },
    addTasks(state, payload) {
        state.tasks = payload
    },
    addTaskList(state, payload) {
        state.task_list = payload
    },
    updateTask(state, payload) {
        //~ console.log("store-tasks>mutations>updateTask payload: ", payload)
        // note: we are not updating the store here as we update the db and reload
        var querystring = require('querystring');
        api.put('/tasks/?' + querystring.stringify(payload))
            .then((response) => {
                //~ console.log("task update response: ", response)
            })
        .catch((error) => {
            // note: this is hit when an unauthenticated user hits / which redirects them to /login
            //       no harm in the message so we just dump to console.
            //console.log('No points found.')
            console.log("Error updating task: ", error)
        })
    },
    deleteTask(state, id_) {
        var querystring = require('querystring');
        //~ console.log("store-tasks>mutations>deleteTask id: ", id_)
        // delete from db
        api.delete('/tasks/?' + querystring.stringify({ _id: id_ }), { params: {account: "default"}})
            .then((response) => {
                //~ console.log('fetch tasks returned: ', JSON.stringify(response.data))
                //this.tasks = response.data
                //~ commit('addTasks', response.data)
                //~ console.log("task deleted with id: ", id_)
            })
        .catch((error) => {
            //       no harm in the message so we just dump to console.
            //~ console.log('No tasks found.')
            console.log("Error in deleting task: ", error)
        })
        // if db update all good, update store.

        // you need to get the index of the item to delete it from vue
        // NOTE: I'm not sure this is a worthwhile effort, its likely better (although slightly slower)
        //   to simply reload tasks from db (fire the event) than to worry about this.
        //~ var index = state.tasks.findIndex(task => task.id_ == id_);
        //~ console.log(" --> index is: ", index)
        //~ Vue.delete(state.tasks, index)
    },
    addTask(state, task) {
        //~ console.log("store-tasks>mutations>deleteTask id: ", id_)
        var querystring = require('querystring');
        api.post('/tasks/?' + querystring.stringify(task))
            .then((response) => {
                //~ console.log('fetch tasks returned: ', JSON.stringify(response.data))
                //this.tasks = response.data
                //~ commit('addTasks', response.data)
                //~ console.log("task added: ", task)
            })
        .catch((error) => {
            // note: this is hit when an unauthenticated user hits / which redirects them to /login
            //       no harm in the message so we just dump to console.
            //~ console.log('No tasks found.')
            console.log("Error in deleting task: ", error)
        })
    },
    toggleTaskEnable(state, task) {
        //~ console.log("store-tasks>mutations>toggleTaskEnable: task=", task)
        var querystring = require('querystring');
        let obj = {
            _id: task.id_,
            enabled: !task.enabled
        }
        //~ api.post('/tasks/?' + querystring.stringify(task), { params: {account: "default"}})
        api.put('/tasks/?' + querystring.stringify(obj))
            .then((response) => {
                //~ console.log('fetch tasks returned: ', JSON.stringify(response.data))
                //~ console.log("task enable toggled: ", task)
            })
        .catch((error) => {
            // note: this is hit when an unauthenticated user hits / which redirects them to /login
            //       no harm in the message so we just dump to console.
            //~ console.log('No tasks found.')
            console.log("Error in deleting task: ", error)
        })
    },
    set_active_task(state, task) {
      state.active_task = task
    },
    show_quick_submit(state, show) {
      state.show_quick_submit = show
    }
}

const actions = {
    fetchTasks({ commit }, payload) {
        commit('setTasksLoading', true)
        //~ console.log("store > tasks > fetch")
        api.get('/tasks/', { params: {account: "default"}})
            .then((response) => {
                //~ console.log(' ⟶ fetch tasks return:', JSON.stringify(response.data))
                commit('addTasks', response.data)
            })
            .catch(() => {
                // note: this is hit when an unauthenticated user hits / which redirects them to /login
                //       no harm in the message so we just dump to console.
                console.log('No tasks found.')
            })
        commit('setTasksLoading', false)
    },
    fetchTaskList({ commit }, payload) {
        commit('setTaskListLoading', true)
        //~ console.log("store > tasklist > fetch")
        api.get('/tasks/task_list', { params: {limit: 50, skip: 0}})
            .then((response) => {
                //~ console.log(' ⟶ fetch task list return:', JSON.stringify(response.data))
                commit('addTaskList', response.data)
            })
            .catch(() => {
                // note: this is hit when an unauthenticated user hits / which redirects them to /login
                //       no harm in the message so we just dump to console.
                console.log('No task list found.')
            })
        commit('setTasksLoading', false)
    },
    addTask({ commit }, task) {
      commit('addTask', task)
    },
    updateTask({ commit }, payload) {
      commit('updateTask', payload)
    },
    deleteTask({ commit }, id_) {
      commit('deleteTask', id_)
    },
    toggleTaskEnable({ commit }, enabled) {
      commit('toggleTaskEnable', enabled)
    },
    setActiveTask({ commit }, task) {
      commit('set_active_task', task)
    },
    showQuickSubmit({ commit }, show) {
      commit('show_quick_submit', show)
    },
}

const getters = {
    tasks_loading: (state) => {
      return state.tasks_loading
    },
    tasklist_loading: (state) => {
      return state.tasklist_loading
    },
    tasks: (state) => {
      return state.tasks
    },
    active_task: (state) => {
      return state.active_task
    },
    show_quick_submit: (state) => {
      return state.show_quick_submit
    },
    task_list: (state) => {
        return state.task_list
    }
}

export default {
    namespaced: true,

    state,
    mutations,
    actions,
    getters
}
