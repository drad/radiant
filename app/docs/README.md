# Application Documentation

## Notices
- this app uses flask-migrate for all DDL changes
- this app is configured to use boi for builds


## Database Migration ##
- create migration
  - make some changes to model
  - create migration: `docker-compose run app flask db migrate -m 'revision message'`
    - or create a manual migration: `docker-compose run app flask db revision -m 'revision message'`
  - review created migration and make changes if necessary (should not be necessary)
- apply migration: `docker-compose run app flask db upgrade`
- other things:
  - show migration history: `flask db history`
  - revert/rollback: `flask db downgrade`


## Building ##

- manually specify build version: `boi --build-version=1.2.3`
- automatic build version: `boi`
  - the above will increment the build version automatically (e.g. if last build was 1.8.0 this build will be 1.9.0 - see `boi --help` for more info)
