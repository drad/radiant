#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#  SPDX-License-Identifier: GPL-3.0-only
#  Copyright 2025 dradux.com

import logging
import uuid
from datetime import datetime
from typing import List

import aiocouch
from _common.models import DocInfo
from _common.utils import fix_id, make_id
from config.config import get_cdb
from fastapi import APIRouter, HTTPException, Query, Security, status
from fastapi.encoders import jsonable_encoder
from projects.utils import get_by_id
from tasks.models import Task, TaskListEnhanced, TaskExt, TaskListResponse, TaskResponse, _db
from users.models import UserBase, get_current_user
from sse.utils import send_sse_message
from sse.models import SSEEventType, SSEActionType

logger = logging.getLogger("default")
tasks_router = APIRouter()


async def _get_or_404(_id, db):
    """
    Get a document or raise a 404
    """

    # ~ logger.debug(f"_get_or_404 request on: {_id}")
    try:
        return await db[_id]
    except aiocouch.exception.NotFoundError:
        raise HTTPException(status_code=404, detail="Not found")


@tasks_router.get("/task_list", response_model=List[TaskListResponse])
async def task_list(
    limit: int = Query(10, ge=1, le=99, description="Limit number of results"),
    skip: int = Query(0, description="Skip number of records before applying limit"),
    current_user: UserBase = Security(get_current_user, scopes=["users"]),
):
    """
    The task list shows a list of tasks with the client_project included.
    - **NOTE:** only enabled tasks of enabled projects and enabled clients are returned
    """

    from clients.utils import get_by_id as get_client_by_id
    from projects.utils import get_by_id as get_project_by_id

    couchdb = get_cdb()
    logger.debug(f"- getting task list for current_user: {current_user}")
    db = await couchdb[_db.value]

    docs = []
    selector = {"enabled": True}
    async for doc in db.find(selector=selector, limit=limit, skip=skip):
        _doc = fix_id(doc)
        _project = await get_project_by_id(_doc["project_id"])
        # only add if project is enabled
        if _project["enabled"]:
            # ~ logger.debug(f"- _project={_project}")
            _client = await get_client_by_id(_project["client_id"])
            # only add if client is enabled
            if _client["enabled"]:
                # ~ logger.debug(f"- _client={_client}")
                _doc["client_project"] = f"{_client['nickname']}/{_project['nickname']}"
                _doc["client"] = _client["_id"]
                # ~ docs.append(_doc)
                _task = TaskListEnhanced(**_doc)
                docs.append(_task)

    return docs


@tasks_router.get("/all", response_model=TaskResponse)
async def get_all(
    limit: int = Query(10, ge=1, le=99, description="Limit number of results"),
    skip: int = Query(0, description="Skip number of records before applying limit"),
    current_user: UserBase = Security(get_current_user, scopes=["admins"]),
):
    """
    Get all tasks
    - only for admins
    """

    couchdb = get_cdb()
    logger.debug(f"- get all: current_user: {current_user}")
    db = await couchdb[_db.value]

    _response = TaskResponse()
    docs = []
    async for doc in db.find(selector={}, limit=limit, skip=skip):
        _doc = fix_id(doc)
        docs.append(Task(**_doc))

    _response.total = len(docs)
    _response.tasks = docs
    # ~ logger.debug(f"TaskResponse: {_response}")

    return _response


@tasks_router.get("/one/{_id}", response_model=Task, summary="Get one Task")
async def get_one(
    _id: str,
    current_user: UserBase = Security(get_current_user, scopes=["users"]),
):
    """
    Get one by id
    """

    couchdb = get_cdb()
    db = await couchdb[_db.value]

    return Task(**fix_id(await _get_or_404(_id, db)))


@tasks_router.post("/", response_model=DocInfo, status_code=status.HTTP_201_CREATED)
async def add(
    project_id: str = Query(..., description="project the task belongs to"),
    name: str = Query(..., description="name of the task"),
    nickname: str = Query(
        ...,
        min_length=1,
        max_length=4,
        description="nickname of the task | <i>value will be uppercased</i>",
    ),
    order: int = Query(
        100,
        description="order of the task (lower number will be sorted to the top of lists)",
    ),
    # ~ ticket: str = Query(None, description="the ticket for the task"),
    # ~ complete: bool = Query(False, description="has the ticket been completed?"),
    note: str = Query(None, description="notes for the task"),
    icon: str = Query(None, description="The icon for the task"),
    enabled: bool = Query(True, description="is this item enabled?"),
    current_user: UserBase = Security(get_current_user, scopes=["admins"]),
):
    """
    Add new
    """

    couchdb = get_cdb()
    db = await couchdb[_db.value]

    logger.debug("- add task")

    # check to ensure project exists.
    _project = await get_by_id(project_id)
    logger.debug(f"- project check for id={project_id} is: {_project}")
    if not _project:
        raise HTTPException(
            status_code=417,
            detail="Project does not exist, perhaps you enter the Name instead of Id?",
        )

    _doc = TaskExt(
        project_id=project_id,
        name=name,
        nickname=nickname.upper(),
        order=order,
        # ~ ticket=ticket,
        # ~ complete=complete,
        note=note,
        enabled=enabled,
        icon=icon,
        created=datetime.now(),
        updated=datetime.now(),
        creator=current_user.username,
    )

    try:
        doc_id = make_id(items=[str(uuid.uuid4())])
        doc = await db.create(
            doc_id,
            data=jsonable_encoder(_doc),
        )
        await doc.save()
        # send sse message.
        await send_sse_message(user=current_user.username, event_type=SSEEventType.task, data=SSEActionType.add)
        return await doc.info()

    except aiocouch.ConflictError:
        raise HTTPException(status_code=409, detail=f"Duplicate Key: cannot add '{doc_id}'.")


@tasks_router.put("/", response_model=DocInfo)
async def update(
    _id: str = Query(..., description="id of the document to update"),
    project_id: str = Query(None, description="project the task belongs to"),
    name: str = Query(None, description="name of the task"),
    nickname: str = Query(
        ...,
        min_length=1,
        max_length=4,
        description="nickname of the task | <i>value will be uppercased</i>",
    ),
    order: int = Query(
        None,
        description="order of the task (lower number will be sorted to the top of lists)",
    ),
    # ~ ticket: str = Query(None, description="the ticket for the task"),
    # ~ complete: bool = Query(False, description="has the ticket been completed?"),
    note: str = Query(None, description="notes for the task"),
    icon: str = Query(None, description="The icon for the task"),
    enabled: bool = Query(True, description="is this item enabled?"),
    current_user: UserBase = Security(get_current_user, scopes=["admins"]),
):
    """
    Update Registration Type
    """

    logger.debug("- updating registration type")
    couchdb = get_cdb()
    db = await couchdb[_db.value]
    doc = await _get_or_404(_id, db)
    logger.debug(f"- updating doc: {doc}")

    doc["project_id"] = project_id if project_id else doc["project_id"]
    doc["name"] = name if name else doc["name"]
    doc["nickname"] = nickname.upper() if nickname else doc["nickname"]
    doc["order"] = order if order else doc["order"]
    # ~ doc["ticket"] = ticket if ticket else doc["ticket"]
    # ~ doc["complete"] = complete
    doc["note"] = note if note else doc["note"]
    doc["enabled"] = enabled
    doc["icon"] = icon if icon else doc["icon"]
    doc["updated"] = jsonable_encoder(datetime.now())
    doc["updator"] = current_user.username
    # logger.debug(f"doc before save: {doc}")
    await doc.save()
    # send sse message.
    await send_sse_message(user=current_user.username, event_type=SSEEventType.task, data=SSEActionType.update)
    return await doc.info()


@tasks_router.delete("/", response_model=DocInfo)
async def delete(
    _id: str,
    current_user: UserBase = Security(get_current_user, scopes=["admins"]),
):
    """
    Hard Delete by id
    """

    couchdb = get_cdb()
    db = await couchdb[_db.value]

    resp = DocInfo(ok=False, id=_id, rev="", msg="unknown")
    doc = await _get_or_404(_id, db)
    dr = await doc.delete()
    if dr and dr.status < 400:
        resp.ok = True
        resp.msg = "deleted"
        # send sse message.
        await send_sse_message(user=current_user.username, event_type=SSEEventType.task, data=SSEActionType.delete)
    else:
        resp.ok = False
        resp.msg = f"Delete status: {dr.status}, headers: {dr.headers}"

    return resp
