# Inventory models.
import datetime
import logging

import arrow
import config.config as Config

from .core import db

logger = logging.getLogger("main_logger")

# asset to energy cross-ref
asset_energy = db.Table(
    "asset_energy",
    db.Column("asset_id", db.Integer(), db.ForeignKey("asset.id")),
    db.Column("energy_id", db.Integer(), db.ForeignKey("energy.id")),
)

# asset to link cross-ref
asset_links = db.Table(
    "asset_links",
    db.Column("asset_id", db.Integer(), db.ForeignKey("asset.id")),
    db.Column("link_id", db.Integer(), db.ForeignKey("link.id")),
)

# asset to spec cross-ref
asset_specs = db.Table(
    "asset_specs",
    db.Column("asset_id", db.Integer(), db.ForeignKey("asset.id")),
    db.Column("spec_id", db.Integer(), db.ForeignKey("spec.id")),
)

# asset to asset status cross-ref
asset_statuses = db.Table(
    "asset_statuses",
    db.Column("asset_id", db.Integer(), db.ForeignKey("asset.id")),
    db.Column("status_id", db.Integer(), db.ForeignKey("asset_status.id")),
)

# asset to tag cross-ref
asset_tags = db.Table(
    "asset_tags",
    db.Column("asset_id", db.Integer(), db.ForeignKey("asset.id")),
    db.Column("tag_id", db.Integer(), db.ForeignKey("tag.id")),
)

# attachment to asset cross-ref
attachment_assets = db.Table(
    "attachment_asset",
    db.Column("asset_id", db.Integer(), db.ForeignKey("asset.id")),
    db.Column("attachment_id", db.Integer(), db.ForeignKey("attachment.id")),
)

# attachment to setup cross-ref
attachment_setups = db.Table(
    "attachment_setup",
    db.Column("setup_id", db.Integer(), db.ForeignKey("setup.id")),
    db.Column("attachment_id", db.Integer(), db.ForeignKey("attachment.id")),
)

# attachment to incident cross-ref
attachment_incidents = db.Table(
    "attachment_incident",
    db.Column("incident_id", db.Integer(), db.ForeignKey("incident.id")),
    db.Column("attachment_id", db.Integer(), db.ForeignKey("attachment.id")),
)


class Asset(db.Model):
    """An asset: an item (e.g. computer, hard drive, light bulb)."""

    __tablename__ = "asset"
    id = db.Column(db.Integer, primary_key=True)
    alias = db.Column(db.String(13), nullable=False)
    name = db.Column(db.String(50), nullable=False)
    asset_type_id = db.Column(
        db.Integer(), db.ForeignKey("asset_type.id"), nullable=False
    )
    asset_type = db.relationship("AssetType", foreign_keys=[asset_type_id])
    description = db.Column(db.Text, nullable=True)
    manufacturer = db.Column(db.String(100), nullable=True)
    model = db.Column(db.String(50), nullable=True)
    version = db.Column(db.String(50), nullable=True)
    serial_number = db.Column(db.String(100), nullable=True)
    part_number = db.Column(db.String(100), nullable=True)
    note = db.Column(db.Text, nullable=True)
    purpose = db.Column(db.Text, nullable=True)
    attain_type_id = db.Column(
        db.Integer(), db.ForeignKey("attain_type.id"), nullable=False
    )
    attain_type = db.relationship("AttainType", foreign_keys=[attain_type_id])
    attain_from = db.Column(db.String(100), nullable=True)
    # attain_date = db.Column(db.DateTime(), nullable=True)
    # purchase_date = db.Column(db.DateTime(), nullable=True)
    purchase_price = db.Column(db.Numeric(10, 2), nullable=True, default=0.00)
    purchase_invoice_number = db.Column(db.String(100), nullable=True)
    warranty = db.Column(db.Text, nullable=True)
    os_name = db.Column(db.String(50), nullable=True)
    os_version = db.Column(db.String(50), nullable=True)
    os_note = db.Column(db.Text, nullable=True)
    networking_nic_ip = db.Column(
        db.String(39), nullable=True
    )  # 0000:0000:0000:0000:0000:0000:0000:0000
    networking_nic_mac = db.Column(db.String(17), nullable=True)  # 00:00:00:00:00:00
    networking_wlan_ip = db.Column(
        db.String(39), nullable=True
    )  # 0000:0000:0000:0000:0000:0000:0000:0000
    networking_wlan_mac = db.Column(db.String(17), nullable=True)  # 00:00:00:00:00:00
    networking_note = db.Column(
        db.Text, nullable=True
    )  # this can be used for vips or any networking related notes
    physical_location_id = db.Column(
        db.Integer(), db.ForeignKey("asset_physical_location.id"), nullable=False
    )
    physical_location = db.relationship(
        "AssetPhysicalLocation", foreign_keys=[physical_location_id]
    )
    # statuses (multiple occurring)
    statuses = db.relationship(
        "AssetStatus",
        secondary=asset_statuses,
        backref=db.backref("asset", lazy="dynamic"),
    )
    # tags (multiple occurring)
    tags = db.relationship(
        "Tag", secondary=asset_tags, backref=db.backref("asset", lazy="dynamic")
    )
    # links (multiple occurring)
    links = db.relationship(
        "Link", secondary=asset_links, backref=db.backref("asset", lazy="dynamic")
    )
    # specs (multiple occurring)
    specs = db.relationship(
        "Spec",
        secondary=asset_specs,
        backref=db.backref("asset", lazy="dynamic"),
        lazy="dynamic",
    )
    # energy (multiple occurring)
    energy = db.relationship(
        "Energy", secondary=asset_energy, backref=db.backref("asset", lazy="dynamic")
    )
    # administrative data
    created_at = db.Column(db.DateTime(), default=datetime.datetime.utcnow)

    # attachments (multiple occurring)
    attachments = db.relationship(
        "Attachment",
        secondary=attachment_assets,
        backref=db.backref("asset", lazy="dynamic"),
    )

    def cpu_core_count(self):
        """cpu core count and cpu EV for an asset"""
        # "CPU Cores"
        # core_count = model.specs.filter_by(spec_type = 'CPU Cores')
        # getting cpu cores should be a filter on model but I can't get it to work so we loop over specs.
        # cores = self.specs.filter_by()
        # ~ cores = self.specs
        # ~ cores = self.specs.first().name
        # ~ cores = self.specs.first().spec_type
        # ~ cores = self.specs.filter(spec_type == 'CPU Cores')
        # ~ cores = self.specs.filter(self.specs.has(spec_type='CPU Cores'))
        # ~ cores = self.specs.filter_by(spec_spec_type == 'CPU Cores').first()
        # ~ cores = self.specs.filter(self.specs.value == 'abc')
        cores = None
        ner = None
        for s in self.specs:
            if str(s.spec_type) == "CPU Cores":
                cores = s.value
                nuw = self.normal_use_watt()
                ner = str(round(nuw / float(cores), 2)) if nuw and cores else ""
                break
        # logger.debug(f"- NOTICE ###: {cores.statement.compile()}")
        # logger.debug(f"- cores: {cores}")
        return cores, ner

    def memory_amount(self):
        """amount of memory and memory EV for an asset"""
        amount = None
        ner = None
        for s in self.specs:
            if str(s.spec_type) == "Memory Amount":
                amount = s.value
                nuw = self.normal_use_watt()
                ner = str(round(nuw / float(amount), 2)) if nuw and amount else ""
                break
        return amount, ner

    def normal_use_watt(self):
        """get the energy normal use watt."""
        for i in self.energy:
            if str(i.state) == "Normal Use Watt":
                return float(i.value)
        return None

    def compute_cost(self):
        nuw = self.normal_use_watt()
        cost_hour = (
            (nuw / 1000) * Config.DefaultConfig.APP_ENERGY_KWH_PRICE if nuw else 0
        )
        cost_day = Config.DefaultConfig.APP_ENERGY_HOURS_PER_DAY * cost_hour
        cost_month = 30.42 * cost_day
        cost_year = 12 * cost_month
        return cost_hour, cost_day, cost_month, cost_year

    # normal_energy_ratio: the ratio of 'normal' energy used to the number of cpu cores.
    # ~ def normal_energy_ratio(self):
    # ~ # get spec cpu core amount (sca)
    # ~ sca = ""
    # ~ ner = ""
    # ~ nef = ""
    # ~ for i in self.specs:
    # ~ if str(i.spec_type) == "CPU Cores":
    # ~ nuw = self.normal_use_watt()
    # ~ sca = float(i.value)
    # ~ ner = str(round(nuw / sca, 2)) if nuw and sca else ""
    # ~ nef = (
    # ~ "Watt={0}\nCores={1}".format(str(nuw), str(sca))
    # ~ if nuw and sca
    # ~ else ""
    # ~ )
    # ~ return ner, nef

    # memory_energy_ratio: the ratio of 'normal' energy used to the amount of memory.
    # ~ def memory_energy_ratio(self):
    # ~ # get spec memory amount (sca)
    # ~ ner = ""
    # ~ nef = ""
    # ~ for i in self.specs:
    # ~ if str(i.spec_type) == "Memory Amount":
    # ~ nuw = self.normal_use_watt()
    # ~ sma = float(i.value)
    # ~ ner = str(round(nuw / sma, 2)) if nuw and sma else ""
    # ~ nef = (
    # ~ "Watt={0}\nMem={1}".format(str(nuw), str(sma))
    # ~ if nuw and sma
    # ~ else ""
    # ~ )
    # ~ return ner, nef

    def __str__(self):
        return "{0} ({1}/{2})".format(
            self.alias, self.asset_type, self.physical_location
        )


class AssetPhysicalLocation(db.Model):
    """Asset Physical Location: dx lab | dx server room | unknown"""

    __tablename__ = "asset_physical_location"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(20), nullable=False)

    def __str__(self):
        return "{0}".format(self.name)


class AssetStatus(db.Model):
    """Asset Status: various statuses of an asset"""

    __tablename__ = "asset_status"
    id = db.Column(db.Integer, primary_key=True)
    status_date = db.Column(
        db.DateTime, nullable=False, default=datetime.datetime.utcnow
    )
    status_id = db.Column(
        db.Integer(), db.ForeignKey("asset_status_status.id"), nullable=False
    )
    status = db.relationship("AssetStatusStatus", foreign_keys=[status_id])
    note = db.Column(db.Text, nullable=True)

    def __str__(self):
        note = f"{self.note}" if self.note else ""
        return f"{self.status} ({arrow.get(self.status_date).to('local').format('YYYY-MM-DD HH:mm')}){note}"


class AssetStatusStatus(db.Model):
    """Asset Status Status: Received, Pending, Active, Inactive, Broke, Decommissioned, etc."""

    __tablename__ = "asset_status_status"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(20), nullable=False)

    def __str__(self):
        return "{0}".format(self.name)


class AssetType(db.Model):
    """Asset type: Server | Desktop | Phone | Switch | Router | Monitor | Mouse | Keyboard | etc."""

    __tablename__ = "asset_type"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(20), nullable=False)

    def __str__(self):
        return "{0}".format(self.name)


class AttainType(db.Model):
    """How an asset was attained: New, Freecycle, Street, etc."""

    __tablename__ = "attain_type"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(20), nullable=False)

    def __str__(self):
        return "{0}".format(self.name)


class Energy(db.Model):
    """Energy (power consumption): asset or other"""

    __tablename__ = "energy"
    id = db.Column(db.Integer, primary_key=True)
    state_id = db.Column(
        db.Integer(), db.ForeignKey("energy_measure_state.id"), nullable=False
    )
    state = db.relationship("EnergyMeasureState", foreign_keys=[state_id])
    value = db.Column(db.String(100), nullable=False)
    measure_type_id = db.Column(
        db.Integer(), db.ForeignKey("measure_type.id"), nullable=False
    )
    measure_type = db.relationship("MeasureType", foreign_keys=[measure_type_id])
    note = db.Column(db.Text, nullable=True)

    def __str__(self):
        # return "{0} - {1}".format(self.state, self.value)
        method = f" (via {self.measure_type})" if self.measure_type else ""
        note = f" {self.note}" if self.note else ""
        return f"{self.state}: {self.value}{note}{method}"


class EnergyMeasureState(db.Model):
    """Energy measure state (e.g. power consumption state): Power Off, Power Up, Base, Device Max"""

    __tablename__ = "energy_measure_state"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(30), nullable=False)

    def __str__(self):
        return "{0}".format(self.name)


class Incident(db.Model):
    """An asset incident (e.g. server1 disk fail, etc.)"""

    __tablename__ = "incident"
    id = db.Column(db.Integer, primary_key=True)
    incident_date = db.Column(db.DateTime(), default=datetime.datetime.utcnow)
    asset_id = db.Column(db.Integer(), db.ForeignKey("asset.id"), nullable=False)
    asset = db.relationship("Asset", foreign_keys=[asset_id])

    status_id = db.Column(
        db.Integer(), db.ForeignKey("incident_status.id"), nullable=False
    )
    status = db.relationship("IncidentStatus", foreign_keys=[status_id])
    issue = db.Column(db.Text, nullable=False)
    resolution = db.Column(db.Text, nullable=True)
    impact = db.Column(db.Text, nullable=True)
    note = db.Column(db.Text, nullable=True)
    # administrative data
    created_at = db.Column(db.DateTime(), default=datetime.datetime.utcnow)

    # attachments (multiple occurring)
    attachments = db.relationship(
        "Attachment",
        secondary=attachment_incidents,
        backref=db.backref("incident", lazy="dynamic"),
    )

    def __str__(self):
        return "{0} - {1}".format(self.incident_date, self.asset)


class IncidentStatus(db.Model):
    """Ongoing | Resolved"""

    __tablename__ = "incident_status"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(20), nullable=False)

    def __str__(self):
        return "{0}".format(self.name)


class Link(db.Model):
    """Asset or other link"""

    __tablename__ = "link"
    id = db.Column(db.Integer, primary_key=True)
    link_type_id = db.Column(
        db.Integer(), db.ForeignKey("link_type.id"), nullable=False
    )
    link_type = db.relationship("LinkType", foreign_keys=[link_type_id])
    link = db.Column(db.String(250), nullable=False)
    note = db.Column(db.Text, nullable=True)

    def __str__(self):
        return "{0} - {1}".format(self.link_type, self.link)


class LinkType(db.Model):
    """Product, Drivers, Tips, etc."""

    __tablename__ = "link_type"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(25), nullable=False)

    def __str__(self):
        return "{0}".format(self.name)


class MeasureType(db.Model):
    """Meter, manufacturer, etc."""

    __tablename__ = "measure_type"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(20), nullable=False)

    def __str__(self):
        return "{0}".format(self.name)


class Setup(db.Model):
    """An asset setup (e.g. info about how/when an asset (server) was set up)"""

    __tablename__ = "setup"
    id = db.Column(db.Integer, primary_key=True)
    alias = db.Column(db.String(13), nullable=False)
    name = db.Column(db.String(50), nullable=False)

    asset_id = db.Column(db.Integer(), db.ForeignKey("asset.id"), nullable=False)
    asset = db.relationship("Asset", foreign_keys=[asset_id])

    setup_date = db.Column(db.DateTime(), default=datetime.datetime.utcnow)
    status_id = db.Column(
        db.Integer(), db.ForeignKey("setup_status.id"), nullable=False
    )
    status = db.relationship("SetupStatus", foreign_keys=[status_id])
    note = db.Column(db.Text, nullable=True)
    purpose = db.Column(db.Text, nullable=True)
    os = db.Column(db.String(50), nullable=True)
    hostname = db.Column(db.String(100), nullable=True)
    ip_address = db.Column(db.String(15), nullable=True)
    ip_netmask = db.Column(db.String(15), nullable=True)
    ip_gateway = db.Column(db.String(15), nullable=True)
    prep_setup = db.Column(db.Text, nullable=True)
    disk_setup = db.Column(db.Text, nullable=True)
    os_setup = db.Column(db.Text, nullable=True)
    apps_setup = db.Column(db.Text, nullable=True)
    additional_setup = db.Column(db.Text, nullable=True)
    # administrative data
    created_at = db.Column(db.DateTime(), default=datetime.datetime.utcnow)

    # attachments (multiple occurring)
    attachments = db.relationship(
        "Attachment",
        secondary=attachment_setups,
        backref=db.backref("setup", lazy="dynamic"),
    )

    def __str__(self):
        return "{0}".format(self.name)


class SetupStatus(db.Model):
    """Pending | In Process | Complete | On Hold"""

    __tablename__ = "setup_status"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(20), nullable=False)

    def __str__(self):
        return "{0}".format(self.name)


class Spec(db.Model):
    """asset or other spec"""

    __tablename__ = "spec"
    id = db.Column(db.Integer, primary_key=True)
    spec_type_id = db.Column(
        db.Integer(), db.ForeignKey("spec_type.id"), nullable=False
    )
    spec_type = db.relationship("SpecType", foreign_keys=[spec_type_id])
    value = db.Column(db.String(100), nullable=False)
    note = db.Column(db.Text, nullable=True)

    def __str__(self):
        return "{0} - {1}".format(self.spec_type, self.value)


class SpecType(db.Model):
    """CPU Cores, CPU Model, Memory Amount, etc."""

    __tablename__ = "spec_type"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(30), nullable=False)
    note = db.Column(db.Text, nullable=True)

    def __str__(self):
        return "{0}".format(self.name)
