# Application Database Configuration for: indexes

"""
This file contains all explicitly created indexes for the application and is ran
upon application start (via check_databases() on fastapi 'startup' event).

Structure:
  - get current index contents with {db}/_index
  - add an entry in the list below as:
    views.append({"database": "{db}", "views": [{ <contents from get response> }, {etc...}]})
    indexes.append({"database": "activities", "indexes": [{ <contents from get response> }, {etc...}]})
"""

indexes = []

#
# Create ONE instance per database which can have one or more indexes.
#

indexes.append(
    {
        "database": "registrations",
        "indexes": [
            {
                "ddoc": "_design/index-start",
                "def": {"fields": [{"start": "desc"}]},
                "name": "start",
                "partitioned": False,
                "type": "json",
            },
        ],
    }
)

indexes.append(
    {
        "database": "invoices",
        "indexes": [
            {
                "ddoc": "_design/index-created",
                "def": {"fields": [{"created": "desc"}]},
                "name": "created",
                "partitioned": False,
                "type": "json",
            },
        ],
    }
)

# ~ indexes.append(
# ~ {
# ~ "database": "activities",
# ~ "indexes": [
# ~ {
# ~ "ddoc": "_design/index-public-start",
# ~ "def": {"fields": [{"public": "asc"}, {"start": "asc"}]},
# ~ "name": "public-start",
# ~ "partitioned": False,
# ~ "type": "json",
# ~ },
# ~ {
# ~ "ddoc": "_design/index-public",
# ~ "def": {"fields": [{"public": "desc"}]},
# ~ "name": "public",
# ~ "partitioned": False,
# ~ "type": "json",
# ~ },
# ~ {
# ~ "ddoc": "_design/index-start",
# ~ "def": {"fields": [{"start": "desc"}]},
# ~ "name": "start",
# ~ "partitioned": False,
# ~ "type": "json",
# ~ },
# ~ ],
# ~ }
# ~ )

# ~ indexes.append(
# ~ {
# ~ "database": "events",
# ~ "indexes": [
# ~ {
# ~ "ddoc": "_design/index-events-date",
# ~ "def": {"fields": [{"date": "desc"}]},
# ~ "name": "date",
# ~ "partitioned": False,
# ~ "type": "json",
# ~ }
# ~ ],
# ~ }
# ~ )

# ~ indexes.append(
# ~ {
# ~ "database": "news",
# ~ "indexes": [
# ~ {
# ~ "ddoc": "_design/index-news-date",
# ~ "def": {"fields": [{"date": "asc"}, {"position": "asc"}]},
# ~ "name": "date-created",
# ~ "partitioned": False,
# ~ "type": "json",
# ~ }
# ~ ],
# ~ }
# ~ )

# ~ indexes.append(
# ~ {
# ~ "database": "pages",
# ~ "indexes": [
# ~ {
# ~ "ddoc": "_design/index-page-type-created",
# ~ "def": {"fields": [{"page_type": "desc"}, {"created": "desc"}]},
# ~ "name": "page-type-created",
# ~ "partitioned": False,
# ~ "type": "json",
# ~ }
# ~ ],
# ~ }
# ~ )
