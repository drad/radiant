# test_main.py
#
# @NOTES
#  - we can now run with: `pytest`, previously we needed to call as follows:
#    + run with: python -m pytest (as `pytest` does not work - path issues)
#      - normal: python -m pytest -ra --tb=short --no-header
#    + if you wanted detailed debug info (e.g. print statement output) you need to call with the `-rP` params (e.g. `python -m pytest -rP`).
#  - you need to set several envvars to run this test (see os.getenv) you can pass these in when running the test (e.g. $ TEST_APP_USER_USERNAME=tester python -m pytest) or (recommended approach) is to set them in the .env file so they will be set in the app container
#  - if you are sending 'params' in a payload they need to be encoded but you cannot do jsonable_encode() or json.dumps() on the entire object, you must do it at the individual property level (e.g. `"enabled": jsonable_encoder(True)`)
#  - if you want tests to utilize direct ASGI access you need to set app=app
#  - tests are in the following order:
#    1. order of need (e.g. you need an Activity before you can have Related Items)
#    2. alphabetical (i.e. Activities* before Users*, etc.)
# @LINEAGE: 2024-12-13: copied from runrader
#
#  STATUS:
#   - need to add SSE (manual_*) tests and ensure we delete (or set to SENT) all `sse` messages.
#

import arrow
import logging
import os
import pytest
import uuid
from httpx import AsyncClient

# from main import app
from user_messages.models import UserMessageCategory, UserMessagePriority
from config.config import APP_LOGLEVEL

# ~ PATH_SERVER = "http://mitm:8080"
PATH_SERVER = "http://api:8000"
PATH_BASE = "/api"
PATH_APP = f"{PATH_BASE}/v2"

logger = logging.getLogger("default")
logger.setLevel(logging.getLevelName(APP_LOGLEVEL))

# async_client_app is the app (context) sent with the httpx AsyncClient: this should either be app or None:
# - None: use this if you do not want to use direct ASGI access (which is needed if you want to proxy traffic through
#         mitmproxy or sniff the tcp traffic.
# - app: (e.g. from main import app) this is used if you want to send http traffic to the app directly via ASGI which
#         is faster and more efficient but does not allow typical sniffing/debug at the http/tcp layer.
async_client_app = None
# ~ async_client_app = app   # do not forget to ensure app is imported: `from main import app`

runstamp = uuid.uuid4().hex

app_version = {"status": "OK", "version": "4.1.0", "created": "2018-09-30", "modified": "2025-01-30"}
# app_user* is the user used when executing tests
app_user_for_update_username = "tester4update"
app_user = {
    "username": os.getenv("TEST_APP_USER_USERNAME"),
    "password": os.getenv("TEST_APP_USER_PASSWORD"),
    "grant_type": "password",
}
app_user_for_update = {
    "display_name": "updated by pytest",
    "avatar": "https://dradux.com/avatar/tester4update",
    "note": f"updated by pytest: runstamp={runstamp}",
    "enabled": True,
}
# user that tests authentication (as opposed to user used for running tests)
auth_user = {
    "username": os.getenv("TEST_AUTH_USER_USERNAME"),
    "password": os.getenv("TEST_AUTH_USER_PASSWORD"),
    "grant_type": "password",
}
client_1_id = None
client_1 = {
    "name": "PyTest Client 1",
    "nickname": "PTC1",
    "order": 1,
    "preferred_invoice_format": "detail",
    "note": "A test client for pytesting",
    "enabled": False,
}
invoice_1_id = None
invoice_1 = {
    "layout": "summary",
    "name": "Test Invoice",
    "note": "a test invoice created by pytest",
    "detail": "pytest testing",
    "tags": ["test", "pytest", "invoice"],
}
project_1_id = None
project_1 = {
    "name": "PyTest Project 1",
    "nickname": "PTP1",
    "order": 100,
    "bill_rate": 99,
    "expected_hours_per_day": 1,
    "note": "Test project 1 created by pytest",
    "enabled": False,
}
registration_batch_add_amount = 25
registration_1_id = None
registration_1 = {"ticket_number": "PYT-001", "note": "test registration created by pytest", "tags": ["test", "pytest"]}
registration_type_1_id = None
registration_type_1 = {
    "name": "PyTest Registration Type 1",
    "note": "A test registration type used by PyTest",
    "enabled": False,
    "icon": "bi-bootstrap-reboot",
}
task_1_id = None
task_1 = {
    "name": "PyTest Task 1",
    "nickname": "PYT1",
    "order": 0,
    # ~ "ticket": "ABC",
    # ~ "complete": False,
    "note": "Test ticket created by pytest",
    "enabled": False,
    "icon": "bi-ticket",
}
user_message_1_id = None
user_message_1 = {
    "to_user_id": app_user["username"],
    "category": UserMessageCategory.system.value,
    "priority": UserMessagePriority.high.value,
    "subject": "PyTest Test Message #1",
    "message": "This is a test message created by pytest",
}

headers_base = {"content-type": "application/json", "accept": "application/json"}
headers_text = {"content-type": "text/plain", "accept": "application/json"}
headers_form = {"content-type": "application/x-www-form-urlencoded", "accept": "application/json"}


# ~ @pytest.mark.skipif("READYONLY" not in os.environ, reason="Test Not Yet Ready")
@pytest.fixture(scope="module")
async def get_auth_token():
    """
    Get Authentication Token

    @NOTES
    - the token is used throughout the remainder of the tests
    """

    logger.info(
        f"""
---------------------------------
- SERVER: {PATH_SERVER}
- BASE PATH: {PATH_APP}
- Test User: {app_user['username']}
---------------------------------
"""
    )

    logger.info("getting auth token...")

    async with AsyncClient(app=async_client_app, base_url=PATH_SERVER) as ac:
        response = await ac.post(
            f"{PATH_APP}/auth/token",
            data=app_user,
            headers={"content-type": "application/x-www-form-urlencoded"},
        )

    logger.info(f"response: {response.text}")
    if response and response.json():
        token = response.json()["access_token"]
        return token
    else:
        pytest.exit(99)


# ~ @pytest.mark.skipif("READYONLY" not in os.environ, reason="Test Not Yet Ready")
async def authentication_basic_test():
    """
    Authentication

    @NOTES
    - we use this to test auth (which is already 'tested' via our 'get_auth_token' above
          but we want a true test of auth plus we want to authenticate as the tester4update
          user in case the app side of this account is not yet set up.
    """

    async with AsyncClient(app=async_client_app, base_url=PATH_SERVER) as ac:
        response = await ac.post(
            f"{PATH_APP}/auth/token",
            data=auth_user,
            headers={"content-type": "application/x-www-form-urlencoded"},
        )
    assert response.status_code == 200
    assert response.json()["token_type"] == "bearer"


#
# ◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆ Logs ◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆
#
# @NEEDS:
# @NEEDED BY:
#
@pytest.mark.skipif("READYONLY" not in os.environ, reason="LOGS testing disabled.")
async def logs_1_add_test(get_auth_token):
    """
    Log: Add - add a new.
    """

    global logs_1_id
    logs_1["date"] = arrow.utcnow().to("US/Eastern").format("YYYY-MM-DD")
    async with AsyncClient(app=async_client_app, base_url=PATH_SERVER) as ac:
        response = await ac.post(
            f"{PATH_APP}/logs/",
            params=logs_1,
            headers={"Authorization": f"Bearer {get_auth_token}"} | headers_base,
        )

    # ~ logger.info(f"logs_1_add response: {response.text}")
    assert response.status_code == 201
    assert response.json()["ok"] is True
    assert len(response.json()["id"]) > 0
    # set _id so we can use it later.
    logs_1_id = response.json()["id"]


@pytest.mark.skipif("READYONLY" not in os.environ, reason="LOGS testing disabled.")
async def logs_1_get_all_test(get_auth_token):
    """
    Log: get all.

    @NOTES
    - should have more than one as we just added one
    """

    _params = {
        "limit": 10,
        "skip": 0,
    }
    async with AsyncClient(app=async_client_app, base_url=PATH_SERVER) as ac:
        response = await ac.get(
            f"{PATH_APP}/logs/all",
            params=_params,
            headers={"Authorization": f"Bearer {get_auth_token}"} | headers_base,
        )

    # ~ logger.info(f"logs_1_get_all response: {response.text}")
    assert response.status_code == 200
    assert len(response.json()) > 1


@pytest.mark.skipif("READYONLY" not in os.environ, reason="LOGS testing disabled.")
async def logs_1_update_test(get_auth_token):
    """
    Log: update.
    """

    global logs_1
    logs_1["_id"] = logs_1_id
    logs_1["location"] = f"{logs_1['location']} Updated"
    logs_1["message"] = f"{logs_1['message']} Updated"
    async with AsyncClient(app=async_client_app, base_url=PATH_SERVER) as ac:
        response = await ac.put(
            f"{PATH_APP}/logs/",
            params=logs_1,
            headers={"Authorization": f"Bearer {get_auth_token}"} | headers_base,
        )

    # ~ logger.info(f"logs_1_update response: {response.text}")
    assert response.status_code == 200
    assert response.json()["ok"] is True


@pytest.mark.skipif("READYONLY" not in os.environ, reason="LOGS testing disabled.")
async def logs_1_get_one_test(get_auth_token):
    """
    Log: get a specific instance.

    @NOTES
    - we do this *AFTER* the update so we can check that the update data has been persisted!
    """

    async with AsyncClient(app=async_client_app, base_url=PATH_SERVER) as ac:
        response = await ac.get(
            f"{PATH_APP}/logs/one/{logs_1_id}",
            headers={"Authorization": f"Bearer {get_auth_token}"} | headers_base,
        )

    # ~ logger.info(f"logs_1_get_one response: {response.text}")
    assert response.status_code == 200
    assert response.json()["level"] == logs_1["level"]
    assert response.json()["status"] == logs_1["status"]
    assert response.json()["location"] == logs_1["location"]
    assert response.json()["message"] == logs_1["message"]


#
# ◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆ CLIENTS ◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆
#
# @NEEDS:
# @NEEDED BY: project, registration
#
# ~ @pytest.mark.skipif("READYONLY" not in os.environ, reason="Test Not Yet Ready")
async def clients_1_add_test(get_auth_token):
    """
    Client: Add - add a new.
    """

    global client_1_id
    async with AsyncClient(app=async_client_app, base_url=PATH_SERVER) as ac:
        response = await ac.post(
            f"{PATH_APP}/clients/",
            params=client_1,
            headers={"Authorization": f"Bearer {get_auth_token}"} | headers_base,
        )

    logger.info(f"clients_1_add response: {response.text}")
    assert response.status_code == 201
    assert response.json()["ok"] is True
    assert len(response.json()["id"]) > 0
    # set _id so we can use it later.
    client_1_id = response.json()["id"]


# ~ @pytest.mark.skipif("READYONLY" not in os.environ, reason="Test Not Yet Ready")
async def clients_1_get_all_test(get_auth_token):
    """
    Client: get all.

    @NOTES
    - should have more than one as we just added one
    """

    _params = {
        "only_enabled": False,
    }
    async with AsyncClient(app=async_client_app, base_url=PATH_SERVER) as ac:
        response = await ac.get(
            f"{PATH_APP}/clients/all",
            params=_params,
            headers={"Authorization": f"Bearer {get_auth_token}"} | headers_base,
        )

    logger.info(f"clients_1_get_all response: {response.text}")
    assert response.status_code == 200
    assert response.json()["total"] > 1


# ~ @pytest.mark.skipif("READYONLY" not in os.environ, reason="Test Not Yet Ready")
async def clients_1_update_test(get_auth_token):
    """
    Client: update.
    """

    global client_1
    client_1["_id"] = client_1_id
    client_1["enabled"] = True
    client_1["name"] = f"{client_1['name']} Updated"
    client_1["nickname"] = "PYTB"
    client_1["note"] = f"{client_1['note']} Updated"

    async with AsyncClient(app=async_client_app, base_url=PATH_SERVER) as ac:
        response = await ac.put(
            f"{PATH_APP}/clients/",
            params=client_1,
            headers={"Authorization": f"Bearer {get_auth_token}"} | headers_base,
        )
    logger.info(f"clients_1_update response: {response.text}")
    assert response.status_code == 200
    assert response.json()["ok"] is True


# ~ @pytest.mark.skipif("READYONLY" not in os.environ, reason="Test Not Yet Ready")
async def clients_1_get_one_test(get_auth_token):
    """
    Client: get a specific instance.

    @NOTES
    - we do this *AFTER* the update so we can check that the update data has been persisted!
    """

    global client_1_id
    async with AsyncClient(app=async_client_app, base_url=PATH_SERVER) as ac:
        response = await ac.get(
            f"{PATH_APP}/clients/one/{client_1_id}",
            headers={"Authorization": f"Bearer {get_auth_token}"} | headers_base,
        )

    logger.info(f"clients_1_get_one response: {response.text}")
    assert response.status_code == 200
    # ~ # NOTE: we only check the items which can be updated.

    assert response.json()["enabled"] == client_1["enabled"]
    assert response.json()["name"] == client_1["name"]
    assert response.json()["nickname"] == client_1["nickname"]
    assert response.json()["note"] == client_1["note"]


#
# ◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆ PROJECTS ◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆
#
# @NEEDS: client
# @NEEDED BY: task
#
# ~ @pytest.mark.skipif("READYONLY" not in os.environ, reason="Test Not Yet Ready")
async def projects_1_add_test(get_auth_token):
    """
    Project: Add - add a new.
    """

    global project_1_id
    project_1["client"] = client_1_id
    logger.info(f"Adding project: {project_1}")
    async with AsyncClient(app=async_client_app, base_url=PATH_SERVER) as ac:
        response = await ac.post(
            f"{PATH_APP}/projects/",
            params=project_1,
            headers={"Authorization": f"Bearer {get_auth_token}"} | headers_base,
        )

    logger.info(f"projects_1_add response: {response.text}")
    assert response.status_code == 201
    assert response.json()["ok"] is True
    assert len(response.json()["id"]) > 0
    # set _id so we can use it later.
    project_1_id = response.json()["id"]


# ~ @pytest.mark.skipif("READYONLY" not in os.environ, reason="Test Not Yet Ready")
async def projects_1_get_all_test(get_auth_token):
    """
    Project: get all.

    @NOTES
    - should have more than one as we just added one
    """

    _params = {}
    async with AsyncClient(app=async_client_app, base_url=PATH_SERVER) as ac:
        response = await ac.get(
            f"{PATH_APP}/projects/all",
            params=_params,
            headers={"Authorization": f"Bearer {get_auth_token}"} | headers_base,
        )

    logger.info(f"projects_1_get_all response: {response.text}")
    assert response.status_code == 200
    assert response.json()["total"] > 1


# ~ @pytest.mark.skipif("READYONLY" not in os.environ, reason="Test Not Yet Ready")
async def projects_1_by_client_id_test(get_auth_token):
    """
    Project: get by client id.

    @NOTES
    - should have one as we just added one
    """

    # ~ _params = {
    # ~ "_client_id": client_1_id,
    # ~ }
    async with AsyncClient(app=async_client_app, base_url=PATH_SERVER) as ac:
        response = await ac.get(
            f"{PATH_APP}/projects/by-client-id/{client_1_id}",
            # ~ params=_params,
            headers={"Authorization": f"Bearer {get_auth_token}"} | headers_base,
        )

    logger.info(f"projects_1_get_all response: {response.text}")
    assert response.status_code == 200
    assert response.json()["total"] == 1


# ~ @pytest.mark.skipif("READYONLY" not in os.environ, reason="Test Not Yet Ready")
async def projects_1_update_test(get_auth_token):
    """
    Project: update.
    """

    global project_1
    project_1["_id"] = project_1_id
    project_1["enabled"] = True
    project_1["name"] = f"{project_1['name']} Updated"
    project_1["nickname"] = "PTPB"
    project_1["order"] = 2
    project_1["bill_rate"] = 98
    project_1["expected_hours_per_day"] = 2
    project_1["note"] = f"{client_1['note']} Updated"

    async with AsyncClient(app=async_client_app, base_url=PATH_SERVER) as ac:
        response = await ac.put(
            f"{PATH_APP}/projects/",
            params=project_1,
            headers={"Authorization": f"Bearer {get_auth_token}"} | headers_base,
        )
    logger.info(f"projects_1_update response: {response.text}")
    assert response.status_code == 200
    assert response.json()["ok"] is True


# ~ @pytest.mark.skipif("READYONLY" not in os.environ, reason="Test Not Yet Ready")
async def projects_1_get_one_test(get_auth_token):
    """
    Project: get a specific instance.

    @NOTES
    - we do this *AFTER* the update so we can check that the update data has been persisted!
    """

    global project_1_id
    async with AsyncClient(app=async_client_app, base_url=PATH_SERVER) as ac:
        response = await ac.get(
            f"{PATH_APP}/projects/one/{project_1_id}",
            headers={"Authorization": f"Bearer {get_auth_token}"} | headers_base,
        )

    logger.info(f"projects_1_get_one response: {response.text}")
    assert response.status_code == 200
    # ~ # NOTE: we only check the items which can be updated.

    assert response.json()["enabled"] == project_1["enabled"]
    assert response.json()["name"] == project_1["name"]
    assert response.json()["nickname"] == project_1["nickname"]
    assert response.json()["order"] == project_1["order"]
    assert response.json()["bill_rate"] == project_1["bill_rate"]
    assert response.json()["expected_hours_per_day"] == project_1["expected_hours_per_day"]


#
# ◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆ TASKS ◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆
#
# @NEEDS: project
# @NEEDED BY: registration
#
# ~ @pytest.mark.skipif("READYONLY" not in os.environ, reason="tasks_1_add - not ready")
async def tasks_1_add_test(get_auth_token):
    """
    Task: Add - add a new.
    """

    global task_1_id
    task_1["project_id"] = project_1_id
    logger.info(f"Adding task: {task_1}")
    async with AsyncClient(app=async_client_app, base_url=PATH_SERVER) as ac:
        response = await ac.post(
            f"{PATH_APP}/tasks/",
            params=task_1,
            headers={"Authorization": f"Bearer {get_auth_token}"} | headers_base,
        )

    logger.info(f"tasks_1_add response: {response.text}")
    assert response.status_code == 201
    assert response.json()["ok"] is True
    assert len(response.json()["id"]) > 0
    # set id so we can use it later.
    task_1_id = response.json()["id"]


# ~ @pytest.mark.skipif("READYONLY" not in os.environ, reason="tasks_1_get_all - not ready")
async def tasks_1_get_all_test(get_auth_token):
    """
    Task: get all.

    @NOTES
    - should have more than one as we just added one
    """

    _params = {}
    async with AsyncClient(app=async_client_app, base_url=PATH_SERVER) as ac:
        response = await ac.get(
            f"{PATH_APP}/tasks/all",
            params=_params,
            headers={"Authorization": f"Bearer {get_auth_token}"} | headers_base,
        )

    logger.info(f"tasks_1_get_all response: {response.text}")
    assert response.status_code == 200
    assert response.json()["total"] > 1


# ~ @pytest.mark.skipif("READYONLY" not in os.environ, reason="tasks_list - not ready")
async def tasks_list_test(get_auth_token):
    """
    Task: get task_list.

    @NOTES
    - should have more than one as we just added one
    """

    async with AsyncClient(app=async_client_app, base_url=PATH_SERVER) as ac:
        response = await ac.get(
            f"{PATH_APP}/tasks/task_list",
            # ~ params=_params,
            headers={"Authorization": f"Bearer {get_auth_token}"} | headers_base,
        )

    logger.info(f"tasks_list response: {response.text}")
    assert response.status_code == 200
    assert len(response.json()) > 0


# ~ @pytest.mark.skipif("READYONLY" not in os.environ, reason="tasks_1_update - not ready")
async def tasks_1_update_test(get_auth_token):
    """
    Task: update.
    """

    global task_1
    task_1["_id"] = task_1_id
    task_1["project_id"] = project_1_id
    task_1["enabled"] = True
    task_1["name"] = f"{task_1['name']} Updated"
    task_1["nickname"] = "PYPB"
    task_1["order"] = 2
    # ~ task_1["ticket"] = "DEF"
    # ~ task_1["complete"] = True
    task_1["icon"] = "bi-bug"
    task_1["note"] = f"{client_1['note']} Updated"
    async with AsyncClient(app=async_client_app, base_url=PATH_SERVER) as ac:
        response = await ac.put(
            f"{PATH_APP}/tasks/",
            params=task_1,
            headers={"Authorization": f"Bearer {get_auth_token}"} | headers_base,
        )
    logger.info(f"tasks_1_update response: {response.text}")
    assert response.status_code == 200
    assert response.json()["ok"] is True


# ~ @pytest.mark.skipif("READYONLY" not in os.environ, reason="tasks_1_get_one - not ready")
async def tasks_1_get_one_test(get_auth_token):
    """
    Task: get a specific instance.

    @NOTES
    - we do this *AFTER* the update so we can check that the update data has been persisted!
    """

    global task_1_id
    async with AsyncClient(app=async_client_app, base_url=PATH_SERVER) as ac:
        response = await ac.get(
            f"{PATH_APP}/tasks/one/{task_1_id}",
            headers={"Authorization": f"Bearer {get_auth_token}"} | headers_base,
        )

    logger.info(f"tasks_1_get_one response: {response.text}")
    assert response.status_code == 200
    # ~ # NOTE: we only check the items which can be updated.

    assert response.json()["enabled"] == task_1["enabled"]
    assert response.json()["project_id"] == task_1["project_id"]
    assert response.json()["name"] == task_1["name"]
    assert response.json()["nickname"] == task_1["nickname"]
    assert response.json()["order"] == task_1["order"]
    # ~ assert response.json()["ticket"] == task_1["ticket"]
    # ~ assert response.json()["complete"] == task_1["complete"]
    assert response.json()["icon"] == task_1["icon"]
    assert response.json()["note"] == task_1["note"]


#
# ◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆ REGISTRATION TYPES ◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆
#
# @NEEDS: client
# @NEEDED BY: registration
#
# ~ @pytest.mark.skipif("READYONLY" not in os.environ, reason="registration_types_1_add - not ready")
async def registration_types_1_add_test(get_auth_token):
    """
    Registration Type: Add - add a new.
    """

    global registration_type_1_id
    registration_type_1["client"] = client_1_id
    logger.info(f"Adding registration type: {registration_type_1}")
    async with AsyncClient(app=async_client_app, base_url=PATH_SERVER) as ac:
        response = await ac.post(
            f"{PATH_APP}/registration_types/",
            params=registration_type_1,
            headers={"Authorization": f"Bearer {get_auth_token}"} | headers_base,
        )

    logger.info(f"registration_types_1_add response: {response.text}")
    assert response.status_code == 201
    assert response.json()["ok"] is True
    assert len(response.json()["id"]) > 0
    # set id so we can use it later.
    registration_type_1_id = response.json()["id"]


# ~ @pytest.mark.skipif("READYONLY" not in os.environ, reason="registration_types_1_get_all - not ready")
async def registration_types_1_get_all_test(get_auth_token):
    """
    Registration Type: get all.

    @NOTES
    - should have more than one as we just added one
    """

    _params = {}
    async with AsyncClient(app=async_client_app, base_url=PATH_SERVER) as ac:
        response = await ac.get(
            f"{PATH_APP}/registration_types/all",
            params=_params,
            headers={"Authorization": f"Bearer {get_auth_token}"} | headers_base,
        )

    logger.info(f"registration_types_1_get_all response: {response.text}")
    assert response.status_code == 200
    assert response.json()["total"] > 1


# ~ @pytest.mark.skipif("READYONLY" not in os.environ, reason="registration_types_1_update - not ready")
async def registration_types_1_update_test(get_auth_token):
    """
    Registration Type: update.
    """

    global registration_type_1
    registration_type_1["_id"] = registration_type_1_id
    registration_type_1["client"] = client_1_id
    registration_type_1["enabled"] = True
    registration_type_1["name"] = f"{registration_type_1['name']} Updated"
    registration_type_1["icon"] = "bi-bug"
    registration_type_1["note"] = f"{registration_type_1['note']} Updated"
    async with AsyncClient(app=async_client_app, base_url=PATH_SERVER) as ac:
        response = await ac.put(
            f"{PATH_APP}/registration_types/",
            params=registration_type_1,
            headers={"Authorization": f"Bearer {get_auth_token}"} | headers_base,
        )
    logger.info(f"registration_types_1_update response: {response.text}")
    assert response.status_code == 200
    assert response.json()["ok"] is True


# ~ @pytest.mark.skipif("READYONLY" not in os.environ, reason="registration_types_1_get_one - not ready")
async def registration_types_1_get_one_test(get_auth_token):
    """
    Registration Type: get a specific instance.

    @NOTES
    - we do this *AFTER* the update so we can check that the update data has been persisted!
    """

    global registration_type_1_id
    async with AsyncClient(app=async_client_app, base_url=PATH_SERVER) as ac:
        response = await ac.get(
            f"{PATH_APP}/registration_types/one/{registration_type_1_id}",
            headers={"Authorization": f"Bearer {get_auth_token}"} | headers_base,
        )

    logger.info(f"registration_types_1_get_one response: {response.text}")
    assert response.status_code == 200
    # ~ # NOTE: we only check the items which can be updated.

    assert response.json()["enabled"] == registration_type_1["enabled"]
    assert response.json()["client"] == registration_type_1["client"]
    assert response.json()["name"] == registration_type_1["name"]
    assert response.json()["icon"] == registration_type_1["icon"]
    assert response.json()["note"] == registration_type_1["note"]


# ~ @pytest.mark.skipif("READYONLY" not in os.environ, reason="registration_type_list - not ready")
async def registration_type_list_test(get_auth_token):
    """
    Registration Type: get registration_type_list.

    @NOTES
    - THIS MUST occur after registration_types_1_update as this is what sets the regristration_type_1 to ENABLED!
    - should have more than one as we just added one
    """

    _params = {"client": client_1_id}
    async with AsyncClient(app=async_client_app, base_url=PATH_SERVER) as ac:
        response = await ac.get(
            f"{PATH_APP}/registration_types/registration_type_list",
            params=_params,
            headers={"Authorization": f"Bearer {get_auth_token}"} | headers_base,
        )

    logger.info(f"registration_type_list response: {response.text}")
    assert response.status_code == 200
    assert len(response.json()) > 0


#
# ◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆ REGISTRATIONS ◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆
#
# @NEEDS: task, registration_type
# @NEEDED BY: invoice
# @NOTE:
#   - the invoice endpoint (registrations/invoice) is not tested here as it needs an invoice so it is tested in the INVOICES section.
#
# ~ @pytest.mark.skipif("READYONLY" not in os.environ, reason="registrations_1_add - not ready")
async def registrations_1_add_test(get_auth_token):
    """
    Registration: Add - add a new.
    """

    global registration_1_id
    registration_1["user"] = app_user["username"]
    registration_1["task_id"] = task_1_id
    registration_1["registration_type"] = registration_type_1_id
    registration_1["start"] = arrow.utcnow().shift(minutes=-30).to("US/Eastern").format("YYYY-MM-DDTHH:mm:ss.SZ")
    registration_1["end"] = arrow.utcnow().to("US/Eastern").format("YYYY-MM-DDTHH:mm:ss.SZ")
    logger.info(f"Adding registration: {registration_1}")
    async with AsyncClient(app=async_client_app, base_url=PATH_SERVER) as ac:
        response = await ac.post(
            f"{PATH_APP}/registrations/",
            params=registration_1,
            headers={"Authorization": f"Bearer {get_auth_token}"} | headers_base,
        )

    logger.info(f"registrations_1_add response: {response.text}")
    assert response.status_code == 201
    assert response.json()["ok"] is True
    assert len(response.json()["id"]) > 0
    # set id so we can use it later.
    registration_1_id = response.json()["id"]


# ~ @pytest.mark.skipif("READYONLY" not in os.environ, reason="registrations_add_batch - not ready")
async def registrations_add_batch_test(get_auth_token):
    """
    Registration: Add Batch - add a set of registrations.
    """

    _params = {
        "amount": registration_batch_add_amount,
        "start": arrow.utcnow().shift(hours=-8).to("US/Eastern").format("YYYY-MM-DD HH:mm:ss.S"),
        "task_id": task_1_id,
        "registration_type": registration_type_1_id,
        "note": "batch add registration created by pytest",
        "tags": ["test", "pytest", "batch", "add"],
    }
    async with AsyncClient(app=async_client_app, base_url=PATH_SERVER) as ac:
        response = await ac.post(
            f"{PATH_APP}/registrations/batch",
            params=_params,
            headers={"Authorization": f"Bearer {get_auth_token}"} | headers_base,
        )

    logger.info(f"registrations_add_batch response: {response.text}")
    assert response.status_code == 201
    assert response.json()["ok"] is True
    # ~ assert len(response.json()["id"]) > 0
    # set id so we can use it later.
    # registration_1_id = response.json()["id"]


# ~ @pytest.mark.skipif("READYONLY" not in os.environ, reason="registrations_1_get_all - not ready")
async def registrations_1_get_all_test(get_auth_token):
    """
    Registration: get all.

    @NOTES
    - should have more than one as we just added one
    """

    _params = {
        "limit": 99,
        "skip": 0,
    }
    async with AsyncClient(app=async_client_app, base_url=PATH_SERVER) as ac:
        response = await ac.get(
            f"{PATH_APP}/registrations/all",
            params=_params,
            headers={"Authorization": f"Bearer {get_auth_token}"} | headers_base,
        )

    logger.info(f"registrations_1_get_all response: {response.text}")
    assert response.status_code == 200
    assert response.json()["total"] >= registration_batch_add_amount + 1


# ~ @pytest.mark.skipif("READYONLY" not in os.environ, reason="registrations_1_update - not ready")
async def registrations_1_update_test(get_auth_token):
    """
    Registration: update.
    """

    global registration_1
    registration_1["_id"] = registration_1_id
    registration_1["user"] = app_user["username"]
    registration_1["task_id"] = task_1_id
    registration_1["registration_type"] = registration_type_1_id
    registration_1["start"] = arrow.utcnow().shift(minutes=-40).to("US/Eastern").format("YYYY-MM-DDTHH:mm:ss.SZ")
    registration_1["end"] = arrow.utcnow().to("US/Eastern").format("YYYY-MM-DDTHH:mm:ss.SZ")
    registration_1["ticket_number"] = "PYT-001A"
    registration_1["note"] = f"{registration_1['note']} Updated"
    registration_1["tags"] = ["test", "pytest", "updated"]
    logger.info(f"Updating registration: {registration_1}")
    async with AsyncClient(app=async_client_app, base_url=PATH_SERVER) as ac:
        response = await ac.put(
            f"{PATH_APP}/registrations/",
            params=registration_1,
            headers={"Authorization": f"Bearer {get_auth_token}"} | headers_base,
        )
    logger.info(f"registrations_1_update response: {response.text}")
    assert response.status_code == 200
    assert response.json()["ok"] is True


# ~ @pytest.mark.skipif("READYONLY" not in os.environ, reason="registrations_1_get_one - not ready")
async def registrations_1_get_one_test(get_auth_token):
    """
    Registration: get a specific instance.

    @NOTES
    - we do this *AFTER* the update so we can check that the update data has been persisted!
    """

    global registration_1_id
    async with AsyncClient(app=async_client_app, base_url=PATH_SERVER) as ac:
        response = await ac.get(
            f"{PATH_APP}/registrations/one/{registration_1_id}",
            headers={"Authorization": f"Bearer {get_auth_token}"} | headers_base,
        )

    logger.info(f"registrations_1_get_one response: {response.text}")
    assert response.status_code == 200
    # ~ # NOTE: we only check the items which can be updated.
    assert (
        arrow.get(response.json()["start"]).to("US/Eastern").format("YYYY-MM-DDTHH:mm:ss.SZ") == registration_1["start"]
    )
    assert arrow.get(response.json()["end"]).to("US/Eastern").format("YYYY-MM-DDTHH:mm:ss.SZ") == registration_1["end"]
    assert response.json()["ticket_number"] == registration_1["ticket_number"]
    assert response.json()["tags"] == registration_1["tags"]
    assert response.json()["note"] == registration_1["note"]


# ~ @pytest.mark.skipif("READYONLY" not in os.environ, reason="registration_type_list - not ready")
async def registration_all_for_user_test(get_auth_token):
    """
    Registration: get all registrations for a user.

    @NOTES
    - we should have registration_batch_add_amount + 1 registrations at this point
    """

    _params = {
        # ~ "qfilter": ???,
        # ~ "filter_general": ???,
        # ~ "filter_start": ???,
        # ~ "filter_end": ???,
        # ~ "filter_client": ???,
        "include_invoiced": False,
        "limit": 999,
        "skip": 0,
    }
    async with AsyncClient(app=async_client_app, base_url=PATH_SERVER) as ac:
        response = await ac.get(
            f"{PATH_APP}/registrations/all-for-user",
            params=_params,
            headers={"Authorization": f"Bearer {get_auth_token}"} | headers_base,
        )

    logger.info(f"registrations all-for-user response: {response.text}")
    assert response.status_code == 200
    assert response.json()["total"] == registration_batch_add_amount + 1
    assert len(response.json()["registrations"]) > 0


#
# ◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆ INVOICES ◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆
#
# @NEEDS: registration
# @NEEDED BY: registration/invoice
# @NOTE:
#   - the registration/invoice endpoint is within this section as it needs an invoice to use and the Registration/* endpoints are tested before Invoices/*


# ~ @pytest.mark.skipif("READYONLY" not in os.environ, reason="invoices_1_add - not ready")
async def invoices_1_add_test(get_auth_token):
    """
    Invoice: Add - add a new.
    """

    global invoice_1_id
    invoice_1["client"] = client_1_id
    invoice_1["submitted"] = arrow.now("US/Eastern").format("YYYY-MM-DDTHH:mm:ss")
    invoice_1["due"] = arrow.now("US/Eastern").shift(days=30).format("YYYY-MM-DDTHH:mm:ss")
    logger.info(f"Adding invoice: {invoice_1}")
    async with AsyncClient(app=async_client_app, base_url=PATH_SERVER) as ac:
        response = await ac.post(
            f"{PATH_APP}/invoices/",
            params=invoice_1,
            headers={"Authorization": f"Bearer {get_auth_token}"} | headers_base,
        )

    logger.info(f"invoices_1_add response: {response.text}")
    assert response.status_code == 201
    assert response.json()["ok"] is True
    assert len(response.json()["id"]) > 0
    # set id so we can use it later.
    invoice_1_id = response.json()["id"]


# ~ @pytest.mark.skipif("READYONLY" not in os.environ, reason="invoices_1_get_all - not ready")
async def invoices_1_get_all_test(get_auth_token):
    """
    Invoice: get all.

    @NOTES
    - should have more than one as we just added one
    """

    _params = {
        "limit": 99,
        "skip": 0,
    }
    async with AsyncClient(app=async_client_app, base_url=PATH_SERVER) as ac:
        response = await ac.get(
            f"{PATH_APP}/invoices/all",
            params=_params,
            headers={"Authorization": f"Bearer {get_auth_token}"} | headers_base,
        )

    logger.info(f"invoices_1_get_all response: {response.text}")
    assert response.status_code == 200
    assert len(response.json()) > 0


# ~ @pytest.mark.skipif("READYONLY" not in os.environ, reason="invoices_1_update - not ready")
async def invoices_1_update_test(get_auth_token):
    """
    Invoice: update.
    """

    global invoice_1
    invoice_1["_id"] = invoice_1_id
    invoice_1["name"] = f"{invoice_1['name']} Updated"
    invoice_1["client"] = client_1_id
    invoice_1["layout"] = "detail"
    invoice_1["submitted"] = arrow.now("US/Eastern").format("YYYY-MM-DDTHH:mm:ss")
    invoice_1["due"] = arrow.now("US/Eastern").shift(days=30).format("YYYY-MM-DDTHH:mm:ss")
    invoice_1["paid"] = arrow.utcnow().shift(minutes=5).to("US/Eastern").format("YYYY-MM-DDTHH:mm:ss")
    invoice_1["deposited"] = arrow.utcnow().shift(minutes=5).to("US/Eastern").format("YYYY-MM-DDTHH:mm:ss")
    invoice_1["discount"] = 10.0
    invoice_1["paid_amount"] = 0.0
    invoice_1["note"] = f"{invoice_1['note']} Updated"
    invoice_1["detail"] = f"{invoice_1['detail']} Updated"
    invoice_1["tags"] = ["test", "pytest", "updated"]
    logger.info(f"Updating invoice: {invoice_1}")
    async with AsyncClient(app=async_client_app, base_url=PATH_SERVER) as ac:
        response = await ac.put(
            f"{PATH_APP}/invoices/",
            params=invoice_1,
            headers={"Authorization": f"Bearer {get_auth_token}"} | headers_base,
        )
    logger.info(f"invoices_1_update response: {response.text}")
    assert response.status_code == 200
    assert response.json()["ok"] is True


# ~ @pytest.mark.skipif("READYONLY" not in os.environ, reason="registrations_1_invoice - not ready")
async def registrations_1_invoice_test(get_auth_token):
    """
    Registration: invoice a specific registration.

    @NOTES
    - this is done in the Invoice section as we need an invoice to assign the registration to
    - this must come before invoice_1_get_one as it expects one registration to be on the invoice
    """

    _registration = {
        "_id": registration_1_id,
        "invoice": invoice_1_id,
    }
    logger.info(f"Registration to invoice: {_registration}")
    async with AsyncClient(app=async_client_app, base_url=PATH_SERVER) as ac:
        response = await ac.put(
            f"{PATH_APP}/registrations/invoice",
            params=_registration,
            headers={"Authorization": f"Bearer {get_auth_token}"} | headers_base,
        )
    logger.info(f"invoices_1_update response: {response.text}")
    assert response.status_code == 200
    assert response.json()["ok"] is True


# ~ @pytest.mark.skipif("READYONLY" not in os.environ, reason="invoices_1_get_one - not ready")
async def invoices_1_get_one_test(get_auth_token):
    """
    Invoice: get a specific instance.

    @NOTES
    - we do this *AFTER* the update and the registration.invoice so we can check that the update data has been persisted (including the invoiced registration).
    """

    # ~ global invoice_1_id
    async with AsyncClient(app=async_client_app, base_url=PATH_SERVER) as ac:
        response = await ac.get(
            f"{PATH_APP}/invoices/one/{invoice_1_id}",
            headers={"Authorization": f"Bearer {get_auth_token}"} | headers_base,
        )

    logger.info(f"invoices_1_get_one response: {response.text}")
    assert response.status_code == 200
    # ~ # NOTE: we only check the items which can be updated.
    assert response.json()["name"] == invoice_1["name"]
    assert response.json()["client"] == invoice_1["client"]
    assert response.json()["layout"] == invoice_1["layout"]
    if response.json()["submitted"]:
        assert (
            arrow.get(response.json()["submitted"]).to("US/Eastern").format("YYYY-MM-DDTHH:mm:ss")
            == invoice_1["submitted"]
        )
    if response.json()["due"]:
        assert arrow.get(response.json()["due"]).to("US/Eastern").format("YYYY-MM-DDTHH:mm:ss") == invoice_1["due"]
    if response.json()["paid"]:
        assert arrow.get(response.json()["paid"]).to("US/Eastern").format("YYYY-MM-DDTHH:mm:ss") == invoice_1["paid"]
    if response.json()["deposited"]:
        assert (
            arrow.get(response.json()["deposited"]).to("US/Eastern").format("YYYY-MM-DDTHH:mm:ss")
            == invoice_1["deposited"]
        )

    assert response.json()["discount"] == invoice_1["discount"]
    assert response.json()["paid_amount"] == invoice_1["paid_amount"]
    assert len(response.json()["invoice_number"]) == 13
    assert response.json()["status"] == "deposited"
    assert response.json()["client_name"] == client_1["name"]
    assert len(response.json()["registrations"]) == 1
    assert response.json()["note"] == invoice_1["note"]
    assert response.json()["detail"] == invoice_1["detail"]
    assert response.json()["tags"] == invoice_1["tags"]


#
# ◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆ USERS ◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆
#
# ~ @pytest.mark.skipif("READYONLY" not in os.environ, reason="Test Not Yet Ready")
async def users_me_test(get_auth_token):
    """
    User Me: get 'me' info for logged in user

    @NOTES
    - this is as the 'app_user' user
    """

    async with AsyncClient(app=async_client_app, base_url=PATH_SERVER) as ac:
        response = await ac.get(
            f"{PATH_APP}/users/me",
            headers={"Authorization": f"Bearer {get_auth_token}"} | headers_base,
        )
    assert response.status_code == 200
    assert response.json()["username"] == app_user["username"]


# ~ @pytest.mark.skipif("READYONLY" not in os.environ, reason="Test Not Yet Ready")
async def users_get_all_test(get_auth_token):
    """
    User: get all

    @TODO: this should likely be changed to a comprehension to see if drad and tester
           is in the results rather than drad as first response.
    """

    async with AsyncClient(app=async_client_app, base_url=PATH_SERVER) as ac:
        response = await ac.get(
            f"{PATH_APP}/users/",
            headers={"Authorization": f"Bearer {get_auth_token}"} | headers_base,
        )
    assert response.status_code == 200
    assert len(response.json()) >= 1


# ~ @pytest.mark.skipif("READYONLY" not in os.environ, reason="Test Not Yet Ready")
async def users_update_test(get_auth_token):
    """
    User: update

    @NOTES
    - this updates the app_user_for_update user.
    """

    # ~ _user = {
    # ~ "username": app_user_for_update_username,
    # ~ "enabled": True
    # ~ }

    # NOTICE: we did not global import app_user_for_update so changes are local
    app_user_for_update["username"] = app_user_for_update_username
    # ~ if "preferences" in app_user_for_update:
    # ~ _data["preferences"] = app_user_for_update['preferences']
    # ~ del app_user_for_update['preferences']
    # ~ logger.info(f"users_update data: {_data}")

    logger.info(f"users_update params: {app_user_for_update}")
    async with AsyncClient(app=async_client_app, base_url=PATH_SERVER) as ac:
        response = await ac.put(
            f"{PATH_APP}/users/",
            params=app_user_for_update,
            headers={"Authorization": f"Bearer {get_auth_token}"} | headers_base,
        )

    logger.info(f"users_update response: {response.text}")
    assert response.status_code == 200
    assert response.json()["ok"] is True


# ~ @pytest.mark.skipif("READYONLY" not in os.environ, reason="Test Not Yet Ready")
async def users_get_one_test(get_auth_token):
    """
    User: get a specific instance.

    @NOTES
    - we do this *AFTER* the update so we can check that the update data has been persisted!
    """

    async with AsyncClient(app=async_client_app, base_url=PATH_SERVER) as ac:
        response = await ac.get(
            f"{PATH_APP}/users/{app_user_for_update_username}",
            headers={"Authorization": f"Bearer {get_auth_token}"} | headers_base,
        )

    assert response.status_code == 200
    # NOTE: we only check the items which can be updated.
    assert response.json()["display_name"] == app_user_for_update["display_name"]
    assert response.json()["avatar"] == app_user_for_update["avatar"]
    assert response.json()["enabled"] == app_user_for_update["enabled"]
    assert response.json()["note"] == app_user_for_update["note"]


# ~ @pytest.mark.skipif("READYONLY" not in os.environ, reason="Test Not Yet Ready")
async def user_messages_1_add_test(get_auth_token):
    """
    User Message: add

    @NOTES
      - the to_user_id (who you send message to) should be the same user who has
        the auth token or your 'get*' calls will not be able to see the message
        unless you authenticate as a different user.
    """

    global user_message_1_id
    async with AsyncClient(app=async_client_app, base_url=PATH_SERVER) as ac:
        response = await ac.post(
            f"{PATH_APP}/user_messages/",
            params=user_message_1,
            headers={"Authorization": f"Bearer {get_auth_token}"} | headers_base,
        )

    logger.info(f"test user_message add response={response.text}")
    assert response.status_code == 201
    assert response.json()["ok"] is True
    assert len(response.json()["id"]) > 0
    # user_message_1_id so we can use it later.
    user_message_1_id = response.json()["id"]


# ~ @pytest.mark.skipif("READYONLY" not in os.environ, reason="Test Not Yet Ready")
async def user_messages_1_get_one_test(get_auth_token):
    """
    User Message: get a specific instance.

    @NOTES
    - we do this *AFTER* the add so we can check that the data has been persisted!
    """

    async with AsyncClient(app=async_client_app, base_url=PATH_SERVER) as ac:
        response = await ac.get(
            f"{PATH_APP}/user_messages/one/{user_message_1_id}",
            headers={"Authorization": f"Bearer {get_auth_token}"} | headers_base,
        )

    logger.info(f"test user_message get one response={response.text}")
    assert response.status_code == 200
    assert response.json()["to_user_id"] == user_message_1["to_user_id"]
    assert response.json()["status"] == "new"
    assert response.json()["category"] == user_message_1["category"]
    assert response.json()["priority"] == user_message_1["priority"]
    assert response.json()["subject"] == user_message_1["subject"]
    assert response.json()["message"] == user_message_1["message"]


# ~ @pytest.mark.skipif("READYONLY" not in os.environ, reason="Test Not Yet Ready")
async def user_messages_1_get_all_test(get_auth_token):
    """
    User Message: get all

    @NOTES
    - this is for currently authenticated user, we should have at least one (user_message_1)
    """

    async with AsyncClient(app=async_client_app, base_url=PATH_SERVER) as ac:
        response = await ac.get(
            f"{PATH_APP}/user_messages/all",
            headers={"Authorization": f"Bearer {get_auth_token}"} | headers_base,
        )

    logger.info(f"test user_message get all response={response.text}")
    assert response.status_code == 200
    assert len(response.json()) > 0


#
# ◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆ MISCELLANEOUS ◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆
#
# NOTE: this comes before cleanup to give more time for above to complete.
#
# ~ @pytest.mark.skipif("READYONLY" not in os.environ, reason="Test Not Yet Ready")
async def status_test():
    """
    Status
    """

    async with AsyncClient(app=async_client_app, base_url=PATH_SERVER) as ac:
        response = await ac.get(
            f"{PATH_BASE}/status",
            headers={"Authorization": f"Bearer {get_auth_token}"} | headers_base,
        )

    assert response.status_code == 200
    assert response.json()["status"] == "OK"


# ~ @pytest.mark.skipif("READYONLY" not in os.environ, reason="Test Not Yet Ready")
async def version_test():
    """
    Version
    """

    async with AsyncClient(app=async_client_app, base_url=PATH_SERVER) as ac:
        response = await ac.get(
            f"{PATH_BASE}/version",
            headers={"Authorization": f"Bearer {get_auth_token}"} | headers_base,
        )
    assert response.status_code == 200
    assert response.json() == app_version


#
# ◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆ CLEANUP ◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆
#
# ~ @pytest.mark.skipif("READYONLY" not in os.environ, reason="Test Not Yet Ready")
async def user_message_1_delete_test(get_auth_token):
    """
    User Message: delete
    """

    # ~ global user_message_1_id
    params = {"_id": user_message_1_id}
    async with AsyncClient(app=async_client_app, base_url=PATH_SERVER) as ac:
        response = await ac.delete(
            f"{PATH_APP}/user_messages/",
            params=params,
            headers={"Authorization": f"Bearer {get_auth_token}"} | headers_base,
        )

    assert response.status_code == 200
    assert response.json()["ok"] is True
    assert response.json()["msg"] == "deleted"


# ~ @pytest.mark.skipif("READYONLY" not in os.environ, reason="Test Not Yet Ready")
async def user_message_batch_delete_test(get_auth_token):
    """
    User Message: batch delete
    """

    params = {"persist": True}
    async with AsyncClient(app=async_client_app, base_url=PATH_SERVER) as ac:
        response = await ac.delete(
            f"{PATH_APP}/user_messages/batch",
            params=params,
            headers={"Authorization": f"Bearer {get_auth_token}"} | headers_base,
        )

    logger.info(f"user_message_batch_delete response: {response.text}")
    assert response.status_code == 200
    assert response.json()["ok"] is True


# ~ @pytest.mark.skipif("READYONLY" not in os.environ, reason="invoices_1_delete - not ready")
async def invoices_1_delete_test(get_auth_token):
    """
    Invoice: delete - delete Invoice 1.
    """

    params = {"_id": invoice_1_id}
    async with AsyncClient(app=async_client_app, base_url=PATH_SERVER) as ac:
        response = await ac.delete(
            f"{PATH_APP}/invoices/",
            params=params,
            headers={"Authorization": f"Bearer {get_auth_token}"} | headers_base,
        )

    assert response.status_code == 200
    assert response.json()["ok"] is True
    assert response.json()["msg"] == "deleted"


# ~ @pytest.mark.skipif("READYONLY" not in os.environ, reason="registrations_1_delete - not ready")
async def registrations_1_delete_test(get_auth_token):
    """
    Registration: delete - delete Registration 1.

    @NOTE:
    - this must come before the batch_delete of registrations
    """

    params = {"_id": registration_1_id}
    async with AsyncClient(app=async_client_app, base_url=PATH_SERVER) as ac:
        response = await ac.delete(
            f"{PATH_APP}/registrations/",
            params=params,
            headers={"Authorization": f"Bearer {get_auth_token}"} | headers_base,
        )

    assert response.status_code == 200
    assert response.json()["ok"] is True
    assert response.json()["msg"] == "deleted"


# ~ @pytest.mark.skipif("READYONLY" not in os.environ, reason="registrations_batch_delete - not ready")
# @pytest.mark.skipif("RUN_REALLY_LONG" not in os.environ, reason="Skipping really long tests")
# ~ @pytest.mark.async_timeout(30)
async def registrations_batch_delete_test(get_auth_token):
    """
    Registration: batch delete.

    @NOTE
    - we call the batch_delete after the delete of registration to allow testing of the normal delete
    - this endpoint is called to remove any/all registrations for the test user (i.e. from the batch_add)
    """

    params = {
        "user": app_user["username"],
        # ~ "note": ???,
        # ~ "skip": 0,
        "persist": True,
    }
    async with AsyncClient(app=async_client_app, base_url=PATH_SERVER) as ac:
        response = await ac.delete(
            f"{PATH_APP}/registrations/batch_delete",
            params=params,
            headers={"Authorization": f"Bearer {get_auth_token}"} | headers_base,
        )

    logger.debug(f"activities_batch_delete_test response={response.text}")
    assert response.status_code == 200
    assert response.json()["ok"] is True
    # assert response.json()["msg"] == f"Activities Deleted (persist=True): deleted={batch_add['amount']}, errors=0"
    assert response.json()["total"] == registration_batch_add_amount


# ~ @pytest.mark.skipif("READYONLY" not in os.environ, reason="registration_types_1_delete - not ready")
async def registration_types_1_delete_test(get_auth_token):
    """
    Registration Type: delete - delete Registration Type 1.
    """

    params = {"_id": registration_type_1_id}
    async with AsyncClient(app=async_client_app, base_url=PATH_SERVER) as ac:
        response = await ac.delete(
            f"{PATH_APP}/registration_types/",
            params=params,
            headers={"Authorization": f"Bearer {get_auth_token}"} | headers_base,
        )

    assert response.status_code == 200
    assert response.json()["ok"] is True
    assert response.json()["msg"] == "deleted"


# ~ @pytest.mark.skipif("READYONLY" not in os.environ, reason="tasks_1_delete - not ready")
async def tasks_1_delete_test(get_auth_token):
    """
    Task: delete - delete test Task 1.
    """

    params = {"_id": task_1_id}
    async with AsyncClient(app=async_client_app, base_url=PATH_SERVER) as ac:
        response = await ac.delete(
            f"{PATH_APP}/tasks/",
            params=params,
            headers={"Authorization": f"Bearer {get_auth_token}"} | headers_base,
        )

    assert response.status_code == 200
    assert response.json()["ok"] is True
    assert response.json()["msg"] == "deleted"


# ~ @pytest.mark.skipif("READYONLY" not in os.environ, reason="Test Not Yet Ready")
async def projects_1_delete_test(get_auth_token):
    """
    Project: delete - delete test project 1.
    """

    params = {"_id": project_1_id}
    async with AsyncClient(app=async_client_app, base_url=PATH_SERVER) as ac:
        response = await ac.delete(
            f"{PATH_APP}/projects/",
            params=params,
            headers={"Authorization": f"Bearer {get_auth_token}"} | headers_base,
        )

    assert response.status_code == 200
    assert response.json()["ok"] is True
    assert response.json()["msg"] == "deleted"


# ~ @pytest.mark.skipif("READYONLY" not in os.environ, reason="Test Not Yet Ready")
async def clients_1_delete_test(get_auth_token):
    """
    Client: delete - delete test client 1.
    """

    params = {"_id": client_1_id}
    async with AsyncClient(app=async_client_app, base_url=PATH_SERVER) as ac:
        response = await ac.delete(
            f"{PATH_APP}/clients/",
            params=params,
            headers={"Authorization": f"Bearer {get_auth_token}"} | headers_base,
        )

    assert response.status_code == 200
    assert response.json()["ok"] is True
    assert response.json()["msg"] == "deleted"


@pytest.mark.skipif("READYONLY" not in os.environ, reason="LOGS testing disabled.")
async def logs_1_delete_test(get_auth_token):
    """
    Log: delete - delete Log #1.
    """

    params = {"_id": logs_1_id}
    async with AsyncClient(app=async_client_app, base_url=PATH_SERVER) as ac:
        response = await ac.delete(
            f"{PATH_APP}/logs/",
            params=params,
            headers={"Authorization": f"Bearer {get_auth_token}"} | headers_base,
        )

    assert response.status_code == 200
    assert response.json()["ok"] is True
    assert response.json()["msg"] == "deleted"
