/*
 * Setup User
 * NOTICE: 2024-02-28 this is currently not used but leaving it in as a reference on
 *         how to use a composable (Composition API).
 */

/*
import { ref, onMounted, onUnmounted } from 'vue'

export function useSetupUser(token) {
  const user = ref(null);
  const error = ref(null);

  //~ const letsFetchUser = async (data) => {
    //~ try {
      //~ //const response = await axios.get(`/api/users/${userId}`);
      //~ //user.value = response.data;
      //~ console.log("We will fetch user here for userId=" + userId + "\nGot data=" + data);
    //~ } catch (e) {
      //~ error.value = e;
    //~ }
  //~ };

  console.log("Setting up user with token=" + token );

  //~ return { user, error, letsFetchUser };
  return { user, error };
}
*/
