#
# Initial Production Data for: Tasks.
#

tasks_add = []
tasks_error = []
tasks_skip = []

# NOTE: 'project_id' value is the project.nickname and is looked up and replaced with id before the add.
tasks = [
    # note: this list was generated from the design-doc.ods:Task sheet.
    {
        "project_id": "RSUP",
        "name": "Ticket Work",
        "nickname": "TCKT",
        "order": 2,
        "note": "rails support tickets from zendesk",
        "enabled": True,
        "icon": "bi-ticket",
    },
    {
        "project_id": "UKSE",
        "name": "Ticket Work",
        "nickname": "TCKT",
        "order": 3,
        "note": "",
        "enabled": True,
        "icon": "bi-ticket",
    },
    {
        "project_id": "RSUP",
        "name": "Devops",
        "nickname": "DEVO",
        "order": 12,
        "note": "infrastructure and general devops support",
        "enabled": True,
        "icon": "bi-columns-gap",
    },
    {
        "project_id": "RSUP",
        "name": "Customer",
        "nickname": "CUST",
        "order": 13,
        "note": "on-boarding calls with customers (typically the same info always) and general customer support",
        "enabled": True,
        "icon": "bi-telephone-forward",
    },
    {
        "project_id": "RSUP",
        "name": "Security",
        "nickname": "SEC",
        "order": 14,
        "note": "security support (questionnaires, etc.)",
        "enabled": True,
        "icon": "bi-shield-lock",
    },
    {
        "project_id": "RSUP",
        "name": "Engineering",
        "nickname": "ENG",
        "order": 17,
        "note": "",
        "enabled": True,
        "icon": "bi-code-slash",
    },
    {
        "project_id": "RSUP",
        "name": "Meetings",
        "nickname": "MTG",
        "order": 19,
        "note": "",
        "enabled": True,
        "icon": "bi-people",
    },
    {
        "project_id": "UKSE",
        "name": "Meetings",
        "nickname": "MTG",
        "order": 21,
        "note": "general bucket for meetings",
        "enabled": True,
        "icon": "bi-people",
    },
    {
        "project_id": "UKSE",
        "name": "Project Management",
        "nickname": "PRJ",
        "order": 21,
        "note": "",
        "enabled": True,
        "icon": "bi-diagram-3-fill",
    },
    {
        "project_id": "UKSE",
        "name": "Prod Issue Investigation",
        "nickname": "PINV",
        "order": 21,
        "note": "miscellaneous production issue investigation",
        "enabled": True,
        "icon": "bi-incognito",
    },
    {
        "project_id": "WDOS",
        "name": "Support",
        "nickname": "DOSU",
        "order": 30,
        "note": "",
        "enabled": True,
        "icon": "bi-person-raised-hand",
    },
    # TEMPLATE: {"project_id": "", "name": "", "nickname": "", "order": 99, "ticket": "", "complete": False, "note": "", "enabled": True, "icon": ""},
    # ::TEST DATA BELOW::
    # ~ {"project_id": "T33", "name": "Test 33", "nickname": "T33", "order": 99, "ticket": "tst-1234", "complete": False, "note": "A test task attached to Test Project 33", "enabled": True, "icon": "bi-arrow-through-heart-fill"},
]
