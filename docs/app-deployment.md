# App Deployment Guide

## Migrations
This project uses flask-migrate to handle db changes (e.g. migrations). See the "Migrations" section of the app-developer.md for developer related migration information (how to create).

The migration deployment strategy for this application is to manually run migrations after deployment. Autorun of migrations was investigated and is generally not recommended or desired with a containerized application as each container instance would attempt to run the migrations. Flask-migrate would protect against duplicate running of migrations as it tracks what has been ran to not run it again; however, the time/cost associated with this effort along with the lack of flexibility (e.g. determine exactly when a migration runs) led us to choose not to autorun migrations.

The basic steps of a deployment which contains migrations are as follow:
- deploy new version of application (version should be capable of running without migrations being applied)
- log into app container of new version of application
- run migration: `flask db upgrade`
  - to revert/rollback: `flask db downgrade`
