# DB Developer Guide

## Migrations

See the following docs for more info:

- development: app-developer.md
- deployment: app-deployment.md

## Restore DB Dump

- connect to db container: `dc exec db ash`
- navigate to dump you want to restore (e.g. /backups/db/manual/)
- load the dump: `xzcat file.dmp.xz | psql --host=localhost --port=5432 --username=postgres postgres`
- connect to db and verify: `psql --username=postgres postgres`

### From Prod DB

The quickest / easiest method is:

- get the prod dump and place it in `db/dumps/db/loads/`
- restore it as usual: `xzcat file.dmp.xz | psql -U postgres`
    + note: this will restore all dbs from prod
- delete all but the `radiant` db
- delete the `postgres` db
- rename `radiant` to `postgres`
