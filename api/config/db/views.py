# Application Database Configuration for: views
"""
This file contains all views for the application and is ran upon application
start (via check_databases() on fastapi 'startup' event).

Structure:
  - get current view contents with `{db}/_design/{design-name}` (e.g. `http $cdb/points/_design/pa`)
  - add an entry in the list below as:
    views.append({"database": "{db}", "views": [{ <contents from get response> }, {etc...}]})
    + NOTE: you do not need to remove the _id, _rev, etc as the _check_db_views method will handle it all.

Tips:
  - one view to a DesignDocument (this keeps it simple and more safe on updates)
  - DesignDocument name should be lower camel case (e.g. activityStream)
  - View name should be dash separated (e.g. activity-stream)

"""

views = []

#
# Create ONE instance per database which can have one or more design docs which
#   can have one or more views.
#


#
# Time Summary
#
views.append(
    {
        "database": "registrations",
        "views": [
            {
                "_id": "_design/TimeSummary",
                "_rev": "",
                "language": "javascript",
                "views": {
                    "range-time-summary": {
                        "map": """// range time summary
function (doc) {
  if (doc.start && doc.end && doc.user) {
    emit([doc.user, doc.start], doc.duration);
  }
}""",
                        "reduce": "_stats",
                    },
                },
            }
        ],
    }
)
