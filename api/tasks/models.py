#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#  SPDX-License-Identifier: GPL-3.0-only
#  Copyright 2025 dradux.com

import logging
from datetime import datetime
from typing import List
from projects.models import ProjectEnhanced

from config.models import Cdb
from pydantic import BaseModel

logger = logging.getLogger("default")
_db = Cdb.TASKS


class TaskBase(BaseModel):
    """
    Base document
    """

    project_id: str | None = None  # project the task belongs to
    name: str | None = None
    nickname: str | None = None
    order: int | None = None
    # ~ ticket: str | None = None
    # ~ complete: bool = False   # has the ticket been completed?
    note: str | None = None  # notes about item
    enabled: bool = True  # is the item enabled?
    icon: str = None


class TaskExt(TaskBase):
    """
    Extended (added by backend logic)
    """

    # note: this needs to be set/overwrote on result instantiation as using
    #  datetime.now() here will only get you now of when worker was started.
    created: datetime = datetime.now()
    updated: datetime = datetime.now()

    creator: str | None = None
    updator: str | None = None


class Task(TaskExt):
    """
    Actual (at DB level)
    """

    id_: str


class TaskResponse(BaseModel):
    """
    Task Response
    """

    total: int = 0
    tasks: List[Task] | None = None


class TaskListResponse(BaseModel):
    """
    List Response
    """

    id_: str
    nickname: str | None = None
    name: str | None = None
    client: str | None = None
    client_project: str | None = None
    order: int | None = None
    # ~ ticket: str | None = None
    note: str | None = None  # notes about item
    icon: str | None = None


class TaskEnhanced(TaskBase):
    """
    Task Enhanced.
    """

    # ~ task: TaskBase | None = None
    # ~ registration_type_full: RegistrationTypeBase | None = None

    project: ProjectEnhanced | None = None


class TaskListEnhanced(Task):
    """
    Task List enhanced.
    """

    client_project: str | None = None
    client: str | None = None
