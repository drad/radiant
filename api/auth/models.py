#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#  SPDX-License-Identifier: GPL-3.0-only
#  Copyright 2025 dradux.com

from enum import Enum
from typing import List, Optional

from config.config import API_PATH
from fastapi.security import OAuth2PasswordBearer
from pydantic import BaseModel

oauth2_scheme = OAuth2PasswordBearer(
    tokenUrl=f"{API_PATH}/auth/token",
    # Note: we are not using form scopes so the following are not needed.
    # scopes={"me": "Read information about the current user.", "items": "Read items."},
)


class Role(str, Enum):
    admin = "admin"
    member = "member"


class Token(BaseModel):
    access_token: str
    token_type: str


class TokenData(BaseModel):
    username: Optional[str] = None
    scopes: List[str] = []
