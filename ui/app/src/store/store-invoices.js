import Vue from 'vue'
import { api } from 'boot/axios'

const state = {
  invoice_loading: true,
  invoices_list_loading: true,
  invoices_list: [],
  active_invoice: {},
  invoice: {"id": 1234},
}

const mutations = {
  setInvoiceLoading(state, payload) {
    state.invoice_loading = payload
  },
  setInvoicesListLoading(state, payload) {
    state.invoices_list_loading = payload
  },
  addInvoicesList(state, payload) {
    state.invoices_list = payload
  },
  set_active_invoice(state, data) {
    state.active_invoice = data
  },
  setInvoice(state, data) {
    state.invoice = data
  }
}

const actions = {
  fetchInvoice({ commit }, payload) {
    commit('setInvoiceLoading', true)
    //~ console.log("store > invoice > fetch with id", payload.id)
    return new Promise((resolve, reject) => {
      api.get('/invoices/one/' + payload.id)
        .then((response) => {
            //~ console.log(' ⟶ fetch invoice return:', JSON.stringify(response.data))
            commit('setInvoice', response.data)
            commit('setInvoiceLoading', false)
            resolve(true)
        })
        .catch(() => {
            // note: this is hit when an unauthenticated user hits / which redirects them to /login
            //       no harm in the message so we just dump to console.
            console.log('No invoice found with id: ' + payload.id)
            commit('setInvoice', {})
            resolve(false)
        })

    })
  },
    fetchInvoicesList({ commit }, payload) {
        commit('setInvoicesListLoading', true)
        //~ console.log("store > invoices_list > fetch")
        api.get('/invoices/all', { params: {limit: 50, skip: 0}})
            .then((response) => {
                //~ console.log(' ⟶ fetch invoices list return:', JSON.stringify(response.data))
                commit('addInvoicesList', response.data)
            })
            .catch(() => {
                // note: this is hit when an unauthenticated user hits / which redirects them to /login
                //       no harm in the message so we just dump to console.
                console.log('No invoices list found.')
            })
        commit('setInvoicesListLoading', false)
    },
    addInvoice({ commit, dispatch }, data) {
        // console.log("--> AddInvoice:", data);
        return new Promise((resolve, reject) => {
            var querystring = require('querystring');
            // strip off nulls and cruft.
            let _data = Object.fromEntries(Object.entries(data).filter(([_, v]) => v != null));

            api.post('/invoices/?' + querystring.stringify(_data))
            .then((response) => {
                // no state to change as we fetch later to get all
                resolve(true)
            })
            .catch((error) => {
                reject(error)
            })
        })
    },
    updateInvoice({ commit, dispatch }, data) {
        //console.log("--> UpdateInvoice:", data);
        return new Promise((resolve, reject) => {
            var querystring = require('querystring');
            // strip off nulls.
            let _data = Object.fromEntries(Object.entries(data).filter(([_, v]) => v != null));
            // grab the id_ and move it to _id.
            _data._id = JSON.parse(JSON.stringify(data.id_));
            // strip off cruft.
            delete _data.id_;
            delete _data._rev;
            delete _data.created;
            delete _data.updated;
            delete _data.creator;
            delete _data.updator;
            delete _data.invoice_number;
            delete _data.status;
            delete _data.client_name;
            delete _data.client_nickname;
            delete _data.number_registrations;
            delete _data.amount;
            //console.log("    Cleaned Invoice:", _data);
            api.put('/invoices/?' + querystring.stringify(_data))
            .then((response) => {
                // console.log("store-invoices > updateInvoice (action) > response:", response);
                // no state to change as we fetch later to get all
                //~ resolve(true)
                resolve(response.data)
            })
            .catch((error) => {
                reject(error)
            })
        })
    },
    deleteInvoice({ commit }, id) {
        //console.log("store-invoice > action > deleteInvoice with id: ", id)
        var querystring = require('querystring');
        return new Promise((resolve, reject) => {
            api.delete('/invoices/?' + querystring.stringify({ _id: id }))
                .then((response) => {
                    //~ console.log('  - delete registration returned: ', JSON.stringify(response.data))
                    // no state to change as we fetch later to get all
                    resolve(true)
                })
            .catch((error) => {
                // note: this is hit when an unauthenticated user hits / which redirects them to /login
                //       no harm in the message so we just dump to console.
                //~ console.log('No tasks found.')
                console.log("Error in deleting invoice:", error)
                reject(error)
            })
        })
    },
    setActiveInvoice({ commit }, data) {
      commit('set_active_invoice', data)
    },
}

const getters = {
  invoice_loading: (state) => {
    return state.invoice_loading
  },
  invoices_list_loading: (state) => {
    return state.invoices_list_loading
  },
  invoices_list: (state) => {
    return state.invoices_list
  },
  active_invoice: (state) => {
    return state.active_invoice
  },
  invoice: (state) => {
    return state.invoice
  },
}

export default {
    namespaced: true,

    state,
    mutations,
    actions,
    getters
}
