# Ticketing models.

import datetime
import logging

from .core import db, Project

logger = logging.getLogger("main_logger")

# attachment to ticketings cross-ref
attachment_ticketings = db.Table(
    "attachment_ticketing",
    db.Column("ticketing_id", db.Integer(), db.ForeignKey("ticketing.id")),
    db.Column("attachment_id", db.Integer(), db.ForeignKey("attachment.id")),
)

# comments to ticketing cross-ref
comment_ticketings = db.Table(
    "comment_ticketing",
    db.Column("ticketing_id", db.Integer(), db.ForeignKey("ticketing.id")),
    db.Column("comment_id", db.Integer(), db.ForeignKey("comment.id")),
)

# tag to ticketing cross-ref
tag_ticketings = db.Table(
    "tag_ticketing",
    db.Column("ticketing_id", db.Integer(), db.ForeignKey("ticketing.id")),
    db.Column("tag_id", db.Integer(), db.ForeignKey("tag.id")),
)


class TicketingStatus(db.Model):
    __tablename__ = "ticketing_status"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(13), nullable=False)
    desc = db.Column(db.String(255), nullable=False)

    def __str__(self):
        return f"{self.name}"


class TicketingPriority(db.Model):
    __tablename__ = "ticketing_priority"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(13), nullable=False)
    desc = db.Column(db.String(255), nullable=False)

    def __str__(self):
        return f"{self.name}"


class Ticketing(db.Model):
    __tablename__ = "ticketing"
    id = db.Column(db.Integer, primary_key=True)
    tid = db.Column(db.Integer, nullable=False, default=0)

    title = db.Column(db.String(100), nullable=False)
    content = db.Column(db.Text, nullable=True)

    created_by = db.Column(db.Integer, db.ForeignKey("user.id"), nullable=False)
    creator = db.relationship("User", foreign_keys=[created_by])
    created_at = db.Column(
        db.DateTime(), default=datetime.datetime.utcnow, nullable=False
    )
    updated_by = db.Column(db.Integer, db.ForeignKey("user.id"), nullable=False)
    updator = db.relationship("User", foreign_keys=[updated_by])
    updated_at = db.Column(
        db.DateTime(), default=datetime.datetime.utcnow, nullable=False
    )
    assigned_to = db.Column(db.Integer, db.ForeignKey("user.id"), nullable=False)
    assignor = db.relationship("User", foreign_keys=[assigned_to])
    assigned_at = db.Column(
        db.DateTime(), default=datetime.datetime.utcnow, nullable=False
    )

    contact = db.Column(db.String(100), nullable=True)

    status_id = db.Column(
        db.Integer, db.ForeignKey("ticketing_status.id"), nullable=False
    )
    status = db.relationship(TicketingStatus, foreign_keys="Ticketing.status_id")

    priority_id = db.Column(
        db.Integer, db.ForeignKey("ticketing_priority.id"), nullable=False
    )
    priority = db.relationship(TicketingPriority, foreign_keys="Ticketing.priority_id")

    project_id = db.Column(db.Integer, db.ForeignKey("project.id"), nullable=False)
    project = db.relationship(Project, foreign_keys="Ticketing.project_id")

    # tags (multiple occurring)
    tags = db.relationship(
        "Tag",
        secondary=tag_ticketings,
        backref=db.backref("ticketing", lazy="dynamic"),
    )

    # comments (multiple occurring)
    comments = db.relationship(
        "Comment",
        secondary=comment_ticketings,
        backref=db.backref("ticketing", lazy="dynamic"),
    )

    # attachments (multiple occurring)
    attachments = db.relationship(
        "Attachment",
        secondary=attachment_ticketings,
        backref=db.backref("ticketing", lazy="dynamic"),
    )

    def __str__(self):
        return f"{self.tid} - {self.title} - {self.status} - {self.priority}"
