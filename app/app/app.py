import logging
import os
import os.path as op
import secrets
import string
import uuid
from datetime import datetime

import arrow

# need both of the following
import config.config as Config
import markdown
from config.config import BaseConfig
from flask import Flask, flash, jsonify, redirect, render_template, send_file, url_for
from flask_admin import Admin
from flask_ldap3_login import LDAP3LoginManager
from flask_ldap3_login.forms import LDAPLoginForm
from flask_login import LoginManager, login_user
from flask_migrate import Migrate
from flask_security import (
    Security,
    SQLAlchemyUserDatastore,
    login_required,
    logout_user,
)
from ldap3 import (  # NOTE: ldap3 used for secondary (readonly) bind to get user's groups.
    ALL,
    Connection,
)
from ldap3 import Server as LDAPServer
from markupsafe import Markup
from models.core import (  # Attachment,
    Client,
    InvoiceFormat,
    Project,
    Role,
    Tag,
    Task,
    User,
    db,
)
from models.filesystem_benchmarks import (
    FilesystemBenchmark,
    FilesystemBenchmarkDiskType,
    FilesystemBenchmarkFsKind,
    FilesystemBenchmarkFsType,
    FilesystemBenchmarkRunLocationType,
    FilesystemBenchmarkRunMethod,
)
from models.invc_reg import Invoice, InvoiceStatus, Registration, RegistrationTicketType
from models.inventory import (
    Asset,
    AssetPhysicalLocation,
    AssetStatusStatus,
    AssetType,
    AttainType,
    EnergyMeasureState,
    Incident,
    IncidentStatus,
    LinkType,
    MeasureType,
    Setup,
    SetupStatus,
    SpecType,
)
from models.ticketing import Ticketing, TicketingPriority, TicketingStatus
from models.tracker import (
    RunMetric,
    Server,
    ServerType,
    StorageType,
    TestPlan,
    TestResult,
    TestResultStatus,
)
from utils import PrefixMiddleware
from views._utils import seconds_to_minutes
from views.core import (
    AboutView,
    AccountView,
    ClientView,
    HomeView,
    LoginMenuLink,
    LogoutMenuLink,
    OnlineHelpView,
    ProjectView,
    RoleAdmin,
    StatusView,
    TagView,
    TaskView,
    UserAdmin,
    UserAdminUser,
)
from views.filesystem_benchmarks import (
    FilesystemBenchmarkDiskTypeView,
    FilesystemBenchmarkFsKindView,
    FilesystemBenchmarkFsTypeView,
    FilesystemBenchmarkRunLocationTypeView,
    FilesystemBenchmarkRunMethodView,
    FilesystemBenchmarkView,
)
from views.invc_reg import (
    HourSummaryView,
    InvoiceFormatView,
    InvoiceRegistrationHelpView,
    InvoiceStatusView,
    InvoiceView,
    RegistrationTicketTypeView,
    RegistrationView,
)
from views.inventory import (
    AssetPhysicalLocationView,
    AssetStatusStatusView,
    AssetTypeView,
    AssetView,
    AttainTypeView,
    EnergyMeasureStateView,
    IncidentStatusView,
    IncidentView,
    InventoryHelpView,
    LinkTypeView,
    MeasureTypeView,
    SetupStatusView,
    SetupView,
    SpecTypeView,
)
from views.ticketing import (
    TicketingHelpView,
    TicketingPriorityView,
    TicketingStatusView,
    TicketingView,
)
from views.tracker import (
    RunMetricView,
    ServerTypeView,
    ServerView,
    StorageTypeView,
    TestPlanView,
    TestResultStatusView,
    TestResultView,
    TrackerHelpView,
)

# from seeder import Seeder

auth_server = LDAPServer(
    host=Config.DefaultConfig.LDAP_HOST,
    port=Config.DefaultConfig.LDAP_PORT,
    use_ssl=Config.DefaultConfig.LDAP_USE_SSL,
    get_info=ALL,
)


# path = op.join(op.dirname(__file__), 'static')
# check/create attachments directory.
try:
    os.mkdir(Config.DefaultConfig.ATTACHMENTS_DIR)
except OSError:
    pass

# Create application
app = Flask(__name__, static_url_path="/admin")
app.config.from_object(BaseConfig)
if Config.DefaultConfig.DEBUG:
    from flask_debugtoolbar import DebugToolbarExtension

    toolbar = DebugToolbarExtension(app)

db.init_app(app)
migrate = Migrate()
migrate.init_app(app, db)

service_id = str(uuid.uuid4())[:8]
logger_base = logging.getLogger("main_logger")
logger_base.setLevel(logging.getLevelName(Config.DefaultConfig.APP_LOGLEVEL))
console_handler = logging.StreamHandler()
if "console" in Config.DefaultConfig.LOG_TO:
    logger_base.addHandler(console_handler)

logger = logging.LoggerAdapter(
    logging.getLogger("main_logger"),
    {
        "application_name": Config.DefaultConfig.CORE["name"],
        "application_env": Config.DefaultConfig.DEPLOY_ENV,
    },
)

logger.info(
    f"{Config.DefaultConfig.CORE['name']} - v.{Config.DefaultConfig.CORE['version']} ({Config.DefaultConfig.CORE['modified']}) - {service_id}"
)
logger.info(
    f"""App Config Summary
  - app debug:    {Config.DefaultConfig.DEBUG}
  - log level:    {Config.DefaultConfig.APP_LOGLEVEL}
  - logging to:   {", ".join(str(i) for i in Config.DefaultConfig.LOG_TO)}
  - deploy env:   {Config.DefaultConfig.DEPLOY_ENV}
  - auto-tmpl:    {Config.DefaultConfig.TEMPLATES_AUTO_RELOAD}
  - db service:   {Config.DefaultConfig.DB_SERVICE}:{Config.DefaultConfig.DB_PORT}/{Config.DefaultConfig.DB_NAME} (user={Config.DefaultConfig.DB_USER}, pwd={Config.DefaultConfig.DB_PASS[0:5]}...)
  - app base url: {Config.DefaultConfig.APP_BASE_URL}
  - ldap host:    {Config.DefaultConfig.LDAP_HOST}
"""
)

login_manager = LoginManager(app)  # Setup a Flask-Login Manager
ldap_manager = LDAP3LoginManager(app)  # Setup a LDAP3 Login Manager.

# Create a dictionary to store the users in when they authenticate
# This example stores users in memory.
users = {}


# Jinja2 filters
def format_datetime(value, format=""):
    if format == "arrow-humanize":
        return arrow.get(value).to("local").humanize()
    elif format == "date-only":
        return arrow.get(value).to("local").format("YYYY-MM-DD")
    else:
        return arrow.get(value).to("local").format("YYYY-MM-DD HH:mm")


def format_registration_length(length):
    if length:
        return seconds_to_minutes(length)
    else:
        return None


def format_registration_cost(item, formatted=False):
    if item and item.length and item.task.project.rate:
        cost = round((item.length / 3600) * float(item.task.project.rate), 2)
        if formatted:
            return f"${cost:0,.2f}"
        else:
            return cost
    else:
        return None


def format_currency(value):
    if value:
        return f"${value:,.2f}"


def to_float(value):
    if value:
        return float(value)
    else:
        return float(0)


def format_tid(tid):
    if tid:
        return f"{tid:04d}"
    else:
        return None


def format_markdown(content):
    if content:
        return Markup(markdown.markdown(content, extensions=["fenced_code"]))
    else:
        return None


app.jinja_env.filters["registration_length"] = format_registration_length
app.jinja_env.filters["registration_cost"] = format_registration_cost
app.jinja_env.filters["datetime"] = format_datetime
app.jinja_env.filters["currency"] = format_currency
app.jinja_env.filters["float"] = to_float
app.jinja_env.filters["format_tid"] = format_tid
app.jinja_env.filters["markdown"] = format_markdown


@login_manager.user_loader
def load_user(user_id):
    """
    Load User for Flask-Login: simply returns the User if it exists in our 'database',
    otherwise returns None.
    """

    logger.debug(f"load_user by id={user_id}")
    # if id in users:
    #    return users[id]
    # return None
    return User.get(user_id)


@ldap_manager.save_user
def save_user(dn, username, data, memberships):
    """
    Save User for Flask-Ldap3-Login: this method is called whenever
    a LDAPLoginForm() successfully validates. Here you have to save the
    user, and return it so it can be used in the login controller.
    """
    logger.debug(f"saving user as: dn={dn}, username={username}, data={data}, memberships={memberships}")
    # connect to ldap as 'readonly' user to get user's groups.
    # ~ server = LDAPServer(
    # ~ Config.DefaultConfig.LDAP_HOST,
    # ~ use_ssl=Config.DefaultConfig.LDAP_USE_SSL,
    # ~ get_info=ALL,
    # ~ )
    logger.debug(f"- creating connection with server={auth_server}")
    conn = Connection(
        auth_server,
        Config.DefaultConfig.LDAP_BIND_USER_DN,
        Config.DefaultConfig.LDAP_BIND_USER_PASSWORD,
        auto_bind=True,
    )
    logger.debug(f"- created connection={conn}")
    # get all of the groups the user is a member of
    ldap_groups = []
    search_filter = Config.DefaultConfig.LDAP_USER_ROLE_SEARCH_FILTER.format(
        username=username,
        LDAP_BASE_DN=Config.DefaultConfig.LDAP_BASE_DN,
        LDAP_APP_GROUP_PREFIX=Config.DefaultConfig.LDAP_APP_GROUP_PREFIX,
    )
    # logger.debug(
    #    f"- roles lookup base={Config.DefaultConfig.LDAP_USER_ROLE_SEARCH_BASE} - filter={search_filter} - attributes={Config.DefaultConfig.LDAP_USER_ROLE_ATTRIBUTES}"
    # )
    conn.search(
        search_base=Config.DefaultConfig.LDAP_USER_ROLE_SEARCH_BASE,
        search_filter=search_filter,
        attributes=[Config.DefaultConfig.LDAP_USER_ROLE_ATTRIBUTES],
    )
    # ~ logger.debug(f"- conn.entries={conn.entries}")
    for entry in conn.entries:
        # ~ logger.debug(f"  - entry={entry}")
        for roles in entry:
            # ~ logger.debug(f"    - roles={roles}")
            for role in roles:
                if Config.DefaultConfig.LDAP_USER_ROLE_BATCH:
                    logger.debug(f"  - role={role}")
                    if role.startswith(f"ou={Config.DefaultConfig.LDAP_APP_GROUP_PREFIX}"):
                        logger.debug("    - keeping role!")
                        # we need role as admins, users, etc. not trax_admins, trax_users, etc.
                        role_ou = role.split(",")[0]
                        role_name = role_ou.split("=")[1]
                        role_cleaned = role_name.replace(Config.DefaultConfig.LDAP_APP_GROUP_PREFIX, "")
                        ldap_groups.append(role_cleaned)
                else:
                    # this is the old 'ldap' structure.
                    role_cleaned = str(role).replace(Config.DefaultConfig.LDAP_APP_GROUP_PREFIX, "")
                    ldap_groups.append(role_cleaned)

    # dont allow users with no roles.
    if not ldap_groups:
        logger.error(f"No Roles found for user: {username}")
        return None

    # setup user.
    # name = f"{data['givenName'][0]} {data['sn'][0]}"
    name = username
    username = username
    # ~ nickname = data["displayName"]
    nickname = username
    roles = []

    # logger.debug("user: name={0}, nickname={1}".format(name,nickname))
    # roles/groups.
    for r in ldap_groups:
        role = str(r)
        logger.debug(f"- user is member of: [{role}]")
        # roles.append(role)
        # check if roles exist, if not add them
        app_role = Role.query.filter_by(name=role).first()
        if not app_role:
            logger.info(f"- adding new role: {role}")
            app_role = Role(name=role, description="added from ldap login")
        logger.debug(f"- adding app_role: {app_role}")
        roles.append(app_role)

    # check if user exists
    user = User.query.filter_by(username=username).first()
    if user:
        # existing user - update
        for role in user.roles:
            logger.debug(f"app user role: {role.name}")
        user.name = name
        # user["username"] = username   # we dont update this
        user.nickname = nickname
        user.roles = roles
    else:
        # new user - create
        char_classes = (string.ascii_letters, string.digits, string.punctuation)
        password_length = 33
        char = lambda: secrets.choice(secrets.choice(char_classes))  # NOQA
        pw = lambda: "".join([char() for _ in range(password_length)])  # NOQA
        user = User(name=name, username=username, nickname=nickname, password=pw(), roles=roles)
    db.session.add(user)
    db.session.commit()

    logger.debug(f"user is: {user}")
    users[username] = user
    return user


# Setup Flask-Security
user_datastore = SQLAlchemyUserDatastore(db, User, Role)
security = Security(app, user_datastore)


@app.route("/", methods=["GET"])
@login_required
def index():
    return render_template("index.html")


@app.route("/status", methods=["GET"])
def status():
    """status used by external tools to get app status"""
    return (
        jsonify({"status": "OK", "date": datetime.now().strftime("%Y-%m-%d %H:%M:%S")}),
        200,
    )


@app.route("/version", methods=["GET"])
def version():
    """show version"""
    return (
        # f"{Config.DefaultConfig.CORE['name']} - v.{Config.DefaultConfig.CORE['version']} ({Config.DefaultConfig.CORE['modified']}) - {service_id}"
        jsonify(
            {
                "status": "ok",
                "version": Config.DefaultConfig.CORE["version"],
                "modified": Config.DefaultConfig.CORE["modified"],
            }
        ),
        200,
    )


@app.route("/admin/get_file/<url>", methods=["GET"])
@login_required
def get_file(url):
    file_path = op.abspath(Config.DefaultConfig.ATTACHMENTS_DIR)
    return send_file(f"{file_path}/{url}", attachment_filename=f"{url}")


@app.route("/admin/logout")
@login_required
def logout_view():
    logout_user()
    return redirect(url_for("admin.index", next=url_for("admin.index")))


@app.route("/admin/login")
def login_view():
    return redirect(url_for("security.login", next=url_for("admin.index")))


@app.route("/admin/login_ldap", methods=["GET", "POST"])
def ldap_login_view():
    # Instantiate a LDAPLoginForm which has a validator to check if the user
    logger.debug("Performing initial ldap login...")
    # exists in LDAP.
    form = LDAPLoginForm()
    if form.validate_on_submit():
        # logger.debug("- had a submit...")
        # Successfully logged in, We can now access the saved user object via form.user.
        # check if user is set (note: it wont be if user has no roles).
        # logger.debug(
        #    f"You are logging in as user=[{form.user}] pwd=[{form.password.data[0:5]}]"
        # )
        # put_user_session(user_session={'username': form.user, 'password': form.password.data})
        if form.user:
            login_user(form.user)  # Tell flask-login to log them in.
            return redirect(url_for("admin.index", next=url_for("admin.index")))
        else:
            flash("User has no Roles")
            return redirect(url_for("login_ldap.index", next=url_for("admin.index")))
    else:
        logger.debug("- not a submit...")

    return render_template(
        "security/login_ldap.html",
        about=Config.DefaultConfig.CORE,
        form=form,
        ldap_url="{0}:{1}".format(Config.DefaultConfig.LDAP_HOST, str(Config.DefaultConfig.LDAP_PORT)),
    )


# Create admin
admin = Admin(app, name="radiant", base_template="layout.html", index_view=HomeView(name="Home"))
# to change to bootstrap3 add template_mode="bootstrap3"


# =================================
# Menu Items
# =================================

# Time ------------------------------------------------------------------------
admin.add_view(
    RegistrationView(
        Registration,
        db.session,
        name="Registrations",
        endpoint="registrations",
        category="Time",
    )
)
admin.add_view(InvoiceView(Invoice, db.session, name="Invoices", endpoint="invoices", category="Time"))
# Time > Config
# note: the space "Config " matters
admin.add_sub_category(name="Config ", parent_name="Time")
admin.add_view(
    InvoiceFormatView(
        InvoiceFormat,
        db.session,
        name="Invoice Formats",
        endpoint="invoice_format",
        category="Config ",
    )
)
admin.add_view(
    InvoiceStatusView(
        InvoiceStatus,
        db.session,
        name="Invoice Statuses",
        endpoint="invoice_status",
        category="Config ",
    )
)
admin.add_view(
    RegistrationTicketTypeView(
        RegistrationTicketType,
        db.session,
        name="Reg Ticket Type",
        endpoint="registration_ticket_type",
        category="Config ",
    )
)
# Time > Reports
admin.add_sub_category(name="Reports", parent_name="Time")
admin.add_view(HourSummaryView(name="Hours Summary", endpoint="hours_summary", category="Reports"))
# Ticketing -------------------------------------------------------------------
admin.add_view(
    TicketingView(
        Ticketing,
        db.session,
        name="Tickets",
        endpoint="tickets",
        category="Ticketing",
    )
)
# Config/Ticketing
# note: the space "Config  " matter.
admin.add_sub_category(name="Config  ", parent_name="Ticketing")
admin.add_view(
    TicketingPriorityView(
        TicketingPriority,
        db.session,
        name="Ticket Priorities",
        endpoint="ticket_priority",
        category="Config  ",
    )
)
admin.add_view(
    TicketingStatusView(
        TicketingStatus,
        db.session,
        name="Ticket Statuses",
        endpoint="ticket_status",
        category="Config  ",
    )
)
# Inventory -------------------------------------------------------------------
admin.add_view(
    AssetView(
        Asset,
        db.session,
        name="Assets",
        endpoint="assets",
        category="Inventory",
    )
)
admin.add_view(
    SetupView(
        Setup,
        db.session,
        name="Setups",
        endpoint="setups",
        category="Inventory",
    )
)
admin.add_view(
    IncidentView(
        Incident,
        db.session,
        name="Incidents",
        endpoint="incidents",
        category="Inventory",
    )
)
# Inventory/Config
# note: the space "Config   " matter.
admin.add_sub_category(name="Config   ", parent_name="Inventory")
admin.add_view(
    AssetPhysicalLocationView(
        AssetPhysicalLocation,
        db.session,
        name="Asset Physical Locations",
        endpoint="asset_physical_location",
        category="Config   ",
    )
)
admin.add_view(
    AssetStatusStatusView(
        AssetStatusStatus,
        db.session,
        name="Asset Statuses",
        endpoint="asset_statuses",
        category="Config   ",
    )
)
admin.add_view(
    AssetTypeView(
        AssetType,
        db.session,
        name="Asset Types",
        endpoint="asset_types",
        category="Config   ",
    )
)
admin.add_view(
    AttainTypeView(
        AttainType,
        db.session,
        name="Asset Attain Types",
        endpoint="asset_attain_types",
        category="Config   ",
    )
)
admin.add_view(
    EnergyMeasureStateView(
        EnergyMeasureState,
        db.session,
        name="Energy Measure States",
        endpoint="asset_energy_measure_states",
        category="Config   ",
    )
)
admin.add_view(
    MeasureTypeView(
        MeasureType,
        db.session,
        name="Energy Measure Types",
        endpoint="asset_measure_types",
        category="Config   ",
    )
)
admin.add_view(
    IncidentStatusView(
        IncidentStatus,
        db.session,
        name="Incident Statuses",
        endpoint="incident_statuses",
        category="Config   ",
    )
)
admin.add_view(
    LinkTypeView(
        LinkType,
        db.session,
        name="Link Types",
        endpoint="link_types",
        category="Config   ",
    )
)
admin.add_view(
    SpecTypeView(
        SpecType,
        db.session,
        name="Spec Types",
        endpoint="spec_types",
        category="Config   ",
    )
)
admin.add_view(
    SetupStatusView(
        SetupStatus,
        db.session,
        name="Setup Statuses",
        endpoint="setup_statuses",
        category="Config   ",
    )
)
# Benchmarks ---------------------------------------------------------------------
admin.add_view(
    TestResultView(
        TestResult,
        db.session,
        name="Web Apps (WA)",
        endpoint="webapp_benchmarks",
        category="Benchmark",
    )
)
admin.add_view(
    FilesystemBenchmarkView(
        FilesystemBenchmark,
        db.session,
        name="Filesystems (FS)",
        endpoint="filesytem_benchmarks",
        category="Benchmark",
    )
)
# Benchmark>Config
# note: the space "Config    " matter.
admin.add_sub_category(name="Config    ", parent_name="Benchmark")
admin.add_view(
    FilesystemBenchmarkDiskTypeView(
        FilesystemBenchmarkDiskType,
        db.session,
        name="FS Disk Types",
        category="Config    ",
    )
)
admin.add_view(
    FilesystemBenchmarkFsKindView(
        FilesystemBenchmarkFsKind,
        db.session,
        name="FS Filesystem Kinds",
        category="Config    ",
    )
)
admin.add_view(
    FilesystemBenchmarkFsTypeView(
        FilesystemBenchmarkFsType,
        db.session,
        name="FS Filesystem Types",
        category="Config    ",
    )
)
admin.add_view(
    FilesystemBenchmarkRunLocationTypeView(
        FilesystemBenchmarkRunLocationType,
        db.session,
        name="FS Run Location Types",
        category="Config    ",
    )
)
admin.add_view(
    FilesystemBenchmarkRunMethodView(
        FilesystemBenchmarkRunMethod,
        db.session,
        name="FS Run Methods",
        category="Config    ",
    )
)
admin.add_view(TestResultStatusView(TestResultStatus, db.session, name="WA Result Statuses", category="Config    "))
admin.add_view(RunMetricView(RunMetric, db.session, name="WA Run Metrics", category="Config    "))
admin.add_view(ServerView(Server, db.session, name="WA Servers", category="Config    "))
admin.add_view(ServerTypeView(ServerType, db.session, name="WA Server Types", category="Config    "))
admin.add_view(StorageTypeView(StorageType, db.session, name="WA Storage Types", category="Config    "))
admin.add_view(TestPlanView(TestPlan, db.session, name="WA Test Plans", category="Config    "))
# Global ----------------------------------------------------------------------
admin.add_view(AccountView(User, db.session, name="Account", endpoint="account_user", category="Global"))
admin.add_view(ClientView(Client, db.session, name="Clients", endpoint="clients", category="Global"))
admin.add_view(ProjectView(Project, db.session, name="Projects", endpoint="projects", category="Global"))
admin.add_view(RoleAdmin(Role, db.session, name="Roles", endpoint="role", category="Global"))
admin.add_view(TaskView(Task, db.session, name="Tasks", endpoint="tasks", category="Global"))
admin.add_view(UserAdmin(User, db.session, name="Users", endpoint="user", category="Global"))
admin.add_view(UserAdminUser(User, db.session, name="User User", endpoint="user_user", category="Global"))
admin.add_view(TagView(Tag, db.session, name="Tags", category="Global"))
# Global > Reports
# note: the space "Reports " matter.
admin.add_sub_category(name="Reports ", parent_name="Global")
admin.add_view(StatusView(name="Status", endpoint="status", category="Reports "))

# @TODO: we cannot use 'url_for()' here as it generates a 'RuntimeError: Attempted to generate a URL without the application context being pushed. This has to be executed when application context is available.', need to find another way.
admin.add_link(LogoutMenuLink(name="Logout", category="", endpoint="logout_view"))
admin.add_link(LoginMenuLink(name="Login", category="", endpoint="ldap_login_view"))

# Help ------------------------------------------------------------------------
admin.add_view(OnlineHelpView(name="General", endpoint="help_general", category="Help"))
admin.add_sub_category(name="Online", parent_name="Help")
admin.add_view(InventoryHelpView(name="Inventory", endpoint="help_inventory", category="Online"))
admin.add_view(InvoiceRegistrationHelpView(name="Time", endpoint="help_time", category="Online"))
admin.add_view(TicketingHelpView(name="Ticketing", endpoint="help_ticketing", category="Online"))
admin.add_view(TrackerHelpView(name="Benchmark", endpoint="help_benchmarks", category="Online"))
admin.add_view(AboutView(name="About", endpoint="help_about", category="Help"))

app.wsgi_app = PrefixMiddleware(app.wsgi_app, prefix=Config.DefaultConfig.APP_BASE_URL)

# ~ @app.cli.command()
# ~ def seeder():
# ~ """run the db seeder"""
# ~ Seeder.seed_all()


if __name__ == "__main__":
    app_dir = op.realpath(os.path.dirname(__file__))
    app.run()
