#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#  SPDX-License-Identifier: GPL-3.0-only
#  Copyright 2025 dradux.com

import logging
import aiocouch
from tasks.utils import get_by_id as tasks_get_by_id
from projects.utils import get_by_id as projects_get_by_id
from invoices.models import _db, InvoiceStatus
from config.config import get_cdb

logger = logging.getLogger("default")


async def get_by_id(_id: str):
    """
    Get an invoice by id
    """

    couchdb = get_cdb()
    logger.debug(f"Getting invoice by id: {_id}")
    db = await couchdb[_db.value]

    try:
        return await db[_id]
    except aiocouch.exception.NotFoundError:
        return None


async def calculate_status(_doc=None):
    """
    Calculate the invoice Status.

    Status of the invoice is determined as follows:
    - if no submitted/due, paid or deposited date, status is CREATED
    - if submitted/due but no paid or deposited, status is SUBMITTED
    - if submitted/due and paid but no deposited status is PAID
    - if submitted/due and paid and deposited status is DEPOSITED
    - otherwise status is UNKNOWN
    """

    if _doc["submitted"] and _doc["due"] and _doc["paid"] and _doc["deposited"]:
        return InvoiceStatus.deposited
    elif _doc["submitted"] and _doc["due"] and _doc["paid"]:
        return InvoiceStatus.paid
    elif _doc["submitted"] and _doc["due"] or _doc["submitted"]:
        return InvoiceStatus.submitted
    elif not _doc["submitted"] and not _doc["due"] and not _doc["paid"] and not _doc["deposited"]:
        return InvoiceStatus.created
    else:
        return InvoiceStatus.unknown


async def get_registration_amount(_registrations=None):
    """
    Get registrations amount: loops through a set of registrations to calculate
    the amount by multiplying the duration (seconds) by the bill_rate.
    """

    _amount = 0

    _duration = [x["duration"] for x in _registrations if "duration" in x]
    # ~ logger.debug(f"Duration list is: {_duration}")
    # ~ logger.debug(f" ⟶ Billable time={math.fsum(_duration)}")

    # We need to get the duration and project.bill_rate so we can determine how
    # much the time is worth for each registration.
    for x in _registrations:
        _task = await tasks_get_by_id(x["task_id"])
        _project = await projects_get_by_id(_task["project_id"])
        # ~ _bill_rate = await get_bill_rate(x['task_id'])
        logger.debug(f"- duration: {x['duration']} and bill_rate: {_project['bill_rate']}")
        if "bill_rate" in _project and _project["bill_rate"] > 0:
            _amount += x["duration"] * (
                _project["bill_rate"] / 3600
            )  # note: we take bill_rate/3600 to get the per second bill rate.

    return round(_amount, 2)
