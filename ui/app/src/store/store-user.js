import { api } from 'boot/axios'
import { LocalStorage } from 'quasar'

const state = {
    user: {},
    user_points: {
        created: null,   //must initially be null.
        approved: null,   //must initially be null.
        denied: null,   //must initially be null.
    },
    users: [
      {
        "username": "addalie",
        "display_name": "mein",
        "first_name": "Addalie",
        "last_name": "Rader",
        "avatar": "https://avataaars.io/?avatarStyle=Circle&topType=LongHairBigHair&accessoriesType=Blank&hairColor=BrownDark&facialHairType=Blank&clotheType=Hoodie&clotheColor=Pink&eyeType=Side&eyebrowType=UpDown&mouthType=Smile&skinColor=Light"
      },
      {
        "username": "drad",
        "display_name": "drad",
        "first_name": "David",
        "last_name": "Rader",
        "avatar": "https://avataaars.io/?avatarStyle=Transparent&topType=ShortHairDreads01&accessoriesType=Sunglasses&hairColor=Black&facialHairType=BeardLight&facialHairColor=Black&clotheType=GraphicShirt&clotheColor=Blue03&graphicType=Resist&eyeType=Surprised&eyebrowType=UnibrowNatural&mouthType=Serious&skinColor=Light"
      },
      {
        "username": "kailie",
        "display_name": "kai",
        "first_name": "Kailie",
        "last_name": "Rader",
        "avatar": "https://avataaars.io/?avatarStyle=Circle&topType=LongHairBigHair&accessoriesType=Blank&hairColor=BrownDark&facialHairType=Blank&clotheType=Hoodie&clotheColor=Pink&eyeType=Side&eyebrowType=UpDown&mouthType=Smile&skinColor=Light"
      },
      {
        "username": "natalie",
        "display_name": "chunk",
        "first_name": "Natalie",
        "last_name": "Rader",
        "avatar": "https://avataaars.io/?avatarStyle=Circle&topType=LongHairStraight2&accessoriesType=Blank&hairColor=BrownDark&facialHairType=Blank&clotheType=BlazerShirt&eyeType=Default&eyebrowType=Default&mouthType=Eating&skinColor=Light"
      }
    ]
}

/*
 * Mutations are for changes to the state, api calls should be in actions and use mutation to reflect api info.
 * NOTICE: no async in mutations
 */
const mutations = {
    setUser(state, payload) {
        //~ console.log("- user > mutations > setUser:", payload);
        // Object.assign copies props from one object to another.
        Object.assign(state.user, payload)
        //  or you can set manually like:
        //~ state.user.username = payload.username

        // @TODO: currently we update active account/role manually, it should be user selected just get first for now
        //~ state.user.active_account_name = payload.accounts[0].id_
        //~ state.user.active_account_role = payload.accounts[0].role
    },
    setUserPoints(state, payload) {
        state.user_points = payload;
        //~ state.user_points.created: response.data.created
        //~ state.user_points.approved: response.data.approved
        //~ state.user_points.denied: response.data.denied
    },
    updateUser(state, payload) {
        // NOTICE: preferences are in the body, everything else is in QS

        // merge new prefs over old as the user update needs all prefs
        let prefs = Object.assign(state.user.preferences, payload.preferences)

        // set params
        var querystring = require('querystring');
        let params = payload;
        delete params.preferences;

        //~ console.log("  ⟶ prefs: ["+prefs.process_activity_notes+"]", prefs);
        //~ console.log("  ⟶ params:", params);

        //call api to update.
        api.put('/users/?' + querystring.stringify(params) + "&enabled=true", {"preferences": prefs })
        .then((response) => {
            //~ console.log("user update response: ", response)
        })
        .catch((error) => {
            console.log("Error updating user: ", error)
        })

        //merge params over user.
        let new_user = Object.assign(state.user, params)
        state.user.preferences = prefs;

    },
    resetUser(state, payload) {
        //~ console.log("store-user>mutations>resetUser: ", payload)
        state.user = {}
        state.user_points = {
            created: null,
            approved: null,
            denied: null,
        }
    }
}

/*
 * Actions are for api work, use mutations to reflect changes in state.
 */
const actions = {
    fetchUser({ commit }, payload) {
        return new Promise((resolve, reject) => {
            api.get('users/me')
              .then((response) => {
                  commit('setUser', response.data)
                  resolve(true)
              })
              .catch((error) => {
                  //~ console.log("Error in adding Activity:", error)
                  reject(error)
              })
        })
    },
    fetchUserPoints({ commit }, payload) {
        return new Promise((resolve, reject) => {
            api.get('points/totals/', { params: {account: state.user.active_account_name, username: state.user.username}})
              .then((response) => {
                  commit('setUserPoints', response.data)
                  resolve(true)
              })
              .catch((error) => {
                  //~ console.log("Error in adding Activity:", error)
                  reject(error)
              })
        })
    },
    updateUser({ commit }, payload) {
        //~ console.log("user > action> updateUser: ", payload)
        commit('updateUser', payload)
    },
    isAdmin() {
        //~ console.log("- store-user>actions>isAdmin: active_account_role=", state.user.active_account_role)
        if ( state.user.active_account_role == 'admin') {
            return true
        } else {
            return false
        }
    },
    resetUser({ commit }, payload) {
        commit('resetUser', payload)
        // delete the ui access-token.
        LocalStorage.remove('radiant-ui_access-token');
    },
}

const getters = {
    user: (state) => {
        return state.user
    },
    user_points: (state) => {
        return state.user_points
    },
    users: (state) => {
        return state.users
    },
}

export default {
    namespaced: true,

    state,
    mutations,
    actions,
    getters
}
