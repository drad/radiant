"""change benchmark filesystem fs_disk_name to required

Revision ID: 727ab756a97f
Revises: e9063be63484
Create Date: 2022-02-16 08:47:32.116176

"""
import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "727ab756a97f"
down_revision = "e9063be63484"
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.alter_column(
        "filesystem_benchmark",
        "fs_disk_name",
        existing_type=sa.VARCHAR(length=25),
        nullable=False,
    )
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.alter_column(
        "filesystem_benchmark",
        "fs_disk_name",
        existing_type=sa.VARCHAR(length=25),
        nullable=True,
    )
    # ### end Alembic commands ###
