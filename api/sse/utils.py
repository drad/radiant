#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#  SPDX-License-Identifier: GPL-3.0-only
#  Copyright 2025 dradux.com

import logging
from typing import List
from sse.models import SSEEventType, SSEMessagesCountResponse, SSEMessage

from config.config import get_redis_sse_queue_client, REDIS_SSE_QUEUE_DEF_TTL
from _common.models import DocInfo


logger = logging.getLogger("default")


async def send_sse_message(
    user: str = None,
    event_type: SSEEventType = None,
    data: str = None,
) -> DocInfo:
    """
    Send SSE Messages: this adds a SSE [message] for a given user that will be picked up by the sse.routes.message_stream.event_generator and sent to the user.

    NOTES:
      - this action is performed by code and is intended to 'notify' the client (browser) of new data
      - this method does not return anything; instead, issues are Logged as the method is often used by the worker
      - before creating (saving) the message we check to ensure the user that the message is going to is active (has a notify_id), if there is not a notify_id the message is not created as the user is not logged in (browser) therefore, there is no need to send them a message.
    """

    from users.utils import get_by_username

    logger.debug(f"Send SSE Message for user={user}, event_type=[{event_type.value}], data=[{data}]")

    _user = await get_by_username(username=user)

    # if we have a user and user has a valid notify_id then send message, else ignore it.
    if not _user or "notify_id" not in _user or not _user["notify_id"] or len(_user["notify_id"]) < 1:
        logger.debug(
            f"Send SSE Message not delivered as user ({user}) was not found or does not have an active notify id, no sse message created!"
        )
    else:
        # add record to redis.
        rclient = get_redis_sse_queue_client()
        # we need to convert ActionType.ENUM to value if data is not a str.
        _action = data if isinstance(data, str) else data.value
        await rclient.setex(f"sse:{user}:{event_type.value}:{_action}", REDIS_SSE_QUEUE_DEF_TTL, "")
        await rclient.close()


async def message_count(
    user: str = None,
) -> SSEMessagesCountResponse:
    """
    Get Count Of SSE Messages for user.
    """

    rclient = get_redis_sse_queue_client()
    _sse_messages = await rclient.scan(cursor=0, match=f"sse:{user}:*")

    await rclient.close()
    return len(_sse_messages[1])


async def get_messages(
    user: str = None,
) -> List[SSEMessage]:
    """
    Retrieve Messages: this gets all Message(s) for a user that have not already been sent (e.g. sent=False).
    """

    docs = []
    rclient = get_redis_sse_queue_client()
    async for key in rclient.scan_iter(match=f"sse:{user}:*"):
        _key = key.decode()
        _sm: SSEMessage = SSEMessage()
        _sm.from_rstring(_key[4:])
        docs.append(_sm)
        # delete item from redis.
        await rclient.delete(key)

    await rclient.close()

    return docs
