# Invoicing and Registrations models.

import datetime
import logging

from .core import db

logger = logging.getLogger("main_logger")

# invoice registration is the 'invoice info' table (the listing of all registrations in an invoice).
invoice_registration = db.Table(
    "invoice_registration",
    db.Column(
        "registration_id",
        db.Integer(),
        db.ForeignKey("registration.id"),
        unique=True,
        nullable=False,
    ),
    db.Column("invoice_id", db.Integer(), db.ForeignKey("invoice.id"), nullable=False),
)

# tag to registration cross-ref
tag_registration = db.Table(
    "tag_registration",
    db.Column("registration_id", db.Integer(), db.ForeignKey("registration.id")),
    db.Column("tag_id", db.Integer(), db.ForeignKey("tag.id")),
)


class InvoiceStatus(db.Model):
    __tablename__ = "invoice_status"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String)
    desc = db.Column(db.String)

    def __str__(self):
        return f"{self.name}"


class RegistrationTicketType(db.Model):
    __tablename__ = "registration_ticket_type"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(50), nullable=False)
    desc = db.Column(db.String(500), nullable=True)

    def __str__(self):
        return f"{self.name}"


class Registration(db.Model):
    from .core import Task

    __tablename__ = "registration"
    id = db.Column(db.Integer, primary_key=True)
    task_id = db.Column(db.Integer, db.ForeignKey("task.id"), nullable=False)
    task = db.relationship(Task, foreign_keys="Registration.task_id")
    invoiced = db.Column(db.Boolean(), default=False)
    start = db.Column(db.TIMESTAMP, default=datetime.datetime.now, nullable=False)
    end = db.Column(db.TIMESTAMP, default=None, nullable=True)
    length = db.Column(
        db.Float(), default=0, nullable=True
    )  # length of registration in seconds
    ticket_number = db.Column(
        db.String(30), nullable=True
    )  # ticket number e.g. zd-11000, ukse-00005, support-0003
    ticket_type_id = db.Column(
        db.Integer, db.ForeignKey("registration_ticket_type.id"), nullable=True
    )  # type of ticket (used to aggregate tickets) - e.g. BR, User Sync, Data Maintenance
    ticket_type = db.relationship(
        RegistrationTicketType, foreign_keys="Registration.ticket_type_id"
    )
    tags = db.relationship(
        "Tag",
        secondary=tag_registration,
        backref=db.backref("registration", lazy="dynamic"),
    )
    note = db.Column(db.Text, nullable=True)

    def calculate_length(self):
        try:
            self.length = (self.end - self.start).total_seconds()
            db.session.commit()
            return True
        except Exception:
            return False

    def is_invoicable(self):
        if self.invoiced:
            logger.warning("- registration has already been invoiced, cannot rebill...")
            return False
        else:
            # logger.debug("- registration has not yet been invoiced...")
            return True

    def checkout(self):
        """
        Check out of a registration (e.g. create end date if it does not exist) and then call calculate_length.
        """

        if self.end:
            self.calculate_length()
        else:
            self.end = datetime.datetime.now()
            self.calculate_length()
            db.session.commit()
            return True

    def __str__(self):
        return f"{self.task} ({self.start})"


class Invoice(db.Model):
    from .core import Client, InvoiceFormat

    __tablename__ = "invoice"
    id = db.Column(db.Integer, primary_key=True)
    status_id = db.Column(
        db.Integer, db.ForeignKey("invoice_status.id"), nullable=False
    )
    status = db.relationship(InvoiceStatus, foreign_keys="Invoice.status_id")
    invoice_format_id = db.Column(
        db.Integer, db.ForeignKey("invoice_format.id"), nullable=False
    )
    invoice_format = db.relationship(
        InvoiceFormat, foreign_keys="Invoice.invoice_format_id"
    )
    name = db.Column(db.String(50), nullable=False)
    client_id = db.Column(db.Integer, db.ForeignKey("client.id"), nullable=False)
    client = db.relationship(Client, foreign_keys="Invoice.client_id")
    submitted = db.Column(db.TIMESTAMP, default=datetime.datetime.now, nullable=False)
    due = db.Column(
        db.TIMESTAMP,
        default=datetime.datetime.now() + datetime.timedelta(days=30),
        nullable=True,
    )
    paid = db.Column(db.TIMESTAMP, nullable=True)  # date paid
    deposited = db.Column(db.TIMESTAMP, nullable=True)  # date deposited
    # invoice number: YYYY-MM-DD-<invoice.id>-<two-digit-revision-number> (e.g. 2019-05-21-00024-00, 2019-05-21-33-00, 2019-05-21-000000000025-00)
    # 000,000,000,025   ==> print(f'{i:012}')
    invoice_number = db.Column(db.String(30), nullable=False)
    note = db.Column(db.Text, nullable=True)
    note_internal = db.Column(db.Text, nullable=True)
    # list of invoice items.
    invoice_items = db.relationship(
        Registration,
        secondary=invoice_registration,
        backref=db.backref("invoice", lazy="dynamic"),
    )
    discount_amount = db.Column(db.Numeric(10, 2), default=0)

    def invoice_items_condensed(self):
        if self.invoice_items:
            regs = []
            # logger.debug("# of invoice_items: {0}".format(len(self.invoice_items)))
            for i in self.invoice_items:
                # check if item is in regs_list
                # logger.debug("ON: {0}/{1}".format(i.id,i.task))
                if len(regs) > 0:
                    match_found = False
                    for x in regs:
                        # logger.debug("- CHECK: {0}/{1}={2}".format(i.id,i.task,x.task))
                        if i.task == x.task:
                            match_found = True
                            # logger.debug("-- MATCH; add to task: {3}/{0} (length: {1} --> {2})".format(x.task, x.length, x.length + i.length, x.id))
                            x.length = x.length + i.length
                            break
                        # else:
                        #    logger.debug("-- no match, keep checking...")
                    if not match_found:
                        # logger.debug("-- NOMATCH; add task: {2}/{0} (length: {1})".format(i.task,i.length, i.id))
                        regs.append(i)
                    # else:
                    #    logger.debug("-- match was found, length increased...")
                else:
                    # logger.debug("- EMPTY, add task: {0}/{1} (length: {2})".format(i.id,i.task,i.length))
                    regs.append(i)
                # logger.debug("DONE WITH: {0}/{1}, looping to process next record...".format(i.id,i.task))
            # logger.debug("condensing done, returning...")
            return regs
        else:
            logger.warning("No Invoice Items to condense!")

    def mark_submit(self):
        invoice_status_create = InvoiceStatus.query.filter(
            InvoiceStatus.name == "create"
        ).first()
        invoice_status_submitted = InvoiceStatus.query.filter(
            InvoiceStatus.name == "submitted"
        ).first()
        # only allow if current status is 'create'
        if self.status == invoice_status_create:
            self.status = invoice_status_submitted
            db.session.commit()
            return True
        else:
            return False

    def mark_paid(self):
        invoice_status_submitted = InvoiceStatus.query.filter(
            InvoiceStatus.name == "submitted"
        ).first()
        invoice_status_paid = InvoiceStatus.query.filter(
            InvoiceStatus.name == "paid"
        ).first()
        # only allow if current status is 'submitted'
        if self.status == invoice_status_submitted:
            self.status = invoice_status_paid
            db.session.commit()
            return True
        else:
            return False

    def mark_deposited(self):
        invoice_status_deposited = InvoiceStatus.query.filter(
            InvoiceStatus.name == "deposited"
        ).first()
        invoice_status_paid = InvoiceStatus.query.filter(
            InvoiceStatus.name == "paid"
        ).first()
        # only allow if current status is 'paid'
        if self.status == invoice_status_paid:
            self.status = invoice_status_deposited
            db.session.commit()
            return True
        else:
            return False

    def __str__(self):
        return f"{self.id}"
