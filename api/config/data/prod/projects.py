#
# Initial Production Data for: Projects.
#

projects_add = []
projects_error = []
projects_skip = []

# NOTE: 'client' value is the client.nickname and is looked up and replaced with id before the add.
projects = [
    # note: this list was generated from the design-doc.ods:Project sheet.
    {
        "client": "RCOG",
        "name": "Support",
        "nickname": "RSUP",
        "order": 2,
        "bill_rate": 100,
        "expected_hours_per_day": 2.14,
        "note": "this project is for Rails support (including db) of an ECS based webapp, should be 1-1.5 hrs per day plus occasional critical issues which I need to look into/address Note: expected hours of 1 hour per day, 5 days a week.",
        "enabled": True,
    },
    {
        "client": "WWN",
        "name": "UK Website Support & Enhancements",
        "nickname": "UKSE",
        "order": 8,
        "bill_rate": 85,
        "expected_hours_per_day": 0.107142,
        "note": "",
        "enabled": True,
    },
    {
        "client": "AC",
        "name": "Website Support",
        "nickname": "ACWS",
        "order": 50,
        "bill_rate": 0,
        "expected_hours_per_day": 0,
        "note": "",
        "enabled": True,
    },
    {
        "client": "WWN",
        "name": "Devops Support",
        "nickname": "WDOS",
        "order": 90,
        "bill_rate": 120,
        "expected_hours_per_day": 0.000001,
        "note": "this project is for Devops support (AWS, kubernetes, etc.)",
        "enabled": True,
    },
    {
        "client": "DX",
        "name": "Testing",
        "nickname": "TEST",
        "order": 80,
        "bill_rate": 100,
        "expected_hours_per_day": 1,
        "note": "this project is for testing",
        "enabled": True,
    },
    # TEMPLATE: {"client": , "name": "", "nickname": "", "order": , "bill_rate": , "expected_hours_per_day": , "note": "", "enabled": True},
    # ::TEST DATA BELOW::
    # ~ {"client": 'T33', "name": "Test 33", "nickname": "T33", "order": 99, "bill_rate": 99.00, "expected_hours_per_day": 2.3456, "note": "A test project attached to Test Client 33", "enabled": True},
]
