#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#  SPDX-License-Identifier: GPL-3.0-only
#  Copyright 2025 dradux.com

# SSE: Server Sent Event logic contains two key components for the application:
# 1. stream: the SSE stream that clients (browser) connect to in order to receive server sent events
#    - NOTE: the stream is handled by a route in the route module
# 2. data structure: the data structure that stores 'messages' that need to be sent to a channel
#    - NOTE: the model, most of the route, and utils are for the data structure

import logging
from enum import Enum
from pydantic import BaseModel


logger = logging.getLogger("default")


class SSEEventType(str, Enum):
    client = "client"
    invoice = "invoice"
    project = "project"
    registration = "registration"
    registration_type = "registration_type"
    task = "task"
    task_list = "task_list"
    user = "user"
    user_message = "user_message"


class SSEActionType(str, Enum):
    new = "new"
    add = "add"
    update = "update"
    delete = "delete"


class SSEMessagesCountResponse(BaseModel):
    """
    SSE Messages count Response.
    """

    count: int = None


class ESMessage(BaseModel):
    """
    EventSource Message.
    """

    id: str = None
    event: str = None
    retry: int = 0
    data: str = None


class SSEMessage(BaseModel):
    """
    SSE Message.
    """

    user: str = None
    event_type: SSEEventType = None
    event_action: SSEActionType = None

    def from_rstring(self, rstring: str = None):
        # key format: '{user}:{event_type}:{action}'
        #    example: 'drad:activity:add'

        # print(f"Loading from rstring={rstring}")
        _value = rstring.split(":")

        self.user = _value[0]
        _event_type = _value[1]
        self.event_type = SSEEventType(_event_type)

        _event_action = _value[2]
        if "." in _event_action:
            _event_action = _event_action.split(".")[1]

        logger.debug(f"SSEMessage model\n  - EventType.value={_event_type}\n  - EventAction.value={_event_action}")
        self.event_action = SSEActionType(_event_action)
