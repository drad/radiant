#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#  SPDX-License-Identifier: GPL-3.0-only
#  Copyright 2025 dradux.com

import logging
import uuid
from datetime import datetime

import aiocouch
from _common.models import DocInfo
from _common.utils import fix_id, make_id
from clients.models import Client, ClientExt, ClientResponse, InvoiceFormat, _db
from config.config import get_cdb
from fastapi import APIRouter, HTTPException, Query, Security, status
from fastapi.encoders import jsonable_encoder
from users.models import UserBase, get_current_user
from sse.utils import send_sse_message
from sse.models import SSEEventType, SSEActionType

logger = logging.getLogger("default")
clients_router = APIRouter()


async def _get_or_404(_id, db):
    """
    Get a document or raise a 404
    """

    # ~ logger.debug(f"_get_or_404 request on: {_id}")
    try:
        return await db[_id]
    except aiocouch.exception.NotFoundError:
        raise HTTPException(status_code=404, detail="Not found")


@clients_router.get("/all", response_model=ClientResponse)
async def get_all(
    only_enabled: bool = Query(True, description="Only include clients that are enabled (not disabled)"),
    limit: int = Query(10, ge=1, le=99, description="Limit number of results"),
    skip: int = Query(0, description="Skip number of records before applying limit"),
    current_user: UserBase = Security(get_current_user, scopes=["users"]),
):
    """
    Get all Clients
    - only for admins
    """

    couchdb = get_cdb()
    logger.debug(f"Get all: current_user: {current_user} --> [{only_enabled}]")
    db = await couchdb[_db.value]

    _selector = {}
    if only_enabled:
        _selector = {"enabled": True}

    logger.debug(f"  selector: {_selector}")

    _response = ClientResponse()
    docs = []
    async for doc in db.find(selector=_selector, limit=limit, skip=skip):
        _doc = fix_id(doc)
        docs.append(Client(**_doc))

    _response.total = len(docs)
    _response.clients = docs
    return _response


@clients_router.get("/one/{_id}", response_model=Client, summary="get one Client")
async def get_one(
    _id: str,
    current_user: UserBase = Security(get_current_user, scopes=["users"]),
):
    """
    Get one by id
    """

    couchdb = get_cdb()
    db = await couchdb[_db.value]

    return Client(**fix_id(await _get_or_404(_id, db)))


@clients_router.post("/", response_model=DocInfo, status_code=status.HTTP_201_CREATED)
async def add(
    name: str = Query(..., description="name of the client"),
    nickname: str = Query(
        ...,
        min_length=1,
        max_length=4,
        description="nickname of the client | <i>value will be uppercased</i>",
    ),
    order: int = Query(
        100,
        description="order of the client (lower number will be sorted to the top of lists)",
    ),
    preferred_invoice_format: InvoiceFormat = Query(InvoiceFormat.summary, description="preferred invoice format"),
    note: str = Query(None, description="notes for the client"),
    enabled: bool = Query(True, description="is this item enabled?"),
    current_user: UserBase = Security(get_current_user, scopes=["admins"]),
):
    """
    Add new

    @TODO: need to add check to ensure Name and Nickname does not already exist
    """

    couchdb = get_cdb()
    db = await couchdb[_db.value]

    logger.debug("add Client")

    _doc = ClientExt(
        name=name,
        nickname=nickname.upper(),
        order=order,
        preferred_invoice_format=preferred_invoice_format,
        note=note,
        enabled=enabled,
        created=datetime.now(),
        updated=datetime.now(),
        creator=current_user.username,
    )

    try:
        doc_id = make_id(items=[str(uuid.uuid4())])
        doc = await db.create(
            doc_id,
            data=jsonable_encoder(_doc),
        )
        await doc.save()
        # send sse message.
        await send_sse_message(user=current_user.username, event_type=SSEEventType.client, data=SSEActionType.add)
        return await doc.info()

    except aiocouch.ConflictError:
        raise HTTPException(status_code=409, detail=f"Duplicate Key: cannot add '{doc_id}'.")


@clients_router.put("/", response_model=DocInfo)
async def update(
    _id: str = Query(..., description="id of the document to update"),
    name: str = Query(None, description="name of the client"),
    nickname: str = Query(
        None,
        min_length=1,
        max_length=4,
        description="nickname of the client | <i>value will be uppercased</i>",
    ),
    order: int = Query(
        None,
        description="order of the client (lower number will be sorted to the top of lists)",
    ),
    preferred_invoice_format: InvoiceFormat = Query(None, description="preferred invoice format"),
    note: str = Query(None, description="notes for the client"),
    enabled: bool = Query(True, description="is this item enabled?"),
    current_user: UserBase = Security(get_current_user, scopes=["admins"]),
):
    """
    Update Registration Type
    """

    logger.debug("- updating registration type")
    couchdb = get_cdb()
    db = await couchdb[_db.value]
    doc = await _get_or_404(_id, db)
    logger.debug(f"- updating doc: {doc}")

    doc["name"] = name if name else doc["name"]
    doc["nickname"] = nickname.upper() if nickname else doc["nickname"]
    doc["order"] = order if order else doc["order"]
    doc["note"] = note if note else doc["note"]
    doc["enabled"] = enabled

    doc["updated"] = jsonable_encoder(datetime.now())
    doc["updator"] = current_user.username
    # logger.debug(f"doc before save: {doc}")
    await doc.save()
    # send sse message.
    await send_sse_message(user=current_user.username, event_type=SSEEventType.client, data=SSEActionType.update)
    return await doc.info()


@clients_router.delete("/", response_model=DocInfo)
async def delete(
    _id: str,
    current_user: UserBase = Security(get_current_user, scopes=["admins"]),
):
    """
    Hard Delete by id
    """

    couchdb = get_cdb()
    db = await couchdb[_db.value]
    resp = DocInfo(ok=False, id=_id, rev="", msg="unknown")
    doc = await _get_or_404(_id, db)
    dr = await doc.delete()
    if dr and dr.status < 400:
        resp.ok = True
        resp.msg = "deleted"
        # send sse message.
        await send_sse_message(user=current_user.username, event_type=SSEEventType.client, data=SSEActionType.delete)
    else:
        resp.ok = False
        resp.msg = f"Delete status: {dr.status}, headers: {dr.headers}"

    return resp
