import { store } from 'quasar/wrappers'
import { createStore } from 'vuex'

import about from './store-about'
import auth from './store-auth'
import clients from './store-clients'
import invoices from './store-invoices'
import sse from './store-sse'
import tasks from './store-tasks'
import registrations from './store-registrations'
import registration_types from './store-registration-types'
import user from './store-user'
import user_messages from './store-user-messages'

/*
 * If not building with SSR mode, you can
 * directly export the Store instantiation;
 *
 * The function below can be async too; either use
 * async/await or return a Promise which resolves
 * with the Store instance.
 */
export default store(function (/* { ssrContext } */) {
  const Store = createStore({
    modules: {
      about,
      auth,
      clients,
      invoices,
      registrations,
      registration_types,
      sse,
      tasks,
      user,
      user_messages,
    },
    //~ state: {
      //~ debug: process.env.UI_FRONTEND_DEBUG
    //~ },
    // enable strict mode (adds overhead!)
    // for dev mode and --debug builds only
    strict: process.env.DEBUGGING
  })

  return Store
})
