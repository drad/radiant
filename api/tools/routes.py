#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#  SPDX-License-Identifier: GPL-3.0-only
#  Copyright 2025 dradux.com

import logging

from fastapi import APIRouter, Query, Security
from users.models import UserBase, get_current_user
from _common.models import DocInfo

logger = logging.getLogger("default")
tools_router = APIRouter()


def get_id_by_nickname(nickname=None, collctn=None):
    """
    Get the id of an item in a collection by nickname.
    """

    _res = [x for x in collctn if x["nickname"] == nickname]

    if len(_res) == 1:
        # note: there will be no id in the result if we do not persist!
        return _res[0]["id"] if "id" in _res[0] else None
    else:
        logger.warning(f"No item found with nickname={nickname} in collection={collctn}")
        return None


@tools_router.get("/load_base_prod_data", response_model=DocInfo)
async def load_base_prod_data(
    # Params that effect saving
    persist: bool = Query(False, description="Persist (save) changes?"),
    current_user: UserBase = Security(get_current_user, scopes=["admins"]),
):
    """
    Load the base prod data (clients, projects, registration_types, and tasks)
    """

    from clients.routes import add as add_client
    from projects.routes import add as add_project
    from tasks.routes import add as add_task
    from registration_types.routes import add as add_registration_type

    from config.data.prod.clients import clients, clients_add, clients_error, clients_skip
    from config.data.prod.projects import projects, projects_add, projects_error, projects_skip
    from config.data.prod.tasks import tasks, tasks_add, tasks_error, tasks_skip
    from config.data.prod.registration_types import (
        registration_types,
        registration_types_add,
        registration_types_error,
        registration_types_skip,
    )

    # CLIENTS
    for client in clients:
        client["current_user"] = current_user
        logger.debug(f"Adding client: {client['name']}")
        if persist:
            _ret = await add_client(**client)
            if _ret["ok"]:
                client["id"] = _ret["id"]  # add id so we can use it in subsequent items (projects, etc.)
                clients_add.append(client)
            else:
                clients_error.append(client)
        else:
            clients_skip.append(client)

    # PROJECTS
    for project in projects:
        project["current_user"] = current_user
        # lookup id from given nickname
        project["client"] = get_id_by_nickname(project["client"], clients)
        logger.debug(f"Adding project: {project['name']}")
        if persist:
            _ret = await add_project(**project)
            if _ret["ok"]:
                project["id"] = _ret["id"]  # add id so we can use it in subsequent items (projects, etc.)
                projects_add.append(project)
            else:
                projects_error.append(project)
        else:
            projects_skip.append(project)

    # TASKS
    for task in tasks:
        task["current_user"] = current_user
        # lookup id from given nickname
        task["project_id"] = get_id_by_nickname(task["project_id"], projects)
        logger.debug(f"Adding task: {task['name']}")
        if persist:
            _ret = await add_task(**task)
            if _ret["ok"]:
                tasks_add.append(tasks)
            else:
                tasks_error.append(tasks)
        else:
            tasks_skip.append(tasks)

    # REGISTRATION_TYPES
    for registration_type in registration_types:
        registration_type["current_user"] = current_user
        # lookup id from given nickname
        registration_type["client"] = get_id_by_nickname(registration_type["client"], clients)
        logger.debug(f"Adding registration type: {registration_type['name']}")
        if persist:
            _ret = await add_registration_type(**registration_type)
            if _ret["ok"]:
                registration_types_add.append(registration_type)
            else:
                registration_types_error.append(registration_type)
        else:
            registration_types_skip.append(registration_type)

    # create the summary
    _summary = {
        "clients": {"add": len(clients_add), "error": len(clients_error), "skip": len(clients_skip)},
        "projects": {"add": len(projects_add), "error": len(projects_error), "skip": len(projects_skip)},
        "tasks": {"add": len(tasks_add), "error": len(tasks_error), "skip": len(tasks_skip)},
        "registration_types": {
            "add": len(registration_types_add),
            "error": len(registration_types_error),
            "skip": len(registration_types_skip),
        },
    }

    return DocInfo(
        ok=True, id="n/a", msg=f"Completed with the following results: {_summary}"
    )  # , total=_added, runtime=_tend - _tstart)
