#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#  SPDX-License-Identifier: GPL-3.0-only
#  Copyright 2025 dradux.com

import logging
import uuid
import aiocouch
from datetime import datetime
from typing import List
from _common.models import DocInfo
from _common.utils import fix_id, make_id
from clients.utils import get_by_id
from config.config import get_cdb
from fastapi import APIRouter, HTTPException, Query, Security, status
from fastapi.encoders import jsonable_encoder
from registration_types.models import (
    RegistrationType,
    RegistrationTypeExt,
    RegistrationTypeListResponse,
    RegistrationTypeResponse,
    _db,
)
from users.models import UserBase, get_current_user
from sse.utils import send_sse_message
from sse.models import SSEEventType, SSEActionType

logger = logging.getLogger("default")
registration_types_router = APIRouter()


async def _get_or_404(_id, db):
    """
    Get a document or raise a 404
    """

    # ~ logger.debug(f"_get_or_404 request on: {_id}")
    try:
        return await db[_id]
    except aiocouch.exception.NotFoundError:
        raise HTTPException(status_code=404, detail="Not found")


@registration_types_router.get("/registration_type_list", response_model=List[RegistrationTypeListResponse])
async def registration_type_list(
    client: str = Query(..., description="Client id to get registration type list for"),
    limit: int = Query(10, ge=1, le=99, description="Limit number of results"),
    skip: int = Query(0, description="Skip number of records before applying limit"),
    current_user: UserBase = Security(get_current_user, scopes=["users"]),
):
    """
    The registration type list shows a list of registration types for a Client.
    - **NOTE:** only enabled registration types are returned
    """

    from clients.utils import get_by_id as get_client_by_id

    couchdb = get_cdb()
    logger.debug(f"- getting registration type list for client: {client}")
    db = await couchdb[_db.value]

    # check to ensure client exists.
    _client = await get_client_by_id(client)
    if not _client:
        raise HTTPException(status_code=404, detail="Client not found")

    docs = []
    selector = {"enabled": True, "client": client}
    async for doc in db.find(selector=selector, limit=limit, skip=skip):
        _doc = fix_id(doc)
        # ~ docs.append(_doc)
        _rt = RegistrationType(**_doc)
        docs.append(_rt)

    return docs


@registration_types_router.get("/all", response_model=RegistrationTypeResponse)
async def get_all(
    limit: int = Query(10, ge=1, le=99, description="Limit number of results"),
    skip: int = Query(0, description="Skip number of records before applying limit"),
    current_user: UserBase = Security(get_current_user, scopes=["admins"]),
):
    """
    Get all registration types
    - only for admins
    """

    couchdb = get_cdb()
    logger.debug(f"- get all: current_user: {current_user}")
    db = await couchdb[_db.value]

    _response = RegistrationTypeResponse()
    docs = []
    async for doc in db.find(selector={}, limit=limit, skip=skip):
        _doc = fix_id(doc)
        docs.append(RegistrationType(**_doc))

    _response.total = len(docs)
    _response.registration_types = docs
    # ~ logger.critical(f"RegistrationTypeResponse: {_response}")

    return _response


@registration_types_router.get("/one/{_id}", response_model=RegistrationType, summary="Get one Registration Type")
async def get_one(
    _id: str,
    current_user: UserBase = Security(get_current_user, scopes=["users"]),
):
    """
    Get one by id
    """

    couchdb = get_cdb()
    db = await couchdb[_db.value]
    return RegistrationType(**fix_id(await _get_or_404(_id, db)))


@registration_types_router.post("/", response_model=DocInfo, status_code=status.HTTP_201_CREATED)
async def add(
    client: str = Query(..., description="client (id) that the registration type belongs to"),
    name: str = Query(..., description="name of the Registration Type"),
    note: str = Query(..., description="notes for the Registration Type"),
    icon: str = Query(None, description="icon for the Registration Type"),
    enabled: bool = Query(True, description="is this item enabled?"),
    current_user: UserBase = Security(get_current_user, scopes=["admins"]),
):
    """
    Add new
    """

    couchdb = get_cdb()
    db = await couchdb[_db.value]

    logger.debug("- add Registration Type")

    # check to ensure client exists.
    _client = await get_by_id(client)
    if not _client:
        raise HTTPException(
            status_code=417,
            detail="Client does not exist, did you enter the Name instead of Id?",
        )

    _doc = RegistrationTypeExt(
        client=client,
        name=name,
        note=note,
        icon=icon,
        enabled=enabled,
        created=datetime.now(),
        updated=datetime.now(),
        creator=current_user.username,
    )

    try:
        doc_id = make_id(items=[str(uuid.uuid4())])
        doc = await db.create(
            doc_id,
            data=jsonable_encoder(_doc),
        )
        await doc.save()
        # send sse message.
        await send_sse_message(
            user=current_user.username, event_type=SSEEventType.registration_type, data=SSEActionType.add
        )
        return await doc.info()

    except aiocouch.ConflictError:
        raise HTTPException(status_code=409, detail=f"Duplicate Key: cannot add '{doc_id}'.")


@registration_types_router.put("/", response_model=DocInfo)
async def update(
    _id: str = Query(..., description="id of the document to update"),
    client: str = Query(None, description="client that the registration type belongs to"),
    name: str = Query(None, description="name of the Registration Type"),
    note: str = Query(None, description="notes for the Registration Type"),
    icon: str = Query(None, description="icon for the Registration Type"),
    enabled: bool = Query(True, description="is this item enabled?"),
    current_user: UserBase = Security(get_current_user, scopes=["admins"]),
):
    """
    Update
    """

    logger.debug("- updating registration type")
    couchdb = get_cdb()
    db = await couchdb[_db.value]
    doc = await _get_or_404(_id, db)
    logger.debug(f"- updating doc: {doc}")

    if client:
        # check to ensure client exists.
        _client = await get_by_id(client)
        if not _client:
            raise HTTPException(
                status_code=417,
                detail="Client does not exist, did you enter the Name instead of the Id?",
            )
        doc["client"] = client

    doc["name"] = name if name else doc["name"]
    doc["note"] = note if note else doc["note"]
    doc["icon"] = icon if icon else doc["icon"]
    doc["enabled"] = enabled

    doc["updated"] = jsonable_encoder(datetime.now())
    doc["updator"] = current_user.username
    # logger.debug(f"doc before save: {doc}")
    await doc.save()
    # send sse message.
    await send_sse_message(
        user=current_user.username, event_type=SSEEventType.registration_type, data=SSEActionType.update
    )
    return await doc.info()


@registration_types_router.delete("/", response_model=DocInfo)
async def delete(
    _id: str,
    current_user: UserBase = Security(get_current_user, scopes=["admins"]),
):
    """
    Hard Delete by id
    """

    couchdb = get_cdb()
    db = await couchdb[_db.value]
    resp = DocInfo(ok=False, id=_id, rev="", msg="unknown")
    doc = await _get_or_404(_id, db)
    dr = await doc.delete()
    if dr and dr.status < 400:
        resp.ok = True
        resp.msg = "deleted"
        # send sse message.
        await send_sse_message(
            user=current_user.username, event_type=SSEEventType.registration_type, data=SSEActionType.delete
        )
    else:
        resp.ok = False
        resp.msg = f"Delete status: {dr.status}, headers: {dr.headers}"

    return resp
