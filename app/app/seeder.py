import logging
from models import db, InvoiceStatus

logger = logging.getLogger("main_logger")


class Seeder:
    def seed_all():
        logger.debug("- seed all objects...")
        Seeder.seed_invoice_status()

    def seed_invoice_status():
        logger.debug("- seed invoice status...")

        cnt = db.session.query(InvoiceStatus).count()
        if cnt == 0:
            records = [
                InvoiceStatus(id=1, name="create", desc="creating item"),
                InvoiceStatus(
                    id=2, name="submitted", desc="item has been submitted to client"
                ),
                InvoiceStatus(id=3, name="paid", desc="item has been paid"),
            ]
            db.session.bulk_save_objects(records)
            db.session.commit()
            cnt_postseed = db.session.query(InvoiceStatus).count()
            logger.info(f" - inserted [{cnt_postseed}] records")
        else:
            logger.warn(
                "Warning: Records already exist in table, seeding of table skipped..."
            )
