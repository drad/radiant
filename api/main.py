#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#  SPDX-License-Identifier: GPL-3.0-only
#  Copyright 2025 dradux.com
#
# LOGGING: designed to run at INFO loglevel.

import logging
from os import getenv
from contextlib import asynccontextmanager

from _common.utils import check_databases
from auth.routes import auth_router
from clients.routes import clients_router
from config import config
from config.config import cfg, get_cdb
from core.routes import core_router
from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from logs.routes import logs_router
from invoices.routes import invoices_router
from projects.routes import projects_router
from registration_types.routes import registration_types_router
from registrations.routes import registrations_router
from sse.routes import sse_router
from tasks.routes import tasks_router
from tools.routes import tools_router
from user_messages.routes import user_messages_router
from users.routes import users_router

logger_base = logging.getLogger("default")
logger_base.setLevel(logging.getLevelName(config.APP_LOGLEVEL))
console_handler = logging.StreamHandler()
if "console" in config.LOG_TO:
    logger_base.addHandler(console_handler)

logger = logging.LoggerAdapter(
    logging.getLogger("default"),
    {"application_name": cfg.core.name, "application_env": config.DEPLOY_ENV},
)

logger.info(f"{cfg.core.name} - v.{cfg.core.version} ({cfg.core.modified}) - {config.APP_LOGLEVEL} - {config.LOG_TO}")

_lsi = []
# _lsi.append(('', ''))
_lsi.append(("Log Level", config.APP_LOGLEVEL))
_lsi.append(("Log To", config.LOG_TO))
_lsi.append(("Deploy Env", config.DEPLOY_ENV))
_lsi.append(("CDB URI", config.CDB_URI))
_lsi.append(("API Path", config.API_PATH))
_lsi.append(("CORS Origins", config.CORS_ORIGINS))
_lsi.append(
    (
        "JWT Expire",
        f"{config.API_JWT_EXPIRE_DAYS}:{config.API_JWT_EXPIRE_HOURS}:{config.API_JWT_EXPIRE_MINUTES} (Days:Hours:Minutes)",
    )
)
_lsi.append(("Auth URL", f"{config.LDAP_HOST}:{config.LDAP_PORT} {'(using ssl)' if config.LDAP_USE_SSL else ''}"))
_lsi.append(
    (
        "Redis",
        f"{getenv('REDIS_HOST', 'undefined')}:{getenv('REDIS_PORT', 'undefined')} {' (using password)' if getenv('REDIS_PASSWORD') else ' (no password)'}",
    )
)

# find longest key length and add 4
lsi_lk_length = len(sorted(_lsi, key=lambda x: len(x[0]), reverse=True)[0][0]) + 4
# sort items by key
_lsi.sort(key=lambda a: a[0])
for item in _lsi:
    logger.info(f"  ✦ {item[0].upper().ljust(lsi_lk_length, '.')}: {item[1]}")


@asynccontextmanager
async def lifespan(app: FastAPI):
    # open db connection.
    couchdb = get_cdb()
    await couchdb.check_credentials()
    await check_databases()
    yield
    # close db connection.
    await couchdb.close()


app = FastAPI(
    title=f"{cfg.core.name}",
    description=f"{cfg.core.description}",
    version=f"{cfg.core.version}",
    openapi_url=f"{config.API_PATH}/openapi.json",
    docs_url=f"{config.API_PATH}/docs",
    redoc_url=None,
    lifespan=lifespan,
)

app.add_middleware(
    CORSMiddleware,
    allow_origins=config.CORS_ORIGINS,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

app.include_router(
    clients_router,
    prefix=f"{config.API_PATH}/clients",
    tags=["clients"],
    responses={404: {"description": "Not found"}},
)

app.include_router(
    projects_router,
    prefix=f"{config.API_PATH}/projects",
    tags=["projects"],
    responses={404: {"description": "Not found"}},
)

app.include_router(
    invoices_router,
    prefix=f"{config.API_PATH}/invoices",
    tags=["invoices"],
    responses={404: {"description": "Not found"}},
)

app.include_router(
    registrations_router,
    prefix=f"{config.API_PATH}/registrations",
    tags=["registrations"],
    responses={404: {"description": "Not found"}},
)

app.include_router(
    registration_types_router,
    prefix=f"{config.API_PATH}/registration_types",
    tags=["registration_types"],
    responses={404: {"description": "Not found"}},
)

app.include_router(
    tasks_router,
    prefix=f"{config.API_PATH}/tasks",
    tags=["tasks"],
    responses={404: {"description": "Not found"}},
)

app.include_router(
    auth_router,
    prefix=f"{config.API_PATH}/auth",
    tags=["authentication"],
    responses={404: {"description": "Not found"}},
)

app.include_router(
    core_router,
    prefix=f"{config.API_BASE_PATH}",
    tags=["core"],
    responses={404: {"description": "Not found"}},
)

app.include_router(
    logs_router,
    prefix=f"{config.API_PATH}/logs",
    tags=["logs"],
    responses={404: {"description": "Not found"}},
)

app.include_router(
    sse_router,
    prefix=f"{config.API_PATH}/sse",
    tags=["sse"],
    responses={404: {"description": "Not found"}},
)

app.include_router(
    tools_router,
    prefix=f"{config.API_PATH}/tools",
    tags=["tools"],
    responses={404: {"description": "Not found"}},
)

app.include_router(
    users_router,
    prefix=f"{config.API_PATH}/users",
    tags=["users"],
    responses={404: {"description": "Not found"}},
)

app.include_router(
    user_messages_router,
    prefix=f"{config.API_PATH}/user_messages",
    tags=["user messages"],
    responses={404: {"description": "Not found"}},
)
