import { boot } from 'quasar/wrappers'
import VueSSE from 'vue-sse'

// more info on params: https://v2.quasar.dev/quasar-cli/boot-files
export default boot(({ app }) => {

  // see: https://github.com/tserkov/vue-sse
  // format: json|plain|json|(event)
  app.use(VueSSE, {
    format: 'plain',
    //polyfill: true,
    //url: '/my-events-server',
    withCredentials: true,
  });

})
