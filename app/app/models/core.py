# Core models.

import datetime
import logging
import os

from config.config import DefaultConfig
from flask_security import RoleMixin, UserMixin
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.event import listens_for

db = SQLAlchemy()
logger = logging.getLogger("main_logger")


# roles to users cross-ref
roles_users = db.Table(
    "roles_users",
    db.Column("user_id", db.Integer(), db.ForeignKey("user.id")),
    db.Column("role_id", db.Integer(), db.ForeignKey("role.id")),
)


class Attachment(db.Model):
    __tablename__ = "attachment"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.Unicode(64), nullable=False)
    path = db.Column(db.Unicode(128), nullable=False)

    def __unicode__(self):
        return self.name


@listens_for(Attachment, "after_delete")
def del_file(mapper, connection, target):
    if target.path:
        try:
            os.remove(os.path.join(DefaultConfig.ATTACHMENTS_DIR, target.path))
        except OSError:
            # Don't care if was not deleted because it does not exist
            pass


# Notice: this needs to be in core due to circular reference in Client
class InvoiceFormat(db.Model):
    __tablename__ = "invoice_format"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String)
    desc = db.Column(db.String)

    def __str__(self):
        return f"{self.name}"


class Client(db.Model):
    __tablename__ = "client"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(50), nullable=False)
    nickname = db.Column(db.String(7), nullable=False)
    active = db.Column(db.Boolean(), default=True)
    note = db.Column(db.Text, nullable=True)
    order = db.Column(db.SmallInteger, nullable=True, default=99)
    preferred_invoice_format_id = db.Column(
        db.Integer, db.ForeignKey("invoice_format.id"), nullable=False
    )
    preferred_invoice_format = db.relationship(
        InvoiceFormat, foreign_keys="Client.preferred_invoice_format_id"
    )

    def __str__(self):
        return f"{self.nickname}"


class Comment(db.Model):
    __tablename__ = "comment"
    id = db.Column(db.Integer, primary_key=True)
    created_by = db.Column(db.Integer, db.ForeignKey("user.id"), nullable=False)
    creator = db.relationship("User", foreign_keys=[created_by])
    created_at = db.Column(
        db.DateTime(), default=datetime.datetime.utcnow, nullable=False
    )
    comment = db.Column(db.Text, nullable=True)

    def __str__(self):
        return f"{self.comment}"


class Project(db.Model):
    __tablename__ = "project"
    id = db.Column(db.Integer, primary_key=True)
    client_id = db.Column(db.Integer, db.ForeignKey("client.id"), nullable=False)
    client = db.relationship(Client, foreign_keys="Project.client_id")
    name = db.Column(db.String(50), nullable=False)
    nickname = db.Column(db.String(7), nullable=False)
    active = db.Column(db.Boolean(), default=True)
    note = db.Column(db.Text, nullable=True)
    rate = db.Column(db.Numeric(10, 2))
    order = db.Column(db.SmallInteger, nullable=True, default=99)
    expected_hours_per_day = db.Column(db.Numeric(8, 6), nullable=False, default=1)

    def __str__(self):
        return f"{self.client}/{self.nickname}"


class Task(db.Model):
    __tablename__ = "task"
    id = db.Column(db.Integer, primary_key=True)
    project_id = db.Column(db.Integer, db.ForeignKey("project.id"), nullable=False)
    project = db.relationship(Project, foreign_keys="Task.project_id")
    name = db.Column(db.String(100), nullable=False)
    nickname = db.Column(db.String(20), nullable=False)
    ticket = db.Column(db.String(250), nullable=True)
    complete = db.Column(db.Boolean(), default=False)
    note = db.Column(db.Text, nullable=True)
    order = db.Column(db.SmallInteger, nullable=True, default=99)

    def __str__(self):
        return f"{self.project}/{self.nickname}"


class Role(db.Model, RoleMixin):
    id = db.Column(db.Integer(), primary_key=True)
    name = db.Column(db.String(80), unique=True)
    description = db.Column(db.String(255))

    # __str__ is required by Flask-Admin, so we can have human-readable values for the Role when editing a User.
    # If we were using Python 2.7, this would be __unicode__ instead.
    def __str__(self):
        return self.name

    # __hash__ is required to avoid the exception TypeError: unhashable type: 'Role' when saving a User
    def __hash__(self):
        return hash(self.name)


class Tag(db.Model):
    __tablename__ = "tag"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(20), nullable=False)
    created_by = db.Column(db.Integer, db.ForeignKey("user.id"), nullable=False)
    creator = db.relationship("User", foreign_keys=[created_by])

    def __str__(self):
        return f"{self.name}"


class User(db.Model, UserMixin):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100), nullable=False)
    username = db.Column(db.String(33), nullable=False, unique=True)
    nickname = db.Column(db.String(5), nullable=True, unique=True)
    password = db.Column(db.String(255), nullable=False)
    roles = db.relationship(
        "Role", secondary=roles_users, backref=db.backref("users", lazy="dynamic")
    )
    note = db.Column(db.Text, nullable=True)

    def __str__(self):
        return f"{self.username}"

    def is_active(self):
        """True, as all users are active."""
        return True

    def get_id(self):
        """Return the id to satisfy Flask-Login's requirements."""
        return self.id

    def is_authenticated(self):
        """Return True if the user is authenticated."""
        return self.authenticated

    def is_anonymous(self):
        """False, as anonymous users aren't supported."""
        return False
