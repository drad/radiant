"""load filesystem benchmark data

Revision ID: 257e87ea4afc
Revises: 122e9319a1cd
Create Date: 2022-02-14 12:38:55.797347

"""
# import sqlalchemy as sa
from alembic import op
from models.filesystem_benchmarks import (  # FilesystemBenchmarkRunMethod,
    FilesystemBenchmarkDiskType,
    FilesystemBenchmarkFsKind,
    FilesystemBenchmarkFsType,
    FilesystemBenchmarkRunLocationType,
)

# revision identifiers, used by Alembic.
revision = "257e87ea4afc"
down_revision = "122e9319a1cd"
branch_labels = None
depends_on = None


def upgrade():

    op.bulk_insert(
        FilesystemBenchmarkDiskType.__table__,
        [
            {"order": 1, "name": "SSD"},
            {"order": 2, "name": "NVMe"},
            {"order": 3, "name": "HDD"},
            {"order": 4, "name": "USB"},
            {"order": 5, "name": "NFS"},
        ],
    )

    op.bulk_insert(
        FilesystemBenchmarkRunLocationType.__table__,
        [
            {"order": 1, "name": "local"},
            {"order": 2, "name": "nfs"},
        ],
    )

    op.bulk_insert(
        FilesystemBenchmarkFsKind.__table__,
        [
            {"order": 1, "name": "disk"},
            {"order": 2, "name": "partition"},
        ],
    )

    op.bulk_insert(
        FilesystemBenchmarkFsType.__table__,
        [
            {"order": 1, "name": "zfs"},
            {"order": 2, "name": "xfs"},
            {"order": 3, "name": "btrfs"},
            {"order": 4, "name": "ext4"},
        ],
    )

    # ~ op.bulk_insert(
    # ~ FilesystemBenchmarkRunMethod.__table__,
    # ~ [
    # ~ {"order": 1, "name": "fio"},
    # ~ {"order": 2, "name": "bonnie++"},
    # ~ ],
    # ~ )


def downgrade():
    pass
