#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#  SPDX-License-Identifier: GPL-3.0-only
#  Copyright 2025 dradux.com

import logging
from datetime import datetime
from typing import List
from config.models import Cdb
from pydantic import BaseModel

logger = logging.getLogger("default")
_db = Cdb.REGISTRATION_TYPES


class RegistrationTypeBase(BaseModel):
    """
    Base document
    """

    client: str | None = None  # client (id) the registration type belongs to
    name: str | None = None  # registration type name
    note: str | None = None  # notes about item
    enabled: bool = True  # is the item enabled?
    icon: str = None  # icon for registration type


class RegistrationTypeExt(RegistrationTypeBase):
    """
    Extended (added by backend logic)
    """

    # note: this needs to be set/overwrote on result instantiation as using
    #  datetime.now() here will only get you now of when worker was started.
    created: datetime = datetime.now()
    updated: datetime = datetime.now()

    creator: str | None = None
    updator: str | None = None


class RegistrationType(RegistrationTypeExt):
    """
    Actual (at DB level)
    """

    id_: str


class RegistrationTypeResponse(BaseModel):
    """
    Registration Type Response
    """

    total: int = 0
    registration_types: List[RegistrationType] | None = None


class RegistrationTypeListResponse(BaseModel):
    """
    List Response
    """

    id_: str
    name: str | None = None
    note: str | None = None  # notes about item
    icon: str | None = None
