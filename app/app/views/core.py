# core views.

import logging

from config.config import DefaultConfig
from flask import redirect, request, url_for
from flask_admin import AdminIndexView, BaseView, expose, form
from flask_admin.contrib import sqla
from flask_admin.menu import MenuLink
from flask_admin.model.form import InlineFormAdmin
from flask_security import current_user, utils
from markupsafe import Markup
from views._utils import list_files, prefix_name
from wtforms import fields

logger = logging.getLogger("main_logger")

#
# NOTICE: custom forms must come above views.
#


class AttachmentForm(InlineFormAdmin):
    form_columns = ("id", "name", "path")
    form_label = "Attachment"
    # Override form field to use Flask-Admin FileUploadField
    form_overrides = {"path": form.FileUploadField}

    # Pass additional parameters to 'path' to FileUploadField constructor
    form_args = {
        "path": {
            "label": "File",
            "base_path": f"{DefaultConfig.ATTACHMENTS_DIR}",
            "allow_overwrite": False,
            "namegen": prefix_name,
        }
    }


class CommentForm(InlineFormAdmin):
    def is_accessible(self):
        return (
            True
            if current_user.is_authenticated and (current_user.has_role("users") or current_user.has_role("admins"))
            else False
        )

    def inaccessible_callback(self, name, **kwargs):
        return redirect(url_for("ldap_login_view"))

    def _handle_view(self, name, **kwargs):
        if not self.is_accessible():
            return self.inaccessible_callback(name, **kwargs)
        self.can_delete = True if current_user.has_role("admins") else False

    form_columns = ("id", "created_at", "comment")
    form_label = "Comment"
    form_widget_args = {
        "created_at": {"disabled": True},
        "comment": {
            "placeholder": "comments for ticket",
            "title": "enter comments for ticket",
            "class": "markdown",
        },
    }
    column_labels = {"created_at": "Created"}

    def on_model_change(self, form, model, is_created):
        if is_created:
            # ~ logger.debug("- is_created...")
            model.created_by = current_user.id


class AboutView(BaseView):
    @expose("/")
    def index(self):
        return self.render("help/about.html", help_about=DefaultConfig.CORE)


class AccountView(sqla.ModelView):
    def is_accessible(self):
        return True if current_user.is_authenticated and current_user.has_role("admins") else False

    def inaccessible_callback(self, name, **kwargs):
        return redirect(url_for("ldap_login_view"))

    def _handle_view(self, name, **kwargs):
        if not self.is_accessible():
            return self.inaccessible_callback(name, **kwargs)
        self.can_delete = True if current_user.has_role("admins") else False

    @expose("/")
    def index(self):
        return self.render("account.html")


class HomeView(AdminIndexView):
    @expose("/")
    def index(self):
        return self.render("home.html")


class LoginMenuLink(MenuLink):
    def is_accessible(self):
        return not current_user.is_authenticated


class LogoutMenuLink(MenuLink):
    def is_accessible(self):
        return current_user.is_authenticated


class OnlineHelpView(BaseView):
    @expose("/")
    def index(self):
        return self.render("help/general.html")


# Customized Role model for SQL-Admin
class RoleAdmin(sqla.ModelView):
    def is_accessible(self):
        return True if current_user.is_authenticated and current_user.has_role("admins") else False

    def inaccessible_callback(self, name, **kwargs):
        return redirect(url_for("ldap_login_view"))

    def _handle_view(self, name, **kwargs):
        if not self.is_accessible():
            return self.inaccessible_callback(name, **kwargs)
        self.can_delete = True if current_user.has_role("admins") else False


class StatusView(BaseView):
    def is_accessible(self):
        return True if current_user.is_authenticated and current_user.has_role("admins") else False

    def inaccessible_callback(self, name, **kwargs):
        return redirect(url_for("ldap_login_view"))

    def _handle_view(self, name, **kwargs):
        if not self.is_accessible():
            return self.inaccessible_callback(name, **kwargs)
        self.can_delete = True if current_user.has_role("admins") else False

    @expose("/")
    def index(self):
        backups_listing = list_files("/backups/db")
        return self.render("status.html", backups_listing=backups_listing)


class TagView(sqla.ModelView):
    def is_accessible(self):
        return (
            True
            if current_user.is_authenticated
            and (  # noqa: W503
                current_user.has_role("users")  # noqa: W503
                or current_user.has_role("admins")  # noqa: W503
            )
            else False
        )

    def inaccessible_callback(self, name, **kwargs):
        return redirect(url_for("ldap_login_view"))

    def _handle_view(self, name, **kwargs):
        if not self.is_accessible():
            return self.inaccessible_callback(name, **kwargs)
        self.can_delete = True if current_user.has_role("admins") else False

    page_size = 20
    can_view_details = False
    create_modal = True
    edit_modal = True
    # details_modal = True
    can_export = True

    column_searchable_list = ["name"]
    column_filters = ["name"]
    column_editable_list = ["name"]
    column_list = ["name"]
    column_exclude_list = ["creator", "registration"]
    # sort by name, descending
    column_default_sort = ("name", False)

    form_excluded_columns = (
        "creator",
        "registration",
        "asset",
        "ticketing",
        "test_result",
    )

    form_widget_args = {"name": {"placeholder": "tag name"}}

    def on_model_change(self, form, model, is_created):
        if is_created:
            model.creator = current_user


# Customized User model for SQL-Admin
class UserAdmin(sqla.ModelView):
    def is_accessible(self):
        # logger.debug('current user id/query_string id: {0}/{1}'.format(current_user.id, request.args.get('id')))
        is_allowed = False
        if current_user.is_authenticated:
            if current_user.has_role("admins"):
                is_allowed = True
            elif str(current_user.id) == str(request.args.get("id")):
                is_allowed = True
            else:
                is_allowed = False
        # else:
        #    logger.debug("user not allowed (not authenticated)")
        # logger.debug('returning: {0}'.format(is_allowed))
        return is_allowed

    def inaccessible_callback(self, name, **kwargs):
        # redirect to login page if user does not have access.
        return redirect(url_for("security.login"))

    def _handle_view(self, name, **kwargs):
        if not self.is_accessible():
            return self.inaccessible_callback(name, **kwargs)
        if current_user.has_role("admins"):
            self.can_delete = True
        else:
            self.can_delete = False

    # can_view_details = True
    # details_modal = True

    # default sort by name
    column_default_sort = ("name", False)

    # Don't display the password on the list of Users
    column_exclude_list = "password"

    column_searchable_list = ["username", "note"]

    # Don't include the standard password field when creating or editing a User (but see below)
    form_excluded_columns = "password"

    page_size = 20

    # Automatically display human-readable names for the current and available Roles when creating or editing a User
    column_auto_select_related = True

    def _note_formatter(view, context, model, name):
        return Markup(f"{model.note}") if model.note else ""

    column_formatters = {"note": _note_formatter}

    # On the form for creating or editing a User, don't display a field corresponding to the model's password field.
    # There are two reasons for this. First, we want to encrypt the password before storing in the database. Second,
    # we want to use a password field (with the input masked) rather than a regular text field.
    def scaffold_form(self):
        # Start with the standard form as provided by Flask-Admin. We've already told Flask-Admin to exclude the
        # password field from this form.
        form_class = super(UserAdmin, self).scaffold_form()

        # Add a password field, naming it "password2" and labeling it "New Password".
        form_class.password2 = fields.PasswordField("New Password")
        return form_class

    # This callback executes when the user saves changes to a newly-created or edited User -- before the changes are
    # committed to the database.
    def on_model_change(self, form, model, is_created):
        # If the password field isn't blank...
        if len(model.password2):
            model.password = utils.encrypt_password(model.password2)


class UserAdminUser(sqla.ModelView):
    def is_accessible(self):
        # logger.debug('current user id/query_string id: {0}/{1}'.format(current_user.id, request.args.get('id')))
        is_allowed = False
        if current_user.is_authenticated:
            if current_user.has_role("admins"):
                is_allowed = True
            elif str(current_user.id) == str(request.args.get("id")):
                is_allowed = True
            else:
                is_allowed = False
        # else:
        #    logger.debug("user not allowed (not authenticated)")
        # logger.debug('returning: {0}'.format(is_allowed))
        return is_allowed

    def inaccessible_callback(self, name, **kwargs):
        # redirect to login page if user does not have access.
        return redirect(url_for("security.login"))

    def _handle_view(self, name, **kwargs):
        if not self.is_accessible():
            return self.inaccessible_callback(name, **kwargs)
        if current_user.has_role("admins"):
            self.can_delete = True
        else:
            self.can_delete = False

    # can_view_details = True
    # details_modal = True

    # default sort by name
    column_default_sort = ("name", False)

    # Don't display the password on the list of Users
    column_exclude_list = "password"

    column_searchable_list = ["username", "note"]

    # Don't include the standard password field when creating or editing a User (but see below)
    form_excluded_columns = (
        "password",
        "roles",
        "active",
        "confirmed_at",
    )

    page_size = 20

    # Automatically display human-readable names for the current and available Roles when creating or editing a User
    column_auto_select_related = True

    def _note_formatter(view, context, model, name):
        return Markup(f"{model.note}") if model.note else ""

    column_formatters = {"note": _note_formatter}

    form_widget_args = {
        "nickname": {
            "placeholder": "enter short (2-3 character) nickname",
            "title": "nickname (max 5 characters), should be initial or something short",
        }
    }
    column_labels = dict(nickname="Nickname")

    # On the form for creating or editing a User, don't display a field corresponding to the model's password field.
    # There are two reasons for this. First, we want to encrypt the password before storing in the database. Second,
    # we want to use a password field (with the input masked) rather than a regular text field.
    def scaffold_form(self):
        # Start with the standard form as provided by Flask-Admin. We've already told Flask-Admin to exclude the
        # password field from this form.
        form_class = super(UserAdminUser, self).scaffold_form()

        # Add a password field, naming it "password2" and labeling it "New Password".
        form_class.password2 = fields.PasswordField("New Password")
        return form_class

    # This callback executes when the user saves changes to a newly-created or edited User -- before the changes are
    # committed to the database.
    def on_model_change(self, form, model, is_created):
        # If the password field isn't blank...
        if len(model.password2):
            model.password = utils.encrypt_password(model.password2)


class ClientView(sqla.ModelView):
    def is_accessible(self):
        return True if current_user.is_authenticated and current_user.has_role("admins") else False

    def inaccessible_callback(self, name, **kwargs):
        return redirect(url_for("ldap_login_view"))

    def _handle_view(self, name, **kwargs):
        if not self.is_accessible():
            return self.inaccessible_callback(name, **kwargs)
        self.can_delete = True if current_user.has_role("admins") else False

    page_size = 20
    can_view_details = True
    create_modal = False
    edit_modal = False
    details_modal = False
    can_export = True

    form_label = "Clients"

    column_searchable_list = ["name", "nickname", "note"]
    column_filters = ["name", "nickname", "active", "preferred_invoice_format"]
    column_editable_list = [
        "order",
        "name",
        "nickname",
        "active",
        "note",
        "preferred_invoice_format",
    ]
    column_list = [
        "order",
        "name",
        "nickname",
        "active",
        "note",
        "preferred_invoice_format",
    ]

    form_columns = (
        "name",
        "nickname",
        "active",
        "preferred_invoice_format",
        "order",
        "note",
    )
    # column_exclude_list = ["note"]
    column_default_sort = ("order", False)

    form_widget_args = {
        "name": {
            "placeholder": "Client A, Client B, etc.",
            "title": "the name of the Client",
        },
        "nickname": {
            "placeholder": "ca, cb, etc.",
            "title": "a short name for the Client (displayed in most places throughout the app)",
        },
        "active": {"placeholder": "yes/no", "title": "is the Client active?"},
        "note": {
            "placeholder": "Client A has been a great client.",
            "title": "notes about the Client",
        },
        "order": {
            "placeholder": "99",
            "title": "the order for the item (lower order will be sorted to the top of lists)",
        },
        "preferred_invoice_format": {
            "placeholder": "summary|detail",
            "title": "preferred invoice format (summary|detail)",
        },
    }


class ProjectView(sqla.ModelView):
    def is_accessible(self):
        return True if current_user.is_authenticated and current_user.has_role("admins") else False

    def inaccessible_callback(self, name, **kwargs):
        return redirect(url_for("ldap_login_view"))

    def _handle_view(self, name, **kwargs):
        if not self.is_accessible():
            return self.inaccessible_callback(name, **kwargs)
        self.can_delete = True if current_user.has_role("admins") else False

    page_size = 20
    can_view_details = True
    create_modal = False
    edit_modal = False
    details_modal = False
    can_export = True

    form_label = "Projects"

    column_searchable_list = ["name", "nickname", "note"]
    column_filters = [
        "name",
        "nickname",
        "active",
        "rate",
        "client",
        "expected_hours_per_day",
    ]
    column_editable_list = [
        "name",
        "nickname",
        "active",
        "rate",
        "client",
        "note",
        "order",
        "expected_hours_per_day",
    ]
    column_list = [
        "client",
        "order",
        "name",
        "nickname",
        "active",
        "rate",
        "expected_hours_per_day",
        "note",
    ]
    # column_exclude_list = ["note"]
    column_default_sort = ("order", False)
    column_labels = dict(expected_hours_per_day="Exp Hours")

    form_widget_args = {
        "client": {
            "placeholder": "Client A",
            "title": "the Client the Project belongs to",
        },
        "name": {
            "placeholder": "Project A, Project B, etc.",
            "title": "the name of the Project",
        },
        "nickname": {
            "placeholder": "prj-a, prj-b, etc.",
            "title": "a short name for the Project (displayed in most places throughout the app)",
        },
        "active": {"placeholder": "yes/no", "title": "is the project active?"},
        "Rate": {
            "placeholder": "$120.00",
            "title": "the invoice rate for this project",
        },
        "note": {
            "placeholder": "this project is for support of feature A, B and C...",
            "title": "notes about the Project",
        },
        "order": {
            "placeholder": "99",
            "title": "the order for the item (lower order will be sorted to the top of lists)",
        },
        "expected_hours_per_day": {
            "placeholder": "1",
            "title": "expected hours per day for project (keep in mind a 7 day work week - e.g. 1hr a day 5 days a week would be 5/7=0.714286)",
        },
    }


class TaskView(sqla.ModelView):
    def is_accessible(self):
        return True if current_user.is_authenticated and current_user.has_role("admins") else False

    def inaccessible_callback(self, name, **kwargs):
        return redirect(url_for("ldap_login_view"))

    def _handle_view(self, name, **kwargs):
        if not self.is_accessible():
            return self.inaccessible_callback(name, **kwargs)
        self.can_delete = True if current_user.has_role("admins") else False

    page_size = 20
    can_view_details = True
    create_modal = True
    edit_modal = True
    details_modal = True
    can_export = True

    form_label = "Tasks"

    column_searchable_list = ["name", "nickname", "note", "ticket"]
    column_filters = ["name", "nickname", "complete", "project", "ticket"]
    column_editable_list = [
        "name",
        "nickname",
        "complete",
        "project",
        "note",
        "ticket",
        "order",
    ]
    column_list = ["project", "order", "name", "nickname", "ticket", "complete", "note"]
    # column_exclude_list = ["note"]
    # sort by name, descending
    column_default_sort = ("order", False)

    form_widget_args = {
        "project": {
            "placeholder": "Project 1",
            "title": "the Project the Task belongs to",
        },
        "name": {
            "placeholder": "Task 1, Task 2, etc.",
            "title": "the name of the Task",
        },
        "nickname": {
            "placeholder": "task1, task2, etc.",
            "title": "a short name for the Task (displayed in most places throughout the app)",
        },
        "ticket": {
            "placeholder": "https://wwnorton2.atlassian.net/browse/UWP-214",
            "title": "issue/task ticket info such as the JIRA link to the ticket",
        },
        "complete": {"placeholder": "yes/no", "title": "is the task complete?"},
        "note": {
            "placeholder": "this task is for building widget 1...",
            "title": "notes about the Task",
        },
        "order": {
            "placeholder": "99",
            "title": "the order for the item (lower order will be sorted to the top of lists)",
        },
    }
