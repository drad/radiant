# View Utils

import calendar
import logging
import os
import uuid
from datetime import date, datetime, timedelta
from io import StringIO

import arrow
from dateutil.relativedelta import relativedelta
from models.core import Client, Project, Task
from models.invc_reg import Registration

logger = logging.getLogger("main_logger")


def convert_bytes(num):
    """
    Convert bytes to MB.... GB... etc.
    """

    for x in ["bytes", "KB", "MB", "GB", "TB"]:
        if num < 1024.0:
            return "%3.1f %s" % (num, x)
        num /= 1024.0


def get_total_days():
    """
    Get the total number of days between now and inception
    """

    # ~ start = date(2014, 5, 15)
    start = date(2018, 10, 5)
    end = date.today()
    delta = end - start
    logger.debug(f"- get_total_days: {delta.days}")
    return delta.days


def determine_timespan_base(timespan=None):
    """
    Determine the timespan base.
    """

    if timespan == "today":
        return 1
    elif timespan == "week":
        return 7
    elif timespan == "month":
        now = datetime.now()
        return calendar.monthrange(now.year, now.month)[1]
    elif timespan == "year":
        return 365
    elif timespan == "all":
        return get_total_days()
    else:
        return int(timespan)


def determine_timespan(timespan=None):
    """
    Determine timespan given input; numbers are simply returned, everything else
    is mapped. the response is a dict: { type: 'number|string', timespan: timespan }
    """
    if timespan:
        try:
            return {"type": "number", "timespan": int(timespan)}
        except ValueError:
            if timespan == "today":
                return {
                    "type": "string",
                    "timespan": 0.1,
                }  # note: we use 0.1 to avoid divion-by-zero error (floor() keeps us on correct date)
            elif timespan == "week":
                return {
                    "type": "string",
                    "timespan": datetime.now().date().weekday() + 1,
                }  # weekday is the day of the week
            elif timespan == "month":
                return {"type": "string", "timespan": (datetime.now().date().day - 1)}
            elif timespan == "year":
                return {
                    "type": "string",
                    "timespan": (datetime.now().timetuple().tm_yday - 1),
                }
            elif timespan == "all":
                return {"type": "string", "timespan": get_total_days()}


def humanized_time(in_seconds: None):
    attrs = ["days", "hours", "minutes", "seconds"]
    human_readable = lambda delta: [  # noqa: E731
        "%d %s" % (getattr(delta, attr), getattr(delta, attr) > 1 and attr or attr[:-1])
        for attr in attrs
        if getattr(delta, attr)
    ]
    return (
        "".join(human_readable(relativedelta(seconds=in_seconds)))
        .replace(" seconds", "s")
        .replace(" minutes", "m")
        .replace(" minute", "m")
        .replace(" hours", "h")
        .replace(" hour", "h")
        .replace(" days", "D ")
        .replace(" day", "D ")
    )


def get_hour_summary(timespan=None):
    """
    Get hour summary data for a given timespan.
    """

    ts = determine_timespan(timespan)
    ts_base = determine_timespan_base(timespan)

    logger.debug(f"- timespan={timespan}, ts={ts}, ts_base={ts_base}")

    day_adjustment = datetime.today() - timedelta(days=ts["timespan"])
    registrations = None
    if ts["type"] == "string":
        day_adjustment = arrow.get(day_adjustment).to("local").floor("day").datetime
        # logger.debug(f"Query start filter: {arrow.get(day_adjustment).to('local')}")
    # @TODO: find a way to add the filter rather than have two different queries here.
    if timespan == "all":
        registrations = (
            Registration.query.join(Task)
            .join(Project)
            .join(Client)
            .order_by(Client.nickname)
            .order_by(Project.nickname)
        )
    else:
        registrations = (
            Registration.query.join(Task)
            .join(Project)
            .join(Client)
            .filter(Registration.start >= day_adjustment)
            .order_by(Client.nickname)
            .order_by(Project.nickname)
        )

    rollup_data = []
    for r in registrations:
        # ~ logger.debug(f" rollup ==> id={r.id}; start={r.start}; end={r.end} client={r.task.project.client.nickname}; project={r.task.project.nickname}; hours={r.length / 60 / 60} ; expected_hours={r.task.project.expected_hours_per_day}; hourly_rate={r.task.project.rate};")
        rollup_data = rollup_add(
            {
                "client_project": "{0}/{1}".format(
                    r.task.project.client.nickname, r.task.project.nickname
                ),
                "hours": (r.length / 60 / 60),
                "expected_adjusted_hours": float(r.task.project.expected_hours_per_day)
                * ts["timespan"],  # noqa: W503
                "expected_hours": float(r.task.project.expected_hours_per_day)
                * ts_base,  # noqa: W503
                "hourly_rate": float(r.task.project.rate),
            },
            rollup_data,
        )
    return rollup_data


def list_files(startpath):
    """
    List files, (call with: r = list_files("/dir/to/list"))
    """
    output = StringIO()
    for root, dirs, files in os.walk(startpath):
        level = root.replace(startpath, "").count(os.sep)
        indent = " " * 4 * (level)
        output.write(f"{indent}{os.path.basename(root)}/\n")
        subindent = " " * 4 * (level + 1)
        for f in files:
            file_path = f"{root}/{f}"
            file_size = convert_bytes(os.path.getsize(file_path))
            file_modified = os.path.getmtime(file_path)
            file_modified = (
                arrow.Arrow.utcfromtimestamp(os.path.getmtime(file_path))
                .to("local")
                .format("YYYY-MM-DD HH:mm")
            )
            output.write(f"{subindent}{f} ({file_size} - {file_modified})\n")

    return output.getvalue()


def prefix_name(obj, file_data):
    """
    Add uuid to beginning of file name as all attachments are stored in
    the same directory.
    """

    return f"{uuid.uuid4()}_{file_data.filename}"


def rollup_add(data, rollup):
    """
    Roll up data on client_project; if client_project is in rollup then
    add it's hours to the hours in rollup, otherwise add data to rollup.
    """

    if rollup:
        for r in rollup:
            if r["client_project"] == data["client_project"]:
                # ~ logger.debug("- matched (increment hours by: {2} from: {0} to: {1})...".format(r["hours"], (r["hours"] + data["hours"]), data["hours"]))
                r["hours"] = r["hours"] + data["hours"]
                return rollup
        # ~ logger.debug("- no match, add to list...")
        rollup.append(data)
    else:
        # ~ logger.debug("- no items in list, adding...")
        rollup.append(data)

    return rollup


def seconds_to_minutes(in_seconds: None):
    """
    Format input time (in seconds) to decimal minutes notation. For example:
    10789.0 seconds would be 179.8166666666.
    """
    return f"{round(in_seconds / 60, 3)}"
