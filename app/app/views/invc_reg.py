# Invoicing and Registrations views.

import datetime
import logging
import random

import arrow
import markdown
import pygal
from dateutil import tz
from flask import flash, redirect, request, url_for
from flask_admin import BaseView, expose
from flask_admin.actions import action
from flask_admin.contrib import sqla
from flask_security import current_user
from markupsafe import Markup
from models.core import Client, Project, Task, db
from models.invc_reg import Invoice, InvoiceStatus, Registration, RegistrationTicketType
from wtforms import fields, validators

from ._utils import get_hour_summary, humanized_time

logger = logging.getLogger("main_logger")

#
# NOTICE: custom forms must come above views.
#


class InvoiceRegistrationHelpView(BaseView):
    @expose("/")
    def index(self):
        return self.render("help/invoicing-registration.html")


class HourSummaryView(BaseView):
    def is_accessible(self):
        return True if current_user.is_authenticated and current_user.has_role("admins") else False

    def inaccessible_callback(self, name, **kwargs):
        return redirect(url_for("ldap_login_view"))

    def _handle_view(self, name, **kwargs):
        if not self.is_accessible():
            return self.inaccessible_callback(name, **kwargs)
        self.can_delete = True if current_user.has_role("admins") else False

    @expose("/")
    def index(self):
        timespan = request.args.get("timespan") if request.args.get("timespan") else "week"
        from pygal.style import CleanStyle

        actual = []
        expected = []
        expected_adj = []
        labels = []
        chart = pygal.Bar(
            interpolate="cubic",
            style=CleanStyle,
            fill=True,
            show_y_guides=False,
            show_x_guides=False,
            human_readable=True,
        )
        chart.height = 300
        chart.title = f"Hours for {timespan.capitalize()}"
        for r in get_hour_summary(timespan=timespan):
            logger.debug("- processing r of: {0}".format(r))
            val = r["hours"] * r["hourly_rate"]
            # logger.debug(f"  --> adding with value=[{r['hours']}], label=[${value:,.2f}], max_value=[{r['expected_hours']}]")
            actual.append({"value": r["hours"], "label": f"actual - ${val:,.2f}"})
            expected.append({"value": r["expected_hours"], "label": "expected over timespan"})
            expected_adj.append(
                {
                    "value": r["expected_adjusted_hours"],
                    "label": "adjusted for day in week",
                }
            )
            labels.append(r["client_project"])

        chart.x_labels = labels
        chart.add("Actual", actual)
        chart.add("Adjusted", expected_adj)
        chart.add("Expected", expected)

        return self.render("admin/reports/hour-summary.html", chart=chart)


class InvoiceSummaryView(BaseView):
    def is_accessible(self):
        return True if current_user.is_authenticated and current_user.has_role("admins") else False

    def inaccessible_callback(self, name, **kwargs):
        return redirect(url_for("ldap_login_view"))

    def _handle_view(self, name, **kwargs):
        if not self.is_accessible():
            return self.inaccessible_callback(name, **kwargs)
        self.can_delete = True if current_user.has_role("admins") else False

    @expose("/")
    def index(self):
        backups_listing = ""
        return self.render("invoice-summary.html", backups_listing=backups_listing)


class InvoiceView(sqla.ModelView):
    details_template = "admin/invoice/details.html"
    details_modal_template = "admin/invoice/details.html"

    def is_accessible(self):
        return True if current_user.is_authenticated and current_user.has_role("admins") else False

    def inaccessible_callback(self, name, **kwargs):
        return redirect(url_for("ldap_login_view"))

    def _handle_view(self, name, **kwargs):
        if not self.is_accessible():
            return self.inaccessible_callback(name, **kwargs)
        self.can_delete = True if current_user.has_role("admins") else False

    def _invoice_items_formatter(view, context, model, name):
        return "model.invoice_items.name"

    def _submitted_formatter(view, context, model, name):
        if model.submitted is not None:
            return Markup(
                f'<div class="inline-tooltip" title="{arrow.get(model.submitted, "US/Eastern").format("YYYY-MM-DD HH:mm:ss")}">{arrow.get(model.submitted, "US/Eastern").format("MMM, DD")}</div>'
            )

    def _due_formatter(view, context, model, name):
        if model.due is not None:
            return Markup(
                f'<div class="inline-tooltip" title="{arrow.get(model.due, "US/Eastern").format("YYYY-MM-DD HH:mm:ss")}">{arrow.get(model.due, "US/Eastern").format("MMM, DD")}</div>'
            )

    def _paid_formatter(view, context, model, name):
        if model.paid is not None:
            return Markup(
                f'<div class="inline-tooltip" title="{arrow.get(model.paid, "US/Eastern").format("YYYY-MM-DD HH:mm:ss")}">{arrow.get(model.paid, "US/Eastern").format("MMM, DD")}</div>'
            )

    def _deposited_formatter(view, context, model, name):
        if model.deposited is not None:
            return Markup(
                f'<div class="inline-tooltip" title="{arrow.get(model.deposited, "US/Eastern").format("YYYY-MM-DD HH:mm:ss")}">{arrow.get(model.deposited, "US/Eastern").format("MMM, DD YYYY")}</div>'
            )

    def _note_formatter(view, context, model, name):
        return Markup(f"{markdown.markdown(model.note, extensions=['fenced_code'])}") if model.note else ""

    def _amount_formatter(view, context, model, name):
        # sum all registrations for invoice, subtract discount
        regs = []
        for reg in model.invoice_items:
            _cost = round(float(reg.length / 3600) * float(reg.task.project.rate), 2)
            regs.append(_cost)
            # logger.debug(f"  ⟶ reg; task: {reg.task}, invoiced: {reg.invoiced}, length: {reg.length}, rate: {reg.task.project.rate}, cost: {_cost}")

        return "${:,.2f}".format(float(sum(regs)) - float(model.discount_amount))

    page_size = 20
    can_set_page_size = True
    can_view_details = True
    create_modal = False
    edit_modal = False
    details_modal = False
    can_export = True

    form_label = "Invoices"

    column_searchable_list = ["name", "note", "note_internal", "invoice_number"]
    column_filters = [
        "id",
        "invoice_number",
        "name",
        # "client",
        "client.nickname",
        "status.name",
        "submitted",
        "due",
        "paid",
        "deposited",
        "invoice_format.name",
        "note",
        "note_internal",
    ]
    column_editable_list = [
        "status",
        "invoice_format",
    ]
    column_list = [
        "client",
        "name",
        "status",
        "submitted",
        "due",
        "paid",
        "deposited",
        "invoice_number",
        "amount",
    ]

    form_excluded_columns = "invoice_items"
    # form_columns = ("task", "start", "end", "note", "tags")

    # column_exclude_list = ["note"]
    column_default_sort = ("due", True)

    column_labels = {
        "invoice_number": "Invoice Number",
        "invoice_format": "Format",
        "note_internal": "Internal Note",
    }

    column_formatters = {
        "invoice_items": _invoice_items_formatter,
        "submitted": _submitted_formatter,
        "due": _due_formatter,
        "paid": _paid_formatter,
        "deposited": _deposited_formatter,
        "note": _note_formatter,
        "amount": _amount_formatter,
    }

    form_args = {
        "client": {
            # filter to only show 'active' clients.
            "query_factory": lambda: Client.query.filter_by(active=True).order_by(Client.name)
        }
    }

    form_widget_args = {
        "name": {
            "placeholder": "May 2019, June 2019, etc.",
            "title": "the name of the invoice",
        },
        "submitted": {
            "placeholder": "2019-01-01 00:00:00",
            "title": "date invoice was submitted to client",
        },
        "due": {"placeholder": "2019-01-01 00:00:00", "title": "date invoice is due"},
        "paid": {
            "placeholder": "2019-01-01 00:00:00",
            "title": "date invoice was paid",
        },
        "deposited": {
            "placeholder": "2019-01-01 00:00:00",
            "title": "date invoice is deposited",
        },
        "note": {
            "placeholder": "normal monthly invoice...",
            "title": "notes about the Invoice",
            "class": "markdown",
        },
        "note_internal": {
            "placeholder": "internal note...",
            "title": "Internal note about the invoice (not sent to client)",
            "class": "markdown",
        },
        "invoice_items": {
            "placeholder": "invoice registations...",
            "title": "registrations in the invoice",
        },
        "invoice_number": {
            "placeholder": "invoice number (format: YYYY-MM-DD-<invoice-id>-<two-digit-sequence>)",
            "title": "the invoice number",
        },
    }

    @expose("/mark_submit/<pk>", methods=["GET"])
    def mark_submit(self, pk):
        invoice = Invoice.query.get(pk)
        if invoice.mark_submit():
            flash("Successfully Marked Invoice as Submitted!", "info")
        else:
            flash("Failed Marking Invoice as Submitted.", "error")
        return redirect(url_for("invoices.index_view"))

    @expose("/mark_paid/<pk>", methods=["GET"])
    def mark_paid(self, pk):
        invoice = Invoice.query.get(pk)
        if invoice.mark_paid():
            flash("Successfully Marked Invoice as Paid!", "info")
        else:
            flash("Failed Marking Invoice as Paid.", "error")
        return redirect(url_for("invoices.index_view"))

    @expose("/mark_deposited/<pk>", methods=["GET"])
    def mark_deposited(self, pk):
        invoice = Invoice.query.get(pk)
        if invoice.mark_deposited():
            flash("Successfully Marked Invoice as Deposited!", "info")
        else:
            flash("Failed Marking Invoice as Deposited.", "error")
        return redirect(url_for("invoices.index_view"))


class InvoiceFormatView(sqla.ModelView):
    def is_accessible(self):
        return True if current_user.is_authenticated and current_user.has_role("admins") else False

    def inaccessible_callback(self, name, **kwargs):
        return redirect(url_for("ldap_login_view"))

    def _handle_view(self, name, **kwargs):
        if not self.is_accessible():
            return self.inaccessible_callback(name, **kwargs)
        self.can_delete = True if current_user.has_role("admins") else False

    page_size = 10
    can_set_page_size = True
    can_view_details = True
    create_modal = True
    edit_modal = True
    details_modal = True
    can_export = True

    column_searchable_list = ["name", "desc"]
    column_filters = ["name", "desc"]
    column_editable_list = ["name", "desc"]
    column_list = ["name", "desc"]
    # ~ column_default_sort = ("order", False)

    column_labels = dict(name="Name", desc="Desc")

    form_widget_args = {
        "name": {"placeholder": "enter name", "title": "name of the format"},
        "desc": {
            "placeholder": "enter description",
            "title": "description of the format",
        },
    }


class InvoiceStatusView(sqla.ModelView):
    def is_accessible(self):
        return True if current_user.is_authenticated and current_user.has_role("admins") else False

    def inaccessible_callback(self, name, **kwargs):
        return redirect(url_for("ldap_login_view"))

    def _handle_view(self, name, **kwargs):
        if not self.is_accessible():
            return self.inaccessible_callback(name, **kwargs)
        self.can_delete = True if current_user.has_role("admins") else False

    page_size = 10
    can_set_page_size = True
    can_view_details = True
    create_modal = True
    edit_modal = True
    details_modal = True
    can_export = True

    column_searchable_list = ["name", "desc"]
    column_filters = ["name", "desc"]
    column_editable_list = ["name", "desc"]
    column_list = ["name", "desc"]
    # ~ column_default_sort = ("order", False)

    column_labels = dict(name="Name", desc="Desc")

    form_widget_args = {
        "name": {"placeholder": "enter name", "title": "name of the status item"},
        "desc": {
            "placeholder": "enter description",
            "title": "description of the status item",
        },
    }


class RegistrationTicketTypeView(sqla.ModelView):
    def is_accessible(self):
        return True if current_user.is_authenticated and current_user.has_role("admins") else False

    def inaccessible_callback(self, name, **kwargs):
        return redirect(url_for("ldap_login_view"))

    def _handle_view(self, name, **kwargs):
        if not self.is_accessible():
            return self.inaccessible_callback(name, **kwargs)
        self.can_delete = True if current_user.has_role("admins") else False

    page_size = 10
    can_set_page_size = True
    can_view_details = True
    create_modal = True
    edit_modal = True
    details_modal = True
    can_export = True

    column_searchable_list = ["name", "desc"]
    column_filters = ["name", "desc"]
    column_editable_list = ["name", "desc"]
    column_list = ["name", "desc"]
    # ~ column_default_sort = ("order", False)

    column_labels = dict(name="Name", desc="Desc")

    form_widget_args = {
        "name": {"placeholder": "enter name", "title": "name of the ticket type"},
        "desc": {
            "placeholder": "enter description",
            "title": "description of the ticket type",
        },
    }


class RegistrationView(sqla.ModelView):
    def is_accessible(self):
        return True if current_user.is_authenticated and current_user.has_role("admins") else False

    def inaccessible_callback(self, name, **kwargs):
        return redirect(url_for("ldap_login_view"))

    def _handle_view(self, name, **kwargs):
        if not self.is_accessible():
            return self.inaccessible_callback(name, **kwargs)
        self.can_delete = True if current_user.has_role("admins") else False

    def get_query(self):
        # if no query params filter to invoiced=false
        al = list(request.args)
        if "page" in al:
            # remove page from al as we dont care about it from filter's perspective
            al.remove("page")
        # logger.debug(f"- query params are: {len(request.args)} | [{request.args}] | [{al}]")
        if len(al) == 0:
            return self.session.query(self.model).filter(
                self.model.invoiced == False  # noqa
            )
        else:
            return self.session.query(self.model)

    def get_count_query(self):
        # if no query params filter to invoiced=false
        al = list(request.args)
        if "page" in al:
            # remove page from al as we dont care about it from filter's perspective
            al.remove("page")
        if len(al) == 0:
            return self.session.query(sqla.view.func.count("*")).filter(
                self.model.invoiced == False  # noqa
            )
        else:
            return self.session.query(sqla.view.func.count("*"))

    page_size = 200
    can_set_page_size = True
    can_view_details = True
    create_modal = True
    edit_modal = True
    details_modal = True
    can_export = True

    form_label = "Registrations"

    column_searchable_list = ["note"]
    column_filters = [
        "ticket_number",
        "ticket_type",
        "task.name",
        "task.nickname",
        "task.project.nickname",
        "task.project.client.nickname",
        "start",
        "end",
        "length",
        "note",
        "tags",
        "invoiced",
    ]
    column_editable_list = [
        "task",
        "ticket_number",
        "ticket_type",
        "note",
        "tags",
    ]
    column_list = [
        "task",
        "start",
        "end",
        "length",
        "ticket_number",
        "ticket_type",
        "note",
        "tags",
        "actions",
    ]

    column_export_list = [
        "task",
        "start",
        "end",
        "length",
        "ticket_number",
        "ticket_type",
        "note",
    ]

    # form_excluded_columns = ("status")
    form_columns = (
        "task",
        "start",
        "end",
        "ticket_number",
        "ticket_type",
        "note",
        "tags",
        # "invoiced",
    )
    # column_exclude_list = ["note"]
    # sort by start, descending
    column_default_sort = ("start", True)

    def _fmt_start_formatter(view, context, model, name):
        return arrow.get(model.start).format("YYYY-MM-DD HH:mm")

    def _fmt_end_formatter(view, context, model, name):
        if model.end:
            return arrow.get(model.end).format("YYYY-MM-DD HH:mm")
        else:
            return ""

    def _fmt_length_formatter(view, context, model, name):
        if model.length:
            return Markup(f"<span title='{model.length} seconds'>{humanized_time(model.length)}</span>")

    def _length_export_formatter(view, context, model, name):
        return model.length if model.length else ""

    def _fmt_actions_formatter(view, context, model, name):
        # need to check to make sure item is complete and good (success)
        if not model.end:
            actions_html = f"""
<div id="registration-actions">
  <a class="icon" href="{url_for("registrations.checkout", pk=model.id)}" title="Punch Out...">
    <span class="fa fa-pencil glyphicon icon-check"></span>
  </a>
</div>
"""
            return Markup(actions_html)

    def _note_formatter(view, context, model, name):
        return Markup(f"{markdown.markdown(model.note, extensions=['fenced_code'])}") if model.note else ""

    def _note_export_formatter(view, context, model, name):
        return model.note if model.note else ""

    column_formatters = {
        "start": _fmt_start_formatter,
        "end": _fmt_end_formatter,
        "length": _fmt_length_formatter,
        "actions": _fmt_actions_formatter,
    }

    # NOTICE: we have moved note formatter to detail, this means no html formatting in the 'list' view
    #   this is because when you add it to the list view it also displays in the inline edit of the list
    #   view which is very annoying.
    column_formatters_detail = {
        "note": _note_formatter,
    }

    column_formatters_export = {
        # "task": ?,
        "start": _fmt_start_formatter,
        "end": _fmt_end_formatter,
        "length": _length_export_formatter,
        "note": _note_export_formatter,
    }

    column_labels = {
        "ticket_number": "Ticket #",
        "ticket_type": "Type",
        "actions": "",
    }

    form_overrides = {
        "start": fields.StringField,
        "end": fields.StringField,
    }

    form_args = {
        "task": {
            # filter to only show 'active' tasks and sort by ordered.
            "query_factory": lambda: Task.query.filter_by(complete=False)
            .join(Project)
            .filter_by(active=True)
            .order_by(Project.order, Task.order)
        },
        "ticket_type": {"query_factory": lambda: RegistrationTicketType.query.order_by(RegistrationTicketType.name)},
    }

    form_widget_args = {
        "task": {
            "placeholder": "select task",
            "title": "the Task that the registration belongs to",
        },
        "start": {
            "format": "%Y-%m-%d %H:%M:%S.%f",
            "placeholder": "start datetime of registration",
            "title": "the start datetime of the registration",
        },
        "end": {
            "format": "%Y-%m-%d %H:%M:%S.%f",
            "placeholder": "end datetime of registration",
            "title": "the end datetime of the registration",
        },
        "tags": {
            "placeholder": "enter tags for registration",
            "title": "any tags for the registration",
        },
        "note": {
            "placeholder": "this task is for building widget 1...",
            "title": "notes about the Task",
            "class": "markdown",
        },
    }

    def on_model_change(self, form, model, is_created):
        if is_created and form.ticket_number:
            # note: we skip this if there is already a '-' in the ticket_number.
            if form.ticket_number.data and "-" not in form.ticket_number.data:
                # @TODO: we currently only use zd (ZenDesk) as this is the most commonly used prefix but
                #        we should ultimately change to a 'prefix' added at the Task level so it can be
                #        configured per task.
                _prefix = "zd"
                _ticket_number = f"{_prefix}-{form.ticket_number.data}"
                form.ticket_number.data = _ticket_number
                model.ticket_number = _ticket_number

        # ~ logger.debug(
        # ~ f"ticket_number: form={form.ticket_number.data}, model={model.ticket_number}, is_created={is_created}"
        # ~ )

        # start/end fields are treated as String fields in the form (to avoid the DatePicker widget which I dislike).
        if hasattr(model, "start") and type(model.start) is datetime.datetime:
            pass
        elif (
            hasattr(model, "start")
            and type(model.start) == str  # noqa
            and len(model.start) < 1  # noqa
        ):
            # important! set to none as opposed to empty string which will cause a date save error on db
            model.start = None
        else:
            form_start = arrow.get(form.start.data, tzinfo=tz.tzlocal()).datetime
            model.start = form_start

        if not (model.end) or hasattr(model, "end") and type(model.end) is str and len(model.end) < 1:
            # important! set to none as opposed to empty string which will cause a date save error on db
            model.end = None
        else:
            # check to ensure end date is not in future!
            model.end = arrow.get(model.end, tzinfo=tz.tzlocal()).datetime
            if hasattr(model, "end") and model.end > arrow.now().datetime:
                raise validators.ValidationError("\nInvalid end date - cannot be in the future!")

        # calculate length
        if model.start and model.end:
            model.calculate_length()

    @action(
        "add_to",
        "Add to Invoice",
        "Add selected registration(s) to Invoice?",
    )
    def action_add_to_invoice(self, ids):
        """
        Add registrations to an invoice
        NOTICE: you can ONLY add registrations to an invoice with a status of 'create' AND there must be only one invoice with this status. The reasoning here is that the 'Add to Invoice' feature will add to the invoice with a status of create - if there are more than one we would not know which one to add to.
        """
        try:
            count = 0
            regs = Registration.query.filter(Registration.id.in_(ids)).all()
            # find the invoice to add to (must be only one in 'create' status)
            create = InvoiceStatus.query.filter(InvoiceStatus.name == "create").first().id
            invs = Invoice.query.filter(Invoice.status_id == create).all()
            if len(invs) == 1:
                inv = invs[0]
                for reg in regs:
                    if reg.is_invoicable():
                        count += 1
                        # mark reg as 'invoiced'
                        reg.invoiced = True
                        db.session.add(reg)
                        inv.invoice_items.append(reg)
                    else:
                        flash(
                            f"Registration failed (not available to invoice): {reg}",
                            "error",
                        )
                db.session.add(inv)
                db.session.commit()
                flash(
                    Markup(
                        f"""{count} registrations added to <a href='{url_for("invoices.index_view", flt1_0=inv.id)}'>{inv.name}</a>."""
                    ),
                    "info",
                )
            else:
                flash(
                    f"Unable to find Invoice to add to, there must be only one invoice with a status of 'create'. Found [{len(invs)}] invoices with status 'create', cannot continue.",
                    "error",
                )

        except Exception as e:
            if not self.handle_view_exception(e):
                raise
            flash(f"Failed to make Invoice. {e}")

    @action(
        "create_invoice",
        "Create Invoice",
        "Create an Invoice out of the selected Registrations?",
    )
    def action_invoice(self, ids):
        try:
            count = 0
            # 2023-11-15: changed to order by start - DESC so we get the last one first which means the
            #   first registration will be the latest to make the name use the latest regisration's date.
            regs = Registration.query.filter(Registration.id.in_(ids)).order_by(Registration.start.desc()).all()
            client_names = []
            project_names = []
            invoice_date = ""
            for reg in regs:
                if reg.is_invoicable():
                    # grab month from first reg to use in the Invoice name.
                    if count == 0:
                        invoice_date = (arrow.get(reg.start).to("local").format("YYYY MMM"),)
                    count += 1
                    client_names.append(reg.task.project.client)
                    project_names.append(reg.task.project.nickname)
                    # mark reg as 'invoiced'
                    reg.invoiced = True
                    db.session.add(reg)
                else:
                    flash(
                        f"Registration failed (not available to invoice): {reg}",
                        "error",
                    )
            # remove duplicates from client_names.
            clients = list(set(client_names))
            # remove duplicates from project_names.
            projects = list(set(project_names))
            # create new invoice
            # invoice_name format: "{year} > {mon} > {nick} > {prj}":
            #   - year: year of first registration in registration stack
            #   - mon: month of first registration in registration stack
            #   - nick: first client nickname
            #   - prj: list of all projects of registrations joined by /
            invoice_name = f"{invoice_date[0]} > {clients[0].nickname} > {'/'.join(list(dict.fromkeys(projects)))}"
            # invoice_number: <invoice-date>-<invoice-id>-<two-digit-revno> (e.g. 2019-05-21-607398165463-00)
            #   invoice-date:    YYYY-MM-DD formatted date of date invoice was created
            #   invoice-id:      random 12 digit number
            #   two-digit-revno: two digit revision number (manually incremented if needed)
            # invoice_number = f"{arrow.utcnow().format('YYYY-MM-DD')}-{'%012d' % random.randrange(10 ** 12)}-00"
            # invoice number format: "{now}-{seq}":
            #  - now: is current date/time represented as a timestamp
            #  - seq: a random 4 digit number
            invoice_number = f"{arrow.now().int_timestamp}-{'%02d' % random.randrange(10 ** 2)}"  # nosec

            if len(clients) > 1:
                flash(
                    f"Invoice creation failed, invoice cannot contain multiple clients: {', '.join(clients)}",
                    "error",
                )
            else:
                invoice = Invoice(
                    status_id=1,
                    name=invoice_name,
                    client_id=clients[0].id,
                    invoice_items=regs,
                    invoice_number=invoice_number,
                    invoice_format_id=clients[0].preferred_invoice_format_id,
                )
                db.session.add(invoice)
                db.session.commit()
                flash(
                    Markup(
                        f"""New Invoice created <a href='{url_for("invoices.index_view", flt1_0=invoice.id)}'>{invoice.name}</a> with {count} registrations added to Invoice, please review/update as needed."""
                    ),
                    "info",
                )
        except Exception as e:
            if not self.handle_view_exception(e):
                raise
            flash(f"Failed to make Invoice. {e}")

    @expose("/checkout/<pk>", methods=["GET"])
    def checkout(self, pk):
        reg = Registration.query.get(pk)
        if reg.checkout():
            flash("Successfully Punched Out!")
        else:
            flash("Failed Punching Out (already punched out).")
        return redirect(url_for("registrations.index_view"))
