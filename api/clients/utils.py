#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#  SPDX-License-Identifier: GPL-3.0-only
#  Copyright 2025 dradux.com

import aiocouch
from clients.models import _db
from config.config import get_cdb


async def get_by_id(_id: str):
    """
    Get a client by id
    """

    couchdb = get_cdb()
    db = await couchdb[_db.value]

    try:
        return await db[_id]
    except aiocouch.exception.NotFoundError:
        return None
