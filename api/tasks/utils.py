#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#  SPDX-License-Identifier: GPL-3.0-only
#  Copyright 2025 dradux.com

import logging

import aiocouch
from config.config import get_cdb
from fastapi.encoders import jsonable_encoder
from projects.utils import get_enhanced_by_id as get_enhanced_project_by_id
from tasks.models import _db
from _common.utils import fix_id

logger = logging.getLogger("default")


async def get_by_id(_id: str):
    """
    Get task by id
    """

    couchdb = get_cdb()
    db = await couchdb[_db.value]

    try:
        return await db[_id]
    except aiocouch.exception.NotFoundError:
        return None


async def get_by_project_id(_project_id: str):
    """
    Get all tasks for a given project id.
    """

    couchdb = get_cdb()
    # ~ logger.debug(f"Get tasks for project_id={_project_id}")
    db = await couchdb[_db.value]

    _selector = {"project_id": _project_id}

    docs = []
    async for doc in db.find(selector=_selector):
        _doc = fix_id(doc)
        docs.append(_doc)

    return docs


async def get_enhanced_by_id(_id: str):
    """
    Get enhanced task (with client and project data) by id
    """

    # first get the task so we have the project id
    _task = await get_by_id(_id)
    if _task:
        # now get the enhanced project.
        logger.debug(f"- getting enhanced project for id={_task['project_id']}")
        _project = await get_enhanced_project_by_id(_task["project_id"])
        # ~ logger.debug(f"- enhanced project:\n{_project}")
        _task["project"] = jsonable_encoder(_project)
        # ~ logger.debug(f"- enhanced task:\n{_task}")

    return _task
