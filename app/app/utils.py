import logging

logger = logging.getLogger("main_logger")


class PrefixMiddleware(object):
    def __init__(self, app, prefix=""):
        self.app = app
        self.prefix = prefix

    def __call__(self, environ, start_response):
        try:
            if environ["PATH_INFO"].startswith(self.prefix):
                environ["PATH_INFO"] = environ["PATH_INFO"][
                    len(self.prefix) :  # noqa: E203
                ]  # noqa: E203
                environ["SCRIPT_NAME"] = self.prefix
                return self.app(environ, start_response)
            else:
                start_response("404", [("Content-Type", "text/plain")])
                return ["This url does not belong to the app.".encode()]
        except Exception as e:
            logger.debug(f"An error occurred in adjusting the path:\n{e}")
            start_response("500", [("Content-Type", "text/plain")])
            return [f"Path Error: {e}".encode()]
