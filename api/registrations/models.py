#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#  SPDX-License-Identifier: GPL-3.0-only
#  Copyright 2025 dradux.com

import logging
from datetime import datetime
from typing import List
from decimal import Decimal

from config.models import Cdb
from pydantic import BaseModel
from registration_types.models import RegistrationTypeBase
from tasks.models import TaskEnhanced

logger = logging.getLogger("default")
_db = Cdb.REGISTRATIONS


class RegistrationBase(BaseModel):
    """
    Base document
    """

    user: str = None  # user that the registration belongs to
    start: datetime = datetime.now()  # start datetime (note: this is UTC and TZ-aware)
    end: datetime | None = None  # end datetime (note: this is UTC and TZ-aware)
    duration: float | None = None  # [calculated] the duration of the registration in seconds (with fractions)
    task_id: str | None = None  # task of the registration
    # ~ invoiced: bool = False            # has the registration been invoiced (NOTICE: we originally used this but have moved to 'invoice' (str) and no longer use invoiced!
    invoice: str | None = None  # the invoice (id) that the registration belongs to
    ticket_number: str | None = None  # client related ticket number or id
    registration_type: str | None = None  # registration type of the registration
    note: str | None = None  # notes about activity
    tags: List[str] | None = None  # list of tags for a registration


class RegistrationExt(RegistrationBase):
    """
    Extended (added by backend logic)
    """

    # note: this needs to be set/overwrote on result instantiation as using
    #  datetime.now() here will only get you now of when worker was started.
    created: datetime = datetime.now()
    updated: datetime = datetime.now()

    creator: str | None = None
    updator: str | None = None


class Registration(RegistrationExt):
    """
    Actual (at DB level)
    """

    id_: str


class RegistrationWithLabels(Registration):
    """
    Response
    """

    task: TaskEnhanced | None = None
    registration_type_full: RegistrationTypeBase | None = None
    duration_humanized: str | None = None


class RegistrationWithTaskNameResponse(RegistrationBase):
    """
    Registration with Task Name only.
    """

    bill_rate: Decimal | None
    registration_type_name: str | None
    task_name: str | None


class RegistrationResponse(BaseModel):
    """
    Registration Response.
    """

    total: int | None = None
    registrations: List[Registration] | None = None


class RegistrationWithLabelResponse(BaseModel):
    """
    Registration With Labels response.
    """

    total: int | None = None
    registrations: List[RegistrationWithLabels] | None = None


class RegistrationTimeSummaryDetail(BaseModel):
    """
    Time Summary detail.
    """

    length: float | None = None  # total length of time in seconds
    amount: int | None = None  # number of registrations


class RegistrationTimeSummaryReportResponse(BaseModel):
    """
    The Time Summary Report response.

    @NOTE:
    - all time is returned as a float which is amount of time in fractional seconds.
    """

    time_summary_day: RegistrationTimeSummaryDetail | None = None
    time_summary_week: RegistrationTimeSummaryDetail | None = None
    time_summary_month: RegistrationTimeSummaryDetail | None = None
