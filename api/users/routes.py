#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#  SPDX-License-Identifier: GPL-3.0-only
#  Copyright 2025 dradux.com

import logging
import aiocouch

from typing import List
from uuid import UUID
from _common.models import DocInfo
from _common.utils import fix_id
from config.config import get_cdb
from sse.utils import send_sse_message
from sse.models import SSEEventType, SSEActionType
from fastapi import APIRouter, Depends, HTTPException, Query, Security
from fastapi.encoders import jsonable_encoder
from pydantic import HttpUrl
from users.models import (
    User,
    UserBase,
    UserPreferences,
    _db,
    get_current_active_user,
    get_current_user,
)
from users.utils import fix_doc

logger = logging.getLogger("default")
users_router = APIRouter()


async def _get_or_404(_id, db):
    """
    Get a document or raise a 404
    """

    logger.debug(f"_get_or_404 request on: {_id}")
    try:
        return await db[_id]
    except aiocouch.exception.NotFoundError:
        raise HTTPException(status_code=404, detail="Not found")


@users_router.get("/me", response_model=UserBase)
async def me(current_user: UserBase = Depends(get_current_active_user)):
    """
    Get current user info.
    """

    return current_user


@users_router.get("/", response_model=List[User])
async def get_all(
    limit: int = 10,
    skip: int = 0,
    current_user: UserBase = Security(get_current_user, scopes=["admins"]),
):
    """
    Get all
    """

    # @TODO: add limit and skip logic.
    couchdb = get_cdb()
    db = await couchdb[_db.value]

    docs = []
    async for doc in db.docs():
        docs.append(User(**fix_id(doc)))

    return docs


# NOTICE: we do not need an 'add' route in api as users are added through
#   ldap; however, there is a non-exposed add in utils.py.


@users_router.get("/{username}", response_model=User)
async def get_one(
    username: str,
    current_user: UserBase = Security(get_current_user, scopes=["admins"]),
):
    """Get by username"""

    couchdb = get_cdb()
    db = await couchdb[_db.value]

    return User(**await fix_doc(await _get_or_404(username, db)))


# 2020-07-01:drad update of user is limited to updating the following as other items are set via ldap:
#   - disabled: this will disable the user in the app
#   - note: an app specific note for the user
#   - accounts: a list of accounts the user belongs to
@users_router.put(
    "/",
    response_model=dict,
)
async def update(
    username: str = Query(..., description="username of user"),
    enabled: bool = Query(True, description="Is the user enabled?"),
    display_name: str = Query(None, description="name used in application display"),
    avatar: HttpUrl = Query(
        None, description="user's avatar url (use a service such as https://getavataaars.com/ to generate a url)."
    ),
    user_timezone: str = Query(None, description="Timezone for user (e.g. 'US/Eastern')"),
    preferences: UserPreferences | None = None,
    note: str = Query(None, description="note on user"),
    notify_id: str = Query(None, description="user notify id (used by API to store the websockets notification id)"),
    current_user: UserBase = Security(get_current_user, scopes=["users"]),
):
    """
    Update

    __NOTE:__ only items supplied will be updated.
    """

    couchdb = get_cdb()
    db = await couchdb[_db.value]
    doc = await _get_or_404(username, db)
    # ~ fernet = Fernet(API_APP_KEY)
    # logger.debug(f"- updating user;\n  ⟶ preferences: {preferences}")
    doc["enabled"] = enabled if enabled else doc["enabled"]
    doc["note"] = note if note else doc["note"]
    doc["display_name"] = display_name if display_name else doc["display_name"]
    doc["avatar"] = jsonable_encoder(avatar) if avatar else doc["avatar"]
    doc["user_timezone"] = user_timezone if user_timezone else doc["user_timezone"]
    doc["preferences"] = jsonable_encoder(preferences) if preferences else doc["preferences"]
    doc["notify_id"] = UUID(notify_id) if notify_id else None

    # logger.debug(f"- doc before save: {doc}")

    await doc.save()
    # send sse event of update.
    await send_sse_message(user=current_user.username, event_type=SSEEventType.user, data=SSEActionType.update)
    return await doc.info()


@users_router.delete("/{username}", response_model=dict)
async def delete(
    username: str,
    current_user: UserBase = Security(get_current_user, scopes=["admins"]),
):
    """
    Hard Delete by id
    """

    resp = DocInfo(ok=False, id=username, rev="", msg="deleted")
    couchdb = get_cdb()
    db = await couchdb[_db.value]
    doc = await _get_or_404(username, db)
    dr = await doc.delete()
    if dr and dr.status < 400:
        resp.ok = True
        resp.msg = "deleted"
        # send sse event of doc delete.
        await send_sse_message(user=current_user.username, event_type=SSEEventType.user, data=SSEActionType.delete)
    else:
        resp.ok = False
        resp.msg = f"Delete status: {dr.status}, headers: {dr.headers}"

    return resp
