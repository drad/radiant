import datetime

from .core import db


class FilesystemBenchmarkFsType(db.Model):
    __tablename__ = "filesystem_benchmark_fs_type"
    id = db.Column(db.Integer, primary_key=True)
    order = db.Column(db.SmallInteger, nullable=False, default=99)
    name = db.Column(db.String(20), nullable=False)

    def __str__(self):
        return f"{self.name}"


class FilesystemBenchmarkFsKind(db.Model):
    __tablename__ = "filesystem_benchmark_fs_kind"
    id = db.Column(db.Integer, primary_key=True)
    order = db.Column(db.SmallInteger, nullable=False, default=99)
    name = db.Column(db.String(10), nullable=False)

    def __str__(self):
        return f"{self.name}"


class FilesystemBenchmarkRunLocationType(db.Model):
    __tablename__ = "filesystem_benchmark_run_location_type"
    id = db.Column(db.Integer, primary_key=True)
    order = db.Column(db.SmallInteger, nullable=False, default=99)
    name = db.Column(db.String(10), nullable=False)

    def __str__(self):
        return f"{self.name}"


class FilesystemBenchmarkDiskType(db.Model):
    __tablename__ = "filesystem_benchmark_disk_type"
    id = db.Column(db.Integer, primary_key=True)
    order = db.Column(db.SmallInteger, nullable=False, default=99)
    name = db.Column(db.String(10), nullable=False)

    def __str__(self):
        return f"{self.name}"


class FilesystemBenchmarkRunMethod(db.Model):
    __tablename__ = "filesystem_benchmark_run_method"
    id = db.Column(db.Integer, primary_key=True)
    order = db.Column(db.SmallInteger, nullable=False, default=99)
    name = db.Column(db.String(10), nullable=False)

    def __str__(self):
        return f"{self.name}"


class FilesystemBenchmark(db.Model):
    __tablename__ = "filesystem_benchmark"
    id = db.Column(db.Integer, primary_key=True)

    run_date = db.Column(db.DateTime, nullable=False, default=datetime.datetime.utcnow)
    run_method_id = db.Column(
        db.Integer(),
        db.ForeignKey("filesystem_benchmark_run_method.id"),
        nullable=False,
    )
    run_method = db.relationship(
        "FilesystemBenchmarkRunMethod", foreign_keys=[run_method_id]
    )
    run_command = db.Column(db.Text, nullable=True)
    run_notes = db.Column(db.Text, nullable=True)
    run_time = db.Column(db.Float(precision=39), nullable=True)

    run_location_host = db.Column(db.String(25), nullable=True)
    run_location_mountpoint = db.Column(db.String(50), nullable=True)
    run_location_type_id = db.Column(
        db.Integer(),
        db.ForeignKey("filesystem_benchmark_run_location_type.id"),
        nullable=False,
    )
    run_location_type = db.relationship(
        "FilesystemBenchmarkRunLocationType", foreign_keys=[run_location_type_id]
    )

    fs_host = db.Column(db.String(25), nullable=False)
    fs_os = db.Column(db.String(20), nullable=True)
    fs_type_id = db.Column(
        db.Integer(), db.ForeignKey("filesystem_benchmark_fs_type.id"), nullable=False
    )
    fs_type = db.relationship("FilesystemBenchmarkFsType", foreign_keys=[fs_type_id])
    fs_kind_id = db.Column(
        db.Integer(), db.ForeignKey("filesystem_benchmark_fs_kind.id"), nullable=False
    )
    fs_kind = db.relationship("FilesystemBenchmarkFsKind", foreign_keys=[fs_kind_id])
    fs_notes = db.Column(db.Text, nullable=True)

    fs_disk_name = db.Column(db.String(25), nullable=False)
    fs_disk_type_id = db.Column(
        db.Integer(), db.ForeignKey("filesystem_benchmark_disk_type.id"), nullable=False
    )
    fs_disk_type = db.relationship(
        "FilesystemBenchmarkDiskType", foreign_keys=[fs_disk_type_id]
    )
    fs_disk_size = db.Column(db.String(10), nullable=True)

    results_read_iops = db.Column(db.Float(precision=28), nullable=False, default=0)
    results_write_iops = db.Column(db.Float(precision=28), nullable=False, default=0)
    results_raw = db.Column(db.Text, nullable=True)
    results_notes = db.Column(db.Text, nullable=True)

    reason = db.Column(db.Text, nullable=False)
