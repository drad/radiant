#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#  SPDX-License-Identifier: GPL-3.0-only
#  Copyright 2025 dradux.com

import logging
from starlette.requests import Request
from sse_starlette.sse import EventSourceResponse
import asyncio
from typing import List
from fastapi import status, APIRouter, HTTPException, Query, Security
from users.models import UserBase, get_current_user
from users.utils import get_by_notify_id
from sse.models import SSEEventType, ESMessage, SSEMessage, SSEMessagesCountResponse
from sse.utils import send_sse_message, get_messages, message_count
from auth.utils import send_user_post_login_ui_events

logger = logging.getLogger("default")
STREAM_DELAY = 1  # second
RETRY_TIMEOUT = 15000  # millisecond

sse_router = APIRouter()


@sse_router.get("/stream/{channel}")
async def message_stream(request: Request, channel: str = "default"):
    """
    Create the message stream.
    NOTES:
      - the 'channel' supplied is in the format of '{user.notify_id}'.
    """

    logger.debug(f"Starting stream for channel={channel} (notify_id)")
    notify_id = channel
    _user = await get_by_notify_id(notify_id=notify_id)
    if not _user:
        # need to raise and error here as we didn't find a user matching the notify_id.
        raise HTTPException(status_code=403, detail="No User (notify_id) found for channel.")

    user = _user.username
    # ~ logger.debug(f"User found, username={user}")

    # send initial user events.
    logger.debug(f"User login - send user post login ui events for username={user}...")
    # create job or do logic to send user's initial SSE events.
    _ret = await send_user_post_login_ui_events(username=user)

    # channel needs to be of the form: {user}
    async def new_messages(user=None):
        # ~ logger.debug(f"Checking for new message with user={user}")
        if await message_count(user=user) > 0:
            logger.debug(f"Found messages for user={user}")
            return True
        else:
            return None

    async def event_generator(user=None):
        while True:
            # If client closes connection, stop sending events
            if await request.is_disconnected():
                break

            # Checks for new messages and return them to client if any
            if await new_messages(user=user):
                # get any/all messages.
                # ~ logger.debug(f"Get messages for user: {user}")
                messages = await get_messages(user=user)
                for message in messages:
                    # note: we need to cast to dict (model_dump()) or the yielded message is not properly formatted.
                    _esm = ESMessage(
                        # ~ id=channel, event=message.event_type.value, retry=RETRY_TIMEOUT, data=message.data
                        id=channel,
                        event=message.event_type,
                        retry=RETRY_TIMEOUT,
                        data=message.event_action,
                    ).model_dump()
                    logger.debug(f"  - SSE Stream [{user}] sending response of: {_esm}")
                    yield _esm

            await asyncio.sleep(STREAM_DELAY)

    return EventSourceResponse(event_generator(user=user), media_type="text/event-stream")


@sse_router.post("/manual_add", response_model=object, status_code=status.HTTP_201_CREATED)
async def manual_add(
    user: str = Query(..., description="user to add sse message for"),
    event_type: SSEEventType = Query(..., description="type of event"),
    data: str = Query(..., description="data to be sent"),
    current_user: UserBase = Security(get_current_user, scopes=["admins"]),
):
    """
    Manual add of SSE Message.
    """

    logger.debug(f"Received manual add with event_type={event_type}")
    await send_sse_message(user=user, event_type=event_type, data=data)

    return {"ok": True}


@sse_router.post("/manual_message_count", response_model=SSEMessagesCountResponse, status_code=status.HTTP_201_CREATED)
async def manual_message_count(
    user: str = Query(..., description="user to add sse message for"),
    current_user: UserBase = Security(get_current_user, scopes=["admins"]),
):
    """
    Manual get Message count.
    """

    logger.debug(f"Received manual get_data_count with user={user}...")
    _ret = await message_count(
        user=user,
    )

    _count = SSEMessagesCountResponse()
    _count.count = _ret

    return _count


@sse_router.post("/manual_get_messages", response_model=List[SSEMessage], status_code=status.HTTP_201_CREATED)
async def manual_get_messages(
    user: str = Query(..., description="user to add sse message for"),
    current_user: UserBase = Security(get_current_user, scopes=["admins"]),
):
    """
    Manual get Messages.
    """

    logger.debug(f"Received manual get_messages with user={user}...")
    _ret = await get_messages(user=user)

    return _ret
