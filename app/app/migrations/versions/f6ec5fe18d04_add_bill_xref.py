"""add bill xref

Revision ID: f6ec5fe18d04
Revises: 17c9a39cde2c
Create Date: 2019-04-04 16:41:26.069769

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "f6ec5fe18d04"
down_revision = "17c9a39cde2c"
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table(
        "bill_registration",
        sa.Column("registration_id", sa.Integer(), nullable=False),
        sa.Column("bill_id", sa.Integer(), nullable=False),
        sa.ForeignKeyConstraint(["bill_id"], ["bill.id"]),
        sa.ForeignKeyConstraint(["registration_id"], ["registration.id"]),
        sa.UniqueConstraint("bill_id"),
    )
    op.add_column("bill", sa.Column("client_id", sa.Integer(), nullable=False))
    op.add_column("bill", sa.Column("name", sa.String(length=50), nullable=False))
    op.create_foreign_key(None, "bill", "client", ["client_id"], ["id"])
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_constraint(None, "bill", type_="foreignkey")
    op.drop_column("bill", "name")
    op.drop_column("bill", "client_id")
    op.drop_table("bill_registration")
    # ### end Alembic commands ###
