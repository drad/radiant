import datetime

from .core import db

server_aa_server_aas = db.Table(
    "server_aa_server_aas",
    db.Column("server_aa_id", db.Integer(), db.ForeignKey("server.id"), nullable=False),
    db.Column(
        "server_id",
        db.Integer(),
        db.ForeignKey("server_additional_attributes.id"),
        nullable=False,
    ),
)

server_storage_server_sds = db.Table(
    "server_storage_server_sds",
    db.Column(
        "server_storage_id", db.Integer(), db.ForeignKey("server.id"), nullable=False
    ),
    db.Column(
        "server_id", db.Integer(), db.ForeignKey("server_storage.id"), nullable=False
    ),
)

server_testresults = db.Table(
    "server_testresults",
    db.Column("testresult_id", db.Integer(), db.ForeignKey("test_result.id")),
    db.Column("server_id", db.Integer(), db.ForeignKey("server.id")),
)

target_serverrunmetric_testresults = db.Table(
    "target_serverrunmetric_testresults",
    db.Column("testresult_id", db.Integer(), db.ForeignKey("test_result.id")),
    db.Column(
        "server_run_metric_id", db.Integer(), db.ForeignKey("server_run_metric.id")
    ),
)

tag_testresults = db.Table(
    "tag_testresults",
    db.Column("testresult_id", db.Integer(), db.ForeignKey("test_result.id")),
    db.Column("tag_id", db.Integer(), db.ForeignKey("tag.id")),
)


class RunMetric(db.Model):
    __tablename__ = "run_metric"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(33), nullable=False)
    note = db.Column(db.Text, nullable=True)
    creator_id = db.Column(db.Integer, db.ForeignKey("user.id"), nullable=False)
    creator = db.relationship("User", foreign_keys=[creator_id])

    def __str__(self):
        return f"{self.name}"


class Server(db.Model):
    __tablename__ = "server"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(20), nullable=False)
    full_name = db.Column(db.String(100), nullable=True)
    active = db.Column(db.Boolean, nullable=True, default=True)
    version = db.Column(db.Integer, nullable=True, default=1)
    cpu_cores = db.Column(
        db.Numeric(5, 2), nullable=False, default=1
    )  # number of cores (999.99)
    memory = db.Column(db.Numeric(9, 1), nullable=False)  # in mb (99,999,999.9)
    notes = db.Column(db.Text, nullable=True)

    server_type_id = db.Column(
        db.Integer, db.ForeignKey("server_type.id"), nullable=True
    )
    server_type = db.relationship("ServerType", foreign_keys=[server_type_id])

    # multiple occurring xref for storage
    storage_details = db.relationship(
        "ServerStorage",
        secondary=server_storage_server_sds,
        backref=db.backref("server", lazy="dynamic"),
    )

    # multiple occurring xref for additional attributes
    additional_attributes = db.relationship(
        "ServerAdditionalAttribute",
        secondary=server_aa_server_aas,
        backref=db.backref("server", lazy="dynamic"),
    )

    creator_id = db.Column(db.Integer, db.ForeignKey("user.id"), nullable=False)
    creator = db.relationship("User", foreign_keys=[creator_id])

    def __str__(self):
        return f"{self.name}"


class ServerAdditionalAttribute(db.Model):
    __tablename__ = "server_additional_attributes"
    id = db.Column(db.Integer, primary_key=True)
    key = db.Column(db.String(25), nullable=False)
    value = db.Column(db.String(255), nullable=False)
    notes = db.Column(db.Text, nullable=True)

    def __str__(self):
        return f"{self.key}:{self.value}"


class ServerStorage(db.Model):
    __tablename__ = "server_storage"
    id = db.Column(db.Integer, primary_key=True)
    storage_size = db.Column(
        db.Numeric(9, 2), nullable=False
    )  # size in gb (9,999,999.99)
    notes = db.Column(db.Text, nullable=True)

    storage_type_id = db.Column(
        db.Integer, db.ForeignKey("storage_type.id"), nullable=True
    )
    storage_type = db.relationship("StorageType", foreign_keys=[storage_type_id])

    def __str__(self):
        return f"{self.storage_type}: {self.storage_size}"


class ServerType(db.Model):
    __tablename__ = "server_type"
    id = db.Column(db.Integer, primary_key=True)
    order = db.Column(db.SmallInteger, nullable=False, default=99)
    name = db.Column(db.String(25), nullable=False)
    notes = db.Column(db.Text, nullable=True)

    def __str__(self):
        return f"{self.name}"


class ServerRunMetric(db.Model):
    __tablename__ = "server_run_metric"
    id = db.Column(db.Integer, primary_key=True)
    run_metric_id = db.Column(
        db.Integer(), db.ForeignKey("run_metric.id"), nullable=False
    )
    run_metric = db.relationship(RunMetric, foreign_keys=[run_metric_id])

    value = db.Column(db.Numeric(5, 2), nullable=False)
    note = db.Column(db.Text, nullable=True)

    def __str__(self):
        return f"{self.run_metric}:{self.value}"


class StorageType(db.Model):
    __tablename__ = "storage_type"
    id = db.Column(db.Integer, primary_key=True)
    order = db.Column(db.SmallInteger, nullable=False, default=99)
    name = db.Column(db.String(25), nullable=False)
    notes = db.Column(db.Text, nullable=True)

    def __str__(self):
        return f"{self.name}"


class TestPlan(db.Model):
    __tablename__ = "test_plan"
    id = db.Column(db.Integer, primary_key=True)
    nickname = db.Column(db.String(6), nullable=False)
    name = db.Column(db.String(100), nullable=False)
    project_id = db.Column(db.Integer(), db.ForeignKey("project.id"), nullable=True)
    project = db.relationship("Project", foreign_keys=[project_id])
    active = db.Column(db.Boolean, nullable=True, default=True)
    version = db.Column(db.String(10), nullable=False)
    source_url = db.Column(db.String, nullable=False)
    summary = db.Column(db.Text, nullable=False)
    description = db.Column(db.Text, nullable=False)
    run_info = db.Column(db.Text, nullable=False)
    dependencies = db.Column(db.Text, nullable=True)
    notes = db.Column(db.Text, nullable=True)

    creator_id = db.Column(db.Integer, db.ForeignKey("user.id"), nullable=False)
    creator = db.relationship("User", foreign_keys=[creator_id])

    def __str__(self):
        return f"{self.project} > {self.nickname}"


class TestResult(db.Model):
    __tablename__ = "test_result"
    id = db.Column(db.Integer, primary_key=True)
    test_date = db.Column(db.DateTime, nullable=False)
    status_id = db.Column(
        db.Integer(), db.ForeignKey("test_result_status.id"), nullable=False
    )
    status = db.relationship("TestResultStatus", foreign_keys=[status_id])

    test_plan_id = db.Column(
        db.Integer(), db.ForeignKey("test_plan.id"), nullable=False
    )
    test_plan = db.relationship("TestPlan", foreign_keys=[test_plan_id])

    source_servers = db.relationship(
        "Server",
        secondary=server_testresults,
        backref=db.backref("test_result", lazy="dynamic"),
    )

    target_server_id = db.Column(
        db.Integer(), db.ForeignKey("server.id"), nullable=False
    )
    target_server = db.relationship(Server, foreign_keys=[target_server_id])
    target_server_quantity = db.Column(db.String(10), nullable=True, default="1")

    run_by_id = db.Column(db.Integer, db.ForeignKey("user.id"), nullable=True)
    run_by = db.relationship("User", foreign_keys=[run_by_id])

    app_version = db.Column(db.String(25), nullable=True)
    number_users = db.Column(db.Integer, nullable=False)
    ramp_up = db.Column(db.Integer, nullable=False)
    loop_amount = db.Column(db.Integer, nullable=True)

    run_length = db.Column(db.Integer, nullable=True)
    number_failures = db.Column(db.Integer, nullable=True)
    number_requests = db.Column(db.Integer, nullable=True)
    average_response_time = db.Column(db.Integer, nullable=True)

    # multiple occurring TargetServerRunMetrics
    target_server_run_metrics = db.relationship(
        "ServerRunMetric",
        secondary=target_serverrunmetric_testresults,
        backref=db.backref("test_result", lazy="dynamic"),
    )
    target_server_run_metrics_url = db.Column(db.String(1333), nullable=True)

    prerun_notes = db.Column(db.Text, nullable=True)
    run_notes = db.Column(db.Text, nullable=True)
    postrun_notes = db.Column(db.Text, nullable=True)
    failure_notes = db.Column(db.Text, nullable=True)
    test_notes = db.Column(db.Text, nullable=True)

    tags = db.relationship(
        "Tag",
        secondary=tag_testresults,
        backref=db.backref("test_result", lazy="dynamic"),
    )

    created_at = db.Column(db.DateTime(), default=datetime.datetime.utcnow)
    creator_id = db.Column(db.Integer, db.ForeignKey("user.id"), nullable=False)
    creator = db.relationship("User", foreign_keys=[creator_id])

    def __str__(self):
        return f"{self.test_plan} ({self.test_date})"


class TestResultStatus(db.Model):
    __tablename__ = "test_result_status"
    id = db.Column(db.Integer, primary_key=True)
    order = db.Column(db.SmallInteger, nullable=False, default=99)
    status = db.Column(db.String(13), nullable=False)
    symbol = db.Column(db.String(1), nullable=False, unique=True)

    def __str__(self):
        return f"{self.status}"
