#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#  SPDX-License-Identifier: GPL-3.0-only
#  Copyright 2025 dradux.com

import logging
import uuid
from datetime import datetime
from typing import Optional

import aiocouch
from _common.utils import make_id
from sse.utils import send_sse_message
from sse.models import SSEEventType, SSEActionType
from config.config import get_cdb
from fastapi import HTTPException
from fastapi.encoders import jsonable_encoder
from user_messages.models import (
    UserMessageCategory,
    UserMessageExt,
    UserMessagePriority,
    UserMessageStatus,
    _db,
)

logger = logging.getLogger("default")


async def add_user_message(
    to_user_id: str = None,
    from_user_id: Optional[str] = None,
    category: UserMessageCategory = None,
    priority: UserMessagePriority = None,
    subject: str = None,
    message: str = None,
    creator: str = None,
):
    """
    Add a message.
    """

    from users.utils import get_by_username

    couchdb = get_cdb()
    db = await couchdb[_db.value]

    # check if 'to' user exists.
    _to = await get_by_username(to_user_id)
    if not _to:
        raise HTTPException(status_code=424, detail="To user does not exist.")
    # if 'from' user present, make sure the user exists.
    if from_user_id:
        _from = await get_by_username(from_user_id)
        if not _from:
            raise HTTPException(status_code=424, detail="From user does not exist.")

    _user_message = UserMessageExt(
        to_user_id=to_user_id,
        from_user_id=from_user_id,
        status=UserMessageStatus.new,
        category=category,
        priority=priority,
        subject=subject,
        message=message,
        created=datetime.now(),
        updated=datetime.now(),
        creator=creator,
    )

    try:
        doc_id = make_id(items=[str(uuid.uuid4())])
        doc = await db.create(
            doc_id,
            data=jsonable_encoder(_user_message),
        )
        await doc.save()
        await send_sse_message(
            user=to_user_id,
            event_type=SSEEventType.user_message,
            data=SSEActionType.new.value,
        )
        return await doc.info()

    except aiocouch.ConflictError:
        logger.error(f"Duplicate key: cannot add '{doc_id}'")
        return None
