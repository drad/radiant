#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#  SPDX-License-Identifier: GPL-3.0-only
#  Copyright 2025 dradux.com

from datetime import date
from enum import Enum
from typing import Optional

from pydantic import BaseModel


class Cdb(Enum):
    """
    Available databases
    """

    # app
    CLIENTS = "clients"
    INVOICES = "invoices"
    PROJECTS = "projects"
    REGISTRATION_TYPES = "registration_types"
    REGISTRATIONS = "registrations"
    TASKS = "tasks"

    # shared
    USERS = "users"
    USER_MESSAGES = "user_messages"
    LOGS = "logs"


class ConfigApiVersions(BaseModel):
    """
    API Versions.
    """

    current: str = None
    supported: str = None
    deprecated: str = None
    unsupported: str = None


class ConfigApiJwt(BaseModel):
    """
    API JWT config.
    """

    algorithm: str = None


class ConfigRouteAuthItem(BaseModel):
    """
    Route Auth Item.
    """

    get_all: Optional[list] = None
    get_one: Optional[list] = None
    add: Optional[list] = None
    update: Optional[list] = None
    delete: Optional[list] = None


class ConfigRouteAuth(BaseModel):
    """
    Route Auth config.
    """

    activities: ConfigRouteAuthItem = None
    related_item_overviews: ConfigRouteAuthItem = None
    related_item_types: ConfigRouteAuthItem = None


class ConfigApi(BaseModel):
    """
    API specific config.
    """

    versions: ConfigApiVersions = None
    jwt: ConfigApiJwt = None
    route_auth: ConfigRouteAuth = None


class ConfigCore(BaseModel):
    """
    Core specific config.
    """

    name: str = None
    description: str = None
    version: str = None
    created: date = None
    modified: date = None


class ConfigBase(BaseModel):
    """
    Config Base.
    """

    api: ConfigApi = None
    core: ConfigCore = None
    # database:
