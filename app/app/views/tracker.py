import logging

import arrow
import markdown
from flask import redirect, url_for

# ~ from flask_admin import BaseView, expose, AdminIndexView
from flask_admin import BaseView, expose
from flask_admin.contrib import sqla

# ~ from wtforms.fields import PasswordField
# ~ from flask_admin.menu import MenuLink
from flask_admin.model.form import InlineFormAdmin

# ~ from flask_security import current_user, utils
from flask_security import current_user
from markupsafe import Markup
from models.core import Project, Tag, User
from models.tracker import (
    Server,
    ServerAdditionalAttribute,
    ServerRunMetric,
    ServerStorage,
    TestPlan,
    TestResultStatus,
)

logger = logging.getLogger("main_logger")

# custom filter for Server: only active servers.
# ~ class FilterActiveServer(BaseSQLAFilter):
# ~ def apply(self, query, value, alias=None):
# ~ if value:
# ~ return query.filter(self.column == True)
# ~ else:
# ~ return query.filter(self.column == False)

# ~ def operation(self):
# ~ return 'is Active'


class TrackerHelpView(BaseView):
    @expose("/")
    def index(self):
        return self.render("help/tracker.html")


class RunMetricView(sqla.ModelView):
    def is_accessible(self):
        return (
            True
            if current_user.is_authenticated and current_user.has_role("admins")
            else False
        )

    def inaccessible_callback(self, name, **kwargs):
        return redirect(url_for("ldap_login_view"))

    def _handle_view(self, name, **kwargs):
        if not self.is_accessible():
            return self.inaccessible_callback(name, **kwargs)
        self.can_delete = True if current_user.has_role("admins") else False

    # note: this yields html code in 'inline' editor.
    def _note_formatter(view, context, model, name):
        return (
            Markup(f"{markdown.markdown(model.note, extensions=['fenced_code'])}")
            if model.note
            else ""
        )

    page_size = 15
    can_view_details = True
    create_modal = True
    edit_modal = True
    details_modal = True
    can_export = True

    column_searchable_list = ["name", "note"]
    column_filters = ["name", "note"]
    column_editable_list = ["name", "note"]
    column_list = ["name", "note", "creator"]
    column_exclude_list = ["creator"]
    # column_labels = dict(cpu_cores='CPU Cores')
    # sort by name, descending
    column_default_sort = ("name", False)

    form_excluded_columns = "creator"

    column_formatters = {
        # ~ "note": _note_formatter
    }

    form_widget_args = {
        "name": {"placeholder": "run metric name"},
        "note": {"placeholder": "notes regarding run metric"},
    }

    def on_model_change(self, form, model, is_created):

        if is_created:
            model.creator_id = current_user.id


class ServerAdditionalAttributesForm(InlineFormAdmin):
    def is_accessible(self):
        return current_user.has_role("users")

    form_columns = ("id", "key", "value", "notes")
    form_label = "Additional Attributes"
    column_labels = {
        "key": "key",
        "value": "value",
    }
    form_widget_args = {
        "key": {
            # 'style': 'width: 33px',
            "placeholder": "a key",
            "title": "a key (e.g. 'ec2Type', 'Form Factor', etc.)",
        },
        "value": {
            # 'style': 'width: 66px',
            "placeholder": "a value",
            "title": "a value (e.g. 'm5.xlarge', 'micro-atx', etc.)",
        },
        "notes": {
            "style": "height: 34px",
            "placeholder": "notes...",
            "title": "notes for additional attribute",
        },
    }


class ServerStorageDetailsForm(InlineFormAdmin):
    def is_accessible(self):
        return current_user.has_role("users")

    form_columns = ("id", "storage_type", "storage_size", "notes")
    form_label = "Storage Details"
    column_labels = {
        "storage_size": "Size",
        "storage_type": "Type",
    }
    form_widget_args = {
        "storage_size": {
            "placeholder": "size (in gb)",
            "title": "storage size in gigabyte - use fraction to denote mb (e.g. 0.1gb = 100mb)",
        },
        "storage_type": {
            "placeholder": "storage type",
            "title": "storage type (SSD, HDD, NFS, etc.)",
        },
        "notes": {
            "style": "height: 34px",
            "placeholder": "notes...",
            "title": "notes for storaqe",
        },
    }


class ServerView(sqla.ModelView):
    def is_accessible(self):
        return (
            True
            if current_user.is_authenticated and current_user.has_role("admins")
            else False
        )

    def inaccessible_callback(self, name, **kwargs):
        return redirect(url_for("ldap_login_view"))

    def _handle_view(self, name, **kwargs):
        if not self.is_accessible():
            return self.inaccessible_callback(name, **kwargs)
        self.can_delete = True if current_user.has_role("admins") else False

    def _notes_formatter(view, context, model, name):
        return Markup(f"{model.notes}") if model.notes else ""

    # example of default filter for list.
    # ~ def get_query(self):
    # ~ return self.session.query(self.model).filter(self.model.active == True)

    inline_models = (
        ServerStorageDetailsForm(ServerStorage),
        ServerAdditionalAttributesForm(ServerAdditionalAttribute),
    )

    page_size = 15
    can_view_details = True
    create_modal = True
    edit_modal = True
    details_modal = True
    can_export = True

    column_searchable_list = ["name", "full_name", "notes"]
    column_filters = [
        "server_type.name",
        "name",
        "full_name",
        "cpu_cores",
        "memory",
        "creator.username",
        "notes",
        "version",
        "active",
    ]  # example custom filter: FilterActiveServer(column=Server.active, name='Only Active', options=((True, 'Yes'), (False, 'No')))]
    column_editable_list = [
        "server_type",
        "name",
        "full_name",
        "cpu_cores",
        "memory",
        "notes",
        "version",
        "active",
    ]
    column_list = [
        "server_type",
        "name",
        "full_name",
        "version",
        "active",
        "cpu_cores",
        "memory",
        "notes",
        "creator",
    ]
    column_exclude_list = ["creator"]
    column_labels = dict(
        cpu_cores="CPU",
        server_type="Type",
    )
    # sort by name, descending
    column_default_sort = [("active", True), ("name", False), ("version", True)]

    form_excluded_columns = ("creator", "test_result")

    column_formatters = {"notes": _notes_formatter}

    form_args = {
        "cpu_cores": {"label": "CPU Cores"},
        "memory": {"label": "Memory (mb)"},
    }

    form_widget_args = {
        "name": {"placeholder": "server short name"},
        "full_name": {"placeholder": "server full name"},
        "cpu_cores": {
            # 'style': 'width: 50px',
            "placeholder": "number of cores",
            "title": "total number of cpu cores (fractional supported: e.g. 0.1 = 100m in a Container)",
        },
        "version": {
            "placeholder": "version of server",
            "title": "version of server (used to distinguish hw upgrades - e.g. rebuilt as larger ec2 instance)",
        },
        "active": {
            "title": "is the server active (used to deactivate old/unavailable instances of a server)"
        },
        "memory": {
            # 'style': 'width: 77px',
            "placeholder": "amount of memory",
            "title": "amount of memory (in mb) in server",
        },
        "server_type": {
            "placeholder": "server type (physical, vm, container, etc.)",
            "title": "server type",
        },
        "notes": {"placeholder": "notes regarding server"},
    }

    def on_model_change(self, form, model, is_created):

        if is_created:
            model.creator_id = current_user.id


class ServerTypeView(sqla.ModelView):
    def is_accessible(self):
        return (
            True
            if current_user.is_authenticated and current_user.has_role("admins")
            else False
        )

    def inaccessible_callback(self, name, **kwargs):
        return redirect(url_for("ldap_login_view"))

    def _handle_view(self, name, **kwargs):
        if not self.is_accessible():
            return self.inaccessible_callback(name, **kwargs)
        self.can_delete = True if current_user.has_role("admins") else False

    def _notes_formatter(view, context, model, name):
        return Markup(f"{model.notes}") if model.notes else ""

    page_size = 15
    can_view_details = True
    create_modal = True
    edit_modal = True
    details_modal = True
    can_export = True

    column_searchable_list = ["name", "notes"]
    column_filters = ["name", "notes"]
    column_editable_list = ["order", "name", "notes"]
    column_list = ["order", "name", "notes", "creator"]
    column_exclude_list = ["creator"]
    # column_labels = dict(cpu_cores='CPU Cores')
    # sort by name, descending
    column_default_sort = ("order", False)

    form_excluded_columns = "creator"

    column_formatters = {"notes": _notes_formatter}

    form_widget_args = {
        "name": {"placeholder": "run metric name"},
        "notes": {"placeholder": "notes regarding run metric"},
    }

    def on_model_change(self, form, model, is_created):

        if is_created:
            model.creator_id = current_user.id


class StorageTypeView(sqla.ModelView):
    def is_accessible(self):
        return (
            True
            if current_user.is_authenticated and current_user.has_role("admins")
            else False
        )

    def inaccessible_callback(self, name, **kwargs):
        return redirect(url_for("ldap_login_view"))

    def _handle_view(self, name, **kwargs):
        if not self.is_accessible():
            return self.inaccessible_callback(name, **kwargs)
        self.can_delete = True if current_user.has_role("admins") else False

    def _notes_formatter(view, context, model, name):
        return Markup(f"{model.notes}") if model.notes else ""

    page_size = 15
    can_view_details = True
    create_modal = True
    edit_modal = True
    details_modal = True
    can_export = True

    column_searchable_list = ["name", "notes"]
    column_filters = ["name", "notes"]
    column_editable_list = ["order", "name", "notes"]
    column_list = ["order", "name", "notes", "creator"]
    column_exclude_list = ["creator"]
    # column_labels = dict(cpu_cores='CPU Cores')
    # sort by name, descending
    column_default_sort = ("order", False)

    form_excluded_columns = "creator"

    column_formatters = {"notes": _notes_formatter}

    form_widget_args = {
        "name": {"placeholder": "run metric name"},
        "notes": {"placeholder": "notes regarding run metric"},
    }

    def on_model_change(self, form, model, is_created):

        if is_created:
            model.creator_id = current_user.id


class TestPlanView(sqla.ModelView):
    def is_accessible(self):
        return (
            True
            if current_user.is_authenticated and current_user.has_role("admins")
            else False
        )

    def inaccessible_callback(self, name, **kwargs):
        return redirect(url_for("ldap_login_view"))

    def _handle_view(self, name, **kwargs):
        if not self.is_accessible():
            return self.inaccessible_callback(name, **kwargs)
        self.can_delete = True if current_user.has_role("admins") else False

    def _summary_formatter(view, context, model, name):
        return Markup(f"{model.summary}") if model.summary else ""

    def _description_formatter(view, context, model, name):
        return Markup(f"{model.description}") if model.description else ""

    def _run_info_formatter(view, context, model, name):
        return Markup(f"{model.run_info}") if model.run_info else ""

    def _dependencies_formatter(view, context, model, name):
        return Markup(f"{model.dependencies}") if model.dependencies else ""

    def _notes_formatter(view, context, model, name):
        return Markup(f"{model.notes}") if model.notes else ""

    can_view_details = True
    create_modal = True
    edit_modal = True
    details_modal = True
    can_export = True
    page_size = 20

    column_searchable_list = [
        "nickname",
        "name",
        "summary",
        "description",
        "run_info",
        "dependencies",
        "notes",
    ]
    column_filters = [
        "nickname",
        "name",
        "version",
        "source_url",
        "summary",
        "description",
        "run_info",
        "dependencies",
        "notes",
        "active",
        "project.nickname",
    ]
    column_editable_list = [
        "nickname",
        "name",
        "version",
        "source_url",
        "summary",
        "description",
        "run_info",
        "notes",
        "active",
        "project",
    ]
    column_list = [
        "nickname",
        "name",
        "active",
        "project",
        "version",
        "source_url",
        "summary",
        "description",
        "run_info",
        "dependencies",
        "notes",
        "creator",
    ]
    column_exclude_list = [
        "description",
        "run_info",
        "dependencies",
        "notes",
        "creator",
    ]
    column_labels = dict(source_url="Source")
    # sort by name, descending
    column_default_sort = ("name", False)

    form_excluded_columns = "creator"

    column_formatters = {
        "summary": _summary_formatter,
        "description": _description_formatter,
        "run_info": _run_info_formatter,
        "dependencies": _dependencies_formatter,
        "notes": _notes_formatter,
    }

    form_args = {
        "source_url": {"label": "Source"},
        "project": {
            # order by name
            "query_factory": lambda: Project.query.order_by(Project.nickname)
        },
    }

    form_widget_args = {
        "name": {"placeholder": "test plan name"},
        "active": {
            "title": "is the Test Plan active (used to deactivate old/unavailable instances of a Test Plan)"
        },
        "version": {
            # 'style': 'width: 50px',
            "placeholder": "version",
            "title": "test script version (e.g. 1.0.0, 1.0.1, etc.)",
        },
        "source_url": {
            # 'style': 'width: 77px',
            "placeholder": "url to source of test script",
            "title": "url to version control (or other) location of test script",
        },
        "summary": {
            # 'style': 'width: 77px',
            "placeholder": "brief summary of test script",
            "title": "a brief summary of the test script",
        },
        "description": {
            # 'style': 'width: 25px',
            "placeholder": "detailed description of test script",
            "title": "a detailed description of the test script",
        },
        "run_info": {
            "placeholder": "info needed to run the script",
            "title": "all info needed to run the script such as variables or properties that need to be set",
        },
        "dependencies": {
            "placeholder": "dependencies of the script",
            "title": "all dependencies of the script (e.g. fakeusers.csv,  or other files, apps, data, info, etc.)",
        },
        "notes": {
            "placeholder": "notes regarding test script",
            "title": "any notes or additional detail about the test script",
        },
    }

    def on_model_change(self, form, model, is_created):

        if is_created:
            model.creator_id = current_user.id


class TestResultStatusView(sqla.ModelView):
    def is_accessible(self):
        return (
            True
            if current_user.is_authenticated and current_user.has_role("admins")
            else False
        )

    def inaccessible_callback(self, name, **kwargs):
        return redirect(url_for("ldap_login_view"))

    def _handle_view(self, name, **kwargs):
        if not self.is_accessible():
            return self.inaccessible_callback(name, **kwargs)
        self.can_delete = True if current_user.has_role("admins") else False

    can_view_details = True
    create_modal = True
    edit_modal = True
    details_modal = True
    can_export = True
    page_size = 20

    column_searchable_list = ["status"]
    column_filters = ["status", "symbol", "order"]
    column_editable_list = ["status", "symbol", "order"]
    column_list = ["order", "status", "symbol"]
    # column_exclude_list = ['notes']
    # column_labels = dict(source_url='Source')
    # sort by name, descending
    column_default_sort = ("order", False)

    # form_excluded_columns = ('creator_id')

    # ~ form_args = {
    # ~ 'source_url': {
    # ~ 'label': 'Source'
    # ~ },
    # ~ }

    form_widget_args = {
        "order": {
            "placeholder": "set order",
            "title": "specify an order for the Status",
        },
        "status": {
            "placeholder": "test result status",
            "title": "the test result status value",
        },
        "symbol": {
            "placeholder": "test result symbol",
            "title": "a symbol (single character) which represents the status",
        },
    }


class TargetServerRunMetricForm(InlineFormAdmin):
    def is_accessible(self):
        return current_user.has_role("users")

    form_columns = ("id", "run_metric", "value", "note")
    form_label = "Metric"
    column_labels = {"run_metric": "Metric"}
    form_widget_args = {
        "run_metric": {
            # 'style': 'width: 33px',
            "placeholder": "run metric",
            "title": "select a run metric",
        },
        "value": {
            # 'style': 'width: 66px',
            "placeholder": "value for metric",
            "title": "enter a value for the metric\nfor example: if metric is CPU Max a value may be [99.9]",
        },
        "note": {
            "style": "height: 34px",
            "placeholder": "note for metric",
            "title": "enter a note for the metric value\nfor example: if metric is CPU Avg a note may be [average determined by prometheus monitoring]",
        },
    }


class TestResultView(sqla.ModelView):
    def is_accessible(self):
        return (
            True
            if current_user.is_authenticated and current_user.has_role("admins")
            else False
        )

    def inaccessible_callback(self, name, **kwargs):
        return redirect(url_for("ldap_login_view"))

    def _handle_view(self, name, **kwargs):
        if not self.is_accessible():
            return self.inaccessible_callback(name, **kwargs)
        self.can_delete = True if current_user.has_role("admins") else False

    def _test_date_formatter(view, context, model, name):
        if model.test_date is not None:
            return Markup(
                f'<div class="inline-tooltip" data-html="true" title="{arrow.get(model.test_date).format("YYYY-MM-DD HH:mm:ss")} \n by: {model.creator.nickname} \n {arrow.get(model.test_date).humanize()}">{arrow.get(model.test_date).format("YYYY-MM-DD HH:mm")}</div>'
            )

    def _status_formatter(view, context, model, name):
        if model.status is not None:
            return Markup(
                f'<div class="inline-tooltip" style="text-align: center; width: *;" title="Status: {model.status}">{model.status.symbol}</div>'
            )

    def _test_plan_formatter(view, context, model, name):
        if model.test_plan is not None:
            return Markup(
                f'<div class="inline-tooltip" style="text-align: center; width: *;" title="{model.test_plan.name} (v.{model.test_plan.version})\n{model.test_plan.summary}">{model.test_plan}</div>'
            )

    # ~ def _target_server_formatter(view, context, model, name):
    # ~ return Markup("<a href='%s'>%s</a>" % (url_for('server.edit_view', id=model.target_server.id), model.target_server.name)) if model.target_server else ""
    # ~ def _source_servers_formatter(view, context, model, name):
    # ~ return "%s" % (model.source_servers.server) if model.source_servers else ""
    # ~ def _test_plan_formatter(view, context, model, name):
    # ~ return Markup("<a href='%s'>%s</a>" % (url_for('test_plan.edit_view', id=model.test_plan.id), model.test_plan.name)) if model.test_plan else ""
    def _test_notes_formatter(view, context, model, name):
        return (
            Markup(f"{markdown.markdown(model.test_notes, extensions=['fenced_code'])}")
            if model.test_notes
            else ""
        )

    def _prerun_notes_formatter(view, context, model, name):
        return (
            Markup(
                f"{markdown.markdown(model.prerun_notes, extensions=['fenced_code'])}"
            )
            if model.prerun_notes
            else ""
        )

    def _run_notes_formatter(view, context, model, name):
        return (
            Markup(f"{markdown.markdown(model.run_notes, extensions=['fenced_code'])}")
            if model.run_notes
            else ""
        )

    def _postrun_notes_formatter(view, context, model, name):
        return (
            Markup(
                f"{markdown.markdown(model.postrun_notes, extensions=['fenced_code'])}"
            )
            if model.postrun_notes
            else ""
        )

    def _failure_notes_formatter(view, context, model, name):
        return (
            Markup(
                f"{markdown.markdown(model.failure_notes, extensions=['fenced_code'])}"
            )
            if model.failure_notes
            else ""
        )

    def _target_server_run_metrics_url_formatter(view, context, model, name):
        return (
            Markup(
                f"<a href='{model.target_server_run_metrics_url}'>{model.target_server_run_metrics_url}</a>"
            )
            if model.target_server_run_metrics_url
            else ""
        )

    # ~ def on_form_prefill(self, form, id):
    # ~ form.run_by = 'admin'
    # form.process()

    # example to add extra row_actions
    # ~ column_extra_row_actions = [
    # ~ LinkRowAction('glyphicon glyphicon-off', 'http://direct.link/?id={row_id}'),
    # ~ LinkRowAction('glyphicon glyphicon-off', 'tya/?id={row_id}'),
    # ~ #EndpointLinkRowAction('glyphicon glyphicon-on', 'testresult.index_view'),
    # ~ ]

    # inline_models = (ServerRunMetric, )
    inline_models = (TargetServerRunMetricForm(ServerRunMetric),)

    page_size = 10
    can_set_page_size = True
    can_view_details = True
    create_modal = False
    edit_modal = False
    details_modal = True
    can_export = True

    column_searchable_list = ["test_plan.name", "test_notes"]
    column_filters = [
        "test_plan.name",
        "status.status",
        "app_version",
        "test_date",
        "number_users",
        "run_length",
        "number_failures",
        "average_response_time",
        "target_server.name",
        "run_by.name",
        "target_server_run_metrics.value",
        "target_server_quantity",
        "tags.name",
        "test_plan.project.nickname"
        # BaseSQLAFilter(column=TestResult.test_notes, name='XSource ServerX')
    ]
    column_editable_list = [
        "status",
        # ~ "test_date",   # note: dont add this as editable as it messes up the entire list page.
        "test_plan",
        "number_users",
        "run_length",
        "number_failures",
        "average_response_time",
        "source_servers",
        "target_server",
        "test_notes",
        "target_server_quantity",
        "tags",
        "app_version",
    ]
    column_list = [
        "status",
        "test_date",
        "test_plan",
        "run_by",
        "source_servers",
        "target_server",
        "target_server_quantity",
        "app_version",
        "number_users",
        "ramp_up",
        "loop_amount",
        "run_length",
        "number_failures",
        "number_requests",
        "average_response_time",
        "prerun_notes",
        "run_notes",
        "postrun_notes",
        "failure_notes",
        "test_notes",
        "target_server_run_metrics",
        "target_server_run_metrics_url",
        "creator",
        "tags",
    ]
    column_exclude_list = [
        "ramp_up",
        "number_requests",
        "prerun_notes",
        "run_notes",
        "postrun_notes",
        "failure_notes",
        "target_server_run_metrics_url",
        "creator",
        "run_by",
        "loop_amount",
    ]
    column_labels = dict(
        source_servers="Src",
        target_server="Tgt",
        target_server_quantity="Tgt Qty",
        number_users="Users",
        number_failures="Fail",
        test_date="Date",
        average_response_time="ART",
        loop_amount="Loops",
        run_length="Run Len",
        prerun_notes="PreRun Notes",
        test_plan="Test",
        number_requests="#Reqs",
        postrun_notes="PostRun Notes",
        target_server_run_metrics="Target SRM",
        target_server_run_metrics_url="Target SRM URL",
        tags="Tags",
        test_notes="Notes",
        ramp_up="User Rate",
        status="Status",
        app_version="App Ver",
    )

    form_columns = (
        "status",
        "test_plan",
        "run_by",
        "source_servers",
        "target_server",
        "target_server_quantity",
        "number_users",
        "ramp_up",
        "loop_amount",
        "app_version",
        "run_length",
        "number_failures",
        "number_requests",
        "average_response_time",
        "target_server_run_metrics_url",
        "prerun_notes",
        "run_notes",
        "postrun_notes",
        "failure_notes",
        "test_notes",
        "target_server_run_metrics",
        "tags",
        "test_date",
    )

    form_excluded_columns = ("created_at", "creator_id", "creator")
    # sort by test_date, descending
    column_default_sort = ("test_date", True)
    # column_default_sort = 'test_plan.name'

    # the following is Flask-AppBuilder syntax
    # ~ add_fieldsets = [
    # ~ ('Summary', {'fields':['test_date','status','test_plan','run_by']})
    # ~ ]

    # column_formatters = dict(target_server='target_server.name')
    column_formatters = {
        "status": _status_formatter,
        "test_date": _test_date_formatter,
        # 'test_plan': _test_plan_formatter,
        # 'target_server': _target_server_formatter,
        "test_notes": _test_notes_formatter,
        "prerun_notes": _prerun_notes_formatter,
        "run_notes": _run_notes_formatter,
        "postrun_notes": _postrun_notes_formatter,
        "failure_notes": _failure_notes_formatter,
        "target_server_run_metrics_url": _target_server_run_metrics_url_formatter,
        "test_plan": _test_plan_formatter,
        # "app_version": _app_version_formatter,
    }

    form_args = {
        "run_by": {
            # order by username
            "query_factory": lambda: User.query.order_by(User.username)
        },
        "source_servers": {
            # filter to only show 'active' servers
            "query_factory": lambda: Server.query.filter_by(active=True).order_by(
                Server.name
            )
        },
        "status": {
            # order by order
            "query_factory": lambda: TestResultStatus.query.order_by(
                TestResultStatus.order
            )
        },
        "tags": {
            # order by name
            "query_factory": lambda: Tag.query.order_by(Tag.name)
        },
        "target_server": {
            # filter to only show 'active' servers
            "query_factory": lambda: Server.query.filter_by(active=True).order_by(
                Server.name
            )
        },
        "test_date": {
            "format": "%Y-%m-%d %H:%M:%S"  # changes how the input is parsed by strptime (e.g. 2017-07-22 11:47:58).
        },
        "test_plan": {
            # order list by name.
            "query_factory": lambda: TestPlan.query.filter_by(active=True).order_by(
                TestPlan.name
            )
        },
        # ~ 'loop_amount': {
        # ~ 'label': 'Loops'
        # ~ },
        # ~ 'number_users': {
        # ~ 'label': '# Users'
        # ~ },
        # ~ 'number_failures': {
        # ~ 'label': '# Fail'
        # ~ },
        # ~ 'number_requests': {
        # ~ 'label': '# Reqs'
        # ~ },
        # ~ 'run_length': {
        # ~ 'label': 'Run Length'
        # ~ },
        # ~ 'average_response_time': {
        # ~ 'label': 'ART'
        # ~ },
        # ~ 'target_server_run_metrics': {
        # ~ 'label': 'Target SRM'
        # ~ },
        # ~ 'target_server_run_metrics_url': {
        # ~ 'label': 'Target SRM URL'
        # ~ },
    }

    form_widget_args = {
        "test_plan": {
            "placeholder": "name of test plan used",
            "title": "select the test plan used for the test run",
        },
        "source_servers": {
            "placeholder": "source server(s)",
            "title": "select the server or servers that the test was ran from\nplease use the notes field to describe any unusual source server(s) info",
        },
        "target_server": {
            "placeholder": "target server",
            "title": "select the server that the test was ran against\nplease use the notes field to describe any unusual target server info",
        },
        "target_server_quantity": {
            "placeholder": "target server quantity",
            "title": "enter the number of target server instances running for test (use range for autoscaling)",
        },
        "test_date": {
            "placeholder": "date/time test was started (format: YYYY-MM-DD HH:mm:ss)",
            "title": "date/time test was started; example: 2019-04-05 06:23:21",
            "data-date-format": "YYYY-MM-DD HH:mm:ss",  # changes how the DateTimeField displays the time
            "data-role": "",  # prevent the datepicker from displaying as it causes more problems than it is worth.
        },
        "loop_amount": {
            "placeholder": "# of loops",
            "title": "number of loops of the test performed (-1 indicates loop forever)",
        },
        "app_version": {"placeholder": "version of application"},
        "number_users": {"placeholder": "number of users ran for test"},
        "ramp_up": {
            "placeholder": "Ramp Up for Jmeter or Hatch Rate for locust (amount is in seconds)",
            "title": "Jmeter: amount of time to spin up all users; Locust: rate at which users are spawned",
        },
        "run_length": {
            "placeholder": "run length (in seconds)",
            "title": "how long it took to run the test (in seconds)",
        },
        "number_failures": {
            "placeholder": "# of failures",
            "title": "number of failures that occurred during the test",
        },
        "number_requests": {
            "placeholder": "# of requests",
            "title": "number of requests generated during the test",
        },
        "average_response_time": {
            "placeholder": "average response time (in milliseconds)",
            "title": "average response time (in milliseconds)",
        },
        "test_notes": {
            "placeholder": "notes related to test ",
            "title": "all notes related to the test run\nthis field accepts html data",
            "class": "markdown",
        },
        "prerun_notes": {
            "placeholder": "prerun notes related to test",
            "title": "any notes related to steps/tasks to be ran before the test is run\nthis field accepts html data",
            "class": "markdown",
        },
        "run_notes": {
            "placeholder": "notes related to the test run",
            "title": "any notes related to the test run itself\nthis field accepts html data",
            "class": "markdown",
        },
        "postrun_notes": {
            "placeholder": "notes related to post run of test",
            "title": "any notes related to the post run of the test run (e.g. things that happened after test completed)\nthis field accepts html data",
            "class": "markdown",
        },
        "failure_notes": {
            "placeholder": "notes related to test failure",
            "title": "any notes related to the test run failure (e.g. error logs, stack dumps, etc.)\nthis field accepts html data",
            "class": "markdown",
        },
        "target_server_run_metrics": {
            "placeholder": "target SRM url",
            "title": "link to the target server run metrics results (if applicable)\n e.g. link to prometheus date range for test",
        },
        "tags": {
            "placeholder": "tag(s) of the run result",
            "title": "any applicable tags for the run result",
        },
        "run_by": {
            "placeholder": "who ran test (leave blank for 'current user' or select a user from the list)",
            "title": "who ran test (leave blank for 'current user' or select a user from the list)",
        },
    }

    def on_model_change(self, form, model, is_created):
        if is_created:
            model.creator_id = current_user.id
            logger.debug(f"==> run_by.data is: {form.run_by.data}")
            if not form.run_by.data:
                logger.debug(
                    f"run_by is empty, setting it to current user: {current_user}"
                )
                model.run_by = current_user

    def create_form(self):
        form = super(TestResultView, self).create_form()
        return form
