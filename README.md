# README

Radiant is a simple Task and Time tracking app.


###  More Info  ###
See the `docs` directory.


### Setup ###

- create a virtual environment in your python root: `python3 -m virtualenv .venv`
- activate the virtualenv: `source .env-setup`
- install packages: `pip install -r requirements.txt`


### Deployment

#### k8s/helm

This application is fully deployable via helm, go to the `.devops/helm` directory and perform the following to deploy:

- configure the `values.yaml.prod` per needs
- deploy the chart: `helm install radiant radiant/ --values radiant/values.yaml.prod --create-namespace --namespace radiant --disable-openapi-validation`
  - update: `helm upgrade radiant radiant/ --values radiant/values.yaml.prod --namespace radiant --disable-openapi-validation`
  - NOTICE: after deploying the updated chart you need to rollout the change to the service impacted:
    + `kubectl rollout restart -n radiant deployment api ui`
    + `kubectl rollout restart -n radiant statefulset redis`
  - check pods: `kubectl get pod -n radiant`

You can uninstall this chart with the following:
  - uninstall: `helm uninstall -n radiant radiant`
