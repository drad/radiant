import Vue from 'vue'
import { api } from 'boot/axios'

const state = {
    user_messages: [],
    selected_activity:
    {
        start: "",
        end: "",
        location: "Testville 32",
    },
}

const mutations = {
    addUserMessages(state, payload) {
        //~ console.log("store-user_messages > mutations > addUserMessages:", payload);
        state.user_messages = payload
    },
    async addUserMessage(state, activity) {
        //~ console.log("- store-user_messages > mutations > addUserMessage activity:", activity)
    },
    updateUserMessage(state, payload) {
        //console.log("store-user_messages>mutations>updateUserMessage - payload: ", payload)
    },
    deleteUserMessage(state, id_) {
        var querystring = require('querystring');
        //console.log("store-user_messages>mutations>deleteUserMessage id: ", id_)
        // delete from db
        api.delete('/user_messages/?' + querystring.stringify({ _id: id_ }))
            .then((response) => {
                //console.log("- delete via API succeeded");
            })
        .catch((error) => {
            console.log("Error in deleting activity: ", error)
        })
        // delete from vuex
        var index = state.user_messages.findIndex(activity => activity.id_ == id_);
        //console.log(" --> index is: ", index);
        state.user_messages.splice(index, 1);
    },
    setSelectedUserMessage(state, activity) {
        // add activity to vuex
        //~ state.selected_activity = activity;
        state.selected_activity = Object.assign({}, state.selected_activity, activity)
    },
}

const actions = {
    fetchUserMessages({ commit }, payload) {
        //~ console.log("store-user_messages: fetching user_messages from server...")
        api.get('/user_messages/all', { params: payload})
            .then((response) => {
                //~ console.log('  -> response: ', JSON.stringify(response.data))
                commit('addUserMessages', response.data)
                //~ console.log('  -> user_messages added to vuex')
            })
            .catch(() => {
                // note: this is hit when an unauthenticated user hits / which redirects them to /login
                //       no harm in the message so we just dump to console.
                console.log('No user_messages found.')
            })
    },
    updateUserMessage({ commit }, payload) {
        //~ console.log("store-user_messages>action>updateUserMessage - payload: ", payload)
        return new Promise((resolve, reject) => {
            var querystring = require('querystring');
            api.put('/user_messages/?' + querystring.stringify(payload))
            .then((response) => {
                    //~ console.log("activity update response: ", response)
                    resolve(true)
                })
            .catch((error) => {
                //~ console.log("Error updating activity: ", error)
                reject(error)
            })
            // NOTE: No need to add activity to vuex as we fetch after adding.
        })
    },
    deleteUserMessage({ commit }, id_) {
        //~ console.log("store-user_messages>action>deleteUserMessage with id: ", id)
        commit('deleteUserMessage', id_)
    },
    addUserMessage({ commit, dispatch }, activity) {
        return new Promise((resolve, reject) => {
            var querystring = require('querystring');
            // strip off nulls and cruft.
            let obj = Object.fromEntries(Object.entries(activity).filter(([_, v]) => v != null));
            //~ console.log('nulls skipped obj:', obj);
            api.post('/user_messages/?' + querystring.stringify(obj))
                .then((response) => {
                    //~ console.log("UserMessage add response:", response)
                    resolve(true)
                })
                .catch((error) => {
                    //~ console.log("Error in adding UserMessage:", error)
                    reject(error)
                })
            // NOTE: No need to add to vuex as we fetch after adding.
        })
    },
    setSelectedUserMessage({ commit }, activity) {
        commit('setSelectedUserMessage', activity)
    },
}

const getters = {
    user_messages: (state) => {
        return state.user_messages
    },
    selected_user_message: (state) => {
        return state.selected_user_message
    }
}

export default {
    namespaced: true,

    state,
    mutations,
    actions,
    getters
}
