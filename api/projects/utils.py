#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#  SPDX-License-Identifier: GPL-3.0-only
#  Copyright 2025 dradux.com

import logging

import aiocouch
from clients.utils import get_by_id as get_client_by_id
from config.config import get_cdb
from fastapi.encoders import jsonable_encoder
from projects.models import _db, Project
from _common.utils import fix_id

logger = logging.getLogger("default")


async def get_by_client_id(_client_id: str):
    """
    Get all projects for a given client id.
    """

    couchdb = get_cdb()
    # ~ logger.debug(f"Get projects for client_id={_client_id}")
    db = await couchdb[_db.value]

    _selector = {"client_id": _client_id}

    docs = []
    async for doc in db.find(selector=_selector):
        _doc = Project(**fix_id(doc))
        docs.append(_doc)

    return docs


async def get_by_id(_id: str):
    """
    Get a project by id
    """

    # ~ logger.debug(f"Get project for id={_id}")
    couchdb = get_cdb()
    db = await couchdb[_db.value]

    try:
        return await db[_id]
    except aiocouch.exception.NotFoundError:
        return None


async def get_enhanced_by_id(_id: str):
    """
    Get enhanced project (with client and project data) by id
    """

    # first get the project so we have the client id
    # ~ logger.debug(f"Get project for id={_id}")
    _project = await get_by_id(_id)
    if _project:
        logger.debug(f"- project found:\n{_project}")
        # get the client
        # ~ logger.debug(f"- getting client with id={_project['client_id']}")
        _client = await get_client_by_id(_project["client_id"])
        # ~ logger.debug(f"- found client={_client}")
        _project["client"] = jsonable_encoder(_client)
    else:
        logger.debug("No project found")

    # ~ logger.debug(f"- returning enhanced project:\n{_project}")

    return _project
