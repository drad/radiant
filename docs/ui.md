# UI documentation


### Upgrade or Reinstall Node ###

- move old venv (from the ui folder in question): `mv .venv .venv-$(date +"%F")`
- create a new nodeenv (from project root): `nodeenv --node=20.11.0 .venv`
    + NOTE: only use even numbered versions as they are LTS while odd are not
- re-install packages:
    + activate virtualenv: `source .venv/bin/activate`
    + install yarn: `npm install -g yarn`
    + install quasar: `yarn global add @quasar/cli`
    + install project packages: `cd app && yarn install`
        - note: this needs to be in the `app` dir
    + update browsers list: `npx browserslist@latest --update-db`
    + update quasar packages: `quasar upgrade --install`
    + check for outdated packages: `yarn outdated`
- start ui `quasar dev` and test as needed

You should now be ready to upgrade the Dockerfile(s) to the corresponding node version.

### Quasar ###

#### Notices ####

- if you get into a state where the UI wont come up or build and it is complaining about missing node modules, its likely the modules need re-installed. Just delete the 'node_modules' directory and do a `yarn install` or see 'Upgrade or Reinstall Node' section above.

#### Setup

Note: typically install to: `${project_rood}/ui/app`

- install [quasar cli](https://quasar.dev/start/quasar-cli)
    - create a new nodeenv in `${project_rood}/ui`: `nodeenv --prebuilt --node=16.12.0 .venv`
        - __NOTICE:__ make sure the node major version is an even number (e.g. 14, 16, 18) not odd (e.g. 13, 15, 17) as odd numbers are experimental versions which are not tested in Quasar.
        - setup your `.env-setup` script
    - active nodeenv
    - install  yarn: `npm install -g yarn`
    - install quasar cli: `yarn global add @quasar/cli`
    - create project: `quasar create app`
    - check quasar version (or to update packages in quasar)
        + check version: `quasar --version`  # should be 2.1.15+
            * __NOTICE:__ this needs to be done in the quasar app directory (e.g. `${project_rood}/ui/app`) not the wrapper directory (e.g. `${project_rood}/ui`
        + check for upgradable packages: `quasar upgrade`
            * __NOTICE:__ this is only for quasar components, see Yarn section below for node packages
        + do the actual upgrade: `quasar upgrade --install`

#### Run ###

- start dev server
    + go to ui directory and activate the virtualenv (`cd ui && ve`)
    + go to app directory and start quasar dev server: `quasar dev`

#### Useful Info ####

Quasar's recommendations:

- npm for global packages
- yarn for local (app) packages

### Yarn ###

- check for outdated packages: `yarn outdated`
- upgrade packages: `yarn upgrade {package|or none for all}`
- clean: `yarn autoclean`
    + NOTICE: DONT USE THIS (it caused a mess)
- check for security issues in packages: `yarn audit`


### Old stuff below here which should not be needed

- quasar: `npm install -g @quasar/cli`
- project: `cd app && npm install`

### UI Development ###

- SSE: SSE uses websockets to notify the UI on backend changes. The 'handler' is in Login.vue, created after a user successfully authenticates.
