const routes = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '/', component: () => import('pages/Welcome.vue') },
      { path: '/invoices', component: () => import('pages/Invoices.vue') },
      { path: '/login', component: () => import('pages/Login.vue') },
      { path: '/help', component: () => import('pages/Help.vue') },
      { path: '/about', component: () => import('pages/About.vue') },
      { path: '/registrations', component: () => import('pages/Registrations.vue') },
      { path: '/edit_profile', component: () => import('pages/EditProfile.vue') },
    ]
  },
  {
    path: '/iv',
    component: () => import('layouts/InvoiceLayout.vue'),
    children: [
      { path: '', component: () => import('pages/InvoicePage.vue') },
    ]
  },


  // Always leave this as last one,
  // but you can also remove it
  {
    path: '/:catchAll(.*)*',
    component: () => import('pages/Error404.vue')
  }
]

export default routes
