#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#  SPDX-License-Identifier: GPL-3.0-only
#  Copyright 2025 dradux.com

import aiocouch
import arrow
import logging
import uuid
import random
from datetime import datetime
from typing import List
from _common.models import DocInfo
from _common.utils import fix_id, make_id, local_date_to_utc, utc_to_local_date
from clients.utils import get_by_id as client_get_by_id
from config.config import get_cdb
from fastapi import APIRouter, HTTPException, Query, Security, status
from fastapi.encoders import jsonable_encoder
from registrations.models import RegistrationWithTaskNameResponse
from registrations.utils import get_for_invoice as get_registrations_for_invoice
from invoices.models import (
    InvoiceExt,
    InvoiceWithClientNameResponse,
    InvoiceWithRegistrationsResponse,
    _db,
    InvoiceLayout,
)
from invoices.utils import calculate_status, get_registration_amount
from users.models import UserBase, get_current_user
from sse.utils import send_sse_message
from sse.models import SSEEventType, SSEActionType

logger = logging.getLogger("default")
invoices_router = APIRouter()


async def _get_or_404(_id, db):
    """
    Get a document or raise a 404
    """

    # ~ logger.debug(f"_get_or_404 request on: {_id}")
    try:
        return await db[_id]
    except aiocouch.exception.NotFoundError:
        raise HTTPException(status_code=404, detail="Not found")


@invoices_router.get("/all", response_model=List[InvoiceWithClientNameResponse])
async def get_all(
    include_submitted: bool = Query(True, description="include submitted invoices?"),
    include_due: bool = Query(True, description="include due invoices?"),
    include_paid: bool = Query(True, description="include paid invoices?"),
    include_deposited: bool = Query(False, description="include deposited invoices?"),
    limit: int = Query(10, ge=1, le=99, description="Limit number of results"),
    skip: int = Query(0, description="Skip number of records before applying limit"),
    current_user: UserBase = Security(get_current_user, scopes=["admins"]),
):
    """
    Get all
    - only for admins
    """

    couchdb = get_cdb()
    logger.debug(f"Get all: current_user: {current_user}")
    db = await couchdb[_db.value]

    _selector = {}
    _status_filter = []
    if include_deposited:
        # do nothing
        logger.debug("No filter on include deposited")
        pass
    else:
        # filter out deposited (deposited does not exist or it is null)
        logger.debug("Exclude deposited invoices...")
        _status_filter.append({"$not": {"deposited": {"$exists": True, "$ne": None}}})
    if include_paid:
        # do nothing
        logger.debug("No filter on include paid")
        pass
    else:
        # filter out deposited (deposited does not exist or it is null)
        logger.debug("Exclude paid invoices...")
        _status_filter.append({"$not": {"paid": {"$exists": True, "$ne": None}}})
    if include_submitted:
        # do nothing
        logger.debug("No filter on include submitted")
        pass
    else:
        # filter out deposited (deposited does not exist or it is null)
        logger.debug("Exclude submitted invoices...")
        _status_filter.append({"$not": {"submitted": {"$exists": True, "$ne": None}}})
    if include_due:
        # do nothing
        logger.debug("No filter on include due")
        pass
    else:
        # filter out deposited (deposited does not exist or it is null)
        logger.debug("Exclude due invoices...")
        _status_filter.append({"$not": {"due": {"$exists": True, "$ne": None}}})

    if len(_status_filter) > 0:
        _selector.update({"$and": _status_filter})

    sort = [{"created": "desc"}]

    logger.debug(
        f"""◆────────────────────[ DB QUERY INFO ]────────────────────◆
- sort:     {sort} (currently not implemented)
- limit:    {limit}
- skip:     {skip}
- selector: {_selector}
"""
    )

    docs = []
    async for doc in db.find(selector=_selector, sort=sort, limit=limit, skip=skip):
        _doc = fix_id(doc)
        _client = await client_get_by_id(_doc["client"])
        if _client:
            _doc["client_name"] = _client["name"]
            _doc["client_nickname"] = _client["nickname"]
        _doc["status"] = await calculate_status(_doc)
        _registrations = await get_registrations_for_invoice(_doc["_id"], user_timezone=current_user.user_timezone)
        _doc["number_registrations"] = len(_registrations)
        _doc["amount"] = await get_registration_amount(_registrations)
        _doc["created"] = await utc_to_local_date(indate=_doc["created"], tz=current_user.user_timezone)
        _doc["submitted"] = await utc_to_local_date(indate=_doc["submitted"], tz=current_user.user_timezone)
        _doc["due"] = await utc_to_local_date(indate=_doc["due"], tz=current_user.user_timezone)
        _doc["paid"] = await utc_to_local_date(indate=_doc["paid"], tz=current_user.user_timezone)
        _doc["deposited"] = await utc_to_local_date(indate=_doc["deposited"], tz=current_user.user_timezone)

        _invoice = InvoiceWithClientNameResponse(**_doc)
        docs.append(_invoice)

    return docs


@invoices_router.get(
    "/one/{_id}",
    response_model=InvoiceWithRegistrationsResponse,
    summary="Get one Invoice",
)
async def get_one(
    _id: str,
    current_user: UserBase = Security(get_current_user, scopes=["users"]),
):
    """
    Get one by id.

    @NOTE
    - this includes all registrations for the Invoice
    """

    couchdb = get_cdb()
    db = await couchdb[_db.value]

    _registrations = []

    # get invoice
    _invoice = fix_id(await _get_or_404(_id, db))
    # ~ logger.debug(f"INVOICE: {_invoice}")
    # get client name
    _client = await client_get_by_id(_invoice["client"])
    _invoice["client_name"] = _client["name"]
    # get invoice registrations
    for reg in await get_registrations_for_invoice(_id, current_user.user_timezone):
        _registrations.append(RegistrationWithTaskNameResponse(**reg))
    _invoice["registrations"] = _registrations
    # get status
    _invoice["status"] = await calculate_status(_invoice)
    logger.debug(f"Invoice is: {jsonable_encoder(_invoice)}")

    return InvoiceWithRegistrationsResponse(**_invoice)


@invoices_router.post("/", response_model=DocInfo, status_code=status.HTTP_201_CREATED)
async def add(
    layout: InvoiceLayout = Query(..., description="layout of the invoice"),
    name: str = Query(..., description="invoice name (for reference purposes)"),
    client: str = Query(..., description="client (id) the invoice belongs to"),
    discount: float = Query(0, description="discount amount (if any)"),
    paid_amount: float = Query(0, description="paid amount (if any)"),
    submitted: datetime = Query(None, description="date invoice was submitted to client"),
    due: datetime = Query(None, description="date invoice is due"),
    paid: datetime = Query(None, description="date invoice was paid"),
    deposited: datetime = Query(None, description="date invoice was deposited"),
    note: str = Query(None, description="notes for the Invoice"),
    detail: str = Query(None, description="details about the invoice (note: this is internal notes)"),
    tags: List[str] = Query(
        None,
        description="tags for the registration<br/><strong>NOTICE:</strong> all lowercase",
    ),
    current_user: UserBase = Security(get_current_user, scopes=["users"]),
):
    """
    Add new
    - all dates are in the form YYYY-MM-DD HH:MM:SS (in user's specified TZ); however, we store them in UTC
    - invoice_number is generated
    - __remember__: you do not add a registration to the invoice but rather add the invoice to the registration
    """

    couchdb = get_cdb()
    db = await couchdb[_db.value]

    logger.debug(f"Add Invoice by user: {current_user.username}")

    _client = await client_get_by_id(client)
    if not _client or not _client["enabled"]:
        raise HTTPException(status_code=404, detail="Client not found")

    _invoice_number = f"{arrow.now().int_timestamp}-{'%02d' % random.randrange(10 ** 2)}"  # nosec
    _curdts = await local_date_to_utc(indate=datetime.now(), tz=current_user.user_timezone)

    _doc = InvoiceExt(
        layout=layout,
        name=name,
        client=client,
        invoice_number=_invoice_number,
        discount=discount,
        paid_amount=paid_amount,
        submitted=await local_date_to_utc(indate=submitted, tz=current_user.user_timezone),
        due=await local_date_to_utc(indate=due, tz=current_user.user_timezone),
        paid=await local_date_to_utc(indate=paid, tz=current_user.user_timezone),
        deposited=await local_date_to_utc(indate=deposited, tz=current_user.user_timezone),
        note=note,
        detail=detail,
        tags=[t.lower() for t in tags] if tags else None,  # lowercase all tags
        created=_curdts,
        updated=_curdts,
        creator=current_user.username,
    )

    try:
        doc_id = make_id(items=[str(uuid.uuid4())])
        doc = await db.create(
            doc_id,
            data=jsonable_encoder(_doc),
        )
        await doc.save()
        # send sse message of add
        await send_sse_message(user=current_user.username, event_type=SSEEventType.invoice, data=SSEActionType.add)

        return await doc.info()

    except aiocouch.ConflictError:
        raise HTTPException(status_code=409, detail=f"Duplicate Key: cannot add '{doc_id}'.")


@invoices_router.put("/", response_model=DocInfo)
async def update(
    _id: str = Query(..., description="id of the document to update"),
    layout: InvoiceLayout = Query(None, description="layout of the invoice"),
    name: str = Query(None, description="invoice name (for reference purposes)"),
    client: str = Query(None, description="client (id) the invoice belongs to"),
    discount: float = Query(None, description="discount amount (if any)"),
    paid_amount: float = Query(None, description="paid amount (if any)"),
    submitted: datetime = Query(None, description="date invoice was submitted to client"),
    due: datetime = Query(None, description="date invoice is due"),
    paid: datetime = Query(None, description="date invoice was paid"),
    deposited: datetime = Query(None, description="date invoice was deposited"),
    note: str = Query(None, description="notes for the Registration"),
    detail: str = Query(None, description="details about the invoice (note: this is internal notes)"),
    tags: List[str] = Query(
        None,
        description="tags for the registration<br/><strong>NOTICE:</strong> all lowercase",
    ),
    current_user: UserBase = Security(get_current_user, scopes=["users"]),
):
    """
    Update Invoice

    __NOTICE:__
    - all non-required fields are truly option, if not supplied no change to that field will be made with the following exceptions:
      + layout: if not supplied __AND__ the invoice has a value for it, it will be cleared
      + name: if not supplied __AND__ the invoice has a value for it, it will be cleared
      + client: if not supplied __AND__ the invoice has a value for it, it will be cleared
      + discount: if not supplied __AND__ the invoice has a value for it, it will be cleared
      + paid_amount: if not supplied __AND__ the invoice has a value for it, it will be cleared
      + submitted: if not supplied __AND__ the invoice has a value for it, it will be cleared
      + due: if not supplied __AND__ the invoice has a value for it, it will be cleared
      + paid: if not supplied __AND__ the invoice has a value for it, it will be cleared
      + deposited: if not supplied __AND__ the invoice has a value for it, it will be cleared
      + note: if not supplied __AND__ the invoice has a value for it, it will be cleared
      + detail: if not supplied __AND__ the invoice has a value for it, it will be cleared
      + tags: if not supplied __AND__ the invoice has any values for it, it will be cleared
    """

    logger.debug(f"Updating Invoice with id: {_id}")
    couchdb = get_cdb()
    db = await couchdb[_db.value]
    doc = await _get_or_404(_id, db)
    logger.debug(f"Updating doc: {doc}")
    response_msg = None

    _curdts = await local_date_to_utc(indate=datetime.now(), tz=current_user.user_timezone)

    if layout:
        doc["layout"] = layout
    if name:
        doc["name"] = name
    if client:
        doc["client"] = client
    if discount:
        doc["discount"] = discount
    if paid_amount:
        doc["paid_amount"] = paid_amount

    if submitted:
        doc["submitted"] = jsonable_encoder(await local_date_to_utc(indate=submitted, tz=current_user.user_timezone))
    elif not submitted and doc["submitted"]:
        doc["submitted"] = None
    if due:
        doc["due"] = jsonable_encoder(await local_date_to_utc(indate=due, tz=current_user.user_timezone))
    elif not due and doc["due"]:
        doc["due"] = None
    if paid:
        doc["paid"] = jsonable_encoder(await local_date_to_utc(indate=paid, tz=current_user.user_timezone))
    elif not paid and doc["paid"]:
        doc["due"] = None
    if deposited:
        doc["deposited"] = jsonable_encoder(await local_date_to_utc(indate=deposited, tz=current_user.user_timezone))
    elif not deposited and doc["deposited"]:
        doc["deposited"] = None

    if note:
        doc["note"] = note
    elif not note and doc["note"]:
        doc["note"] = None
    if detail:
        doc["detail"] = detail
    elif not detail and doc["detail"]:
        doc["detail"] = None
    if tags:
        doc["tags"] = tags
    elif not tags and doc["tags"]:
        doc["tags"] = []

    doc["updated"] = jsonable_encoder(_curdts)
    doc["updator"] = current_user.username
    doc["user"] = current_user.username

    # logger.debug(f"doc before save: {doc}")
    await doc.save()
    # send sse message of update
    await send_sse_message(user=current_user.username, event_type=SSEEventType.invoice, data=SSEActionType.update)
    resp = DocInfo(**await doc.info())
    resp.msg = response_msg
    logger.debug(f"Returning: {resp}")
    return resp


@invoices_router.delete("/", response_model=DocInfo)
async def delete(
    _id: str,
    current_user: UserBase = Security(get_current_user, scopes=["admins"]),
):
    """
    Hard Delete by id
    """

    couchdb = get_cdb()
    db = await couchdb[_db.value]
    resp = DocInfo(ok=False, id=_id, rev="", msg="unknown")
    doc = await _get_or_404(_id, db)
    dr = await doc.delete()
    if dr and dr.status < 400:
        resp.ok = True
        resp.msg = "deleted"
        # send sse message.
        await send_sse_message(user=current_user.username, event_type=SSEEventType.invoice, data=SSEActionType.delete)
    else:
        resp.ok = False
        resp.msg = f"Delete status: {dr.status}, headers: {dr.headers}"

    return resp
