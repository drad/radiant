#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#  SPDX-License-Identifier: GPL-3.0-only
#  Copyright 2025 dradux.com

import logging
from datetime import datetime

import aiocouch
from _common.utils import fix_id, make_id
from config.config import get_cdb, DEFAULT_USER_TIMEZONE
from config.models import Cdb
from fastapi import HTTPException
from fastapi.encoders import jsonable_encoder
from user_messages.models import UserMessageCategory, UserMessagePriority
from sse.utils import send_sse_message
from sse.models import SSEEventType, SSEActionType

logger = logging.getLogger("default")


async def fix_doc(doc):
    """
    Fix doc by fixing its id.
    """

    _doc = fix_id(doc)
    return _doc


async def add_user(user=None):
    """
    Add user
    """

    from user_messages.utils import add_user_message
    from users.models import UserPreferences

    couchdb = get_cdb()
    db = await couchdb[Cdb.USERS.value]

    # if no user.preferences create.
    if "preferences" not in user or not user.preferences:
        user.preferences = UserPreferences()

    user.created = datetime.now()
    user.updated = datetime.now()

    try:
        doc = await db.create(make_id(items=[user.username]), data=jsonable_encoder(user))
        await doc.save()
        # send sse event of add.
        await send_sse_message(
            user=user.username,
            event_type=SSEEventType.user,
            data=SSEActionType.add,
        )
        # send user 'welcome' message.
        await add_user_message(
            to_user_id=doc["username"],
            category=UserMessageCategory.system,
            priority=UserMessagePriority.normal,
            subject="Welcome",
            message="Welcome, have a look around! We hope you like the app. If you have any questions of find any issues see the Help page or contact us!",
            creator="welcome",
        )
        return await doc.info()

    except aiocouch.ConflictError:
        raise HTTPException(status_code=409, detail=f"Duplicate Key: cannot add '{user}'.")


async def get_by_notify_id(notify_id: str = None):
    """
    Get user by notify_id - this is used by the SSE [setup] stream.
    """

    from users.models import User

    couchdb = get_cdb()
    db = await couchdb[Cdb.USERS.value]

    selector = {
        "enabled": True,
        "notify_id": notify_id,
    }

    docs = []
    async for doc in db.find(selector=selector):
        docs.append(fix_id(doc))

    if len(docs) == 1:
        return User(**docs[0])
    else:
        logger.error(
            f"Error Getting User By Notify Id: '{notify_id}', lookup found '{len(docs)}' matches. Matches are as follow:\n{docs}"
        )
        return User()


async def get_by_username(username):
    """
    Get user by username
    """

    couchdb = get_cdb()
    db = await couchdb[Cdb.USERS.value]

    try:
        return await db[username]
    except aiocouch.exception.NotFoundError:
        # NOTICE: we cannot raise exception here as downstream (ldap auth check) needs to account for no user found (so it can create).
        return None


async def get_user_activities(selector=None, speed_type=None, distance_unit=None):
    """
    Get user activities given a selector.
    """

    from activities.utils import calculate_distance, calculate_speed
    from users.models import UserActivityStat

    logger.debug("- get user activities...")
    couchdb = get_cdb()
    db = await couchdb["activities"]

    logger.debug(f"--> selector: {selector}")
    distance = []
    duration = []
    async for doc in db.find(selector=selector):
        distance.append(doc["distance"])
        duration.append(doc["duration"])

    uas = UserActivityStat()
    if len(distance) > 0:
        uas.activities = len(distance)
        uas.distance = await calculate_distance(distance_unit=distance_unit, distance=sum(distance))
        uas.duration = sum(duration)
        uas.speed = await calculate_speed(
            speed_type=speed_type,
            distance=uas.distance,
            duration=uas.duration,
            distance_unit=distance_unit,
        )

    return uas


async def update_user(user=None):
    """
    Update user
    """

    # ~ logger.debug(f"- updating user with: {user}")
    user.updated = datetime.now()

    try:
        doc = await get_by_username(user.username)
        _timezone = DEFAULT_USER_TIMEZONE
        # override doc timezone with user if we have it
        if user.user_timezone:
            doc["user_timezone"] = user.user_timezone

        if "user_timezone" not in doc or (doc and doc["user_timezone"] and len(doc["user_timezone"]) < 1):
            doc["user_timezone"] = DEFAULT_USER_TIMEZONE

        doc["enabled"] = user.enabled
        doc["note"] = user.note
        doc["first_name"] = user.first_name
        doc["last_name"] = user.last_name
        doc["display_name"] = user.display_name
        doc["email"] = user.email
        doc["avatar"] = jsonable_encoder(user.avatar)
        doc["user_timezone"] = _timezone
        doc["notify_id"] = user.notify_id

        # ~ logger.debug(f"  - updating user with: {doc}")
        await doc.save()

        return await doc.info()

    except aiocouch.ConflictError:
        raise HTTPException(status_code=409, detail=f"Duplicate Key: cannot update '{user}'.")

    except Exception as e:
        raise HTTPException(status_code=409, detail=f"Other update user error for user='{user}', error: {e}")


async def verify_user(user=None):
    """
    Verify given user exists.
    Returns the user if found else Raise 404.
    """

    if user:
        return await get_by_username(user)
    else:
        raise HTTPException(status_code=404, detail="Not found")


async def check_preference(user=None, preference=None, value=None):
    """
    Check if user has a given preference.
    check_preference(user='drad', preference='process_activity_notes', True)
    """

    logger.debug(f"Check Preference on user=[{user}], preference=[{preference}], value=[{value}]")

    if user and preference and value:
        _user = await get_by_username(user)
        if not _user:
            logger.error(f"ERROR: check_preferences user [{user}] not found!")
            return False
        elif "preferences" not in _user:
            logger.error("User has no preferences defined")
            return False
        elif preference not in _user["preferences"]:
            logger.error(f"User preferences does not have needed preference defined: {preference}")
            return False
        elif not _user["preferences"][preference] == value:
            # logger.debug(
            #    f"User preference of '{preference}' does not match expected value"
            # )
            return False
        else:
            # logger.debug(f"User preferences of '{preference}' matches expected value")
            return True
    else:
        logger.error("User preference and value not supplied")
        return False
