#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#  SPDX-License-Identifier: GPL-3.0-only
#  Copyright 2025 dradux.com

import logging
from datetime import datetime
from enum import Enum
from typing import Optional

from config.models import Cdb
from pydantic import BaseModel

logger = logging.getLogger("default")
_db = Cdb.LOGS


class LogStatus(str, Enum):
    new = "new"
    addressed = "addressed"
    inconsequential = "inconsequential"
    watch = "watch"


class LogLevel(str, Enum):
    error = "error"
    warning = "warning"
    info = "info"


class LogBase(BaseModel):
    """the base"""

    level: LogLevel = None  # the log level
    status: LogStatus = None  # the status of the log item
    location: str = None  # the location of the log (e.g. the component:function)
    message: str = None  # the log message


class LogExt(LogBase):
    """Extended (added by backend logic)"""

    # note: this needs to be set/overwrote on result instantiation as using
    #  datetime.now() here will only get you now of when worker was started.
    created: datetime = datetime.now()
    updated: datetime = datetime.now()

    creator: Optional[str] = None
    updator: Optional[str] = None


class Log(LogExt):
    """Actual (at DB level)"""

    id_: str


class LogResponse(Log):
    """Log Response"""

    pass
