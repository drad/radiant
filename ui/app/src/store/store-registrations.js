import Vue from 'vue'
import { api } from 'boot/axios'

const state = {
    registrations_loading: true,
    registrations: [],
    registrations_count: {
      total: 0
    },
    invoice_registrations: [],
    active_registration: {},
    show_quick_submit: false,
    time_summary_day: { length: 11, amount: 1},
    time_summary_week: { length: 22, amount: 2},
    time_summary_month: { length: 33, amount: 3},
}

const mutations = {
    setRegistrationsLoading(state, payload) {
        state.registrations_loading = payload
    },
    addRegistrations(state, payload) {
        state.registrations = payload
    },
    setRegistrationsCount(state, payload) {
        state.registrations_count.total = payload
    },
    setTimeSummaryReport(state, payload) {
        state.time_summary_day = payload.time_summary_day;
        state.time_summary_week = payload.time_summary_week;
        state.time_summary_month = payload.time_summary_month;
    },
    set_active_registration(state, registration) {
        state.active_registration = registration
    },
    set_invoice_registrations(state, registrations) {
        //~ console.log("store-registrations > mutations> Setting invoice registrations to:", registrations);
        state.invoice_registrations = registrations
    },
}

const actions = {
    fetchRegistrations({ commit }, payload) {
        commit('setRegistrationsLoading', true)
        //~ console.log("store > registrations > fetchRegistrations - payload:", payload)
        api.get('/registrations/all-for-user', { params: payload})
            .then((response) => {
                //~ console.log(' ⟶ fetch registrations return:', JSON.stringify(response.data))
                commit('addRegistrations', response.data.registrations)
                commit('setRegistrationsCount', response.data.total)
            })
            .catch(() => {
                // note: this is hit when an unauthenticated user hits / which redirects them to /login
                //       no harm in the message so we just dump to console.
                console.log('No registrations found.')
            })
        commit('setRegistrationsLoading', false)
    },
    fetchTimeSummaryReport({ commit }, payload) {
        //~ console.log("store > registrations > fetchTimeSummaryReport - payload:", payload)
        api.get('/registrations/time_summary_report', { params: payload})
            .then((response) => {
                //~ console.log(' ⟶ fetch time summary report return:', JSON.stringify(response.data))
                commit('setTimeSummaryReport', response.data)
            })
            .catch(() => {
                // note: this is hit when an unauthenticated user hits / which redirects them to /login
                //       no harm in the message so we just dump to console.
                console.log('No Time Summary Report data found.')
            })
    },
    addRegistration({ commit, dispatch }, registration) {
        //~ console.log("--> Adding Registration:", registration);
        return new Promise((resolve, reject) => {
            var querystring = require('querystring');
            // strip off nulls and cruft.
            let _registration = Object.fromEntries(Object.entries(registration).filter(([_, v]) => v != null));

            api.post('/registrations/?' + querystring.stringify(_registration))
            .then((response) => {
                // no state to change as we fetch later to get all
                resolve(response.data)
            })
            .catch((error) => {
                reject(error)
            })
        })
    },
    updateRegistration({ commit, dispatch }, registration) {
        //~ console.log("--> Updating Registration:", registration);
        return new Promise((resolve, reject) => {
            var querystring = require('querystring');
            // strip off nulls.
            let _registration = Object.fromEntries(Object.entries(registration).filter(([_, v]) => v != null));
            // strip off cruft.
            delete _registration.id_;
            delete _registration._rev;
            delete _registration.created;
            delete _registration.updated;
            delete _registration.creator;
            delete _registration.updator;
            delete _registration.task;
            delete _registration.registration_type_full;
            //~ console.log("    Cleaned Registration:", _registration);
            api.put('/registrations/?' + querystring.stringify(_registration))
            .then((response) => {
                //~ console.log("store-registration > updateRegistration (action) > response:", response);
                // no state to change as we fetch later to get all
                resolve(response.data)
            })
            .catch((error) => {
                reject(error)
            })
        })
    },
    invoiceRegistration({ commit, dispatch }, registration) {
        //~ console.log("--> Invoice Registration:", registration);
        return new Promise((resolve, reject) => {
            var querystring = require('querystring');
            // strip off nulls.
            let _registration = Object.fromEntries(Object.entries(registration).filter(([_, v]) => v != null));
            api.put('/registrations/invoice/?' + querystring.stringify(_registration))
            .then((response) => {
                //~ console.log("store-registration > invoiceRegistration (action) > response:", response);
                // no state to change as we fetch later to get all
                resolve(response.data)
            })
            .catch((error) => {
                reject(error)
            })
        })
    },
    deleteRegistration({ commit }, id) {
        //~ console.log("store-registration > action > deleteRegistration with id: ", id)
        var querystring = require('querystring');
        return new Promise((resolve, reject) => {
            api.delete('/registrations/?' + querystring.stringify({ _id: id }))
                .then((response) => {
                    //~ console.log('  - delete registration returned: ', JSON.stringify(response.data))
                    // no state to change as we fetch later to get all
                    resolve(true)
                })
            .catch((error) => {
                // note: this is hit when an unauthenticated user hits / which redirects them to /login
                //       no harm in the message so we just dump to console.
                //~ console.log('No tasks found.')
                console.log("Error in deleting registration: ", error)
                reject(error)
            })
        })
    },
    setActiveRegistration({ commit }, registration) {
      commit('set_active_registration', registration)
    },
    setInvoiceRegistrations({ commit }, registrations) {
      commit('set_invoice_registrations', registrations)
    },
}

const getters = {
    registrations_loading: (state) => {
      return state.registrations_loading
    },
    registrations: (state) => {
      return state.registrations
    },
    invoice_registrations: (state) => {
        return state.invoice_registrations
    },
    active_registration: (state) => {
      return state.active_registration
    },
    show_quick_submit: (state) => {
      return state.show_quick_submit
    },
    registrations_count: (state) => {
        return state.registrations_count
    },
    time_summary_day: (state) => {
      return state.time_summary_day
    },
    time_summary_week: (state) => {
      return state.time_summary_week
    },
    time_summary_month: (state) => {
      return state.time_summary_month
    },
}

export default {
    namespaced: true,

    state,
    mutations,
    actions,
    getters
}
