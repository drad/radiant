#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#  SPDX-License-Identifier: GPL-3.0-only
#  Copyright 2025 dradux.com

import logging
import uuid
from datetime import datetime
import aiocouch
from _common.models import DocInfo
from _common.utils import fix_id, make_id
from clients.utils import get_by_id as client_get_by_id
from config.config import get_cdb
from fastapi import APIRouter, HTTPException, Query, Security, status
from fastapi.encoders import jsonable_encoder
from projects.models import Project, ProjectExt, ProjectResponse, _db
from projects.utils import get_by_client_id
from users.models import UserBase, get_current_user
from sse.utils import send_sse_message
from sse.models import SSEEventType, SSEActionType

logger = logging.getLogger("default")
projects_router = APIRouter()


async def _get_or_404(_id, db):
    """
    Get a document or raise a 404
    """

    # ~ logger.debug(f"_get_or_404 request on: {_id}")
    try:
        return await db[_id]
    except aiocouch.exception.NotFoundError:
        raise HTTPException(status_code=404, detail="Not found")


@projects_router.get("/all", response_model=ProjectResponse)
async def get_all(
    limit: int = Query(10, ge=1, le=99, description="Limit number of results"),
    skip: int = Query(0, description="Skip number of records before applying limit"),
    current_user: UserBase = Security(get_current_user, scopes=["admins"]),
):
    """
    Get all projects
    """

    couchdb = get_cdb()
    logger.debug(f"Get all: current_user: {current_user}")
    db = await couchdb[_db.value]

    _response = ProjectResponse()
    docs = []
    async for doc in db.find(selector={}, limit=limit, skip=skip):
        _doc = fix_id(doc)
        docs.append(Project(**_doc))

    _response.total = len(docs)
    _response.projects = docs
    # ~ logger.critical(f"ProjectResponse: {_response}")

    return _response


@projects_router.get(
    "/by-client-id/{_client_id}", response_model=ProjectResponse, summary="Get all Projects for Client Id"
)
async def get_for_client_id(
    _client_id: str,
    current_user: UserBase = Security(get_current_user, scopes=["users"]),
):
    """
    Get all for client_id.
    """

    _response = ProjectResponse()

    _projects = await get_by_client_id(_client_id)
    # ~ logger.critical(f"Projects: {_projects}")
    _response.total = len(_projects)
    _response.projects = _projects

    return _response


@projects_router.get("/one/{_id}", response_model=Project, summary="Get one Project")
async def get_one(
    _id: str,
    current_user: UserBase = Security(get_current_user, scopes=["users"]),
):
    """
    Get one by id
    """

    couchdb = get_cdb()
    db = await couchdb[_db.value]

    return Project(**fix_id(await _get_or_404(_id, db)))


@projects_router.post("/", response_model=DocInfo, status_code=status.HTTP_201_CREATED)
async def add(
    client: str = Query(..., description="id of the client that the project belongs to"),
    name: str = Query(..., description="name of the project"),
    nickname: str = Query(
        ...,
        min_length=1,
        max_length=4,
        description="nickname of the project | <i>value will be uppercased</i>",
    ),
    order: int = Query(
        100,
        description="order of the project (lower number will be sorted to the top of lists)",
    ),
    bill_rate: float = Query(0.0, description="hourly bill rate"),
    expected_hours_per_day=Query(0.0, description="expected hours per day (used for reporting)"),
    note: str = Query(None, description="notes for the project"),
    enabled: bool = Query(True, description="is this item enabled?"),
    current_user: UserBase = Security(get_current_user, scopes=["admins"]),
):
    """
    Add new
    """

    logger.debug(f"Add project with client id of: {client}")
    couchdb = get_cdb()
    db = await couchdb[_db.value]

    # check to ensure client exists.
    _client = await client_get_by_id(client)
    if not _client:
        raise HTTPException(
            status_code=417,
            detail="Client does not exist, did you enter the Name instead of Id?",
        )

    _doc = ProjectExt(
        client_id=client,
        name=name,
        nickname=nickname.upper(),
        order=order,
        bill_rate=bill_rate,
        expected_hours_per_day=expected_hours_per_day,
        note=note,
        enabled=enabled,
        created=datetime.now(),
        updated=datetime.now(),
        creator=current_user.username,
    )

    try:
        doc_id = make_id(items=[str(uuid.uuid4())])
        doc = await db.create(
            doc_id,
            data=jsonable_encoder(_doc),
        )
        await doc.save()
        # send sse message.
        await send_sse_message(user=current_user.username, event_type=SSEEventType.project, data=SSEActionType.add)
        return await doc.info()

    except aiocouch.ConflictError:
        raise HTTPException(status_code=409, detail=f"Duplicate Key: cannot add '{doc_id}'.")


@projects_router.put("/", response_model=DocInfo)
async def update(
    _id: str = Query(..., description="id of the document to update"),
    client_id: str = Query(None, description="client that the Project belongs to"),
    name: str = Query(None, description="name of the project"),
    nickname: str = Query(None, min_length=1, max_length=4, description="nickname of the project"),
    order: int = Query(
        None,
        description="order of the project (lower number will be sorted to the top of lists)",
    ),
    bill_rate: float = Query(None, description="hourly bill rate"),
    expected_hours_per_day=Query(None, description="expected hours per day (used for reporting)"),
    note: str = Query(None, description="notes for the project"),
    enabled: bool = Query(True, description="is this item enabled?"),
    current_user: UserBase = Security(get_current_user, scopes=["admins"]),
):
    """
    Update
    """

    logger.debug("- updating registration type")
    couchdb = get_cdb()
    db = await couchdb[_db.value]
    doc = await _get_or_404(_id, db)
    logger.debug(f"- updating doc: {doc}")

    if client_id:
        # check to ensure client exists.
        _client = await client_get_by_id(client_id)
        if not _client:
            raise HTTPException(
                status_code=417,
                detail="Client does not exist, did you enter the Name instead of the Id?",
            )
        doc["client_id"] = client_id

    doc["name"] = name if name else doc["name"]
    doc["nickname"] = nickname.upper() if nickname else doc["nickname"]
    doc["order"] = order if order else doc["order"]
    doc["bill_rate"] = bill_rate if bill_rate else doc["bill_rate"]
    doc["expected_hours_per_day"] = expected_hours_per_day if expected_hours_per_day else doc["expected_hours_per_day"]
    doc["note"] = note if note else doc["note"]
    doc["enabled"] = enabled

    doc["updated"] = jsonable_encoder(datetime.now())
    doc["updator"] = current_user.username
    # logger.debug(f"doc before save: {doc}")
    await doc.save()
    # send sse message.
    await send_sse_message(user=current_user.username, event_type=SSEEventType.project, data=SSEActionType.update)
    return await doc.info()


@projects_router.delete("/", response_model=DocInfo)
async def delete(
    _id: str,
    current_user: UserBase = Security(get_current_user, scopes=["admins"]),
):
    """
    Hard Delete by id
    """

    couchdb = get_cdb()
    db = await couchdb[_db.value]
    resp = DocInfo(ok=False, id=_id, rev="", msg="unknown")
    doc = await _get_or_404(_id, db)
    dr = await doc.delete()
    if dr and dr.status < 400:
        resp.ok = True
        resp.msg = "deleted"
        # send sse message.
        await send_sse_message(user=current_user.username, event_type=SSEEventType.project, data=SSEActionType.delete)
    else:
        resp.ok = False
        resp.msg = f"Delete status: {dr.status}, headers: {dr.headers}"

    return resp
