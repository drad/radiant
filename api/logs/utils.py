#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#  SPDX-License-Identifier: GPL-3.0-only
#  Copyright 2025 dradux.com

import logging
import uuid
from datetime import datetime

import aiocouch
from _common.utils import make_id
from config.config import get_cdb
from fastapi.encoders import jsonable_encoder
from logs.models import LogExt, LogLevel, LogStatus, _db

logger = logging.getLogger("default")


async def add_log_message(
    level: LogLevel = LogLevel.error,
    status: LogStatus = LogStatus.new,
    location: str = None,
    message: str = None,
    creator: str = None,
):
    """
    Add a log message.
    """

    couchdb = get_cdb()
    db = await couchdb[_db.value]

    log = LogExt(
        level=level,
        status=status,
        location=location,
        message=message,
        created=datetime.now(),
        updated=datetime.now(),
        creator=creator,
    )

    logger.critical(
        f"Log Message Received at {log.created}: level={level}, status={status}, location={location}, message={message}"
    )

    try:
        doc_id = make_id(items=[str(uuid.uuid4())])
        doc = await db.create(
            doc_id,
            data=jsonable_encoder(log),
        )
        await doc.save()
        return await doc.info()

    except aiocouch.ConflictError:
        logger.error(f"Duplicate key: cannot add '{doc_id}'")
        return None
