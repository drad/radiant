"""cleanup ticketing

Revision ID: d867a3326adc
Revises: 473671d91ba3
Create Date: 2020-10-15 06:30:43.543360

"""
from alembic import op


# revision identifiers, used by Alembic.
revision = "d867a3326adc"  # pragma: allowlist secret
down_revision = "473671d91ba3"  # pragma: allowlist secret
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_constraint("ticketing_tid_key", "ticketing", type_="unique")
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_unique_constraint("ticketing_tid_key", "ticketing", ["tid"])
    # ### end Alembic commands ###
