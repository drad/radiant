/*
 * SSE: handles SSE related items.
 */

import VueSSE from 'vue-sse';


const state = {
  loading: false,
  sseClient: {},
}

const mutations = {
  setLoading(state, payload) {
    state.loading = payload
  },
  setSseClient(state, payload) {
    state.sseClient = payload
  },
  // 2022-03-05:drad not yet implemented
  //~ connectSseClient(state, payload) {
    //~ state.sseClient =
  //~ },
  disconnectSseClient(state, payload) {
    state.sseClient.disconnect()
  },
}

const actions = {
  setSseClient( { commit }, payload) {
    //set sseClient.
    commit('setSseClient', payload)
  },
  disconnectSseClient( { commit }, payload) {
    commit('disconnectSseClient', payload)
  },
  setupStream({ commit, dispatch, rootState, rootGetters }, payload) {
    //~ console.log("Setting up stream with payload:", payload);
    let sse_url = process.env.UI_SSE_URL + payload.notify_id   // + ":" + payload.account;
    //~ console.log("SSE Stream URL: " + sse_url);
    payload.sseClient = payload.sse.create(sse_url)
      /*
       * NOTICE: on 2024-08-23 we removed all redis logic for SSE and change the structure of the Event being sent back to use the Type as the type of event (e.g. user_message, task, etc.) rather than pass back a generic 'message' and then parse it on the UI to see what type of message was sent. Before we had a listener as follows:
       *   .on('message', (msg) => console.info('Message:', msg))
       * Now we have a listener as follows:
       *   .on('{Event}', (data) => {})
       * In addition, when a user logs in the API sends a defined set of events which are handled by the following to fetch the initial data for a user, this is done in the `auth.utils.send_user_post_login_ui_events` endpoint.
       */
      .on('invoice', (data) => {
        const actions = ['new', 'add', 'update', 'delete']
        //~ console.log("SSEMessage - type=invoice, data=" + data);
        if ( actions.includes(data) ) {
          //console.log("Need to fetchActivityMedia...");
          //~ dispatch("events/fetchEvents", {enabled: true, limit: 999, skip: 0}, { root: true });
          dispatch("invoices/fetchInvoicesList", null, { root: true });
        } else {
          console.error("Unknown/unhandled data for invoice, data=[" + data + "]");
        }
      })
      .on('registration', (data) => {
        const actions = ['new', 'add', 'update', 'delete']
        //let user = rootGetters['user/user'];
        //~ console.log("SSEMessage - type=registration, data=" + data);
        if ( actions.includes(data) ) {
          dispatch("registrations/fetchRegistrations", {limit: 20}, { root: true });
          // also fetch time summary report.
          dispatch("registrations/fetchTimeSummaryReport", {}, { root: true });
        } else {
          console.error("Unknown/unhandled data for registration, data=[" + data + "]");
        }
      })
      .on('client', (data) => {
        const actions = ['new', 'add', 'update', 'delete']
        //let user = rootGetters['user/user'];
        //~ console.log("SSEMessage - type=client, data=" + data);
        if ( actions.includes(data) ) {
          dispatch("clients/fetchClients", null, { root: true });
        } else {
          console.error("Unknown/unhandled data for registration, data=[" + data + "]");
        }
      })
      .on('task_list', (data) => {
        const actions = ['new', 'add', 'update', 'delete']
        //let user = rootGetters['user/user'];
        //~ console.log("SSEMessage - type=task_list, data=" + data);
        if ( actions.includes(data) ) {
          dispatch("tasks/fetchTaskList", null, { root: true });
        } else {
          console.error("Unknown/unhandled data for registration, data=[" + data + "]");
        }
      })
      .on('user_message', (data) => {
        const actions = ['new', 'add', 'update', 'delete']
        //~ console.log("SSEMessage - type=user_message, data=" + data);
        if ( actions.includes(data) ) {
          dispatch("user_messages/fetchUserMessages", null, { root: true });
        } else {
          console.error("Unknown/unhandled data for user_message, data=[" + data + "]");
        }
      })
      .on('error', (err) => {
        // handle connection error which happens if BE is reloaded or there are network issues.
        console.error('Failed to parse or lost connection:', err)
        payload.sseClient.connect();
      })
      .connect()
        .then(sse => {
          console.log('⚓ SSE Stream connected: ' + sse_url);
        })
      .catch((err) => console.error('Failed to make initial connection:', err));
  },
}

const getters = {
    loading: (state) => {
      return state.loading
    },
    sseClient: (state) => {
      return state.sseClient
    },
}

export default {
    namespaced: true,

    state,
    mutations,
    actions,
    getters
}
