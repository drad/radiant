import { api } from 'boot/axios'

const state = {
    clients: [],
    clients_count: {
      total: 0
    },
}

const mutations = {
    setClients(state, payload) {
        state.clients = payload;
    },
    setClientsCount(state, payload) {
        state.clients_count.total = payload
    },
}

const actions = {
    fetchClients({ commit }, payload) {
        return new Promise((resolve, reject) => {
            api.get('clients/all')
              .then((response) => {
                  commit('setClients', response.data.clients)
                  commit('setClientsCount', response.data.total)
                  resolve(true)
              })
              .catch((error) => {
                  //~ console.log("Error in adding Activity:", error)
                  reject(error)
              })
        })
    },
}

const getters = {
    //~ client: (state) => {
        //~ return state.client
    //~ },
    clients: (state) => {
        return state.clients
    },
}

export default {
    namespaced: true,

    state,
    mutations,
    actions,
    getters
}
