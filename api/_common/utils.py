#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#  SPDX-License-Identifier: GPL-3.0-only
#  Copyright 2025 dradux.com

import arrow
import logging
from typing import List, Union
from datetime import datetime

import aiocouch
from config.config import CDB_URI, ROUTE_AUTH, cdb_httpx_auth, get_cdb
from config.models import Cdb
from httpx import AsyncClient, codes

logger = logging.getLogger("default")


async def check_databases():
    """
    Check to ensure needed databases, indexes, and views exist, if not create them.

    @NOTES
    - indexes and views are dropped and recreated
    """

    couchdb = get_cdb()
    logger.info("Create Databases:")
    for db in Cdb:
        try:
            # attempt connect - throws NotFoundError.
            await couchdb[db.value]
            logger.info(f"  ✓ {db.value}")
        except aiocouch.NotFoundError:
            await couchdb.create(db.value)
            logger.info(f"  ✦ {db.value} (missing but we created it!)")
        except Exception as e:
            logger.error(f"Other error occurred during create databases: {e}")

    # check views and indexes
    async with AsyncClient() as session:
        await _check_db_views(session=session)
        await _check_db_indexes(session=session)


async def _check_db_views(session=None):
    """
    Check db views.
    """

    from config.db.views import views

    logger.info("Create Views:")
    try:
        if len(views) < 1:
            logger.info("  - no views defined")
        for view in views:
            # ~ logger.debug(f"- create / update views for database: {view['database']}")
            for ddoc in view["views"]:
                _local_ddoc_id = ddoc["_id"]
                # ~ logger.debug(f"  - {view['database']}/{_local_ddoc_id}")
                _rev_tag = ""

                # ~ logger.debug(f"  - local (before): {ddoc}")
                # remove the rev as it will get replaced (on updates) or needs to be empty (on creates).
                del ddoc["_rev"]
                # remove the _id as it is not needed
                del ddoc["_id"]
                del ddoc["language"]

                # get currently deployed ddoc to get rev# for update
                #   if no ddoc exists it is a new (create) so we dont need a rev#
                _curr = await session.get(f"{CDB_URI}/{view['database']}/{_local_ddoc_id}", auth=cdb_httpx_auth)

                if _curr.status_code == codes.ok:
                    j = _curr.json()
                    if "error" in j and j["error"] == "not_found":
                        # new design, need to create.
                        _rev_tag = ""
                    else:
                        # existing view, update it.
                        ddoc["rev"] = j["_rev"]
                        _rev_tag = f"?rev={j['_rev']}"

                else:
                    logger.error("ERROR: Something else happened...")

                # create/update the view...
                # ~ logger.debug(f"    - updating [{view['database']}/{_local_ddoc_id}{_rev_tag}] with: {ddoc}")
                _upd = await session.put(
                    f"{CDB_URI}/{view['database']}/{_local_ddoc_id}{_rev_tag}", auth=cdb_httpx_auth, json=ddoc
                )
                updj = _upd.json()
                # ~ logger.debug(f"VIEW Create/Update response: {updj}")
                if updj and "ok" in updj and updj["ok"] is True:
                    # ~ logger.debug("    - ✓ view created/updated")
                    logger.info(f"  ✓ {view['database']}/{_local_ddoc_id}")
                else:
                    # ~ logger.error(f"    - ✗ error updating view: {j}")
                    logger.info(f"  ✗ {view['database']}/{_local_ddoc_id}: {updj}")

    except Exception as e:
        logger.error(f"Other error occurred during create/update view: {e}")


async def _check_db_indexes(session=None):
    """
    Check db views.
    """
    from config.db.indexes import indexes

    logger.info("Create Indexes:")
    try:
        if len(indexes) < 1:
            logger.info("  - no indexes defined")
        for index in indexes:
            # ~ logger.debug(f"- create/update index for database: {index['database']}")
            for i in index["indexes"]:
                # ~ logger.debug(f"  - index name: {i['name']}")
                if i["type"] == "special":
                    logger.info(f"  ✗ {index['database']}/{i['name']}: cannot create special index (skipped)")
                else:
                    _index_def = i.pop("def")
                    i["index"] = _index_def
                    # ~ logger.debug(f"- built index is: {i}")
                    _resp = await session.post(f"{CDB_URI}/{index['database']}/_index", auth=cdb_httpx_auth, json=i)
                    j = _resp.json()
                    # ~ logger.debug(f"INDEX Create/Update response: {j}")

                    if j and "result" in j and j["result"] == "created":
                        # ~ logger.debug("    - ✓ view created/updated")
                        logger.info(f"  ✓ {index['database']}/{i['name']}: created")
                    elif j and "result" in j and j["result"] == "exists":
                        logger.info(f"  ✓ {index['database']}/{i['name']}: already exists (skipped)")
                    else:
                        # ~ logger.error(f"    - ✗ error updating view: {j}")
                        logger.error(f"  ✗ {index['database']}/{i['name']}: other error ({j})")

    except Exception as e:
        logger.error(f"Other error occurred during create/update index: {e}")
        raise


def codes_to_list(codes: Union[str, List] = None):
    """
    Convert string of codes to list of codes - supporting ranges
    """

    if isinstance(codes, List):
        return codes
    else:
        r = []
        for i in codes.split(","):
            if "-" not in i:
                r.append(int(i))
            else:
                _l, _h = map(int, i.split("-"))
                r += range(_l, _h + 1)
        return r


def fix_id(obj):
    if obj and obj.get("_id", False):
        obj["id_"] = str(obj["_id"])
        return obj
    else:
        # ~ raise ValueError(f"No `_id` found! Unable to fix ID for: {obj}")
        return None


def get_route_roles(frame):
    """
    Get a route role
    Note: this uses the inspect.currentframe() to get the package (e.g. notifiers)
      and the method (e.g. get_by_username) which are used to get the appropriate
      route role from configs. Route roles in configs must be in form of
      package.method (e.g. notifiers.get_by_username).
    """

    from auth.models import Role

    try:
        auth = ROUTE_AUTH.get(frame.f_globals["__package__"]).get(frame.f_code.co_name)
        roles = []
        for r in auth:
            roles.append(Role(r))

        return roles
    except ValueError as e:
        logger.error(
            f"Looks like the package / method is not set up in api.route_auth (in config.toml)\n Error Message: {e}"
        )
        return None


async def local_date_to_utc(indate: datetime = None, tz: str = "US/Eastern"):
    """
    Convert local date (YYYY-MM-DD HH:mm.SSS) in user's Timezone setting to UTC.
    """

    _ret = None
    try:
        _ret = arrow.get(indate, tz).to("utc").datetime if indate else None
    except Exception:
        _ret = arrow.get(indate).to("utc").datetime if indate else None

    return _ret


def make_id(items: list = []):
    """
    Make id given the items supplied
        - concatenate all items with a : (e.g. item1:item2:item3
        - replace all spaces with - (dashes) (e.g. item 1 --> item-1)
        - transform entire string to lower case (e.g. Item1 --> item1)
    @return: id (as string)
    """

    _id = ":".join(items)

    return _id.lower().replace(" ", "-")


async def is_user(username: str) -> bool:
    """
    Check if user is a valid application user.
    """

    from users.utils import get_by_username

    # @TODO: this should likely get list of users out of redis to improve performance but should check.
    user = await get_by_username(username)
    if user:
        return True
    else:
        return False


async def utc_to_local_date(indate: datetime = None, tz: str = "US/Eastern"):
    """
    Convert utc to local date (YYYY-MM-DD HH:mm.SSS).
    - NOTE: indate is assumed to already be in UTC timezon.
    """

    return arrow.get(indate).to(tz).format("YYYY-MM-DD HH:mm:ss") if indate else None
