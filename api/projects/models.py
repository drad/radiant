#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#  SPDX-License-Identifier: GPL-3.0-only
#  Copyright 2025 dradux.com

import logging
from datetime import datetime
from typing import List

from config.models import Cdb
from pydantic import BaseModel
from clients.models import ClientBase

logger = logging.getLogger("default")
_db = Cdb.PROJECTS


class ProjectBase(BaseModel):
    """
    Base document
    """

    client_id: str | None = None  # client the project belongs to
    name: str | None = None  # client name
    nickname: str | None = None  # client nickname
    order: int | None = None  # lower number will be sorted to the top of lists
    bill_rate: float = 0  # the hourly bill rate
    expected_hours_per_day: float = 0  # expected hours per day (used for reporting)
    note: str | None = None  # notes about item
    enabled: bool = True  # is the item enabled?


class ProjectExt(ProjectBase):
    """
    Extended (added by backend logic)
    """

    # note: this needs to be set/overwrote on result instantiation as using
    #  datetime.now() here will only get you now of when worker was started.
    created: datetime = datetime.now()
    updated: datetime = datetime.now()

    creator: str | None = None
    updator: str | None = None


class Project(ProjectExt):
    """
    Actual (at DB level)
    """

    id_: str


class ProjectResponse(BaseModel):
    """
    Project Response
    """

    total: int = 0
    projects: List[Project] | None = None


class ProjectEnhanced(ProjectBase):
    """
    Project enhanced.
    """

    client: ClientBase | None = None
