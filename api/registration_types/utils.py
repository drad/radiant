#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#  SPDX-License-Identifier: GPL-3.0-only
#  Copyright 2025 dradux.com

# ~ import logging

import aiocouch
from config.config import get_cdb
from registration_types.models import _db


async def get_by_id(_id: str):
    """
    Get a registraton type by id
    """

    couchdb = get_cdb()
    db = await couchdb[_db.value]

    try:
        return await db[_id]
    except aiocouch.exception.NotFoundError:
        return None
