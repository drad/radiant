# include all 'non-dev' requirements
-r requirements.txt

# add dev requirements

pip-review==1.3.0
alt_pytest_asyncio==0.8.2
# alt_pytest_asyncio pinned to 0.8.2 as 0.9.0 has breaking event loop issues
watchfiles==1.0.4
