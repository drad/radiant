import json
import logging

import arrow
from flask import redirect, url_for
from flask_admin import BaseView, expose
from flask_admin.contrib import sqla
from flask_admin.form import rules
from flask_security import current_user
from wtforms.fields import BooleanField

logger = logging.getLogger("main_logger")


class FilesystemBenchmarkHelpView(BaseView):
    @expose("/")
    def index(self):
        return self.render("help/filesystem_benchmark.html")


class FilesystemBenchmarkRunMethodView(sqla.ModelView):
    def is_accessible(self):
        return (
            True
            if current_user.is_authenticated and current_user.has_role("admins")
            else False
        )

    def inaccessible_callback(self, name, **kwargs):
        return redirect(url_for("ldap_login_view"))

    def _handle_view(self, name, **kwargs):
        if not self.is_accessible():
            return self.inaccessible_callback(name, **kwargs)
        self.can_delete = True if current_user.has_role("admins") else False

    can_view_details = True
    create_modal = True
    edit_modal = True
    details_modal = True
    can_export = True
    page_size = 20

    column_searchable_list = ["name"]
    column_filters = ["name", "order"]
    column_editable_list = ["name", "order"]
    column_list = ["order", "name"]
    column_default_sort = ("order", False)

    form_widget_args = {
        "order": {
            "placeholder": "set order",
            "title": "specify an order for the item",
        },
        "name": {
            "placeholder": "name",
            "title": "name of item",
        },
    }


class FilesystemBenchmarkDiskTypeView(sqla.ModelView):
    def is_accessible(self):
        return (
            True
            if current_user.is_authenticated and current_user.has_role("admins")
            else False
        )

    def inaccessible_callback(self, name, **kwargs):
        return redirect(url_for("ldap_login_view"))

    def _handle_view(self, name, **kwargs):
        if not self.is_accessible():
            return self.inaccessible_callback(name, **kwargs)
        self.can_delete = True if current_user.has_role("admins") else False

    can_view_details = True
    create_modal = True
    edit_modal = True
    details_modal = True
    can_export = True
    page_size = 20

    column_searchable_list = ["name"]
    column_filters = ["name", "order"]
    column_editable_list = ["name", "order"]
    column_list = ["order", "name"]
    column_default_sort = ("order", False)

    form_widget_args = {
        "order": {
            "placeholder": "set order",
            "title": "specify an order for the item",
        },
        "name": {
            "placeholder": "name",
            "title": "name of item",
        },
    }


class FilesystemBenchmarkFsKindView(sqla.ModelView):
    def is_accessible(self):
        return (
            True
            if current_user.is_authenticated and current_user.has_role("admins")
            else False
        )

    def inaccessible_callback(self, name, **kwargs):
        return redirect(url_for("ldap_login_view"))

    def _handle_view(self, name, **kwargs):
        if not self.is_accessible():
            return self.inaccessible_callback(name, **kwargs)
        self.can_delete = True if current_user.has_role("admins") else False

    can_view_details = True
    create_modal = True
    edit_modal = True
    details_modal = True
    can_export = True
    page_size = 20

    column_searchable_list = ["name"]
    column_filters = ["name", "order"]
    column_editable_list = ["name", "order"]
    column_list = ["order", "name"]
    column_default_sort = ("order", False)

    form_widget_args = {
        "order": {
            "placeholder": "set order",
            "title": "specify an order for the item",
        },
        "name": {
            "placeholder": "name",
            "title": "name of item",
        },
    }


class FilesystemBenchmarkFsTypeView(sqla.ModelView):
    def is_accessible(self):
        return (
            True
            if current_user.is_authenticated and current_user.has_role("admins")
            else False
        )

    def inaccessible_callback(self, name, **kwargs):
        return redirect(url_for("ldap_login_view"))

    def _handle_view(self, name, **kwargs):
        if not self.is_accessible():
            return self.inaccessible_callback(name, **kwargs)
        self.can_delete = True if current_user.has_role("admins") else False

    can_view_details = True
    create_modal = True
    edit_modal = True
    details_modal = True
    can_export = True
    page_size = 20

    column_searchable_list = ["name"]
    column_filters = ["name", "order"]
    column_editable_list = ["name", "order"]
    column_list = ["order", "name"]
    column_default_sort = ("order", False)

    form_widget_args = {
        "order": {
            "placeholder": "set order",
            "title": "specify an order for the item",
        },
        "name": {
            "placeholder": "name",
            "title": "name of item",
        },
    }


class FilesystemBenchmarkRunLocationTypeView(sqla.ModelView):
    def is_accessible(self):
        return (
            True
            if current_user.is_authenticated and current_user.has_role("admins")
            else False
        )

    def inaccessible_callback(self, name, **kwargs):
        return redirect(url_for("ldap_login_view"))

    def _handle_view(self, name, **kwargs):
        if not self.is_accessible():
            return self.inaccessible_callback(name, **kwargs)
        self.can_delete = True if current_user.has_role("admins") else False

    can_view_details = True
    create_modal = True
    edit_modal = True
    details_modal = True
    can_export = True
    page_size = 20

    column_searchable_list = ["name"]
    column_filters = ["name", "order"]
    column_editable_list = ["name", "order"]
    column_list = ["order", "name"]
    column_default_sort = ("order", False)

    form_widget_args = {
        "order": {
            "placeholder": "set order",
            "title": "specify an order for the item",
        },
        "name": {
            "placeholder": "name",
            "title": "name of item",
        },
    }


class FilesystemBenchmarkView(sqla.ModelView):
    def is_accessible(self):
        return (
            True
            if current_user.is_authenticated and current_user.has_role("admins")
            else False
        )

    def inaccessible_callback(self, name, **kwargs):
        return redirect(url_for("ldap_login_view"))

    def _handle_view(self, name, **kwargs):
        if not self.is_accessible():
            return self.inaccessible_callback(name, **kwargs)
        self.can_delete = True if current_user.has_role("admins") else False

    def _results_read_iops_formatter(view, context, model, name):
        if model.results_read_iops is not None:
            return round(model.results_read_iops, 2)

    def _results_write_iops_formatter(view, context, model, name):
        if model.results_write_iops is not None:
            return round(model.results_write_iops, 2)

    def _run_time_formatter(view, context, model, name):
        if model.run_time is not None:
            return round(model.run_time, 2)

    page_size = 10
    can_set_page_size = True
    can_view_details = True
    create_modal = True
    edit_modal = True
    details_modal = True
    can_export = True

    column_searchable_list = ["reason", "fs_notes", "run_command", "run_notes", "fs_os"]
    column_filters = [
        "run_date",
        "fs_host",
        "fs_type.name",
        "fs_kind.name",
        "fs_disk_name",
        "fs_disk_type.name",
        "run_location_type.name",
        "run_method.name",
        "run_time",
        "run_location_host",
        "run_location_mountpoint",
        "fs_os",
        "run_command",
        "run_notes",
        "fs_notes",
        "fs_disk_size",
        "results_read_iops",
        "results_write_iops",
        "results_notes",
        "reason",
    ]
    column_list = [
        "run_date",
        "fs_host",
        "fs_type",
        "fs_kind",
        "fs_disk_type",
        "fs_disk_name",
        "run_location_type",
        "run_method",
        "run_time",
        "results_read_iops",
        "results_write_iops",
        "reason",
    ]
    column_editable_list = [
        "run_method",
        "run_time",
        "run_location_host",
        "run_location_type",
        "fs_host",
        "fs_type",
        "fs_kind",
        "fs_disk_type",
        "fs_disk_name",
        "run_location_type",
        # "results_read_iops",
        # "results_write_iops",
        "reason",
    ]
    column_labels = dict(
        fs_host="FS Host",
        fs_type="FS Type",
        fs_kind="FS Kind",
        fs_os="FS OS",
        fs_notes="FS Notes",
        fs_disk_name="FS Disk Name",
        fs_disk_type="FS Disk Type",
        fs_disk_size="FS Disk Size",
        results_read_iops="Read IOPS",
        results_write_iops="Write IOPS",
    )
    # sort by test_date, descending
    column_default_sort = ("run_date", True)
    column_formatters = {
        "results_read_iops": _results_read_iops_formatter,
        "results_write_iops": _results_write_iops_formatter,
        "run_time": _run_time_formatter,
    }
    form_extra_fields = {
        "results_raw_use": BooleanField(
            "Use Raw Results", default="checked", false_values=(False, "false", "")
        ),
        # ~ 'results_raw_use': BooleanField("Use Raw Results"),
    }
    form_columns = (
        "run_date",
        "run_location_host",
        "run_location_type",
        "run_location_mountpoint",
        "run_method",
        "run_command",
        "run_notes",
        "reason",
        "fs_host",
        "fs_os",
        "fs_type",
        "fs_kind",
        "fs_notes",
        "fs_disk_name",
        "fs_disk_type",
        "fs_disk_size",
        "run_time",
        "results_read_iops",
        "results_write_iops",
        "results_raw",
        "results_raw_use",
        "results_notes",
    )
    form_widget_args = {
        "run_date": {
            "placeholder": "YYYY-MM-DD HH:mm:ss",
            "title": "date/time run was started",
            "data-date-format": "YYYY-MM-DD HH:mm:ss",  # changes how the DateTimeField displays the time
            "data-role": "",  # prevent the datepicker from displaying as it causes more problems than it is worth.
        },
        "run_location_host": {
            "placeholder": "gaz, val, server1, client2, etc.",
            "title": "hostname of where the test was ran from",
        },
        "run_location_type": {
            "placeholder": "local, nfs, etc.",
            "title": "run location type",
        },
        "run_location_mountpoint": {
            "placeholder": "/tank/test",
            "title": "run location mountpoint (if applicable)",
        },
        "run_method": {
            "placeholder": "fio, bonnie++, etc",
            "title": "run method (typically fio)",
        },
        "run_command": {
            "placeholder": "fio --randrepeat=1 --ioengine=libaio --direct=1 --gtod_reduce=1 --name=test --filename=test --bs=4k --iodepth=64 --size=4G --readwrite=randrw --rwmixread=75 --output-format=json > my-output.json",
            "title": "run command used",
        },
        "reason": {
            "placeholder": "initial benchmark of new disk, rebenchmark of disk to measure degredation, etc.",
            "title": "reason for the run",
        },
        "fs_host": {
            "placeholder": "rancher, festus, etc.",
            "title": "hostname of the machine where the disk being tested resides",
        },
        "fs_os": {
            "placeholder": "Debian 10, Artix, Alpine 3.15, etc.",
            "title": "Operating System of the machine where the disk being tested resides",
        },
        "fs_type": {
            "placeholder": "zfs, xfs, etc",
            "title": "filesystem type of the machine where the disk being tested resides",
        },
        "fs_kind": {
            "placeholder": "disk, partition, etc.",
            "title": "kind of filesystem (full|partition) of the machine where the disk being tested resides",
        },
        "fs_notes": {
            "placeholder": "notes about the filesystem such as parameters/options used in filesystem",
            "title": "filesystem notes such as parameters/setup of the filesystem",
        },
        "fs_disk_name": {
            "placeholder": "opt, sam, bob",
            "title": "name of disk the filesystem being tested resides on",
        },
        "fs_disk_type": {
            "placeholder": "SSD, HDD, etc.",
            "title": "type of disk the filesystem being tested resides on",
        },
        "fs_disk_size": {
            "placeholder": "100G, 2T, etc.",
            "title": "size of disk the filesystem being tested resides on",
        },
        "run_time": {
            "placeholder": "59, 123, 93.43 etc.",
            "title": "amount of time (in minutes) the benchmark took\nNOTE: https://www.timeanddate.com/date/timeduration.html is a nice tool for this",
        },
        "results_read_iops": {
            "placeholder": "300.32, 1234.331223",
            "title": "read IOPS attained through the benchmark",
        },
        "results_write_iops": {
            "placeholder": "150.3, 288.3432",
            "title": "write IOPS attained through the benchmark",
        },
        "results_raw": {
            "placeholder": "{json-results-from-fio}",
            "title": "raw (json or other) run results from benchmark tool",
        },
        "results_notes": {
            "placeholder": "results were skewed because of high concurrent traffic on disk",
            "title": "notes about the results",
        },
    }
    form_create_rules = (
        rules.FieldSet(
            (
                "run_date",
                "run_location_host",
                "run_location_type",
                "run_location_mountpoint",
                "run_method",
                "run_command",
                "run_notes",
                "reason",
            ),
            "Run Info",
        ),
        rules.FieldSet(
            (
                "fs_host",
                "fs_os",
                "fs_type",
                "fs_kind",
                "fs_notes",
                "fs_disk_name",
                "fs_disk_type",
                "fs_disk_size",
            ),
            "Filesystem",
        ),
        rules.FieldSet(
            (
                "run_time",
                "results_read_iops",
                "results_write_iops",
                "results_raw",
                "results_raw_use",
                "results_notes",
            ),
            "Results",
        ),
        # ~ rules.HTML(
        # ~ '<button onclick="let x = parseFilesystemBenchmarkRawResults(results_raw.value); run_time.value=x.runtime; results_read_iops.value=x.read_iops; results_write_iops.value=x.write_iops; run_date.value=x.run_date;">Parse Raw</button>'
        # ~ ),
        # ~ rules.HTML(
        # ~ '<div id="parse_raw_results_wrapper" class="parse_raw_results_wrapper"><input type="checkbox" id="parse_fs_raw_results" name="parse_fs_raw_results" checked><label for="parse_fs_raw_results">Use Raw Results</label></div>'
        # ~ ),
    )
    form_edit_rules = (
        rules.FieldSet(
            (
                "run_date",
                "run_location_host",
                "run_location_type",
                "run_location_mountpoint",
                "run_method",
                "run_command",
                "run_notes",
                "reason",
            ),
            "Run Info",
        ),
        rules.FieldSet(
            (
                "fs_host",
                "fs_os",
                "fs_type",
                "fs_kind",
                "fs_notes",
                "fs_disk_name",
                "fs_disk_type",
                "fs_disk_size",
            ),
            "Filesystem",
        ),
        rules.FieldSet(
            (
                "run_time",
                "results_read_iops",
                "results_write_iops",
                "results_raw",
                "results_raw_use",
                "results_notes",
            ),
            "Results",
        ),
    )

    def on_model_change(self, form, model, is_created):
        # ~ # if we have results_raw, parse it and extract run date, run time, read and write iops.
        if (
            "results_raw_use" in form
            and form.results_raw_use
            and form.results_raw_use.data
        ):
            # ~ logger.debug(f"- we have request to parse raw results: {form.results_raw_use.data}")
            if form.results_raw.data:
                # ~ logger.debug(f"- we have results_raw to parse...")
                j = json.loads(form.results_raw.data)
                # logger.debug(j)
                # time is in format: Tue Feb 15 05:15:06 2022|Fri May  5 17:45:43 2023
                # notice: 'clean' the date as there can be a double space between MMM and D!
                run_date = arrow.get(
                    " ".join(j["time"].split()), "ddd MMM D HH:mm:ss YYYY"
                ).format("YYYY-MM-DD HH:mm:ss")
                # logger.debug(f"- run date: {j['time']} ⟶ {run_date}")
                model.run_date = run_date
                model.run_time = float(j["jobs"][0]["elapsed"] / 60)
                model.results_read_iops = j["jobs"][0]["read"]["iops"]
                model.results_write_iops = j["jobs"][0]["write"]["iops"]
            else:
                logger.info("No Raw Results to process!")
        else:
            logger.info("No request to parse raw results")
