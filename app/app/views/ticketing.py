# Ticketing views.

import logging

import arrow
import markdown
from flask import flash, redirect, request, url_for
from flask_admin import BaseView, expose
from flask_admin.contrib import sqla
from flask_security import current_user
from markupsafe import Markup
from models.core import Attachment, Comment, Project
from models.ticketing import Ticketing, TicketingStatus
from sqlalchemy import func
from views.core import AttachmentForm, CommentForm

logger = logging.getLogger("main_logger")


class TicketingHelpView(BaseView):
    @expose("/")
    def index(self):
        return self.render("help/ticketing.html")


class TicketingView(sqla.ModelView):
    def is_accessible(self):
        return (
            True
            if current_user.is_authenticated and current_user.has_role("admins")
            else False
        )

    def inaccessible_callback(self, name, **kwargs):
        return redirect(url_for("ldap_login_view"))

    def _handle_view(self, name, **kwargs):
        if not self.is_accessible():
            return self.inaccessible_callback(name, **kwargs)
        self.can_delete = True if current_user.has_role("admins") else False

    def get_query(self):
        # if no query params filter to invoiced=false
        al = list(request.args)
        if "page" in al:
            # remove page from al as we dont care about it from filter's perspective
            al.remove("page")
        # logger.debug(f"- query params are: {len(request.args)} | [{request.args}] | [{al}]")
        if len(al) == 0:
            return (
                self.session.query(self.model)
                .join(TicketingStatus)
                .filter(TicketingStatus.name != "close")
            )  # noqa
        else:
            return self.session.query(self.model)
        # return super(TicketingView, self).get_query().join(TicketingStatus).filter(TicketingStatus.name != 'close')

    def get_count_query(self):
        # if no query params filter to invoiced=false
        al = list(request.args)
        if "page" in al:
            # remove page from al as we dont care about it from filter's perspective
            al.remove("page")
        if len(al) == 0:
            return (
                self.session.query(sqla.view.func.count("*"))
                .select_from(Ticketing)
                .join(TicketingStatus)
                .filter(TicketingStatus.name != "close")
            )  # noqa
        else:
            return self.session.query(sqla.view.func.count("*"))

    inline_models = (CommentForm(Comment), AttachmentForm(Attachment))

    page_size = 20
    can_set_page_size = True
    details_template = "admin/ticketing/details.html"
    details_modal_template = "admin/ticketing/details.html"
    can_view_details = True
    create_modal = True
    edit_modal = True
    details_modal = True
    can_export = True

    form_label = "Ticketing"

    column_searchable_list = ["title", "content"]
    column_filters = [
        "title",
        "status.name",
        "priority.name",
        "project.nickname",
        "tags.name",
        "contact",
    ]
    column_editable_list = [
        "status",
        "priority",
        "title",
        "assignor",
        "contact",
        "tags",
    ]
    column_list = [
        "created_at",
        "tid",
        "title",
        "status",
        "priority",
        "assignor",
        "contact",
        "tags",
    ]
    column_exclude_list = ["updated_at", "assigned_at"]
    form_columns = (
        "project",
        "title",
        "status",
        "priority",
        "content",
        "assignor",
        "contact",
        "tags",
    )
    form_excluded_columns = (
        "tid",
        "created_at",
        "updated_at",
        "assigned_at",
        "creator",
        "updator",
    )
    # sort by name, descending
    column_default_sort = [("status_id", False), ("priority_id", False), ("tid", False)]

    def _created_at_formatter(view, context, model, name):
        return arrow.get(model.created_at).to("local").format("YYYY-MM-DD HHmm")

    def _tid_formatter(view, context, model, name):
        return f"{model.project}-{model.tid:04d}"

    def _content_formatter(view, context, model, name):
        return Markup(markdown.markdown(model.content, extensions=["fenced_code"]))

    column_labels = {
        "created_at": "Created",
        "tid": "Ticket #",
        "assignor": "Assigned To",
    }

    column_formatters = {
        "created_at": _created_at_formatter,
        "tid": _tid_formatter,
        "content": _content_formatter,
    }

    form_args = {
        "project": {
            # filter to only show 'active' projects.
            "query_factory": lambda: Project.query.filter_by(active=True).order_by(
                Project.order
            )
        }
    }

    form_widget_args = {
        "project": {
            "placeholder": "Project 1",
            "title": "Project the Ticket belongs to",
        },
        "title": {
            "placeholder": "Ticket title",
            "title": "title of ticket (max 100 characters)",
        },
        "content": {
            "placeholder": "Ticket details",
            "title": "details of ticket",
            "class": "markdown",
        },
        "assignor": {
            "placeholder": "Assigned to",
            "title": "person ticket is assigned to",
        },
        "contact": {
            "placeholder": "Contact for ticket",
            "title": "contact(s) for the ticket",
        },
        "status": {"placeholder": "Ticket status", "title": "status of the ticket"},
        "priority": {"placeholder": "Ticket priority", "title": "priority of ticket"},
        "tags": {"placeholder": "Ticket tags", "title": "add tags to the ticket"},
    }

    def on_model_change(self, form, model, is_created):
        if is_created:
            model.created_by = current_user.id
            model.updated_by = current_user.id

            logger.debug(f"- filtering max tid for project: {form.project.data}")
            # get the max tid for associated project and increment by 1 or set to 1
            r = (
                self.session.query(func.max(Ticketing.tid))
                .filter_by(project=form.project.data)
                .scalar()
            )

            model.tid = r + 1 if r else 1
            flash(
                f"New ticket created, ticket#: {model.project}-{model.tid:04d}", "info"
            )


class TicketingPriorityView(sqla.ModelView):
    def is_accessible(self):
        return (
            True
            if current_user.is_authenticated and current_user.has_role("admins")
            else False
        )

    def inaccessible_callback(self, name, **kwargs):
        return redirect(url_for("ldap_login_view"))

    def _handle_view(self, name, **kwargs):
        if not self.is_accessible():
            return self.inaccessible_callback(name, **kwargs)
        self.can_delete = True if current_user.has_role("admins") else False

    page_size = 10
    can_set_page_size = True
    can_view_details = True
    create_modal = True
    edit_modal = True
    details_modal = True
    can_export = True

    column_searchable_list = ["name", "desc"]
    column_filters = ["name", "desc"]
    column_editable_list = ["name", "desc"]
    column_list = ["name", "desc"]
    # ~ column_default_sort = ("order", False)

    column_labels = dict(name="Name", desc="Desc")

    form_widget_args = {
        "name": {"placeholder": "enter name", "title": "name of the item"},
        "desc": {
            "placeholder": "enter description",
            "title": "description of the item",
        },
    }


class TicketingStatusView(sqla.ModelView):
    def is_accessible(self):
        return (
            True
            if current_user.is_authenticated and current_user.has_role("admins")
            else False
        )

    def inaccessible_callback(self, name, **kwargs):
        return redirect(url_for("ldap_login_view"))

    def _handle_view(self, name, **kwargs):
        if not self.is_accessible():
            return self.inaccessible_callback(name, **kwargs)
        self.can_delete = True if current_user.has_role("admins") else False

    page_size = 10
    can_set_page_size = True
    can_view_details = True
    create_modal = True
    edit_modal = True
    details_modal = True
    can_export = True

    column_searchable_list = ["name", "desc"]
    column_filters = ["name", "desc"]
    column_editable_list = ["name", "desc"]
    column_list = ["name", "desc"]
    # ~ column_default_sort = ("order", False)

    column_labels = dict(name="Name", desc="Desc")

    form_widget_args = {
        "name": {"placeholder": "enter name", "title": "name of the item"},
        "desc": {
            "placeholder": "enter description",
            "title": "description of the item",
        },
    }
