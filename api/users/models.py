#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#  SPDX-License-Identifier: GPL-3.0-only
#  Copyright 2025 dradux.com

import logging
import uuid
from datetime import datetime
from typing import List, Optional

import jwt
from auth.models import Role, TokenData, oauth2_scheme
from config import config
from config.config import cfg
from config.models import Cdb
from fastapi import Depends, HTTPException, Security, status
from fastapi.security import SecurityScopes
from jwt import PyJWTError
from pydantic import BaseModel, EmailStr, HttpUrl, ValidationError
from users.utils import get_by_username, update_user

logger = logging.getLogger("default")
_db = Cdb.USERS


class UserPreferences(BaseModel):
    """
    User Preferences
    """

    show_first_run: bool = True


class UserAccount(BaseModel):
    id_: str
    role: Role


class UserBase(BaseModel):
    """
    Used to abstract out basic fields
    """

    username: str = None
    display_name: str = None
    first_name: str = None
    last_name: str = None
    email: Optional[EmailStr] = None
    avatar: Optional[HttpUrl] = None
    notify_id: Optional[str] = None
    note: Optional[str] = None
    roles: List[str] = []
    preferences: UserPreferences = UserPreferences()
    user_timezone: str = None

    enabled: bool = True


class UserExt(UserBase):
    """
    Non-User supplied data (added by backend logic)
    """

    # note: this needs to be set/overwrote on result instantiation as using
    #  datetime.now() here will only get you now of when worker was started.
    created: datetime = datetime.now()
    updated: datetime = datetime.now()


class User(UserExt):
    """
    Actual model used at DB level
    """

    id_: str


class UserActivityStat(BaseModel):
    """
    User activity stats.
    """

    activities: int = 0
    distance: float = 0
    duration: int = 0
    speed: str = "0"


class UserActivityStatsResponse(BaseModel):
    """
    User activity stats response
    """

    day: UserActivityStat = None
    week: UserActivityStat = None
    month: UserActivityStat = None
    year: UserActivityStat = None
    total: UserActivityStat = None
    timing: float = None


async def get_current_user(security_scopes: SecurityScopes, token: str = Depends(oauth2_scheme)):
    """
    Get the current user
    """

    if security_scopes.scopes:
        authenticate_value = f'Bearer scope="{security_scopes.scope_str}"'
    else:
        authenticate_value = "Bearer"

    credentials_exception = HTTPException(
        status_code=status.HTTP_401_UNAUTHORIZED,
        detail="Could not validate credentials",
        headers={"WWW-Authenticate": authenticate_value},
    )
    token_scopes = []
    try:
        payload = jwt.decode(
            token,
            config.API_JWT_SECRET_KEY,
            algorithms=[cfg.api.jwt.algorithm],
        )
        username: str = payload.get("sub")
        if username is None:
            raise credentials_exception

        token_scopes = payload.get("scopes", [])
        token_data = TokenData(scopes=token_scopes, username=username)
    except (PyJWTError, ValidationError):
        raise credentials_exception
    # get user from db.
    user = await get_by_username(token_data.username)
    if user is None:
        raise credentials_exception
    if not user["user_timezone"]:
        default_user_timezone = "US/Eastern"
        logger.debug(f"\n\n───────────\nNo user timezone: {user}\n\n───────────")
        # set user_timezone to default
        user["user_timezone"] = default_user_timezone
        _ret = await update_user(user=UserExt(**user))
        logger.debug(f"\n\n───────────\nupdate_user response:\n{_ret}\n\n───────────")

    has_scope = False
    for scope in security_scopes.scopes:
        if scope in token_data.scopes:
            has_scope = True
            break

    if not has_scope:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Not enough permissions",
            headers={"WWW-Authenticate": authenticate_value},
        )

    ru = UserBase(**user)
    ru.roles = token_scopes
    return ru


async def get_current_active_user(
    current_user: UserBase = Security(get_current_user, scopes=["users"]),
):
    if not current_user.enabled:
        raise HTTPException(status_code=400, detail="Inactive user")
    return current_user


async def authenticate_user(username: str, password: str):
    from auth.ldap import login

    user, roles = await login(username, password)
    if not user:
        return None, None
    else:
        # Generate notify_id and attach to user.
        user.notify_id = str(uuid.uuid4())
        await update_user(user)
        return user, roles
