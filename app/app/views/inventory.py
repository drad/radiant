# Inventory views.

import logging

import arrow
import markdown
from flask import redirect, url_for
from flask_admin import BaseView, expose
from flask_admin.contrib import sqla
from flask_admin.model.form import InlineFormAdmin
from flask_security import current_user
from markupsafe import Markup
from models.core import Attachment
from models.inventory import (
    Asset,
    AssetPhysicalLocation,
    AssetStatus,
    AssetStatusStatus,
    AssetType,
    AttainType,
    Energy,
    EnergyMeasureState,
    IncidentStatus,
    Link,
    LinkType,
    MeasureType,
    SetupStatus,
    Spec,
    SpecType,
)
from views.core import AttachmentForm

logger = logging.getLogger("main_logger")

# custom filter for Server: only active servers.
# ~ class FilterActiveServer(BaseSQLAFilter):
# ~ def apply(self, query, value, alias=None):
# ~ if value:
# ~ return query.filter(self.column == True)
# ~ else:
# ~ return query.filter(self.column == False)

# ~ def operation(self):
# ~ return 'is Active'


#
# Standard / Base views
#
# ~ class HomeView(AdminIndexView):
# ~ @expose("/")
# ~ def index(self):
# ~ from app import about

# ~ return self.render("home.html", about=about)


# ~ class OnlineHelpView(BaseView):
# ~ @expose("/")
# ~ def index(self):
# ~ return self.render("help-online.html")


# ~ class AccountView(BaseView):
# ~ def is_accessible(self):
# ~ return True if current_user.is_authenticated else False

# ~ def inaccessible_callback(self, name, **kwargs):
# ~ return redirect(url_for("ldap_login_view"))

# ~ @expose("/")
# ~ def index(self):
# ~ return self.render("account.html")


# ~ class LoginMenuLink(MenuLink):
# ~ def is_accessible(self):
# ~ return not current_user.is_authenticated


# ~ class LogoutMenuLink(MenuLink):
# ~ def is_accessible(self):
# ~ return current_user.is_authenticated


# ~ class AboutView(BaseView):
# ~ @expose("/")
# ~ def index(self):
# ~ from app import about

# ~ help_about = about
# ~ return self.render("help-about.html", help_about=help_about)


#
# Customized User model for SQL-Admin
#
# ~ class UserAdmin(sqla.ModelView):
# ~ def is_accessible(self):
# ~ return (
# ~ True
# ~ if current_user.is_authenticated
# ~ and (current_user.has_role("users") or current_user.has_role("admins"))
# ~ else False
# ~ )

# ~ def inaccessible_callback(self, name, **kwargs):
# ~ return redirect(url_for("ldap_login_view"))

# ~ def _handle_view(self, name, **kwargs):
# ~ if not self.is_accessible():
# ~ return self.inaccessible_callback(name, **kwargs)
# ~ self.can_delete = True if current_user.has_role("admins") else False

# ~ # can_view_details = True
# ~ # details_modal = True

# ~ # default sort by name
# ~ column_default_sort = ("name", False)

# ~ # Don't display the password on the list of Users
# ~ column_exclude_list = ("password",)

# ~ # column_searchable_list = ["email"]

# ~ # Don't include the standard password field when creating or editing a User (but see below)
# ~ form_excluded_columns = ("password",)

# ~ page_size = 20

# ~ # Automatically display human-readable names for the current and available Roles when creating or editing a User
# ~ column_auto_select_related = True

# ~ def _note_formatter(view, context, model, name):
# ~ return Markup("%s" % (model.note)) if model.note else ""

# ~ column_formatters = {"note": _note_formatter}

# ~ # Prevent administration of Users unless the currently logged-in user has the "admin" role
# ~ # def is_accessible(self):
# ~ #    return current_user.has_role("admins")

# ~ # On the form for creating or editing a User, don't display a field corresponding to the model's password field.
# ~ # There are two reasons for this. First, we want to encrypt the password before storing in the database. Second,
# ~ # we want to use a password field (with the input masked) rather than a regular text field.
# ~ def scaffold_form(self):

# ~ # Start with the standard form as provided by Flask-Admin. We've already told Flask-Admin to exclude the
# ~ # password field from this form.
# ~ form_class = super(UserAdmin, self).scaffold_form()

# ~ # Add a password field, naming it "password2" and labeling it "New Password".
# ~ form_class.password2 = PasswordField("New Password")
# ~ return form_class

# ~ # This callback executes when the user saves changes to a newly-created or edited User -- before the changes are
# ~ # committed to the database.
# ~ def on_model_change(self, form, model, is_created):

# ~ # If the password field isn't blank...
# ~ if len(model.password2):

# ~ # ... then encrypt the new password prior to storing it in the database. If the password field is blank,
# ~ # the existing password in the database will be retained.
# ~ model.password = utils.encrypt_password(model.password2)


# ~ # Customized Role model for SQL-Admin
# ~ class RoleAdmin(sqla.ModelView):
# ~ def is_accessible(self):
# ~ return (
# ~ True
# ~ if current_user.is_authenticated
# ~ and (current_user.has_role("users") or current_user.has_role("admins"))
# ~ else False
# ~ )

# ~ def inaccessible_callback(self, name, **kwargs):
# ~ return redirect(url_for("ldap_login_view"))

# ~ def _handle_view(self, name, **kwargs):
# ~ if not self.is_accessible():
# ~ return self.inaccessible_callback(name, **kwargs)
# ~ self.can_delete = True if current_user.has_role("admins") else False


#
# Inline Forms
#
class AssetStatusForm(InlineFormAdmin):
    def is_accessible(self):
        return (
            True
            if current_user.is_authenticated
            and (current_user.has_role("users") or current_user.has_role("admins"))
            else False
        )

    def inaccessible_callback(self, name, **kwargs):
        return redirect(url_for("ldap_login_view"))

    def _handle_view(self, name, **kwargs):
        if not self.is_accessible():
            return self.inaccessible_callback(name, **kwargs)
        self.can_delete = True if current_user.has_role("admins") else False

    form_columns = ("id", "status_date", "status", "note")
    form_label = "Status"
    form_args = {
        "status": {
            "query_factory": lambda: AssetStatusStatus.query.order_by(
                AssetStatusStatus.name
            )
        }
    }


class AssetLinkForm(InlineFormAdmin):
    def is_accessible(self):
        return (
            True
            if current_user.is_authenticated
            and (current_user.has_role("users") or current_user.has_role("admins"))
            else False
        )

    def inaccessible_callback(self, name, **kwargs):
        return redirect(url_for("ldap_login_view"))

    def _handle_view(self, name, **kwargs):
        if not self.is_accessible():
            return self.inaccessible_callback(name, **kwargs)
        self.can_delete = True if current_user.has_role("admins") else False

    form_columns = ("id", "link_type", "link", "note")
    form_label = "Link"
    form_args = {
        "link_type": {"query_factory": lambda: LinkType.query.order_by(LinkType.name)}
    }


class AssetSpecForm(InlineFormAdmin):
    def is_accessible(self):
        return (
            True
            if current_user.is_authenticated
            and (current_user.has_role("users") or current_user.has_role("admins"))
            else False
        )

    def inaccessible_callback(self, name, **kwargs):
        return redirect(url_for("ldap_login_view"))

    def _handle_view(self, name, **kwargs):
        if not self.is_accessible():
            return self.inaccessible_callback(name, **kwargs)
        self.can_delete = True if current_user.has_role("admins") else False

    form_columns = ("id", "spec_type", "value", "note")
    form_label = "Spec"
    form_args = {
        "spec_type": {"query_factory": lambda: SpecType.query.order_by(SpecType.name)}
    }


class AssetEnergyForm(InlineFormAdmin):
    def is_accessible(self):
        return (
            True
            if current_user.is_authenticated
            and (current_user.has_role("users") or current_user.has_role("admins"))
            else False
        )

    def inaccessible_callback(self, name, **kwargs):
        return redirect(url_for("ldap_login_view"))

    def _handle_view(self, name, **kwargs):
        if not self.is_accessible():
            return self.inaccessible_callback(name, **kwargs)
        self.can_delete = True if current_user.has_role("admins") else False

    form_columns = ("id", "state", "value", "measure_type", "note")
    form_label = "Energy"
    form_args = {
        "state": {
            "query_factory": lambda: EnergyMeasureState.query.order_by(
                EnergyMeasureState.name
            )
        },
        "measure_type": {
            "query_factory": lambda: MeasureType.query.order_by(MeasureType.name)
        },
    }


#
# App Views
#


class InventoryHelpView(BaseView):
    @expose("/")
    def index(self):
        return self.render("help/inventory.html")


class AssetView(sqla.ModelView):
    def is_accessible(self):
        return (
            True
            if current_user.is_authenticated
            and (current_user.has_role("users") or current_user.has_role("admins"))
            else False
        )

    def inaccessible_callback(self, name, **kwargs):
        return redirect(url_for("ldap_login_view"))

    def _handle_view(self, name, **kwargs):
        if not self.is_accessible():
            return self.inaccessible_callback(name, **kwargs)
        self.can_delete = True if current_user.has_role("admins") else False

    def _compute_costs_formatter(view, context, model, name):
        cost_hour, cost_day, cost_month, cost_year = model.compute_cost()
        title = f"Yearly Compute Cost\n - Hour={str(round(cost_hour, 4))} \n - Day={str(round(cost_day, 4))} \n - Month={str(round(cost_month, 4))}"
        val = f"{str(round(cost_year, 2))}"
        return Markup(
            '<div class="inline-tooltip" title="{1}">{0}</div>'.format(val, title)
        )

    def _cpu_cores_formatter(view, context, model, name):
        core_count, cpu_ev = model.cpu_core_count()
        return Markup(
            f'<div class="inline-tooltip" title="Number of CPU Cores\n - CPU EV: {cpu_ev}">{core_count}</div>'
        )

    def _memory_amount_formatter(view, context, model, name):
        amount, mem_ev = model.memory_amount()
        return Markup(
            f'<div class="inline-tooltip" title="Amount of Memory\n - Memory EV: {mem_ev}">{amount}</div>'
        )

    def _purchase_price_formatter(view, context, model, name):
        if model.purchase_price:
            return "${:,.2f}".format(model.purchase_price)

    def _extra_row_data_formatter(view, context, model, name):
        custom_detail_view = "<a href='asset_detail.html?id={0}'>D</a>".format(model.id)

        data_tip = "this is the tooltip"
        data_value = "{0}".format(custom_detail_view)
        return Markup("<div title='{0}'>{1}</div>".format(data_tip, data_value))

    # inline models.
    inline_models = (
        AssetEnergyForm(Energy),
        AssetLinkForm(Link),
        AssetSpecForm(Spec),
        AssetStatusForm(AssetStatus),
        AttachmentForm(Attachment),
    )

    can_set_page_size = True
    details_template = "admin/inventory/asset_detail.html"
    details_modal_template = "admin/inventory/asset_detail.html"
    can_view_details = True
    create_modal = False
    edit_modal = False
    details_modal = True
    can_export = True
    page_size = 20

    # ~ @expose('/details/', methods=('GET', 'POST'))
    # ~ def details_view(self):
    # ~ return super(AssetView, self).details_view()

    column_searchable_list = [
        "alias",
        "name",
        "description",
        "manufacturer",
        "model",
        "serial_number",
        "note",
        "purpose",
        "attain_from",
        "warranty",
        "os_name",
        "os_version",
        "os_note",
        "networking_nic_ip",
        "networking_wlan_ip",
        "networking_note",
    ]
    column_filters = [
        "alias",
        "name",
        "description",
        "manufacturer",
        "model",
        "serial_number",
        "note",
        "purpose",
        "attain_from",
        "statuses.status_date",
        "tags.name",
        "asset_type",
        "physical_location",
        "warranty",
        "os_name",
        "os_version",
        "os_note",
        "networking_nic_ip",
        "networking_nic_mac",
        "networking_wlan_ip",
        "networking_wlan_mac",
        "networking_note",
    ]
    column_editable_list = ["alias", "name", "asset_type", "tags"]
    column_list = [
        "alias",
        "name",
        "asset_type",
        # "statuses",
        # "energy",
        "cpu_cores",
        "memory_amount",
        "compute_costs",
        "tags",
        # "extra_row_data",
    ]
    form_excluded_columns = ("creator", "created_at")
    form_columns = (
        "alias",
        "name",
        "asset_type",
        "description",
        "purpose",
        "tags",
        "physical_location",
        "manufacturer",
        "model",
        "version",
        "serial_number",
        "part_number",
        "attain_type",
        "attain_from",
        "purchase_price",
        "purchase_invoice_number",
        "warranty",
        "os_name",
        "os_version",
        "os_note",
        "networking_nic_ip",
        "networking_nic_mac",
        "networking_wlan_ip",
        "networking_wlan_mac",
        "networking_note",
        "note",
        "energy",
        "links",
        "specs",
        "statuses",
    )
    column_labels = {
        "alias": "Hostname",
        "asset_type": "Type",
        "compute_costs": "YC",
        "cpu_cores": "CPU",
        "memory_amount": "Mem",
        "part_number": "p/n",
        "purchase_invoice_number": "Invoice #",
        "serial_number": "s/n",
        "extra_row_data": "",
        "os_name": "OS Name",
        "os_version": "OS Version",
        "os_note": "OS Note",
        "networking_nic_ip": "NIC IP",
        "networking_nic_mac": "NIC MAC",
        "networking_wlan_ip": "WLAN IP",
        "networking_wlan_mac": "WLAN MAC",
        "networking_note": "Networking Note",
    }
    column_default_sort = ("alias", False)
    column_formatters = {
        "cpu_cores": _cpu_cores_formatter,
        "memory_amount": _memory_amount_formatter,
        "compute_costs": _compute_costs_formatter,
        "purchase_price": _purchase_price_formatter,
        "extra_row_data": _extra_row_data_formatter,
    }
    form_args = {
        "asset_type": {
            "query_factory": lambda: AssetType.query.order_by(AssetType.name)
        },
        "attain_type": {
            "query_factory": lambda: AttainType.query.order_by(AttainType.name)
        },
        "physical_location": {
            "query_factory": lambda: AssetPhysicalLocation.query.order_by(
                AssetPhysicalLocation.name
            )
        },
    }
    form_widget_args = {
        "alias": {
            "placeholder": "blake",
            "title": "the hostname (or an alias if hostname is not applicable/available) for an asset",
        },
        "name": {
            "placeholder": "HP NW8440",
            "title": "the name for the asset - generally a concise, descriptive name of asset",
        },
        "asset_type": {
            "title": "the asset type",
        },
        "manufacturer": {
            "placeholder": "HP",
            "title": "manufacturer of the asset",
        },
        "model": {
            "placeholder": "NW8440",
            "title": "model of the asset",
        },
        "version": {
            "placeholder": "v1",
            "title": "version of the asset",
        },
        "serial_number": {
            "placeholder": "CNU7160083",
            "title": "serial number of the asset",
        },
        "part_number": {
            "placeholder": "RB556UT#ABA",
            "title": "part number of the asset",
        },
        "description": {
            "placeholder": "HP Compaq Mobile Workstation...",
            "title": "detailed description of the asset",
            "class": "markdown",
        },
        "purpose": {
            "placeholder": "work laptop",
            "title": "the purpose of the asset",
            "class": "markdown",
        },
        "os_name": {
            "placeholder": "Debian Linux",
            "title": "name of operating system",
        },
        "os_version": {
            "placeholder": "11 Bullseye",
            "title": "version of operating system",
        },
        "os_note": {
            "placeholder": "OS set to 'testing' branch",
            "title": "any note(s) related to the operating system",
            "class": "markdown",
        },
        "networking_nic_ip": {
            "placeholder": "10.33.1.110",
            "title": "NIC IP Address",
        },
        "networking_nic_mac": {
            "placeholder": "00:17:a4:df:58:b0",
            "title": "NIC MAC Address",
        },
        "networking_wlan_ip": {
            "placeholder": "10.33.1.110",
            "title": "NIC IP Address",
        },
        "networking_wlan_mac": {
            "placeholder": "00:17:a4:df:58:b0",
            "title": "NIC MAC Address",
        },
        "networking_note": {
            "placeholder": "server has the following VIPs: xxx, yyy, etc.",
            "title": "NIC MAC Address",
            "class": "markdown",
        },
        "note": {
            "placeholder": "this laptop has had stability issues",
            "title": "note(s) for the asset",
            "class": "markdown",
        },
        "warranty": {
            "placeholder": "warranty info",
            "title": "warranty info about the item",
            "class": "markdown",
        },
        "tags": {
            "title": "tags for the asset",
        },
        "physical_location": {
            "title": "physical location of asset",
        },
    }


class AssetPhysicalLocationView(sqla.ModelView):
    def is_accessible(self):
        return (
            True
            if current_user.is_authenticated
            and (current_user.has_role("users") or current_user.has_role("admins"))
            else False
        )

    def inaccessible_callback(self, name, **kwargs):
        return redirect(url_for("ldap_login_view"))

    def _handle_view(self, name, **kwargs):
        if not self.is_accessible():
            return self.inaccessible_callback(name, **kwargs)
        self.can_delete = True if current_user.has_role("admins") else False

    page_size = 20
    can_view_details = False
    create_modal = True
    edit_modal = True
    # details_modal = True
    can_export = True
    column_searchable_list = ["name"]
    column_filters = ["name"]
    column_editable_list = ["name"]
    column_list = ["name"]
    column_exclude_list = []
    # sort by name, descending
    column_default_sort = ("name", False)
    form_excluded_columns = ()
    form_widget_args = {"name": {"placeholder": "physical location name"}}


class AssetStatusStatusView(sqla.ModelView):
    def is_accessible(self):
        return (
            True
            if current_user.is_authenticated
            and (current_user.has_role("users") or current_user.has_role("admins"))
            else False
        )

    def inaccessible_callback(self, name, **kwargs):
        return redirect(url_for("ldap_login_view"))

    def _handle_view(self, name, **kwargs):
        if not self.is_accessible():
            return self.inaccessible_callback(name, **kwargs)
        self.can_delete = True if current_user.has_role("admins") else False

    page_size = 20
    can_view_details = False
    create_modal = True
    edit_modal = True
    # details_modal = True
    can_export = True
    column_searchable_list = ["name"]
    column_filters = ["name"]
    column_editable_list = ["name"]
    column_list = ["name"]
    column_exclude_list = []
    # sort by name, descending
    column_default_sort = ("name", False)
    form_excluded_columns = ()
    form_widget_args = {"name": {"placeholder": "asset status name"}}


class AssetTypeView(sqla.ModelView):
    def is_accessible(self):
        return (
            True
            if current_user.is_authenticated
            and (current_user.has_role("users") or current_user.has_role("admins"))
            else False
        )

    def inaccessible_callback(self, name, **kwargs):
        return redirect(url_for("ldap_login_view"))

    def _handle_view(self, name, **kwargs):
        if not self.is_accessible():
            return self.inaccessible_callback(name, **kwargs)
        self.can_delete = True if current_user.has_role("admins") else False

    page_size = 20
    can_view_details = False
    create_modal = True
    edit_modal = True
    # details_modal = True
    can_export = True
    column_searchable_list = ["name"]
    column_filters = ["name"]
    column_editable_list = ["name"]
    column_list = ["name"]
    column_exclude_list = []
    # sort by name, descending
    column_default_sort = ("name", False)
    form_excluded_columns = ()
    form_widget_args = {"name": {"placeholder": "asset type name"}}


class AttainTypeView(sqla.ModelView):
    def is_accessible(self):
        return (
            True
            if current_user.is_authenticated
            and (current_user.has_role("users") or current_user.has_role("admins"))
            else False
        )

    def inaccessible_callback(self, name, **kwargs):
        return redirect(url_for("ldap_login_view"))

    def _handle_view(self, name, **kwargs):
        if not self.is_accessible():
            return self.inaccessible_callback(name, **kwargs)
        self.can_delete = True if current_user.has_role("admins") else False

    page_size = 20
    can_view_details = False
    create_modal = True
    edit_modal = True
    # details_modal = True
    can_export = True
    column_searchable_list = ["name"]
    column_filters = ["name"]
    column_editable_list = ["name"]
    column_list = ["name"]
    column_exclude_list = []
    # sort by name, descending
    column_default_sort = ("name", False)
    form_excluded_columns = ()
    form_widget_args = {"name": {"placeholder": "attain type name"}}


class IncidentView(sqla.ModelView):
    def is_accessible(self):
        return (
            True
            if current_user.is_authenticated
            and (current_user.has_role("users") or current_user.has_role("admins"))
            else False
        )

    def inaccessible_callback(self, name, **kwargs):
        return redirect(url_for("ldap_login_view"))

    def _handle_view(self, name, **kwargs):
        if not self.is_accessible():
            return self.inaccessible_callback(name, **kwargs)
        self.can_delete = True if current_user.has_role("admins") else False

    def _incident_date_formatter(view, context, model, name):
        if model.incident_date is not None:
            date_long = (
                arrow.get(model.incident_date).to("local").format("YYYY-MM-DD HH:mm:ss")
            )
            date_short = arrow.get(model.incident_date).to("local").humanize()
            return Markup(
                f"<div class='inline-tooltip' title='{date_long}'>{date_short}</div>"
            )

    def _issue_formatter(view, context, model, name):
        return (
            Markup(f"{markdown.markdown(model.issue, extensions=['fenced_code'])}")
            if model.issue
            else ""
        )

    def _resolution_formatter(view, context, model, name):
        return (
            Markup(f"{markdown.markdown(model.resolution, extensions=['fenced_code'])}")
            if model.resolution
            else ""
        )

    def _impact_formatter(view, context, model, name):
        return (
            Markup(f"{markdown.markdown(model.impact, extensions=['fenced_code'])}")
            if model.impact
            else ""
        )

    def _note_formatter(view, context, model, name):
        return (
            Markup(f"{markdown.markdown(model.note, extensions=['fenced_code'])}")
            if model.note
            else ""
        )

    inline_models = (AttachmentForm(Attachment),)

    can_view_details = True
    create_modal = False
    edit_modal = False
    details_modal = True
    can_export = True
    page_size = 20
    can_set_page_size = True
    details_template = "admin/inventory/incident_detail.html"
    details_modal_template = "admin/inventory/incident_detail.html"

    column_searchable_list = ["issue", "resolution", "impact", "note"]
    column_filters = [
        "incident_date",
        "status",
        "asset.alias",
        "issue",
        "resolution",
        "impact",
        "note",
    ]
    column_editable_list = [
        "incident_date",
        "status",
        "asset",
        "issue",
        "resolution",
        "impact",
        "note",
    ]
    column_list = ["incident_date", "status", "asset"]
    form_excluded_columns = ("creator", "created_at")
    form_columns = (
        "incident_date",
        "status",
        "asset",
        "issue",
        "resolution",
        "impact",
        "note",
    )
    column_labels = {"incident_date": "Date"}
    column_formatters = {
        "incident_date": _incident_date_formatter,
        "issue": _issue_formatter,
        "resolution": _resolution_formatter,
        "impact": _impact_formatter,
        "note": _note_formatter,
    }
    form_args = {
        "status": {
            "query_factory": lambda: IncidentStatus.query.order_by(IncidentStatus.name)
        },
        "asset": {"query_factory": lambda: Asset.query.order_by(Asset.alias)},
    }
    form_widget_args = {
        "issue": {"class": "markdown"},
        "resolution": {"class": "markdown"},
        "impact": {"class": "markdown"},
        "note": {"class": "markdown"},
    }
    column_default_sort = ("incident_date", True)


class IncidentStatusView(sqla.ModelView):
    def is_accessible(self):
        return (
            True
            if current_user.is_authenticated
            and (current_user.has_role("users") or current_user.has_role("admins"))
            else False
        )

    def inaccessible_callback(self, name, **kwargs):
        return redirect(url_for("ldap_login_view"))

    def _handle_view(self, name, **kwargs):
        if not self.is_accessible():
            return self.inaccessible_callback(name, **kwargs)
        self.can_delete = True if current_user.has_role("admins") else False

    page_size = 20
    can_view_details = False
    create_modal = True
    edit_modal = True
    # details_modal = True
    can_export = True
    column_searchable_list = ["name"]
    column_filters = ["name"]
    column_editable_list = ["name"]
    column_list = ["name"]
    column_exclude_list = []
    # sort by name, descending
    column_default_sort = ("name", False)
    form_excluded_columns = ()
    form_widget_args = {"name": {"placeholder": "incident status name"}}


class LinkTypeView(sqla.ModelView):
    def is_accessible(self):
        return (
            True
            if current_user.is_authenticated
            and (current_user.has_role("users") or current_user.has_role("admins"))
            else False
        )

    def inaccessible_callback(self, name, **kwargs):
        return redirect(url_for("ldap_login_view"))

    def _handle_view(self, name, **kwargs):
        if not self.is_accessible():
            return self.inaccessible_callback(name, **kwargs)
        self.can_delete = True if current_user.has_role("admins") else False

    page_size = 20
    can_view_details = False
    create_modal = True
    edit_modal = True
    # details_modal = True
    can_export = True
    column_searchable_list = ["name"]
    column_filters = ["name"]
    column_editable_list = ["name"]
    column_list = ["name"]
    column_exclude_list = []
    # sort by name, descending
    column_default_sort = ("name", False)
    form_excluded_columns = ()
    form_widget_args = {"name": {"placeholder": "link type name"}}


class MeasureTypeView(sqla.ModelView):
    def is_accessible(self):
        return (
            True
            if current_user.is_authenticated
            and (current_user.has_role("users") or current_user.has_role("admins"))
            else False
        )

    def inaccessible_callback(self, name, **kwargs):
        return redirect(url_for("ldap_login_view"))

    def _handle_view(self, name, **kwargs):
        if not self.is_accessible():
            return self.inaccessible_callback(name, **kwargs)
        self.can_delete = True if current_user.has_role("admins") else False

    page_size = 20
    can_view_details = False
    create_modal = True
    edit_modal = True
    # details_modal = True
    can_export = True
    column_searchable_list = ["name"]
    column_filters = ["name"]
    column_editable_list = ["name"]
    column_list = ["name"]
    column_exclude_list = []
    # sort by name, descending
    column_default_sort = ("name", False)
    form_excluded_columns = ()
    form_widget_args = {"name": {"placeholder": "measure type name"}}


class EnergyMeasureStateView(sqla.ModelView):
    def is_accessible(self):
        return (
            True
            if current_user.is_authenticated
            and (current_user.has_role("users") or current_user.has_role("admins"))
            else False
        )

    def inaccessible_callback(self, name, **kwargs):
        return redirect(url_for("ldap_login_view"))

    def _handle_view(self, name, **kwargs):
        if not self.is_accessible():
            return self.inaccessible_callback(name, **kwargs)
        self.can_delete = True if current_user.has_role("admins") else False

    page_size = 20
    can_view_details = False
    create_modal = True
    edit_modal = True
    # details_modal = True
    can_export = True
    column_searchable_list = ["name"]
    column_filters = ["name"]
    column_editable_list = ["name"]
    column_list = ["name"]
    column_exclude_list = []
    # sort by name, descending
    column_default_sort = ("name", False)
    form_excluded_columns = ()
    form_widget_args = {"name": {"placeholder": "measure state name"}}


class SpecTypeView(sqla.ModelView):
    def is_accessible(self):
        return (
            True
            if current_user.is_authenticated
            and (current_user.has_role("users") or current_user.has_role("admins"))
            else False
        )

    def inaccessible_callback(self, name, **kwargs):
        return redirect(url_for("ldap_login_view"))

    def _handle_view(self, name, **kwargs):
        if not self.is_accessible():
            return self.inaccessible_callback(name, **kwargs)
        self.can_delete = True if current_user.has_role("admins") else False

    page_size = 20
    can_view_details = False
    create_modal = True
    edit_modal = True
    # details_modal = True
    can_export = True
    column_searchable_list = ["name", "note"]
    column_filters = ["name", "note"]
    column_editable_list = ["name", "note"]
    column_list = ["name", "note"]
    column_exclude_list = []
    # sort by name, descending
    column_default_sort = ("name", False)
    form_excluded_columns = ()
    form_widget_args = {
        "name": {"placeholder": "spec type name"},
        "note": {"placeholder": "note or description of spec type"},
    }


class SetupView(sqla.ModelView):
    def is_accessible(self):
        return (
            True
            if current_user.is_authenticated
            and (current_user.has_role("users") or current_user.has_role("admins"))
            else False
        )

    def inaccessible_callback(self, name, **kwargs):
        return redirect(url_for("ldap_login_view"))

    def _handle_view(self, name, **kwargs):
        if not self.is_accessible():
            return self.inaccessible_callback(name, **kwargs)
        self.can_delete = True if current_user.has_role("admins") else False

    def _setup_date_formatter(view, context, model, name):
        if model.setup_date is not None:
            return Markup(
                '<div class="inline-tooltip" title="{1}">{0}</div>'.format(
                    arrow.get(model.setup_date).to("local").humanize(),
                    arrow.get(model.setup_date)
                    .to("local")
                    .format("YYYY-MM-DD HH:mm:ss"),
                )
            )

    def _prep_setup_formatter(view, context, model, name):
        return (
            Markup(f"{markdown.markdown(model.prep_setup, extensions=['fenced_code'])}")
            if model.prep_setup
            else ""
        )

    def _disk_setup_formatter(view, context, model, name):
        return (
            Markup(f"{markdown.markdown(model.disk_setup, extensions=['fenced_code'])}")
            if model.disk_setup
            else ""
        )

    def _apps_setup_formatter(view, context, model, name):
        return (
            Markup(f"{markdown.markdown(model.apps_setup, extensions=['fenced_code'])}")
            if model.apps_setup
            else ""
        )

    def _additional_setup_formatter(view, context, model, name):
        return (
            Markup(
                f"{markdown.markdown(model.additional_setup, extensions=['fenced_code'])}"
            )
            if model.additional_setup
            else ""
        )

    def _os_setup_formatter(view, context, model, name):
        return (
            Markup(f"{markdown.markdown(model.os_setup, extensions=['fenced_code'])}")
            if model.os_setup
            else ""
        )

    def _purpose_formatter(view, context, model, name):
        return (
            Markup(f"{markdown.markdown(model.purpose, extensions=['fenced_code'])}")
            if model.purpose
            else ""
        )

    def _note_formatter(view, context, model, name):
        return (
            Markup(f"{markdown.markdown(model.note, extensions=['fenced_code'])}")
            if model.note
            else ""
        )

    inline_models = (AttachmentForm(Attachment),)

    can_view_details = True
    create_modal = False
    edit_modal = False
    details_modal = True
    can_export = True
    page_size = 20
    can_set_page_size = True
    details_template = "admin/inventory/setup_detail.html"
    details_modal_template = "admin/inventory/setup_detail.html"

    column_searchable_list = [
        "alias",
        "name",
        "note",
        "purpose",
        "os",
        "ip_address",
        "ip_gateway",
        "ip_netmask",
        "hostname",
    ]
    column_filters = [
        "alias",
        "name",
        "note",
        "purpose",
        "status",
        # ~ "asset.name",
        "asset.alias",
        "os",
        "ip_address",
        "ip_gateway",
        "ip_netmask",
        "hostname",
    ]
    column_editable_list = [
        "alias",
        "name",
        "asset",
        "status",
        "purpose",
        "os",
        "hostname",
        "ip_address",
    ]
    column_list = [
        "alias",
        "name",
        "setup_date",
        "status",
        "asset",
        "purpose",
        "os",
        "hostname",
        "ip_address",
    ]
    form_excluded_columns = ("creator", "created_at")
    form_columns = (
        "alias",
        "name",
        "status",
        "setup_date",
        "asset",
        "purpose",
        "note",
        "os",
        "hostname",
        "ip_address",
        "ip_netmask",
        "ip_gateway",
        "prep_setup",
        "disk_setup",
        "os_setup",
        "apps_setup",
        "additional_setup",
        # ~ "tags"
    )
    column_labels = {
        "asset_type": "Type",
        "ip_address": "IP Address",
        "ip_netmask": "Netmask",
        "ip_gateway": "Gateway",
        "os": "OS",
        "os_setup": "OS Setup",
        "physical_location": "Loc",
        "setup_date": "Setup",
    }
    column_formatters = {
        "setup_date": _setup_date_formatter,
        "prep_setup": _prep_setup_formatter,
        "disk_setup": _disk_setup_formatter,
        "os_setup": _os_setup_formatter,
        "apps_setup": _apps_setup_formatter,
        "additional_setup": _additional_setup_formatter,
        "purpose": _purpose_formatter,
        "note": _note_formatter,
    }
    form_args = {
        "status": {
            "query_factory": lambda: SetupStatus.query.order_by(SetupStatus.name)
        },
        "asset": {
            "query_factory": lambda: Asset.query.filter_by(asset_type_id=1).order_by(
                Asset.alias
            )
        },
    }

    form_widget_args = {
        "purpose": {"class": "markdown"},
        "note": {"class": "markdown"},
        "prep_setup": {"class": "markdown"},
        "disk_setup": {"class": "markdown"},
        "os_setup": {"class": "markdown"},
        "apps_setup": {"class": "markdown"},
        "additional_setup": {"class": "markdown"},
    }
    column_default_sort = ("alias", False)


class SetupStatusView(sqla.ModelView):
    def is_accessible(self):
        return (
            True
            if current_user.is_authenticated
            and (current_user.has_role("users") or current_user.has_role("admins"))
            else False
        )

    def inaccessible_callback(self, name, **kwargs):
        return redirect(url_for("ldap_login_view"))

    def _handle_view(self, name, **kwargs):
        if not self.is_accessible():
            return self.inaccessible_callback(name, **kwargs)
        self.can_delete = True if current_user.has_role("admins") else False

    page_size = 20
    can_view_details = False
    create_modal = True
    edit_modal = True
    # details_modal = True
    can_export = True
    column_searchable_list = ["name"]
    column_filters = ["name"]
    column_editable_list = ["name"]
    column_list = ["name"]
    column_exclude_list = []
    # sort by name, descending
    column_default_sort = ("name", False)
    form_excluded_columns = ()
    form_widget_args = {"name": {"placeholder": "setup status name"}}
