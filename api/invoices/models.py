#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#  SPDX-License-Identifier: GPL-3.0-only
#  Copyright 2025 dradux.com

import logging
from datetime import datetime
from typing import List
from enum import Enum
from config.models import Cdb
from pydantic import BaseModel
from registrations.models import RegistrationWithTaskNameResponse

logger = logging.getLogger("default")
_db = Cdb.INVOICES


class InvoiceLayout(str, Enum):
    summary = "summary"
    detail = "detail"


class InvoiceStatus(str, Enum):
    created = "created"
    submitted = "submitted"
    paid = "paid"
    deposited = "deposited"
    unknown = "unknown"


class InvoiceBase(BaseModel):
    """
    Base document

    Note: status is calculated, see utils.calculate_status for details.
    """

    layout: InvoiceLayout | None = None  # layout of the invoice
    name: str | None = None  # invoice name (only useful to see/reference the invoice)
    client: str | None = None  # client the invoice belongs to
    invoice_number: str | None = None  # invoice number
    discount: float = 0  # discount amount for invoice
    paid_amount: float = 0  # amount already paid
    submitted: datetime | None = None  # date invoice was submitted to client
    due: datetime | None = None  # date invoice is due
    paid: datetime | None = None  # date invoice was paid
    deposited: datetime | None = None  # date invoice was deposited

    # NOTICE: registrations are linked to invoice by adding the invoice id to the registration.invoice
    # registrations: List[str] | None    # registrations that belong to the invoice

    note: str | None = None  # notes about invoice
    detail: str | None = None  # details about the invoice (note: this is internal notes)
    tags: List[str] | None = None  # list of tags for an invoice


class InvoiceExt(InvoiceBase):
    """
    Extended (added by backend logic)
    """

    # note: this needs to be set/overwrote on result instantiation as using
    #  datetime.now() here will only get you now of when worker was started.
    created: datetime = datetime.now()
    updated: datetime = datetime.now()

    creator: str | None = None
    updator: str | None = None


class Invoice(InvoiceExt):
    """
    Actual (at DB level)
    """

    id_: str


class InvoiceResponse(Invoice):
    """
    Response
    """

    total: int | None = None
    Invoices: List[Invoice] | None = None


class InvoiceWithStatus(Invoice):
    """
    Invoice with status.
    """

    status: InvoiceStatus | None = None


class InvoiceWithClientNameResponse(InvoiceWithStatus):
    """
    Response with client name and nickname (in addition to client id).
    """

    client_name: str | None = None
    client_nickname: str | None = None
    number_registrations: int | None = None
    amount: float | None = None


class InvoiceWithRegistrationsResponse(InvoiceWithStatus):
    """
    Invoice with Registrations
    """

    client_name: str | None
    # ~ registrations: List[RegistrationBase] = []
    registrations: List[RegistrationWithTaskNameResponse] = []
