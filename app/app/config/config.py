#
# Global config
#

import tomllib
from os import getenv, path

from .utils import env_strtobool


class BaseConfig(object):
    # ~ def env_strtobool(val: str) -> bool:
    # ~ if val in ["true", "'true'"]:
    # ~ return True
    # ~ return False

    def load_config() -> dict:
        """Load config data from toml config file"""
        conf = {}
        try:
            with open("config/config.toml", "rb") as f:
                conf = tomllib.load(f)
        except FileNotFoundError as e:
            print(f"File not found: {e}")
        except Exception as e:
            print(f"Other Exception: {e}")
        return conf

    DEPLOY_ENV = getenv("DEPLOY_ENV", "prd")

    # development/debugging.
    DEBUG = env_strtobool(getenv("APP_DEBUG", "False"))
    SQLALCHEMY_ECHO = env_strtobool(getenv("APP_SQLALCHEMY_ECHO", "False"))
    #   flask debug toolbar.
    DEBUG_TB_INTERCEPT_REDIRECTS = env_strtobool(
        getenv("APP_DEBUG_TB_INTERCEPT_REDIRECTS", "false")
    )

    # security
    SECRET_KEY = getenv("SECRET_KEY", None)
    # SECURITY_PASSWORD_HASH = getenv("SECURITY_PASSWORD_HASH", "pbkdf2_sha512")
    # SECURITY_PASSWORD_SALT = getenv("SECURITY_PASSWORD_SALT", None)
    # SECURITY_RECOVERABLE = getenv("SECURITY_RECOVERABLE", "True")
    # SECURITY_CHANGEABLE = getenv("SECURITY_CHANGEABLE", "True")

    # database
    DB_NAME = getenv("DB_NAME", "postgres")
    DB_USER = getenv("DB_USER", "postgres")
    DB_PASS = getenv("DB_PASS", "postgres")
    DB_SERVICE = getenv("DB_SERVICE", "db")
    DB_PORT = getenv("DB_PORT", "5432")
    SQLALCHEMY_DATABASE_URI = (
        f"postgresql://{DB_USER}:{DB_PASS}@{DB_SERVICE}:{DB_PORT}/{DB_NAME}"
    )
    SQLALCHEMY_TRACK_MODIFICATIONS = False

    # application
    APP_BASE_URL = getenv("APP_BASE_URL", "/radiant")
    # NOTICE: keep in mind this dir is in the container (and should likely be mounted to something persistent).
    ATTACHMENTS_DIR = path.abspath(getenv("APP_ATTACHMENTS_DIR", "/opt/attachments"))
    LDAP_HOST = getenv("LDAP_HOST", None)
    LDAP_PORT = int(getenv("LDAP_PORT", "636"))
    LDAP_USE_SSL = env_strtobool(getenv("LDAP_USE_SSL", "true"))
    LDAP_USE_STARTTLS = env_strtobool(getenv("LDAP_USE_STARTTLS", "false"))
    # The RDN attribute for your user schema on LDAP
    LDAP_USER_RDN_ATTR = "cn"
    # The Attribute you want users to authenticate to LDAP with.
    LDAP_USER_LOGIN_ATTR = "cn"
    # user attributes to get.
    # LDAP_GET_USER_ATTRIBUTES = ldap3.ALL_ATTRIBUTES
    # search with bind user.
    # ~ LDAP_ALWAYS_SEARCH_BIND = True
    LDAP_ALWAYS_SEARCH_BIND = False
    # The Username to bind to LDAP with
    LDAP_BIND_USER_DN = getenv("LDAP_BIND_USER_DN", None)
    # The Password to bind to LDAP with
    LDAP_BIND_USER_PASSWORD = getenv("LDAP_BIND_USER_PASSWORD", None)
    # Base DN of your directory
    LDAP_BASE_DN = getenv("LDAP_BASE_DN", None)
    # Users DN to be prepended to the Base DN (ou=users)
    # ~ LDAP_USER_DN = 'ou=users'
    LDAP_USER_DN = "ou=app"
    # Group settings
    # NOTICE: keep the following to False; we do not use Flask-Ldap3-Login's group search as something dont work with it, rather we use ldap3 directly.
    LDAP_SEARCH_FOR_GROUPS = False
    # Groups DN to be prepended to the Base DN
    # LDAP_GROUP_DN = 'ou=groups'
    # LDAP_GROUP_OBJECT_FILTER = '(objectClass=groupOfNames)'
    # LDAP_GROUP_OBJECT_FILTER = None
    # LDAP_GROUP_MEMBERS_ATTR = 'member'
    # Group scope ([LEVEL], SUBTREE) there are other levels (base/object, subordinates
    # LDAP_GROUP_SEARCH_SCOPE = 'LEVEL'
    LDAP_APP_GROUP_PREFIX = getenv("LDAP_APP_GROUP_PREFIX", None)
    LDAP_USER_ROLE_SEARCH_BASE = getenv("LDAP_USER_ROLE_SEARCH_BASE", None)
    LDAP_USER_ROLE_SEARCH_FILTER = getenv("LDAP_USER_ROLE_SEARCH_FILTER", None)
    LDAP_USER_ROLE_ATTRIBUTES = getenv("LDAP_USER_ROLE_ATTRIBUTES", None)
    LDAP_USER_ROLE_BATCH = env_strtobool(getenv("LDAP_USER_ROLE_BATCH", "true"))
    LDAP_USER_CHECK_FOR_ENABLED = env_strtobool(
        getenv("LDAP_USER_CHECK_FOR_ENABLED", "true")
    )
    LDAP_USER_SEARCH_BASE = getenv("LDAP_USER_SEARCH_BASE", None)
    LDAP_USER_SEARCH_FILTER = getenv("LDAP_USER_SEARCH_FILTER", None)

    # logging
    LOG_TO = getenv("LOG_TO", "console").split(",")
    APP_LOGLEVEL = getenv("LOG_LEVEL", "DEBUG")

    # autoreload templates on change.
    TEMPLATES_AUTO_RELOAD = True if DEPLOY_ENV == "dev" else False

    #
    #  Inventory config items
    #
    APP_ENERGY_HOURS_PER_DAY = float(getenv("APP_ENERGY_HOURS_PER_DAY", 24))
    APP_ENERGY_KWH_PRICE = float(getenv("APP_ENERGY_KWH_PRICE", 0.10))
    # from .inventory import *
    # from .inventory import BaseConfig.APP_ENERGY_HOURS_PER_DAY
    # APP_ENERGY_HOURS_PER_DAY = inv.APP_ENERGY_HOURS_PER_DAY

    BASE = load_config()
    CORE = BASE["core"]


class DefaultConfig(BaseConfig):
    # Statement for enabling the development environment
    # DEBUG = True
    pass
