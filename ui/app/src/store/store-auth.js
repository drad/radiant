/*
 * AUTH: handles authentication related items.
 */

import { api } from 'boot/axios'
import { LocalStorage } from 'quasar'

const state = {}

const mutations = {}

const actions = {
  getToken({ commit }, payload) {
     // Get token does the following:
    //    1. check for local Token (return if it does)
    //    2. fetch, set and return new Token

    //~ console.log("store > auth > getToken with payload", payload)
    var querystring = require('querystring');
    return new Promise((resolve, reject) => {
      api.post('/auth/token', querystring.stringify(payload))
        .then((response) => {
            // Set localStorage.
            LocalStorage.set('radiant-ui_access-token', response.data.access_token);
            //~ console.log("LocalStorage set!");
            api.defaults.headers.common['Authorization'] = 'Bearer ' + response.data.access_token;
            //~ console.log("API Auth set!");
            // return access token.
            resolve(response.data.access_token)
        })
        .catch((error) => {
            // note: this is hit when an unauthenticated user hits / which redirects them to /login
            //       no harm in the message so we just dump to console.
            console.error('Error getting token:', error)
            //~ commit('setInvoice', {})
            reject(error)
        })
    })
  },
}

const getters = {}

export default {
  namespaced: true,

  state,
  mutations,
  actions,
  getters
}
