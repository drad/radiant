# App Developer Guide

## NOTICES

- after commit be0d649 (2020-03-14) we no longer change the docker-comose extra_hosts to set/change the ldap server url which the app points to
  - we instead change the LDAP_HOST envvar in the .env file to point to a remote / local (docker) ldap server - the main reason for this is so the user can see what ldap server the app is using on the login page.

## Migrations

This project uses flask-migrate.

- make changes to model, then create migration: `docker-compose run app flask db migrate -m 'revision message'`
  - or you can create a manual migration: `docker-compose run app flask db revision -m 'revision message'`
- review created migration and make changes if necessary (should not be necessary)
- apply migration: `docker-compose run app flask db upgrade`
- other/useful information:
  - show migration history: `flask db history`
  - revert/rollback: `flask db downgrade`

### Deployment (e.g. production deployment of migration)

See the app-deployment.md guide for deployment related migration information.

### Notes

- if you mess up a migration and need to start over simply rollback and delete the migration file then create new migration.
