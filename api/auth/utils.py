#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#  SPDX-License-Identifier: GPL-3.0-only
#  Copyright 2025 dradux.com

import logging
from datetime import datetime, timedelta
from typing import Optional

import jwt
from config import config
from config.config import cfg
from users.models import UserBase
from sse.models import SSEEventType, SSEActionType
from sse.utils import send_sse_message

logger = logging.getLogger("default")


def create_access_token(data: dict, expires_delta: Optional[timedelta] = None):
    to_encode = data.copy()
    if expires_delta:
        expire = datetime.utcnow() + expires_delta
    else:
        expire = datetime.utcnow() + timedelta(minutes=15)
    to_encode.update({"exp": expire})
    encoded_jwt = jwt.encode(
        to_encode,
        config.API_JWT_SECRET_KEY,
        algorithm=cfg.api.jwt.algorithm,
    )
    return encoded_jwt


# ~ async def has_account_access(
# ~ current_user: UserBase = None,
# ~ account: Account = None,
# ~ roles: Role = None,
# ~ ) -> bool:
# ~ """
# ~ Check if user has account access
# ~ """

# ~ # get role for account in question.
# ~ # logger.debug(f"- current user accounts: {current_user.accounts}")
# ~ # logger.debug(f"- account: {account}")
# ~ ac_role = [a.role for a in current_user.accounts if a.id_ == account.id_][0]
# ~ # return if role for account is in role(s) list.
# ~ return ac_role in roles


async def has_role(user: UserBase = None, role: str = None) -> bool:
    """
    Check if user has a given role
    """

    return role in user.roles


async def send_user_post_login_ui_events(username: str = None):
    """
    Send user post login ui events.

    aka: startup events, login events

    This is where we send events to the UI causing it to fetch the data needed on startup.
    """

    if username:
        logger.debug("Sending post login ui events...")
        # notify via sse.
        #  NOTE: activity.new fetched UserActivitySummary as well.
        # @TODO: at some point in the future we may want to get these from the db (system events & user events)
        await send_sse_message(user=username, event_type=SSEEventType.registration, data=SSEActionType.new.value)
        await send_sse_message(user=username, event_type=SSEEventType.invoice, data=SSEActionType.new.value)
        await send_sse_message(user=username, event_type=SSEEventType.client, data=SSEActionType.new.value)
        await send_sse_message(user=username, event_type=SSEEventType.task_list, data=SSEActionType.new.value)

        await send_sse_message(user=username, event_type=SSEEventType.user_message, data=SSEActionType.new.value)
        logger.debug("Post login ui events sent!")
    else:
        # @TODO: should likely write to logs here
        logger.error("No username supplied, cannot sent user post login ui events!")
