#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#  SPDX-License-Identifier: GPL-3.0-only
#  Copyright 2025 dradux.com

from datetime import timedelta

from auth.models import Token
from auth.utils import create_access_token
from config import config
from fastapi import APIRouter, Depends, HTTPException, status
from fastapi.security import OAuth2PasswordRequestForm
from users.models import authenticate_user

auth_router = APIRouter()


@auth_router.post("/token", include_in_schema=False, response_model=Token)
async def login_for_access_token(form_data: OAuth2PasswordRequestForm = Depends()):
    user, roles = await authenticate_user(form_data.username, form_data.password)
    if not user or not roles:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Incorrect username or password or missing role(s)",
        )
    access_token_expires = timedelta(
        minutes=config.API_JWT_EXPIRE_MINUTES,
        hours=config.API_JWT_EXPIRE_HOURS,
        days=config.API_JWT_EXPIRE_DAYS,
    )
    access_token = create_access_token(
        # let user specify scopes on login form.
        # ~ data={"sub": user.username, "scopes": form_data.scopes},
        # get scopes from roles.
        data={"sub": user.username, "scopes": roles},
        expires_delta=access_token_expires,
    )
    return {"access_token": access_token, "token_type": "bearer"}
