// custom scripts


/*
 * Copy input text to clipboard.
 */
function clipit(in_string, obj) {
    var input = document.createElement('input')
    input.id="__copyText__";
    input.value = in_string;
    document.body.appendChild(input);
    input.select();
    document.execCommand("copy");
    var txt = input.value
    input.remove()
    console.log("Text Copied to Clipboard: '"+txt+"'");
    clipitAlert("Copied to clipboard!", obj)
}


/*
 * Clipit alert (div popup).
 */
function clipitAlert(message, obj) {
    x = document.getElementById(obj);
    x.innerHTML=message;
    x.classList.toggle("show");
    setTimeout('x.classList.toggle("show")', 3000);
}

/*
 * Parse filesystem benchmark raw results to pull out read/write IOPS and runtime.
 */

//~ No longer needed as we do this in python.
//~ function parseFilesystemBenchmarkRawResults(results_raw) {
    //~ if ( results_raw ) {
        //~ try {
            //~ let j = JSON.parse(results_raw);
            //~ var run_date = new Date(Date.parse(j.time)).toISOString()
            //~ var d = new Date(Date.parse(j.time))
            //~ var year = d.getFullYear()
            //~ var month = ((d.getMonth()+1) < 10 ? '0' + (d.getMonth()+1) : (d.getMonth()+1))
            //~ var day = d.getDate() < 10 ? '0' + d.getDate() : d.getDate()
            //~ var hour = d.getHours() < 10 ? '0' + d.getHours() : d.getHours()
            //~ var minute = d.getMinutes() < 10 ? '0' + d.getMinutes() : d.getMinutes()
            //~ var second = d.getSeconds() < 10 ? '0' + d.getSeconds() : d.getSeconds()
            //~ var formatted_date = year + "-" + month + "-" + day + " " + hour + ":" + minute + ":" + second
            //~ return { "runtime": (j.jobs[0].elapsed/60), "read_iops": j.jobs[0].read.iops, "write_iops": j.jobs[0].write.iops, "run_date": formatted_date}
        //~ } catch(e) {
            //~ console.error("Error parsing Raw Results:", e);
            //~ return {}
        //~ }
    //~ } else {
        //~ console.log("No Raw Results to process.");
        //~ return {}
    //~ }
//~ }
