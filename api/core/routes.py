#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#  SPDX-License-Identifier: GPL-3.0-only
#  Copyright 2025 dradux.com

from datetime import datetime

from config.config import cfg
from fastapi import APIRouter

core_router = APIRouter()


@core_router.get("/status", status_code=200)
def status():
    """[summary]
    Standard status

    [description]
    Return 200 for health checks.
    """

    return {"status": "OK", "date": datetime.now().strftime("%Y-%m-%d %H:%M:%S")}


@core_router.get("/version", status_code=200)
def version():
    """[summary]
    Application version

    [description]
    Shows application version info
    """

    return {
        "status": "OK",
        "version": cfg.core.version,
        "created": cfg.core.created,
        "modified": cfg.core.modified,
    }
