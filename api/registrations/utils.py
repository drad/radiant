#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#  SPDX-License-Identifier: GPL-3.0-only
#  Copyright 2025 dradux.com

import aiocouch
import logging
import arrow
from time import perf_counter
from datetime import datetime
from registrations.models import _db
from config.config import get_cdb, CDB_URI, cdb_httpx_auth
from typing import List
from fastapi import HTTPException
from registrations.models import RegistrationExt
from projects.utils import get_by_id as projects_get_by_id
from tasks.utils import get_by_id as tasks_get_by_id
from _common.models import DocInfo

import uuid
from fastapi.encoders import jsonable_encoder
from registration_types.utils import get_by_id as get_registration_type_by_id
from _common.utils import make_id, local_date_to_utc, utc_to_local_date
from sse.utils import send_sse_message
from sse.models import SSEEventType, SSEActionType

from httpx import AsyncClient

logger = logging.getLogger("default")


async def add_registration(
    start: datetime = None,
    end: datetime = None,
    task_id: str = None,
    ticket_number: str = None,
    registration_type: str = None,
    note: str = None,
    tags: List[str] = None,
    username: str = None,
    user_timezone: str = None,
):
    """
    Add a Registration.
    """

    _now = await local_date_to_utc(indate=datetime.now(), tz=user_timezone)
    _start = await local_date_to_utc(indate=start, tz=user_timezone)
    _end = await local_date_to_utc(indate=end, tz=user_timezone)
    logger.debug(
        f"Registrations > Utils > add_registration:\n  - start: {_start} ({type(_start)})\n  - end:   {_end} ({type(_end)})\n  - now: {_now} ({type(_now)})"
    )

    couchdb = get_cdb()
    db = await couchdb[_db.value]

    # do not allow Start in the future.

    if _start > _now:
        raise HTTPException(
            status_code=417,
            detail="Start date cannot be in the future!",
        )
    if _end and _end > _now:
        raise HTTPException(
            status_code=417,
            detail="End date cannot be in the future!",
        )
    if _end and _end < _start:
        raise HTTPException(
            status_code=417,
            detail="Start date cannot be greater than End date!",
        )

    # if task is supplied, check if exists
    if task_id:
        _rr = await tasks_get_by_id(task_id)
        if not _rr:
            raise HTTPException(
                status_code=417,
                detail="Task not found, did you enter the Name instead of the Id?",
            )

    # if registration_type supplied, check if exists
    if registration_type:
        # check to ensure client exists.
        _rt = await get_registration_type_by_id(registration_type)
        if not _rt:
            raise HTTPException(
                status_code=417,
                detail="Registration Type not found, did you enter the Name instead of the Id?",
            )

    _curdts = await local_date_to_utc(indate=datetime.now(), tz=user_timezone)

    _doc = RegistrationExt(
        start=_start,
        end=_end,
        duration=(end - start).total_seconds() if end else None,
        task_id=task_id,
        ticket_number=ticket_number,
        registration_type=registration_type,
        note=note,
        tags=[t.lower() for t in tags] if tags else None,  # lowercase all tags
        created=_curdts,
        updated=_curdts,
        creator=username,
        user=username,
    )
    response_msg = ""
    if start and end:
        _length = arrow.get(start).humanize(arrow.get(end), only_distance=True)
        logger.debug(f"######> Duration: {_length}")
        response_msg = f"Duration: {_length}"

    logger.debug(f"Adding Registration: {_doc}")

    try:
        doc_id = make_id(items=[str(uuid.uuid4())])
        doc = await db.create(
            doc_id,
            data=jsonable_encoder(_doc),
        )
        await doc.save()
        # send sse message
        await send_sse_message(user=username, event_type=SSEEventType.registration, data=SSEActionType.add)
        resp = DocInfo(**await doc.info())
        resp.msg = response_msg
        logger.debug(f"Returning: {resp}")
        return resp

    except aiocouch.ConflictError:
        raise HTTPException(status_code=409, detail=f"Duplicate Key: cannot add '{doc_id}'.")


async def get_for_invoice(_invoice: str = None, user_timezone: str = None):
    """
    Get Registrations for an Invoice.
    """

    couchdb = get_cdb()
    db = await couchdb[_db.value]

    selector = {"invoice": _invoice}

    logger.debug(f"- selector is: {selector}")

    docs = []
    # NOTICE: db.find uses a sort which requires the needed db index(es)
    async for doc in db.find(selector=selector):
        # _doc = fix_id(doc)
        # convert start/end back to user tz
        if user_timezone:
            doc["start"] = await utc_to_local_date(indate=doc["start"], tz=user_timezone)
            doc["end"] = await utc_to_local_date(indate=doc["end"], tz=user_timezone)
        # get task label
        if doc["task_id"]:
            _task = await tasks_get_by_id(doc["task_id"])
            if _task:
                doc["task_name"] = _task["name"]
                # get bill_rate
                _project = await projects_get_by_id(_task["project_id"])
                if _project:
                    doc["bill_rate"] = _project["bill_rate"]
        if doc["registration_type"]:
            _registration_type = await get_registration_type_by_id(doc["registration_type"])
            if _registration_type:
                doc["registration_type_name"] = _registration_type["name"]

        docs.append(doc)

    return docs


async def get_time_summary(user: str = None, start: str = None, end: str = None):
    """
    Get the time summary (for time summary report) for a given user, start and end.
    """

    logger.debug(f"Get time summary for user={user}, start={start}, end={end}")
    _return = []
    async with AsyncClient() as session:
        params = {
            "group": "true",
            "group_level": 1,  # group on user
            "startkey": [user, start],
            "endkey": [user, end],
        }
        _resp = await session.post(
            f"{CDB_URI}/registrations/_design/TimeSummary/_view/range-time-summary", auth=cdb_httpx_auth, json=params
        )
        j = _resp.json()

        _return = j["rows"][0] if "rows" in j and len(j["rows"]) > 0 else []
    return _return


async def get_user_count(
    user: str = None,
    selector: dict = {},
):
    """
    Get count for user + filter.
    """

    # @TODO: this should be moved to a view to enhance performance.
    logger.debug(f"Get all for user={user} with selector: {selector}")
    start = perf_counter()
    couchdb = get_cdb()
    db = await couchdb[_db.value]
    cnt = 0
    async for doc in db.find(selector=selector):
        cnt += 1

    end = perf_counter()
    logger.debug(f"Total docs for user+filter: {cnt} (took {end-start}s)")
    return cnt
