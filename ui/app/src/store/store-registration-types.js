import Vue from 'vue'
import { api } from 'boot/axios'

const state = {
    registration_type_list_loading: true,
    registration_type_list: []
}

const mutations = {
    setRegistrationTypeListLoading(state, payload) {
        state.registration_type_list_loading = payload
    },
    addRegistrationTypeList(state, payload) {
        state.registration_type_list = payload
    },

}

const actions = {
    fetchRegistrationTypeList({ commit }, payload) {
        commit('setRegistrationTypeListLoading', true)
        //~ console.log("store > registration_type_list > fetch", payload)
        api.get('/registration_types/registration_type_list', { params: {client: payload.client, limit: 50, skip: 0}})
            .then((response) => {
                //~ console.log(' ⟶ fetch task list return:', JSON.stringify(response.data))
                commit('addRegistrationTypeList', response.data)
            })
            .catch(() => {
                // note: this is hit when an unauthenticated user hits / which redirects them to /login
                //       no harm in the message so we just dump to console.
                console.log('No registration type list found.')
            })
        commit('setRegistrationTypeListLoading', false)
    },
}

const getters = {
    registration_type_list_loading: (state) => {
      return state.registration_type_list_loading
    },
    registration_type_list: (state) => {
        return state.registration_type_list
    }
}

export default {
    namespaced: true,

    state,
    mutations,
    actions,
    getters
}
