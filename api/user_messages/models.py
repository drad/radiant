#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#  SPDX-License-Identifier: GPL-3.0-only
#  Copyright 2025 dradux.com

import logging
from datetime import datetime
from enum import Enum

from config.models import Cdb
from pydantic import BaseModel

logger = logging.getLogger("default")
_db = Cdb.USER_MESSAGES


class UserMessageCategory(str, Enum):
    activity_note_extractor = "activity note extractor"
    enricher = "enricher"
    user = "user"
    webdav_handler = "webdav handler"
    worker = "worker"
    system = "system"


class UserMessagePriority(str, Enum):
    high = "high"
    normal = "normal"
    low = "low"


class UserMessageStatus(str, Enum):
    new = "new"
    read = "read"
    deleted = "deleted"


class UserMessageBase(BaseModel):
    """
    Base
    """

    to_user_id: str = None  # user the message belongs to
    from_user_id: str | None = None  # user id of user sending message (if applicable).
    status: UserMessageStatus = None  # the status of the message
    category: UserMessageCategory = None  # category of message
    priority: UserMessagePriority = None  # priority of message
    subject: str = None  # subject of message
    message: str = None  # the message
    note: str | None = None  # notes for the message


class UserMessageExt(UserMessageBase):
    """
    Extended (added by backend logic)
    """

    # note: this needs to be set/overwrote on result instantiation as using
    #  datetime.now() here will only get you now of when worker was started.
    created: datetime = datetime.now()
    updated: datetime = datetime.now()

    creator: str | None = None
    updator: str | None = None


class UserMessage(UserMessageExt):
    """
    Actual (at DB level)
    """

    id_: str


class UserMessageResponse(UserMessage):
    """
    User Message Response
    """

    pass
