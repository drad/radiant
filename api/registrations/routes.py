#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#  SPDX-License-Identifier: GPL-3.0-only
#  Copyright 2025 dradux.com

import aiocouch
import arrow
import logging
import time
from datetime import datetime
import random
from _common.models import DocInfo
from _common.utils import fix_id, local_date_to_utc, utc_to_local_date
from sse.utils import send_sse_message
from sse.models import SSEEventType, SSEActionType
from config.config import get_cdb
from fastapi import APIRouter, HTTPException, Query, Security, status
from fastapi.encoders import jsonable_encoder
from registration_types.utils import get_by_id as get_registration_type_by_id
from registrations.models import (
    RegistrationResponse,
    RegistrationWithLabelResponse,
    RegistrationWithLabels,
    RegistrationTimeSummaryReportResponse,
    RegistrationTimeSummaryDetail,
    Registration,
    _db,
)
from registrations.utils import add_registration, get_user_count, get_time_summary
from tasks.utils import get_enhanced_by_id as get_enhanced_task_by_id
from invoices.utils import get_by_id as get_invoice_by_id
from users.models import UserBase, get_current_user

logger = logging.getLogger("default")
registrations_router = APIRouter()


async def _get_or_404(_id, db):
    """
    Get a document or raise a 404
    """

    # ~ logger.debug(f"_get_or_404 request on: {_id}")
    try:
        return await db[_id]
    except aiocouch.exception.NotFoundError:
        raise HTTPException(status_code=404, detail="Not found")


@registrations_router.get("/all", response_model=RegistrationResponse)
async def get_all(
    limit: int = Query(10, ge=1, le=99, description="Limit number of results"),
    skip: int = Query(0, description="Skip number of records before applying limit"),
    current_user: UserBase = Security(get_current_user, scopes=["admins"]),
):
    """
    Get all
    - only for admins
    """

    couchdb = get_cdb()
    logger.debug(f"- get all: current_user: {current_user}")
    db = await couchdb[_db.value]

    _response = RegistrationResponse()
    docs = []
    async for doc in db.find(selector={}, limit=limit, skip=skip):
        _doc = fix_id(doc)
        docs.append(Registration(**_doc))

    _response.total = len(docs)
    _response.registrations = docs
    # ~ logger.debug(f"RegistrationResponse: {_response}")

    return _response


# ~ @registrations_router.get("/all-for-user", response_model=list[dict])
@registrations_router.get("/all-for-user", response_model=RegistrationWithLabelResponse)
async def get_all_for_user(
    filter_general: str = Query(None, description="filter results on Ticket#, Note, or Tag"),
    filter_start: str = Query(None, description="filter on start date (YYYY-MM-DD)"),
    filter_end: str = Query(None, description="filter on end date (YYYY-MM-DD)"),
    filter_client: str = Query(None, description="filter on client"),
    include_invoiced: bool = Query(False, description="include invoiced Registrations"),
    limit: int = Query(
        10, ge=0, le=9999, description="Limit number of results; a value of 0 indicates no limit (fetch all)"
    ),
    skip: int = Query(0, description="Skip number of records before applying limit"),
    current_user: UserBase = Security(get_current_user, scopes=["users"]),
):
    """
    Get registrations for [current] user.
    - if doc has a task_id, the associated task is added to result.
    - if doc has a registration_type, the associated registration type is added to result.
    __NOTICE:__ filtering on client is a much more taxing task than other filter mechanisms as we need to get all projects for the client and then all tasks for those projects and then use this list of tasks to filter the Registrations.
    """

    start = time.time()
    couchdb = get_cdb()
    logger.critical(f"Get all for user with current_user: {current_user.username}, limit={limit}, skip={skip}")
    db = await couchdb[_db.value]

    selector = {}
    selector["user"] = current_user.username

    if filter_general:
        selector.update(
            {
                "$or": [
                    {"ticket_number": {"$regex": f"{filter_general}"}},
                    {"note": {"$regex": f"{filter_general}"}},
                    {"tags": {"$elemMatch": {"$eq": filter_general}}},
                ]
            }
        )
    if filter_start:
        selector.update({"start": {"$regex": f"{filter_start}"}})
    if filter_end:
        selector.update({"end": {"$regex": f"{filter_end}"}})
    if filter_client:
        logger.debug("Filter by client requested...")
        # get all projects for client
        from projects.utils import get_by_client_id
        from tasks.utils import get_by_project_id

        _projects = await get_by_client_id(filter_client)
        _tasks = []
        # get all tasks for each projects
        for _project in _projects:
            # ~ logger.debug(f"  - project: {_project['nickname']}: {_project['name']} > {_project['id_']}")
            # ~ _project_tasks = await get_by_project_id(_project["id_"])
            _project_tasks = await get_by_project_id(_project.id_)
            _tasks.extend(_project_tasks)
            # ~ for task in _project_tasks:
            # ~ logger.debug(f"    - task: {task['nickname']}: {task['name']} > {task['id_']}")
        selector.update(
            {
                "task_id": {"$in": [x["_id"] for x in _tasks]},
            }
        )
    if include_invoiced:
        # do nothing as all will be shown
        logger.debug("No filter on invoice - all included")
        pass
    else:
        # filter out invoices
        logger.debug("Invoice exclude - do not include invoiced registrations...")
        selector.update(
            {
                "$nor": [
                    {"invoice": {"$exists": True, "$ne": None}},
                    {"invoice": {"$exists": False}},
                ]
            }
        )

    sort = [{"start": "desc"}]

    _limit = limit if limit > 0 else None

    logger.debug(
        f"""◆────────────────────[ DB QUERY INFO ]────────────────────◆
- sort:     {sort}
- limit:    {_limit}
- skip:     {skip}
- selector: {selector}
"""
    )

    _response = RegistrationWithLabelResponse()
    docs = []
    # NOTICE: db.find uses a sort which requires the appropriate db index(es)
    async for doc in db.find(selector=selector, sort=sort, limit=_limit, skip=skip):
        _doc = fix_id(doc)
        # if task supplied, get task name
        if _doc["task_id"]:
            logger.debug(f"--> has task_id={_doc['task_id']}")
            _task = await get_enhanced_task_by_id(_doc["task_id"])
            # ~ logger.debug(f"- enhanced task returned:\n{_task}")
            if _task:
                _doc["task"] = jsonable_encoder(_task)
        # if registration type, get registration type name
        if _doc["registration_type"]:
            _rt = await get_registration_type_by_id(_doc["registration_type"])
            # ~ logger.debug(f"⟶ full registration type: {_rt}")
            if _rt:
                _doc["registration_type_full"] = jsonable_encoder(_rt)
        if _doc["start"]:
            _doc["start"] = await utc_to_local_date(indate=_doc["start"], tz=current_user.user_timezone)
        if _doc["end"]:
            _doc["end"] = await utc_to_local_date(indate=_doc["end"], tz=current_user.user_timezone)
        if _doc["duration"]:
            _length = arrow.get(_doc["start"]).humanize(arrow.get(_doc["end"]), only_distance=True)
            _doc["duration_humanized"] = _length

        _registration = RegistrationWithLabels(**_doc)
        docs.append(_registration)

    # @TODO: the following is the issue as it is returning total docs of query and it needs to return total number of registrations for pagination to work!!!
    # ~ _response.total = len(docs)
    _response.total = await get_user_count(user=current_user.username, selector=selector)
    _response.registrations = docs
    end = time.time()
    logger.debug(f"Got registrations in: {end-start}")

    return _response


@registrations_router.get("/one/{_id}", response_model=Registration, summary="Get one Registration")
async def get_one(
    _id: str,
    current_user: UserBase = Security(get_current_user, scopes=["users"]),
):
    """
    Get one by id
    """

    couchdb = get_cdb()
    db = await couchdb[_db.value]
    return Registration(**fix_id(await _get_or_404(_id, db)))


@registrations_router.post("/", response_model=DocInfo, status_code=status.HTTP_201_CREATED)
async def add(
    start: datetime = Query(..., description="start datetime of the registration"),
    end: datetime = Query(None, description="end datetime of the registration"),
    # duration (this is calculated)
    task_id: str = Query(None, description="task of the registration"),
    # invoiced
    ticket_number: str = Query(None, description="ticket number for the registration"),
    registration_type: str = Query(None, description="registration type of the registration"),
    note: str = Query(None, description="notes for the Registration"),
    tags: list[str] = Query(
        None,
        description="tags for the registration<br/><strong>NOTICE:</strong> all lowercase",
    ),
    current_user: UserBase = Security(get_current_user, scopes=["users"]),
):
    """
    Add new

    NOTES:
    - Start and End dates format are `YYYY-MM-DD HH:MM:SS` (in user's specified TZ) (note: they are stored in UTC)
    - Start date cannot be in the future
    - End date cannot be in the future
    - End date must be greater than Start date
    """

    logger.debug(f"Add Registration as user: {current_user.username}")

    _reg = await add_registration(
        start=start,
        end=end,
        task_id=task_id,
        ticket_number=ticket_number,
        registration_type=registration_type,
        note=note,
        tags=tags,
        username=current_user.username,
        user_timezone=current_user.user_timezone,
    )

    return _reg


@registrations_router.post("/batch", response_model=DocInfo, status_code=status.HTTP_201_CREATED)
async def add_batch(
    amount: int = Query(10, description="amount of registrations to create"),
    timespan_min: float = Query(1.3, description="minimum amount of time for registration"),
    timespan_max: float = Query(4.5, description="maximum amount of time for registration"),
    start: datetime = Query(..., description="start datetime of the registration"),
    # end: datetime = Query(None, description="end datetime of the registration"),
    task_id: str = Query("c7a02c71-0269-4d87-a664-c4870c1ea511", description="task of the registration"),
    # ticket_number: str = Query(None, description="ticket number for the registration"),
    registration_type: str = Query(
        "b8b88ab4-760f-493d-b296-5534b32ec956",
        description="registration type of the registration",
    ),
    note: str = Query(None, description="notes for the Registration"),
    tags: list[str] = Query(
        None,
        description="tags for the registration<br/><strong>NOTICE:</strong> all lowercase",
    ),
    current_user: UserBase = Security(get_current_user, scopes=["admins"]),
):
    """
    Add batch of Registrations: this will create _amount_ (number) of registrations each with an amount of time between _timespan_min_ and _timespan_max_. The first registration will
    be created with a start datetime of _start_, each subsequent start will be after the end time of the previous registration.

    - ticket_number will be of form TST-0 (where 0 will be replaced by the item # within the batch being added)
    - note and tags are optional, if supplied will be applied for all Registrations of the batch
    """

    logger.debug(
        f"""Add Batch Registrations
- user:   {current_user.username} (tz={current_user.user_timezone})
- amount: {amount}
- start:  {start}
"""
    )

    _tstart = time.perf_counter()
    _last_end = start
    _added = 0

    for r in range(amount):
        _pad = round(random.uniform(timespan_min, timespan_max), 3)  # nosec: we do not need true randomness here as its only a skew on duration
        _start = _last_end
        _end = arrow.get(_start).shift(minutes=+_pad)
        _ticket_number = f"TST-{r}"
        _reg = await add_registration(
            start=_start,
            end=_end.datetime.replace(tzinfo=None),
            task_id=task_id,
            ticket_number=_ticket_number,
            registration_type=registration_type,
            note=note,
            tags=tags,
            username=current_user.username,
            user_timezone=current_user.user_timezone,
        )
        if _reg:
            _added += 1
        # update _last_end to last end datetime.
        _last_end = _end.datetime.replace(tzinfo=None)

    _tend = time.perf_counter()
    resp = DocInfo(ok=True, id="n/a", msg="Batch Add complete", total=_added, runtime=_tend - _tstart)
    # send sse message.
    await send_sse_message(user=current_user.username, event_type=SSEEventType.registration, data=SSEActionType.add)
    return resp


@registrations_router.put("/invoice", response_model=DocInfo)
async def invoice(
    _id: str = Query(..., description="id of the document to invoice"),
    invoice: str = Query(..., description="invoice (id) that the registration belongs to"),
    batch_complete: bool = Query(True, description="SSE messages are only sent if batch_complete=True"),
    current_user: UserBase = Security(get_current_user, scopes=["users"]),
):
    """
    Invoice a Registration.
    """

    couchdb = get_cdb()
    logger.debug(f"Invoice registration with id: {_id}")
    db = await couchdb[_db.value]
    doc = await _get_or_404(_id, db)
    logger.debug(f"- updating doc: {doc}")

    # check invoice
    logger.debug("Checking invoice...")
    _invoice = await get_invoice_by_id(invoice)
    if _invoice:
        doc["invoice"] = invoice
        # logger.debug(f"doc before save: {doc}")
        await doc.save()
        if batch_complete:
            # send sse messages
            await send_sse_message(
                user=current_user.username, event_type=SSEEventType.registration, data=SSEActionType.update
            )
            await send_sse_message(
                user=current_user.username, event_type=SSEEventType.invoice, data=SSEActionType.update
            )
        resp = DocInfo(**await doc.info())
        resp.msg = "Registration invoiced"
        logger.debug(f"Returning: {resp}")
        return resp
    else:
        raise HTTPException(status_code=404, detail="Invoice Not found")


@registrations_router.put("/", response_model=DocInfo)
async def update(
    _id: str = Query(..., description="id of the document to update"),
    start: datetime = Query(None, description="start datetime of the registration"),
    # ~ end: datetime | str = Query(None, description="end datetime of the registration"),
    end: datetime = Query(None, description="end datetime of the registration"),
    # duration (this is calculated)
    task_id: str = Query(None, description="task of the registration"),
    # invoiced
    invoice: str = Query(None, description="invoice (id) that the registration belongs to"),
    ticket_number: str = Query(None, description="ticket number for the registration"),
    registration_type: str = Query(None, description="registration type of the registration"),
    note: str = Query(None, description="notes for the Registration"),
    tags: list[str] = Query(
        None,
        description="tags for the registration<br/><strong>NOTICE:</strong> all lowercase",
    ),
    current_user: UserBase = Security(get_current_user, scopes=["users"]),
):
    """
    Update a Registration

    __NOTICE:__
    - all non-required fields are truly optional, if not supplied no change to that field will be made with the following exceptions:
      + start: if not supplied __AND__ the registration has a value for it, it will be cleared
      + end: if not supplied __AND__ the registration has a value for it, it will be cleared
      + ticket_number: if not supplied __AND__ the registration has a value for it, it will be cleared
      + task_id: if not supplied __AND__ the registration has a value for it, it will be cleared
      + invoice: if not supplied __AND__ the registration has a value for it, it will be cleared
      + registration_type: if not supplied __AND__ the registration has a value for it, it will be cleared
      + note: if not supplied __AND__ the registration has a value for it, it will be cleared
      + tags: if not supplied __AND__ the registration has any values for it, it will be cleared
    """

    logger.debug(f"Updating registration with id: {_id}")
    logger.debug(f"  ⟶ end is: [{end}]")
    couchdb = get_cdb()
    db = await couchdb[_db.value]
    doc = await _get_or_404(_id, db)
    logger.debug(f"- updating doc: {doc}")
    response_msg = None

    if start:
        doc["start"] = jsonable_encoder(await local_date_to_utc(indate=start, tz=current_user.user_timezone))
    elif not start and doc["start"]:
        doc["start"] = None
    if end:
        doc["end"] = jsonable_encoder(await local_date_to_utc(indate=end, tz=current_user.user_timezone))
    elif not end and doc["end"]:
        # if no end and doc had end clear it and the duration.
        doc["duration"] = None
        doc["end"] = None
    if (start or doc["start"]) and (end or doc["end"]):
        _start = start if start else doc["start"]
        _end = end if end else doc["end"]
        # calculate duration
        _duration = arrow.get(_end).timestamp() - arrow.get(_start).timestamp()
        doc["duration"] = _duration
        # _dend = arrow.get(_end)
        # _dstart = arrow.get(_start)
        _length = arrow.get(_start).humanize(arrow.get(_end), only_distance=True)
        logger.debug(f"######> Duration: {_length}")
        response_msg = f"Duration: {_length}"
    if task_id:
        doc["task_id"] = task_id
    elif not task_id and doc["task_id"]:
        doc["task_id"] = None
    if invoice:
        doc["invoice"] = invoice
    elif not invoice and "invoice" in doc and doc["invoice"]:
        doc["invoice"] = None
    if ticket_number:
        doc["ticket_number"] = ticket_number
    elif not ticket_number and doc["ticket_number"]:
        doc["ticket_number"] = None
    if registration_type:
        doc["registration_type"] = registration_type
    elif not registration_type and doc["registration_type"]:
        doc["registration_type"] = None
    if note:
        doc["note"] = note
    elif not note and doc["note"]:
        doc["note"] = None
    if tags:
        doc["tags"] = tags
    elif not tags and doc["tags"]:
        doc["tags"] = []

    _curdts = await local_date_to_utc(indate=datetime.now(), tz=current_user.user_timezone)
    doc["updated"] = jsonable_encoder(_curdts)
    doc["updator"] = current_user.username
    doc["user"] = current_user.username

    # logger.debug(f"doc before save: {doc}")
    await doc.save()
    # send sse message of add
    await send_sse_message(user=current_user.username, event_type=SSEEventType.registration, data=SSEActionType.update)
    resp = DocInfo(**await doc.info())
    resp.msg = response_msg
    logger.debug(f"Returning: {resp}")
    return resp


@registrations_router.delete("/batch_delete", response_model=DocInfo)
async def delete_batch(
    user: str = Query(..., description="Limit: User who registration(s) belong to"),
    note: str = Query(None, description="Limit: Note of the registration(s) (supports regex)"),
    limit: int = Query(None, description="Limit: number of records (leave blank for no limit)"),
    skip: int = Query(0, description="Limit: skip number of records before applying limit"),
    # Params that effect saving
    persist: bool = Query(False, description="Persist (save) changes?"),
    current_user: UserBase = Security(get_current_user, scopes=["admins"]),
):
    """
    Batch delete allows deleting Registrations in bulk.
    NOTES:
    - this endpoint is used to delete a set of registrations (generally a set that was created by batch add).
    """

    couchdb = get_cdb()
    db = await couchdb[_db.value]
    selector = {}
    if user:
        selector["user"] = user
    if note:
        selector["note"] = {"$regex": note}

    logger.debug(f"Registration - Batch Delete selector: {selector}")
    cnt = 0
    _deleted = 0
    _errors = 0

    async for doc in db.find(selector=selector, limit=limit, skip=skip):
        logger.debug(f"Deleting doc: {doc['_id']}")
        cnt += 1
        if persist:
            dr = await doc.delete(discard_changes=True)
            if dr and dr.status < 400:
                _deleted += 1
            else:
                logger.error(f"- delete of document: {doc['id']} failed (status={dr.status}, headers: {dr.headers}")
                _errors += 1

    if persist and cnt > 0:
        # send sse message for delete
        await send_sse_message(
            user=current_user.username, event_type=SSEEventType.registration, data=SSEActionType.delete
        )

    return DocInfo(
        ok=True, msg=f"Registrations Deleted (persist={persist}): deleted={_deleted}, errors={_errors}", total=cnt
    )


@registrations_router.delete("/", response_model=DocInfo)
async def delete(
    _id: str,
    current_user: UserBase = Security(get_current_user, scopes=["admins"]),
):
    """
    Hard Delete by id
    """

    couchdb = get_cdb()
    db = await couchdb[_db.value]
    resp = DocInfo(ok=False, id=_id, rev="", msg="unknown")
    doc = await _get_or_404(_id, db)
    dr = await doc.delete()
    if dr and dr.status < 400:
        resp.ok = True
        resp.msg = "deleted"
        # send sse message.
        await send_sse_message(
            user=current_user.username, event_type=SSEEventType.registration, data=SSEActionType.delete
        )
    else:
        resp.ok = False
        resp.msg = f"Delete status: {dr.status}, headers: {dr.headers}"

    return resp


@registrations_router.get("/time_summary_report", response_model=RegistrationTimeSummaryReportResponse)
async def time_summary_report(
    current_user: UserBase = Security(get_current_user, scopes=["users"]),
):
    """
    The Time Summary Report shows the current amount of time per unit (day, week, month) the current user has registered.
    @TODO: unsure why but the .to("UTC") on daily/weekly/monthly start/end does not get me the correct offset so I have to manually shift (-5), need to find a more dynamic solution!!
    """

    start = time.time()
    logger.debug(f"Time Summary Report for current_user: {current_user.username}")

    # Daily time summary
    _daily_start = arrow.now("US/Eastern").floor("day").shift(hours=-5)
    # ~ _daily_end = arrow.utcnow().to("US/Eastern").ceil('day')
    _daily_end = arrow.now("US/Eastern").ceil("day").shift(hours=-5)
    _daily_summary = await get_time_summary(user=current_user.username, start=str(_daily_start), end=str(_daily_end))
    logger.debug(f"Daily time summary: {_daily_summary}")

    # Weekly time summary
    _weekly_start = arrow.now("US/Eastern").floor("day").shift(hours=-5, days=-7)
    _weekly_end = arrow.now("US/Eastern").ceil("day").shift(hours=-5)
    _weekly_summary = await get_time_summary(user=current_user.username, start=str(_weekly_start), end=str(_weekly_end))
    logger.debug(f"Weekly time summary: {_weekly_summary}")

    # Monthly time summary
    _monthly_start = arrow.now("US/Eastern").floor("day").shift(hours=-5, months=-1)
    _monthly_end = arrow.now("US/Eastern").ceil("day").shift(hours=-5)
    _monthly_summary = await get_time_summary(
        user=current_user.username, start=str(_monthly_start), end=str(_monthly_end)
    )
    logger.debug(f"Monthly time summary: {_monthly_summary}")

    _response = RegistrationTimeSummaryReportResponse()
    _response.time_summary_day = RegistrationTimeSummaryDetail(
        **{
            "length": _daily_summary["value"]["sum"] if _daily_summary else 0,
            "amount": _daily_summary["value"]["count"] if _daily_summary else 0,
        }
    )
    _response.time_summary_week = RegistrationTimeSummaryDetail(
        **{
            "length": _weekly_summary["value"]["sum"] if _weekly_summary else 0,
            "amount": _weekly_summary["value"]["count"] if _weekly_summary else 0,
        }
    )
    _response.time_summary_month = RegistrationTimeSummaryDetail(
        **{
            "length": _monthly_summary["value"]["sum"] if _monthly_summary else 0,
            "amount": _monthly_summary["value"]["count"] if _monthly_summary else 0,
        }
    )

    end = time.time()
    logger.debug(f"Got registrations in: {end-start}")

    return _response
