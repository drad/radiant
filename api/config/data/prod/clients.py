#
# Initial Production Data for: Clients.
#

clients_add = []
clients_error = []
clients_skip = []

clients = [
    # note: this list was generated from the design-doc.ods:Client sheet.
    {
        "name": "Recognize",
        "nickname": "RCOG",
        "order": 1,
        "preferred_invoice_format": "summary",
        "note": "",
        "enabled": True,
    },
    {
        "name": "W.W. Norton",
        "nickname": "WWN",
        "order": 2,
        "preferred_invoice_format": "summary",
        "note": "",
        "enabled": True,
    },
    {
        "name": "Adercon",
        "nickname": "AC",
        "order": 50,
        "preferred_invoice_format": "summary",
        "note": "",
        "enabled": True,
    },
    {
        "name": "Test Client",
        "nickname": "TST",
        "order": 90,
        "preferred_invoice_format": "summary",
        "note": "",
        "enabled": True,
    },
    {
        "name": "dradux",
        "nickname": "DX",
        "order": 70,
        "preferred_invoice_format": "detail",
        "note": "dradux projects",
        "enabled": True,
    },
    # TEMPLATE: {"name": "", "nickname": "", "order": , "preferred_invoice_format": "", "note": None, "enabled": True},
    # ::TEST DATA BELOW::
    # ~ {"name": "Test 33", "nickname": "T33", "order": 88, "preferred_invoice_format": "summary", "note": "Just a test client, #33", "enabled": True},
]
