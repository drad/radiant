#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#  SPDX-License-Identifier: GPL-3.0-only
#  Copyright 2025 dradux.com

from typing import Optional

from pydantic import BaseModel


class Recipient(BaseModel):
    """
    Monitor Recipient
    """

    notifier: str = None  # id of the notifier to which the recipient belongs
    address: str = None

    note: Optional[str] = None
    disabled: bool = False


class DocInfo(BaseModel):
    """
    Document Info
    """

    ok: bool = None
    id: str | None = None
    rev: str | None = None
    msg: str | None = None
    total: int | None = None
    runtime: float | None = None
